package com.zxm.test;


import com.zxm.LinkAccountApplication;
import com.zxm.component.SmsComponent;
import groovy.util.logging.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LinkAccountApplication.class)
@Slf4j
public class SmsTest {

    @Autowired
    private SmsComponent smsComponent;

    @Test
    public void testSendSms(){
        for(int i = 0; i < 6; i++){
            smsComponent.send("18370847419","M105EABDEC","223344");
            smsComponent.send("15170659864","M105EABDEC","223344");
        }
    }

}