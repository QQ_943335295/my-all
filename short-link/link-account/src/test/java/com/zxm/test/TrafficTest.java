package com.zxm.test;

import com.zxm.LinkAccountApplication;
import com.zxm.model.TrafficDO;
import com.zxm.service.TrafficService;
import groovy.util.logging.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LinkAccountApplication.class)
@Slf4j
public class TrafficTest {

    @Autowired
    private TrafficService trafficService;

    @Test
    public void testSendSms(){
        Random random = new Random();
        for(int i = 0; i < 6; i++){
            TrafficDO trafficDO = new TrafficDO();
            trafficDO.setAccountNo((long) random.nextInt(100));
            trafficService.save(trafficDO);
        }
    }
}
