package com.zxm.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务
 * @author zxm
 * @date 2022/9/7 22:56
 */
public interface FileService {

    /**
     * 上传文件
     * @param file 文件
     * @return 返回文件路径
     */
    String uploadUserImg(MultipartFile file);
}
