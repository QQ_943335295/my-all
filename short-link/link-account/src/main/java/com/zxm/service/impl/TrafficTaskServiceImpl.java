package com.zxm.service.impl;

import com.zxm.model.TrafficTaskDO;
import com.zxm.mapper.TrafficTaskMapper;
import com.zxm.service.TrafficTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
@Service
public class TrafficTaskServiceImpl extends ServiceImpl<TrafficTaskMapper, TrafficTaskDO> implements TrafficTaskService {

}
