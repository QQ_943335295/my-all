package com.zxm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxm.bean.JsonData;
import com.zxm.constants.CommonConstants;
import com.zxm.enums.AuthTypeEnum;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.SendCodeEnum;
import com.zxm.manager.AccountManager;
import com.zxm.mapper.AccountMapper;
import com.zxm.model.AccountDO;
import com.zxm.request.AccountLoginRequest;
import com.zxm.request.AccountRegisterRequest;
import com.zxm.service.AccountService;
import com.zxm.service.NotifyService;
import com.zxm.utils.CheckUtil;
import com.zxm.utils.CommonUtil;
import com.zxm.utils.IDUtil;
import com.zxm.utils.JWTUtils;
import com.zxm.vo.LoginAccountVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
@Slf4j
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, AccountDO> implements AccountService {

    @Autowired
    private NotifyService notifyService;
    @Autowired
    private AccountManager accountManager;

    /**
     * 用户注册
     * 1、校验手机验证码
     * 2、密码加密
     * 3、账号唯一性校验(TODO)
     * 4、插入数据库
     * 5、新注册用户发送福利
     * @param registerRequest 账户请求
     * @return JsonData
     */
    @Override
    public JsonData register(AccountRegisterRequest registerRequest) {
        if (StringUtils.isBlank(registerRequest.getPhone()) || !CheckUtil.isPhone(registerRequest.getPhone())){
            return JsonData.buildResult(BizCodeEnum.CODE_TO_ERROR);
        }
        boolean checkCode = notifyService.checkCode(SendCodeEnum.USER_REGISTER, registerRequest.getPhone(), registerRequest.getCode());
        if (!checkCode){
            return JsonData.buildResult(BizCodeEnum.CODE_ERROR);
        }
        // 密码加密及插入
        AccountDO accountDO = new AccountDO();
        BeanUtils.copyProperties(registerRequest, accountDO);
        accountDO.setAuth(AuthTypeEnum.DEFAULT.name());
        accountDO.setSecret(CommonConstants.SALT_PREFIX + CommonUtil.getRandomCode(8));
        String crypt = Md5Crypt.md5Crypt(accountDO.getPwd().getBytes(StandardCharsets.UTF_8), accountDO.getSecret());
        accountDO.setPwd(crypt);
        accountDO.setAccountNo((Long) IDUtil.geneSnowFlakeID());
        int rows = accountManager.insert(accountDO);
        log.info("rows:{}，注册成功：{}", rows, accountDO);
        // 发送福利
        userRegisterInitTask(accountDO);

        return JsonData.buildSuccess();
    }

    /**
     * 用户初始化：发送用户福利、流量包 TODO
     * @param accountDO 账户信息
     */
    private void userRegisterInitTask(AccountDO accountDO) {
    }

    @Override
    public JsonData login(AccountLoginRequest loginRequest) {
        if (StringUtils.isBlank(loginRequest.getPhone()) || !CheckUtil.isPhone(loginRequest.getPhone())){
            return JsonData.buildResult(BizCodeEnum.CODE_TO_ERROR);
        }
        List<AccountDO> list = accountManager.listAccountByPhone(loginRequest.getPhone());
        if (list == null || list.size() != 1){
            return JsonData.buildResult(BizCodeEnum.ACCOUNT_UNREGISTER);
        }
        AccountDO accountDO = list.get(0);
        String inputPwd = Md5Crypt.md5Crypt(loginRequest.getPwd().getBytes(StandardCharsets.UTF_8), accountDO.getSecret());
        if (!accountDO.getPwd().equalsIgnoreCase(inputPwd)){
            return JsonData.buildResult(BizCodeEnum.ACCOUNT_PWD_ERROR);
        }
        LoginAccountVo accountVo = new LoginAccountVo();
        BeanUtils.copyProperties(accountDO, accountVo);
        String jwt = JWTUtils.getJWT(accountVo);
        return JsonData.buildSuccess(jwt);
    }
}
