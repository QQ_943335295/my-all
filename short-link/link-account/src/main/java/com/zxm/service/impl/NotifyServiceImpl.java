package com.zxm.service.impl;

import com.zxm.bean.JsonData;
import com.zxm.component.SmsComponent;
import com.zxm.config.SmsConfig;
import com.zxm.constants.CacheKeyConstants;
import com.zxm.constants.CommonConstants;
import com.zxm.constants.ExpireConstants;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.SendCodeEnum;
import com.zxm.service.NotifyService;
import com.zxm.utils.CheckUtil;
import com.zxm.utils.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 通知服务实现类
 * @author zxm
 * @date 2022/8/28 18:46
 */
@Service
@Slf4j
public class NotifyServiceImpl implements NotifyService {

    @Autowired
    private SmsComponent smsComponent;
    @Autowired
    private SmsConfig smsConfig;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // @Async("threadPoolTaskExecutor")
    @Override
    public void testSendCode() {
        long startTime = CommonUtil.getCurrentTimestamp();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JsonData sendCode(SendCodeEnum userRegister, String to) {
        String key = String.format(CacheKeyConstants.CHECK_CODE_KEY, userRegister, to);
        String cacheValue = stringRedisTemplate.opsForValue().get(key);
        // 判断是否在60秒内发送过
        if (StringUtils.isNotBlank(cacheValue)){
            long ttl = Long.parseLong(cacheValue.split(CommonConstants.UNDERLINE)[CommonConstants.ONE]);
            //当前时间戳-验证码发送的时间戳，如果小于60秒，则不给重复发送
            long leftTime = CommonUtil.getCurrentTimestamp() - ttl;
            if( leftTime < ExpireConstants.REPEAT_SEND_TIME){
                log.info("重复发送短信验证码，时间间隔:{}秒",leftTime);
                return JsonData.buildResult(BizCodeEnum.CODE_LIMITED);
            }
        }
        // 短信验证码 用code_time拼接
        String code = CommonUtil.getRandomCode(6);
        stringRedisTemplate.opsForValue().set(key,
                code + CommonConstants.UNDERLINE + CommonUtil.getCurrentTimestamp(),
                ExpireConstants.CAPTCHA_CODE_EXPIRED, TimeUnit.MILLISECONDS);
        if(CheckUtil.isEmail(to)){
            //邮箱验证码
        } else if(CheckUtil.isPhone(to)){
            smsComponent.send(to, smsConfig.getTemplateId(), code);
        } else {
            return JsonData.buildResult(BizCodeEnum.CODE_TO_ERROR);
        }
        return JsonData.buildSuccess();
    }

    @Override
    public boolean checkCode(SendCodeEnum userRegister, String phone, String code) {
        String key = String.format(CacheKeyConstants.CHECK_CODE_KEY, userRegister, phone);
        if (StringUtils.isNotBlank(code)){
            String cacheValue = stringRedisTemplate.opsForValue().get(key);
            if (StringUtils.isNotBlank(cacheValue)){
                String cacheCode = cacheValue.split(CommonConstants.UNDERLINE)[CommonConstants.ZERO];
                if (code.equalsIgnoreCase(cacheCode)){
                    stringRedisTemplate.delete(key);
                    return true;
                }
            }
        }
        return false;
    }
}
