package com.zxm.service.impl;

import com.zxm.model.TrafficDO;
import com.zxm.mapper.TrafficMapper;
import com.zxm.service.TrafficService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
@Service
public class TrafficServiceImpl extends ServiceImpl<TrafficMapper, TrafficDO> implements TrafficService {

}
