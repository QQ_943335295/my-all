package com.zxm.service;

import com.zxm.bean.JsonData;
import com.zxm.enums.SendCodeEnum;

/**
 * 消息通知服务类
 * @author zxm
 * @since 2022-08-21
 */
public interface NotifyService {

    /**
     * 测试发送code
     */
    void testSendCode();

    /**
     * 发送短信验证码
     * @param userRegister 用户组测类型
     * @param to 发送目的地
     * @return JsonData
     */
    JsonData sendCode(SendCodeEnum userRegister, String to);

    /**
     * 校验验证码
     * @param userRegister 用户注册类型
     * @param phone 手机号
     * @param code 验证码
     * @return
     */
    boolean checkCode(SendCodeEnum userRegister, String phone, String code);
}
