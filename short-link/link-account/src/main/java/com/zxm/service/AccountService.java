package com.zxm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.bean.JsonData;
import com.zxm.model.AccountDO;
import com.zxm.request.AccountLoginRequest;
import com.zxm.request.AccountRegisterRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
public interface AccountService extends IService<AccountDO> {

    /**
     * 账户登录
     * @param registerRequest 账户请求
     * @return JsonData
     */
    JsonData register(AccountRegisterRequest registerRequest);

    /**
     * 用户登录
     * @param loginRequest 登录信息
     * @return JsonData
     */
    JsonData login(AccountLoginRequest loginRequest);
}
