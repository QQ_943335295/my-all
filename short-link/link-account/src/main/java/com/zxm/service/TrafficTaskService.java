package com.zxm.service;

import com.zxm.model.TrafficTaskDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
public interface TrafficTaskService extends IService<TrafficTaskDO> {

}
