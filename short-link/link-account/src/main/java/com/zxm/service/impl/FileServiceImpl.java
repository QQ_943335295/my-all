package com.zxm.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectResult;
import com.zxm.config.OSSConfig;
import com.zxm.constants.CommonConstants;
import com.zxm.constants.DateConstants;
import com.zxm.constants.FileConstants;
import com.zxm.service.FileService;
import com.zxm.utils.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@Slf4j
public class FileServiceImpl implements FileService {

    @Autowired
    private OSSConfig ossConfig;

    @Override
    public String uploadUserImg(MultipartFile file) {
        //获取相关配置
        String bucketName = ossConfig.getBucketName();
        String endpoint = ossConfig.getEndpoint();
        String accessKeyId = ossConfig.getAccessKeyId();
        String accessKeySecret = ossConfig.getAccessKeySecret();
        //创建OSS对象
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        //获取原生文件名  xxx.jpg
        String originalFileName = file.getOriginalFilename();
        //JDK8的日期格式
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DateConstants.YMD_BACKSLASH);
        //拼装路径,oss上存储的路径  user/2022/12/1/sdfdsafsdfdsf.jpg
        String folder = dtf.format(ldt);
        String fileName = CommonUtil.generateUUID();
        String extension = originalFileName.substring(originalFileName.lastIndexOf(CommonConstants.DOT));
        // 在OSS上的bucket下创建 user 这个文件夹
        String newFileName = String.format(FileConstants.OOS_USER_FILENAME, folder, fileName, extension);
        try {
            PutObjectResult putObjectResult = ossClient.putObject(bucketName,newFileName,file.getInputStream());
            //拼装返回路径
            if(putObjectResult != null){
                return String.format(FileConstants.OOS_IMG_URL, bucketName, endpoint, newFileName);
            }
        } catch (IOException e) {
            log.error("文件上传失败:{}", e.getMessage());
        } finally {
            //oss关闭服务，不然会造成OOM
            ossClient.shutdown();
        }
        return null;
    }
}
