package com.zxm.service;

import com.zxm.model.TrafficDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
public interface TrafficService extends IService<TrafficDO> {

}
