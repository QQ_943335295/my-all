package com.zxm.request;

import lombok.Data;

/**
 * 账号登录请求
 * @author zxm
 * @date 2022/9/8 22:11
 */
@Data
public class AccountLoginRequest {

    private String phone;

    private String pwd;

}
