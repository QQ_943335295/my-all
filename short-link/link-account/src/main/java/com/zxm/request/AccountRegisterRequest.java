package com.zxm.request;

import lombok.Data;

/**
 * 账号登录请求
 * @author zxm
 * @date 2022/9/8 22:11
 */
@Data
public class AccountRegisterRequest {

    private String headImg;

    private String phone;

    private String pwd;

    private String mail;

    private String username;

    private String auth;

    private String code;
}
