package com.zxm.controller;


import com.zxm.bean.JsonData;
import com.zxm.enums.BizCodeEnum;
import com.zxm.request.AccountLoginRequest;
import com.zxm.request.AccountRegisterRequest;
import com.zxm.service.AccountService;
import com.zxm.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
@RestController
@RequestMapping("/api/v1/account")
public class AccountController {

    @Autowired
    private FileService fileService;
    @Autowired
    private AccountService accountService;

    /**
     * 上传用户头像
     * @param file 文件
     * @return JsonData
     */
    @PostMapping(value = "/uploadUserImg")
    public JsonData uploadHeaderImg(@RequestPart("file") MultipartFile file){
        String result = fileService.uploadUserImg(file);
        return result != null?JsonData.buildSuccess(result):JsonData.buildResult(BizCodeEnum.FILE_UPLOAD_USER_IMG_FAIL);
    }

    /**
     * 用户注册
     * @param registerRequest 登录请求
     * @return JsonData
     */
    @PostMapping("/register")
    public JsonData register(@RequestBody AccountRegisterRequest registerRequest){
        return accountService.register(registerRequest);
    }

    /**
     * 用户登录
     * @param loginRequest 登录请求
     * @return JsonData
     */
    @PostMapping("/login")
    public JsonData login(@RequestBody AccountLoginRequest loginRequest){
        return accountService.login(loginRequest);
    }

}

