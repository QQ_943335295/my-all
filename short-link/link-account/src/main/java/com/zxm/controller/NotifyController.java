package com.zxm.controller;


import com.google.code.kaptcha.Producer;
import com.zxm.bean.JsonData;
import com.zxm.constants.CacheKeyConstants;
import com.zxm.constants.CommonConstants;
import com.zxm.constants.ExpireConstants;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.SendCodeEnum;
import com.zxm.request.SendCodeRequest;
import com.zxm.service.NotifyService;
import com.zxm.utils.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 通知控制层
 * @author zxm
 * @since 2022-08-21
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/notify")
public class NotifyController {

    @Autowired
    private NotifyService notifyService;
    @Autowired
    private Producer captchaProducer;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping("/test/sendCode")
    public JsonData testSendCode(){
        notifyService.testSendCode();
        return JsonData.buildSuccess();
    }

    /**
     * 获取图形验证码
     * @param request 请求对象
     * @param response 响应对象
     */
    @GetMapping("captcha")
    public void getCaptcha(HttpServletRequest request, HttpServletResponse response) {
        String captchaText = captchaProducer.createText();
        log.info("图形验证码:{}", captchaText);
        //存储
        stringRedisTemplate.opsForValue().set(getCaptchaKey(request), captchaText,
                ExpireConstants.CAPTCHA_CODE_EXPIRED, TimeUnit.MILLISECONDS);
        BufferedImage bufferedImage = captchaProducer.createImage(captchaText);
        try (ServletOutputStream outputStream = response.getOutputStream()){
            ImageIO.write(bufferedImage, CommonConstants.JPG, outputStream);
            outputStream.flush();
        } catch (IOException e) {
            log.error("获取图形验证码异常:{}", e.getMessage());
        }

    }

    @PostMapping("/sendCode")
    public JsonData sendCode(@RequestBody SendCodeRequest sendCodeRequest, HttpServletRequest request){
        String key = getCaptchaKey(request);
        String code = stringRedisTemplate.opsForValue().get(key);
        if (code != null && sendCodeRequest.getCode() != null && code.equalsIgnoreCase(sendCodeRequest.getCode())){
            stringRedisTemplate.delete(key);
            return notifyService.sendCode(SendCodeEnum.USER_REGISTER, sendCodeRequest.getTo());
        }
        return JsonData.buildResult(BizCodeEnum.CODE_CAPTCHA_ERROR);
    }

    /**
     * 获取验证码缓存键
     * @param request 请求对象
     * @return String
     */
    private String getCaptchaKey(HttpServletRequest request) {
        String ipAddress = CommonUtil.getIpAddress(request);
        String userAgent = request.getHeader(CommonConstants.USER_AGENT);
        String key = CommonUtil.MD5(ipAddress + userAgent);
        return String.format(CacheKeyConstants.ACCOUNT_SERVICE_CAPTCHA, key);
    }

}

