package com.zxm.manager;

import com.zxm.model.AccountDO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
public interface AccountManager {

    /**
     * 插入账户信息
     * @param accountDO 账户信息
     * @return int
     */
    int insert(AccountDO accountDO);

    /**
     * 根据手机号长训账户信息
     * @param phone 手机号
     * @return List<AccountDO>
     */
    List<AccountDO> listAccountByPhone(String phone);
}
