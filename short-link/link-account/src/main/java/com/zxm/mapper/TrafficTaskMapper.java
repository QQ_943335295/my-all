package com.zxm.mapper;

import com.zxm.model.TrafficTaskDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2022-08-21
 */
public interface TrafficTaskMapper extends BaseMapper<TrafficTaskDO> {

}
