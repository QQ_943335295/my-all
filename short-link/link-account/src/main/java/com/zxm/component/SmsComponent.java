package com.zxm.component;

import com.zxm.config.SmsConfig;
import com.zxm.constants.CommonConstants;
import com.zxm.utils.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * 发送短信组件
 * @author zxm
 * @date 2022/8/25 22:59
 */
@Component
@Slf4j
public class SmsComponent {

    @Autowired
    private SmsConfig smsConfig;
    @Autowired
    private RestTemplate restTemplate;

    @Async("threadPoolTaskExecutor")
    public void send(String to, String templateId, String value) {
        long startTime = CommonUtil.getCurrentTimestamp();
        String url = String.format(smsConfig.getUrl(), to, templateId, value);
        HttpHeaders headers = new HttpHeaders();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.set(CommonConstants.AUTHORIZATION, CommonConstants.APP_CODE_HEADER + smsConfig.getAppCode());
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        log.info("url={},body={},useTime:{}", url, response.getBody(), CommonUtil.getCurrentTimestamp() - startTime);
        if (response.getStatusCode() == HttpStatus.OK) {
            log.info("发送短信成功,响应信息:{}", response.getBody());
        } else {
            log.error("发送短信失败,响应信息:{}", response.getBody());
        }
    }

}
