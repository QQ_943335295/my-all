package com.zxm.config;

import com.zxm.constants.CommonConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 短信配置
 * @author zxm
 * @date 2022/8/25 23:02
 */
@ConfigurationProperties(prefix = CommonConstants.SMS)
@Configuration
@Data
public class SmsConfig {

    private String templateId;

    private String appCode;

    private String url;

}
