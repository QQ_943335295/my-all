package com.zxm.aspect;

import com.zxm.annotation.RepeatSubmit;
import com.zxm.constants.CacheKeyConstants;
import com.zxm.constants.CommonConstants;
import com.zxm.constants.ExpireConstants;
import com.zxm.enums.BizCodeEnum;
import com.zxm.exception.BizException;
import com.zxm.interceptor.LoginInterceptor;
import com.zxm.utils.CommonUtil;
import com.zxm.vo.LoginAccountVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * 防重提交注解
 * @author zxm
 * @date 2022/11/27 19:40
 */
@Aspect
@Component
@Slf4j
public class RepeatSubmitAspect {

    private static final String RESUBMIT_PARAM_KEY = "%s:%s:%s";

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;



    @Pointcut(value = "@annotation(repeatSubmit)")
    public void pointcutNoRepeatSubmit(RepeatSubmit repeatSubmit){};

    /**
     * 环绕通知, 围绕着方法执行
     * @Around 可以用来在调用一个具体方法前和调用后来完成一些具体的任务。
     *
     * 方式一：单用 @Around("execution(* net.xdclass.controller.*.*(..))")可以
     * 方式二：用@Pointcut和@Around联合注解也可以（我们采用这个）
     *
     *
     * 两种方式
     * 方式一：加锁 固定时间内不能重复提交
     * <p>
     * 方式二：先请求获取token，这边再删除token,删除成功则是第一次提交
     */
    @Around(value = "pointcutNoRepeatSubmit(noRepeatSubmit)", argNames = "joinPoint,noRepeatSubmit")
    public Object around(ProceedingJoinPoint joinPoint, RepeatSubmit noRepeatSubmit) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        boolean res = false;
        String type = noRepeatSubmit.limitType().name();
        if (type.equals(RepeatSubmit.Type.PARAM.name())){
            // 如果是参数类型
            // 方式一方法参数
            long lockTime = noRepeatSubmit.lockTime();
            String ip = CommonUtil.getIpAddress(request);
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            String name = method.getName();
            String className = method.getDeclaringClass().getName();
            String key = String.format(CacheKeyConstants.REPEAT_SUBMIT_KEY, accountNo, CommonUtil.MD5(String.format(RESUBMIT_PARAM_KEY, name, className, ip)));
            log.info("防重提交锁：{}", key);
            res = Boolean.TRUE.equals(redisTemplate.opsForValue().setIfAbsent(key, String.valueOf(CommonConstants.ONE), lockTime, TimeUnit.SECONDS));
//            // 分布式锁
//            RLock lock = redissonClient.getLock(key);
//            // 尝试加锁，最多等待1秒，上锁以后5秒自动解锁 [lockTime默认为5s, 可以自定义]
//            res = lock.tryLock(CommonConstants.ONE, lockTime, TimeUnit.SECONDS);
        } else {
            // 否则为token类型
            String requestToken = request.getHeader(CommonConstants.HEADER_REPEAT_SUBMIT);
            if (StringUtils.isBlank(requestToken)) {
                throw new BizException(BizCodeEnum.REPEAT_SUBMIT_TOKEN_NOT_EXIST);
            }
            String key = String.format(CacheKeyConstants.REPEAT_SUBMIT_KEY, accountNo,requestToken);
            /**
             * 提交表单的token key
             * 方式一：不用lua脚本获取再判断，之前是因为 key组成是 order:submit:accountNo, value是对应的token，所以需要先获取值，再判断
             * 方式二：可以直接key是 order:submit:accountNo:token,然后直接删除成功则完成
             */
            res = Boolean.TRUE.equals(redisTemplate.delete(key));
        }
        if (!res) {
            log.error("请求重复提交");
            return null;
        }
        log.info("环绕通知执行前");
        Object obj = joinPoint.proceed();
        log.info("环绕通知执行后");
        return obj;
    }

}
