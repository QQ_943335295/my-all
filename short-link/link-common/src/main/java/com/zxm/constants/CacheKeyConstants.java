package com.zxm.constants;

/**
 * 缓存键
 * @author zxm
 * @date 2022/9/4 21:05
 */
public interface CacheKeyConstants {

    /**
     * 账户服务验证码键
     */
    String ACCOUNT_SERVICE_CAPTCHA = "account-service:captcha:%s";

    /**
     * 验证码键
     */
    String CHECK_CODE_KEY = "code:%s:%s";

    /**
     * 防重提交键
     */
    String REPEAT_SUBMIT_KEY = "repeat-submit:%s:%s";
}
