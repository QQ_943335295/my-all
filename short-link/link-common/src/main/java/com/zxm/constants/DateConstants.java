package com.zxm.constants;

/**
 * 日期常量
 * @author zxm
 * @date 2022/9/7 23:02
 */
public interface DateConstants {

    String YMD_BACKSLASH = "yyyy/MM/dd";

}
