package com.zxm.constants;

/**
 * 通用常量
 * @author zxm
 * @date 2022/8/28 18:10
 */
public interface CommonConstants {

    /**
     * 短信
     */
    String SMS = "sms";

    /**
     * 认证字符串
     */
    String AUTHORIZATION = "Authorization";

    /**
     * 短信
     */
    String APP_CODE_HEADER = "APPCODE ";

    /**
     * JPG格式
     */
    String JPG = "jpg";

    /**
     * 用户代理
     */
    String USER_AGENT = "user-agent";

    /**
     * 下划线
     */
    String UNDERLINE = "_";

    /**
     * -1
     */
    int NEGATIVE = -1;

    /**
     * 1
     */
    int ONE = 1;

    /**
     * 0
     */
    int ZERO = 0;

    /**
     * 点
     */
    String DOT = ".";

    /**
     * 盐前缀
     */
    String SALT_PREFIX = "$1$";


    /**
     * token
     */
    String TOKEN = "token";

    /**
     * 重复提交token
     */
    String HEADER_REPEAT_SUBMIT = "resubmit-token";


}
