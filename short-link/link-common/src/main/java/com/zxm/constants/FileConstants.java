package com.zxm.constants;

/**
 * 文件常量
 * @author zxm
 * @date 2022/9/7 23:17
 */
public interface FileConstants {

    /** OOS对象存储文件名 */
    String OOS_USER_FILENAME =  "user/%s/%s%s";

    /** OOS图片路径 */
    String OOS_IMG_URL =  "https://%s.%s/%s";
}

