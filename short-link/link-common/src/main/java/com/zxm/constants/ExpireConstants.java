package com.zxm.constants;

/**
 * 过期时间常量
 * @author zxm
 * @date 2022/9/4 21:04
 */
public interface ExpireConstants {

    /**
     * 图形验证码 10分钟
     */
    long CAPTCHA_CODE_EXPIRED = 10 * 60 * 1000;

    /**
     * 重复发送时间
     */
    long REPEAT_SEND_TIME = 60 * 1000;

    /**
     * 默认30分钟超时未支付，单位毫秒
     */
    long ORDER_PAY_TIMEOUT_MILLS =1000 * 60 * 30;

    /**
     * 过期时间
     */
    long REPEAT_SUBMIT_EXPIRED = 30;

}
