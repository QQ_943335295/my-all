package com.zxm.request;

import lombok.Data;

/**
 * 发送验证码请求
 * @author zxm
 * @date 2022/9/4 21:25
 */
@Data
public class SendCodeRequest {

    private String code;

    private String to;

}
