package com.zxm.exception;

import com.zxm.enums.BizCodeEnum;
import lombok.Data;

/**
 * 全局异常处理
 * @author zxm
 * @date 2022/8/20 23:00
 */
@Data
public class BizException extends RuntimeException {

    private Integer code;
    private String msg;

    public BizException(Integer code, String message) {
        super(message);
        this.code = code;
        this.msg = message;
    }

    public BizException(BizCodeEnum bizCodeEnum) {
        super(bizCodeEnum.getMessage());
        this.code = bizCodeEnum.getCode();
        this.msg = bizCodeEnum.getMessage();
    }

}
