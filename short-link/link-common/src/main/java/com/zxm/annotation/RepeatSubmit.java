package com.zxm.annotation;

import java.lang.annotation.*;

/**
 * 防重提交注解
 * @author zxm
 * @date 2022/11/27 19:33
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RepeatSubmit {

    /**
     * 锁定时间
     */
    long lockTime() default 6;

    /**
     *
     */
    Type limitType() default Type.PARAM;

    /**
     * 锁定类型
     */
    enum Type{
        // 校验Token方式
        TOKEN,
        // 参数方式
        PARAM;
    }

}
