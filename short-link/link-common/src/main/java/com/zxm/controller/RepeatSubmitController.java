package com.zxm.controller;

import com.zxm.bean.JsonData;
import com.zxm.constants.CacheKeyConstants;
import com.zxm.constants.CommonConstants;
import com.zxm.constants.ExpireConstants;
import com.zxm.interceptor.LoginInterceptor;
import com.zxm.utils.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 防重提交控制类
 * @author zxm
 * @date 2022/11/27 19:40
 */
@RestController
@Slf4j
public class RepeatSubmitController {

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 获取防重提交token
     */
    @GetMapping("/getRepeatSubmitToken")
    public JsonData getRepeatSubmitToken(){
        String token = CommonUtil.getStringNumRandom(16);
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        String key = String.format(CacheKeyConstants.REPEAT_SUBMIT_KEY, accountNo, token);
        redisTemplate.opsForValue().set(key, String.valueOf(CommonConstants.ONE), ExpireConstants.REPEAT_SUBMIT_EXPIRED, TimeUnit.MINUTES);
        return JsonData.buildSuccess(token);
    }


}
