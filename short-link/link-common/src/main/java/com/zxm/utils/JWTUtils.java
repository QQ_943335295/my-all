package com.zxm.utils;

import com.zxm.vo.LoginAccountVo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * JWT工具类
 * @author zxm
 * @date 2022/9/8 23:27
 */
@Slf4j
public class JWTUtils {

    /**
     * 主题
     */
    private static final String SUBJECT = "zxm";

    /**
     * 加密密钥
     */
    private static final String SECRET = "zxm.com";

    /**
     * 令牌前缀
     */
    private static final String TOKEN_PREFIX = "short-link";

    /**
     * token过期时间，7天
     */
    private static final long EXPIRE = 1000 * 60 * 60 * 24 * 7;

    /**
     * 生成Token
     * @param accountVo 登录账户信息
     * @return String
     */
    public static String getJWT(LoginAccountVo accountVo) {
        if (accountVo == null) {
            throw new NullPointerException("对象为空");
        }
        String token = Jwts.builder().setSubject(SUBJECT)
                //配置payload
                .claim("head_img", accountVo.getHeadImg())
                .claim("account_no", accountVo.getAccountNo())
                .claim("username", accountVo.getUsername())
                .claim("mail", accountVo.getMail())
                .claim("phone", accountVo.getPhone())
                .claim("auth", accountVo.getAuth())
                .setIssuedAt(new Date())
                .setExpiration(new Date(CommonUtil.getCurrentTimestamp() + EXPIRE))
                .signWith(SignatureAlgorithm.HS256, SECRET).compact();
        token = TOKEN_PREFIX + token;
        return token;
    }

    /**
     * 解密jwt
     * @param token 令牌
     * @return Claims
     */
    public static Claims checkJWT(String token) {
        try {
            final Claims claims = Jwts.parser().setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody();
            return claims;
        } catch (Exception e) {
            log.error("jwt 解密失败");
            return null;
        }
    }

}
