package com.zxm.enums;

/**
 * 账号认证等级
 * @author zxm
 * @date 2022/9/8 22:55
 */
public enum AuthTypeEnum {

    /**
     * 默认级别
     */
    DEFAULT,

    /**
     * 实名制
     */
    REALNAME,

    /**
     * 企业
     */
    ENTERPRISE;

}
