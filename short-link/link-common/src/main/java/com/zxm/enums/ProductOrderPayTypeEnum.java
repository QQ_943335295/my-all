package com.zxm.enums;

public enum ProductOrderPayTypeEnum {

    WECHAT_APY,

    ALI_PAY,

    BANK;

}
