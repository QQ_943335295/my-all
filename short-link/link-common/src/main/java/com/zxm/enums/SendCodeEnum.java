package com.zxm.enums;

/**
 * 发送验证码枚举
 * @author zxm
 * @date 2022/9/4 21:30
 */
public enum SendCodeEnum {

    /**
     * 用户注册
     */
    USER_REGISTER;

}
