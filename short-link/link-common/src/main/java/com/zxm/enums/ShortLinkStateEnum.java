package com.zxm.enums;

/**
 * 短链状态
 * @author zxm
 * @date 2022/10/20 23:06
 */
public enum ShortLinkStateEnum {

    /**
     * 锁定
     */
    LOCK,

    /**
     * 可用
     */
    ACTIVE;

}
