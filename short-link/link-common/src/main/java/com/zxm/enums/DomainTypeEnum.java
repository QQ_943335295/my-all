package com.zxm.enums;

/**
 * 域名类型
 * @author zxm
 * @date 2022/11/7 22:28
 */
public enum  DomainTypeEnum {

    /**
     * 自建
     */
    CUSTOM,

    /**
     * 官方
     */
    OFFICIAL;
}
