package com.zxm.test;

import com.zxm.LinkManageApplication;
import com.zxm.config.rabbitmq.RabbitMQConfig;
import com.zxm.config.sharding.ShortLinkComponent;
import com.zxm.manager.ShortLinkManager;
import com.zxm.model.ShortLinkDO;
import com.zxm.vo.EventMessage;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;


@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LinkManageApplication.class)
public class ShortLinkTest {

    @Autowired
    private ShortLinkManager shortLinkManager;

    @Autowired
    private RabbitMQConfig rabbitMQConfig;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 保存
     */
    @Test
    public void testSaveShortLink() {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int num1 = random.nextInt(10);
            int num2 = random.nextInt(10000000);
            int num3 = random.nextInt(10000000);
            String originalUrl = num1 + "zxm" + num2 + ".com" + num3;
            String shortLinkCode = ShortLinkComponent.createShortLinkCode(originalUrl);
            ShortLinkDO shortLinkDO = new ShortLinkDO();
            shortLinkDO.setCode(shortLinkCode);
            shortLinkDO.setAccountNo(Long.valueOf(num3));
            shortLinkDO.setSign(originalUrl);
            shortLinkDO.setDel(0);
            shortLinkManager.addShortLink(shortLinkDO);
        }
    }

    @Test
    public void testFind() {
        ShortLinkDO shortLinkDO = shortLinkManager.findByShortLinkCode("a3LPa7V0");
        log.info(shortLinkDO.toString());
    }


    @Test
    public void test(){
        rabbitTemplate.convertAndSend(rabbitMQConfig.getShortLinkEventExchange(), rabbitMQConfig.getShortLinkAddRoutingKey(),new EventMessage());
    }

}