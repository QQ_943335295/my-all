package com.zxm.test;

import com.google.common.hash.Hashing;
import com.zxm.utils.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Random;

@Slf4j
public class CommonTest {

    @Test
    public void testMurmurHash() {
        Random random = new Random();
        for (int i = 0; i < 50; i++) {
            int num1 = random.nextInt(1000000);
            int num2 = random.nextInt(1000000);
            int num3 = random.nextInt(1000000);
            String originalUrl = num1 + "zxm" + num2 + ".com" + num3;
            long murmur3_32 = Hashing.murmur3_32().hashUnencodedChars(originalUrl).padToLong();
            System.out.println("murmur3_32="+murmur3_32);
        }
    }

    /**
     * 测试短链平台
     */
    @Test
    public void testCreateShortLink() {
        /*Random random = new Random();
        for (int i = 0; i < 100; i++) {
            int num1 = random.nextInt(10);
            int num2 = random.nextInt(1000000);
            int num3 = random.nextInt(1000000);
            String originalUrl = num1 + "zxm" + num2 + ".com" + num3;
            String shortLinkCode = CommonUtil.murmurHash32EncodeToBase62(originalUrl);
            System.out.println("originalUrl:" + originalUrl + ", shortLinkCode=" + shortLinkCode);
        }*/
        System.err.println(CommonUtil.murmurHash32EncodeToBase62("www.baidu.com"));
        System.err.println(CommonUtil.murmurHash32EncodeToBase62("www.baidu.com"));
        System.err.println(CommonUtil.murmurHash32EncodeToBase62("www.baidu.com"));
    }

}
