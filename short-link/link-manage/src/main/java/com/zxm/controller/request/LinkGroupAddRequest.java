package com.zxm.controller.request;

import lombok.Data;

/**
 * 短链分组请求实体
 * @author zxm
 * @date 2022/10/9 22:37
 */
@Data
public class LinkGroupAddRequest  {

    /**
     * 组名
     */
    private String title;
}
