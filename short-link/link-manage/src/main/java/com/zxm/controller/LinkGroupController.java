package com.zxm.controller;


import com.zxm.bean.JsonData;
import com.zxm.controller.request.LinkGroupAddRequest;
import com.zxm.controller.request.LinkGroupUpdateRequest;
import com.zxm.enums.BizCodeEnum;
import com.zxm.service.LinkGroupService;
import com.zxm.vo.LinkGroupVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 短链分组控制层
 * @author zxm
 * @date 2022/10/9 22:38
 */
@RestController
@RequestMapping("/api/group/v1")
public class LinkGroupController {

    @Autowired
    private LinkGroupService linkGroupService;

    /**
     * 创建分组
     */
    @PostMapping("/add")
    public JsonData add(@RequestBody LinkGroupAddRequest addRequest){
        int rows = linkGroupService.add(addRequest);
        return rows == 1 ? JsonData.buildSuccess():JsonData.buildResult(BizCodeEnum.GROUP_ADD_FAIL);
    }


    /**
     * 根据id删除分组
     */
    @DeleteMapping("/del/{group_id}")
    public JsonData del(@PathVariable("group_id") Long groupId){
        int rows = linkGroupService.del(groupId);
        return rows == 1 ? JsonData.buildSuccess():JsonData.buildResult(BizCodeEnum.GROUP_NOT_EXIST);
    }


    /**
     * 根据id找详情
     */
    @GetMapping("detail/{group_id}")
    public JsonData detail(@PathVariable("group_id") Long groupId){
        LinkGroupVO linkGroupVO = linkGroupService.detail(groupId);
        return JsonData.buildSuccess(linkGroupVO);
    }


    /**
     * 列出用户全部分组
     */
    @GetMapping("list")
    public JsonData findUserAllLinkGroup(){
        List<LinkGroupVO> list = linkGroupService.listAllGroup();
        return JsonData.buildSuccess(list);
    }


    /**
     * 列出用户全部分组
     */
    @PutMapping("update")
    public JsonData update(@RequestBody LinkGroupUpdateRequest request){
        int rows = linkGroupService.updateById(request);
        return rows == 1 ? JsonData.buildSuccess():JsonData.buildResult(BizCodeEnum.GROUP_OPERA_FAIL);
    }

}

