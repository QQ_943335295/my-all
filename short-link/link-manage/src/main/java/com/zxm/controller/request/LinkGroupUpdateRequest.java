package com.zxm.controller.request;

import lombok.Data;

/**
 * 短链分组更新请求实体
 * @author zxm
 * @date 2022/10/9 22:38
 */
@Data
public class LinkGroupUpdateRequest {

    /**
     * 组id
     */
    private Long id;
    /**
     * 组名
     */
    private String title;
}
