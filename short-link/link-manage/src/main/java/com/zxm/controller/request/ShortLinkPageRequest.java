package com.zxm.controller.request;

import lombok.Data;

/**
 * 短链分页请求
 * @author zxm
 * @date 2022/11/23 22:54
 */
@Data
public class ShortLinkPageRequest {


    /**
     * 组
     */
    private Long groupId;

    /**
     * 第几页
     */
    private int page;

    /**
     * 每页多少条
     */
    private int size;

}
