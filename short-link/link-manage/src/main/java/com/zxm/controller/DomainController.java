package com.zxm.controller;


import com.zxm.bean.JsonData;
import com.zxm.service.DomainService;
import com.zxm.vo.DomainVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 域名控制器
 * @author zxm
 * @date 2022/11/7 22:26
 */
@RestController
@RequestMapping("/api/domain/v1")
public class DomainController {


    @Autowired
    private DomainService domainService;

    /**
     * 列举全部可用域名列表
     * @return
     */
    @GetMapping("list")
    public JsonData listAll(){
        List<DomainVO> list = domainService.listAll();
        return JsonData.buildSuccess(list);

    }


}

