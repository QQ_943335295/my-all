package com.zxm.controller.request;

import com.alibaba.fastjson.JSONObject;
import com.zxm.enums.DomainTypeEnum;
import lombok.Data;

import java.util.Date;


/**
 * 短链添加请求
 * @author zxm
 * @date 2022/11/10 22:39
 */
@Data
public class ShortLinkAddRequest {

    /**
     * 组
     */
    private Long groupId;

    /**
     * 短链标题
     */
    private String title;

    /**
     * 原生url
     */
    private String originalUrl;

    /**
     * 域名id
     */
    private Long domainId;

    /**
     * 域名类型
     */
    private String domainType;

    /**
     * 过期时间
     */
    private Date expired;


    public static void main(String[] args) {
        ShortLinkAddRequest request = new ShortLinkAddRequest();
        request.setGroupId(1580209869394137089L);
        request.setTitle("ZXM第一个短链");
        request.setOriginalUrl("www.baidu.com");
        request.setDomainId(1580209850565906422L);
        request.setDomainType(DomainTypeEnum.OFFICIAL.name());
        request.setExpired(new Date());
        System.err.println(JSONObject.toJSONString(request));
    }

}
