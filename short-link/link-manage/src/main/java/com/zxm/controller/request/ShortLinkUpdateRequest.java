package com.zxm.controller.request;

import lombok.Data;

/**
 * 短链修改
 * @author zxm
 * @date 2022/11/24 21:29
 */
@Data
public class ShortLinkUpdateRequest {

    /**
     * 组
     */
    private Long groupId;

    /**
     * 短链码
     */
    private String code;

    /**
     * 短链标题
     */
    private String title;

    /**
     * 域名id
     */
    private Long domainId;

    /**
     * 域名类型
     */
    private String domainType;

}
