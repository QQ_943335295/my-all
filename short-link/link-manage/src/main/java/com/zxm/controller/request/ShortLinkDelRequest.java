package com.zxm.controller.request;

import lombok.Data;

/**
 * 短链删除
 * @author zxm
 * @date 2022/11/24 21:29
 */
@Data
public class ShortLinkDelRequest {

    /**
     * 组
     */
    private Long groupId;

    /**
     * 短链码
     */
    private String code;
}