package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.DomainDO;

/**
 * 域名映射层
 * @author zxm
 * @date 2022/11/7 22:33
 */
public interface DomainMapper extends BaseMapper<DomainDO> {

}
