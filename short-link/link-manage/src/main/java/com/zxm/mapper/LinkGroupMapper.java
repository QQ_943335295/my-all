package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.LinkGroupDO;

/**
 * 短链分组映射层
 * @author zxm
 * @date 2022/10/9 22:45
 */
public interface LinkGroupMapper extends BaseMapper<LinkGroupDO> {

}
