package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.GroupCodeMappingDO;

/**
 * 域名分组映射层
 * @author zxm
 * @date 2022/11/7 22:34
 */
public interface GroupCodeMappingMapper extends BaseMapper<GroupCodeMappingDO> {

}
