package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.ShortLinkDO;

/**
 * 短链映射层
 * @author zxm
 * @date 2022/10/9 22:46
 */
public interface ShortLinkMapper extends BaseMapper<ShortLinkDO> {

}
