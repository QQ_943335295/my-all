package com.zxm.config.sharding;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 数据表分片前缀获取
 * @author zxm
 * @date 2022/10/20 21:44
 */
public class ShardingTableConfig {

    /**
     * 存储数据表位置编号
     */
    private static final List<String> TABLE_SUFFIX_LIST = new ArrayList<>();

    private static final Random RANDOM = new Random();

    //配置启用那些表的后缀
    static {
        TABLE_SUFFIX_LIST.add("0");
        TABLE_SUFFIX_LIST.add("a");
    }


    /**
     * 相同的Code获取固定的表位
     */
    public static String getRandomTableSuffix(String code){
        int hashCode = code.hashCode();
        int index = Math.abs(hashCode) % TABLE_SUFFIX_LIST.size();
        return TABLE_SUFFIX_LIST.get(index);
    }

}
