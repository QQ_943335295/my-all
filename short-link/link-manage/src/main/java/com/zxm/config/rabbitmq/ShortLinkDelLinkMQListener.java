package com.zxm.config.rabbitmq;

import com.rabbitmq.client.Channel;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.EventMessageType;
import com.zxm.exception.BizException;
import com.zxm.service.ShortLinkService;
import com.zxm.vo.EventMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 短链删除监听
 * @author zxm
 * @date 2022/11/24 21:50
 */
@Component
@Slf4j
@RabbitListener(queuesToDeclare = { @Queue("short_link.del.link.queue") })
public class ShortLinkDelLinkMQListener {

    @Autowired
    private ShortLinkService shortLinkService;

    @RabbitHandler
    public void shortLinkHandler(EventMessage eventMessage, Message message, Channel channel) throws IOException {
        log.info("监听到消息ShortLinkDelLinkMQListener message消息内容:{}",message);
        try{
            eventMessage.setEventMessageType(EventMessageType.SHORT_LINK_DEL_LINK.name());
            shortLinkService.handleDelShortLink(eventMessage);
        }catch (Exception e){
            //处理业务异常，还有进行其他操作，比如记录失败原因
            log.error("消费失败:{}",eventMessage);
            throw new BizException(BizCodeEnum.MQ_CONSUME_EXCEPTION);
        }
        log.info("消费成功:{}",eventMessage);
    }

}
