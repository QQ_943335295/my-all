package com.zxm.config.sharding;

import com.zxm.utils.CommonUtil;

/**
 * 短链组件
 * @author zxm
 * @date 2022/10/20 22:43
 */
public class ShortLinkComponent {

    /**
     * 生成短链码
     */
    public static String createShortLinkCode(String param){
        //进制转换
        String code = CommonUtil.murmurHash32EncodeToBase62(param);
        return ShardingDBConfig.getRandomDBPrefix(code) + code + ShardingTableConfig.getRandomTableSuffix(code);
    }

}
