package com.zxm.config.rabbitmq;

import com.rabbitmq.client.Channel;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.EventMessageType;
import com.zxm.exception.BizException;
import com.zxm.service.ShortLinkService;
import com.zxm.vo.EventMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
// 懒加载创建队列
@RabbitListener(queues = "short_link.add.mapping.queue")
// 主动创建队列
// @RabbitListener(queuesToDeclare = { @Queue("short_link.add.mapping.queue") })
public class ShortLinkAddMappingMQListener {

    @Autowired
    private ShortLinkService shortLinkService;

    @RabbitHandler
    public void shortLinkHandler(EventMessage eventMessage, Message message, Channel channel) throws IOException {
        log.info("监听到消息ShortLinkAddMappingMQListener：message消息内容：{}", message);
        long msgTag = message.getMessageProperties().getDeliveryTag();
        try {
            //TODO 处理业务
            eventMessage.setEventMessageType(EventMessageType.SHORT_LINK_ADD_MAPPING.name());
            shortLinkService.handlerAddShortLink(eventMessage);
        } catch (Exception e) {
            // 处理业务失败，还要进行其他操作，比如记录失败原因
            log.error("消费失败{}", eventMessage);
            throw new BizException(BizCodeEnum.MQ_CONSUME_EXCEPTION);
        }
        log.info("消费成功{}", eventMessage);
        //确认消息消费成功
        // channel.basicAck(msgTag, false);
    }

}
