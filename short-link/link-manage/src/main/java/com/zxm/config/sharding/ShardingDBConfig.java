package com.zxm.config.sharding;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 数据库分片前缀获取
 * @author zxm
 * @date 2022/10/20 21:44
 */
public class ShardingDBConfig {

    /**
     * 存储数据库位置编号
     */
    private static final List<String> DB_PREFIX_LIST = new ArrayList<>();

    private static final Random RANDOM = new Random();

    //配置启用那些库的前缀
    static {
        DB_PREFIX_LIST.add("0");
        DB_PREFIX_LIST.add("1");
        DB_PREFIX_LIST.add("a");
    }


    /**
     * 相同的Code获取固定的库位
     */
    public static String getRandomDBPrefix(String code){
        int hashCode = code.hashCode();
        int index = Math.abs(hashCode) % DB_PREFIX_LIST.size();
        return DB_PREFIX_LIST.get(index);
    }

}
