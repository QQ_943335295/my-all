package com.zxm.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.zxm.constants.CommonConstants;
import com.zxm.manager.ShortLinkManager;
import com.zxm.mapper.ShortLinkMapper;
import com.zxm.model.ShortLinkDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ShortLinkManagerImpl implements ShortLinkManager {

    @Autowired
    private ShortLinkMapper shortLinkMapper;

    @Override
    public int addShortLink(ShortLinkDO shortLinkDO) {
        return shortLinkMapper.insert(shortLinkDO);
    }

    @Override
    public ShortLinkDO findByShortLinkCode(String shortLinkCode) {
        ShortLinkDO shortLinkDO = shortLinkMapper.selectOne(new LambdaQueryWrapper<ShortLinkDO>()
                .eq(ShortLinkDO::getCode, shortLinkCode)
                .eq(ShortLinkDO::getDel, 0));
        return shortLinkDO;
    }

    /**
     * 逻辑删除
     */
    @Override
    public int del(ShortLinkDO shortLinkDO) {
        return shortLinkMapper.update(null, new LambdaUpdateWrapper<ShortLinkDO>()
                .eq(ShortLinkDO::getCode, shortLinkDO.getCode())
                .eq(ShortLinkDO::getAccountNo, shortLinkDO.getAccountNo())
                .set(ShortLinkDO::getDel, CommonConstants.ONE));
    }

    /**
     * 修改短链域名
     */
    @Override
    public int update(ShortLinkDO shortLinkDO) {
        return shortLinkMapper.update(null, new LambdaUpdateWrapper<ShortLinkDO>()
                .eq(ShortLinkDO::getCode, shortLinkDO.getCode())
                .eq(ShortLinkDO::getDel, CommonConstants.ZERO)
                .eq(ShortLinkDO::getAccountNo,shortLinkDO.getAccountNo())
                .set(ShortLinkDO::getTitle, shortLinkDO.getTitle())
                .set(ShortLinkDO::getDomain, shortLinkDO.getDomain()));
    }
}

