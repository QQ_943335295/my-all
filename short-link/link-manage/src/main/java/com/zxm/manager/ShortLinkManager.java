package com.zxm.manager;

import com.zxm.model.ShortLinkDO;

public interface ShortLinkManager {

    /**
     * 新增域名
     */
    int addShortLink(ShortLinkDO shortLinkDO);

    /**
     * 根据短链码找内容
     */
    ShortLinkDO findByShortLinkCode(String shortLinkCode);

    /**
     * 删除
     */
    int del(ShortLinkDO shortLinkDO);

    /**
     * 更新
     */
    int update(ShortLinkDO shortLinkDO);
}
