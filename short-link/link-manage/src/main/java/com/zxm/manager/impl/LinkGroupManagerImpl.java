package com.zxm.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxm.manager.LinkGroupManager;
import com.zxm.mapper.LinkGroupMapper;
import com.zxm.model.LinkGroupDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 短链分组管理层
 * @author zxm
 * @date 2022/10/9 22:45
 */
@Component
public class LinkGroupManagerImpl implements LinkGroupManager {

    @Autowired
    private LinkGroupMapper linkGroupMapper;

    @Override
    public int add(LinkGroupDO linkGroupDO) {
        return linkGroupMapper.insert(linkGroupDO);
    }

    @Override
    public int del(Long groupId, Long accountNo) {
        return linkGroupMapper.delete(new LambdaQueryWrapper<LinkGroupDO>().eq(LinkGroupDO::getId,groupId).eq(LinkGroupDO::getAccountNo,accountNo));
    }

    @Override
    public LinkGroupDO detail(Long groupId, Long accountNo) {
        return linkGroupMapper.selectOne(new LambdaQueryWrapper<LinkGroupDO>().eq(LinkGroupDO::getId,groupId).eq(LinkGroupDO::getAccountNo,accountNo));
    }

    @Override
    public List<LinkGroupDO> listAllGroup(Long accountNo) {
        return linkGroupMapper.selectList(new LambdaQueryWrapper<LinkGroupDO>().eq(LinkGroupDO::getAccountNo,accountNo));
    }

    @Override
    public int updateById(LinkGroupDO linkGroupDO) {
        return linkGroupMapper.update(linkGroupDO,new LambdaQueryWrapper<LinkGroupDO>().eq(LinkGroupDO::getId,linkGroupDO.getId()).eq(LinkGroupDO::getAccountNo,linkGroupDO.getAccountNo()));
    }
}
