package com.zxm.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxm.constants.CommonConstants;
import com.zxm.enums.ShortLinkStateEnum;
import com.zxm.manager.GroupCodeMappingManager;
import com.zxm.mapper.GroupCodeMappingMapper;
import com.zxm.model.GroupCodeMappingDO;
import com.zxm.vo.GroupCodeMappingVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 域名分组映射管理层
 * @author zxm
 * @date 2022/11/7 22:32
 */
@Component
@Slf4j
public class GroupCodeMappingManagerImpl implements GroupCodeMappingManager {

    @Autowired
    private GroupCodeMappingMapper groupCodeMappingMapper;

    @Override
    public GroupCodeMappingDO findByGroupIdAndMappingId(Long mappingId, Long accountNo, Long groupId) {
        GroupCodeMappingDO groupCodeMappingDO = groupCodeMappingMapper.selectOne(new LambdaQueryWrapper<GroupCodeMappingDO>()
                .eq(GroupCodeMappingDO::getId, mappingId).eq(GroupCodeMappingDO::getAccountNo, accountNo)
                .eq(GroupCodeMappingDO::getGroupId, groupId));
        return groupCodeMappingDO;
    }

    @Override
    public int add(GroupCodeMappingDO groupCodeMappingDO) {
        return groupCodeMappingMapper.insert(groupCodeMappingDO);
    }

    @Override
    public int del(GroupCodeMappingDO groupCodeMappingDO) {
        return groupCodeMappingMapper.update(null, new LambdaUpdateWrapper<GroupCodeMappingDO>()
                .eq(GroupCodeMappingDO::getCode, groupCodeMappingDO.getCode())
                .eq(GroupCodeMappingDO::getAccountNo,groupCodeMappingDO.getAccountNo())
                .eq(GroupCodeMappingDO::getGroupId, groupCodeMappingDO.getGroupId())
                .set(GroupCodeMappingDO::getDel, CommonConstants.ONE));
    }

    @Override
    public int update(GroupCodeMappingDO groupCodeMappingDO) {
        return groupCodeMappingMapper.update(null, new LambdaUpdateWrapper<GroupCodeMappingDO>()
                .eq(GroupCodeMappingDO::getCode, groupCodeMappingDO.getCode())
                .eq(GroupCodeMappingDO::getAccountNo, groupCodeMappingDO.getAccountNo())
                .eq(GroupCodeMappingDO::getGroupId, groupCodeMappingDO.getGroupId())
                .eq(GroupCodeMappingDO::getDel, CommonConstants.ZERO)
                .set(GroupCodeMappingDO::getTitle, groupCodeMappingDO.getTitle())
                .set(GroupCodeMappingDO::getDomain, groupCodeMappingDO.getDomain()));
    }


    @Override
    public Map<String, Object> pageShortLinkByGroupId(Integer page, Integer size, Long accountNo, Long groupId) {
        Page<GroupCodeMappingDO> pageInfo = new Page<>(page, size);
        Page<GroupCodeMappingDO> groupCodeMappingDOPage = groupCodeMappingMapper.selectPage(pageInfo,
                new LambdaQueryWrapper<GroupCodeMappingDO>().eq(GroupCodeMappingDO::getAccountNo, accountNo)
                .eq(GroupCodeMappingDO::getGroupId, groupId)
                        .eq(GroupCodeMappingDO::getDel, CommonConstants.ZERO));
        Map<String, Object> pageMap = new HashMap<>(3);
        pageMap.put("total_record", groupCodeMappingDOPage.getTotal());
        pageMap.put("total_page", groupCodeMappingDOPage.getPages());
        pageMap.put("current_data", groupCodeMappingDOPage.getRecords()
                .stream().map(obj -> beanProcess(obj)).collect(Collectors.toList()));
        return pageMap;
    }

    @Override
    public int updateGroupCodeMappingState(Long accountNo, Long groupId, String shortLinkCode, ShortLinkStateEnum shortLinkStateEnum) {
        return groupCodeMappingMapper.update(null, new LambdaUpdateWrapper<GroupCodeMappingDO>()
                .eq(GroupCodeMappingDO::getCode, shortLinkCode).eq(GroupCodeMappingDO::getAccountNo, accountNo)
                .eq(GroupCodeMappingDO::getGroupId, groupId).eq(GroupCodeMappingDO::getDel, CommonConstants.ZERO)
                .set(GroupCodeMappingDO::getState, shortLinkStateEnum.name()));
    }

    @Override
    public GroupCodeMappingDO findByCodeAndGroupId(String shortLinkCode, Long groupId, Long accountNo) {
        GroupCodeMappingDO groupCodeMappingDO = groupCodeMappingMapper.selectOne(new LambdaQueryWrapper<GroupCodeMappingDO>()
                .eq(GroupCodeMappingDO::getCode, shortLinkCode).eq(GroupCodeMappingDO::getAccountNo, accountNo)
                .eq(GroupCodeMappingDO::getGroupId, groupId));
        return groupCodeMappingDO;
    }


    private GroupCodeMappingVO beanProcess(GroupCodeMappingDO groupCodeMappingDO) {
        GroupCodeMappingVO groupCodeMappingVO = new GroupCodeMappingVO();
        BeanUtils.copyProperties(groupCodeMappingDO, groupCodeMappingVO);
        return groupCodeMappingVO;
    }

}
