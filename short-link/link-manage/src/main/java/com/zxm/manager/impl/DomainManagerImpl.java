package com.zxm.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxm.enums.DomainTypeEnum;
import com.zxm.manager.DomainManager;
import com.zxm.mapper.DomainMapper;
import com.zxm.model.DomainDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 域名管理层
 * @author zxm
 * @date 2022/11/7 22:26
 */
@Component
@Slf4j
public class DomainManagerImpl implements DomainManager {

    @Autowired
    private DomainMapper domainMapper;

    @Override
    public DomainDO findById(Long id, Long accountNO) {
        return domainMapper.selectOne(new LambdaQueryWrapper<DomainDO>().eq(DomainDO::getId, id).eq(DomainDO::getAccountNo, accountNO));
    }

    @Override
    public DomainDO findByDomainTypeAndID(Long id, DomainTypeEnum domainTypeEnum) {
        return domainMapper.selectOne(new LambdaQueryWrapper<DomainDO>().eq(DomainDO::getId, id).eq(DomainDO::getDomainType, domainTypeEnum.name()));
    }

    @Override
    public int addDomain(DomainDO domainDO) {
        return domainMapper.insert(domainDO);
    }


    @Override
    public List<DomainDO> listOfficialDomain() {
        return domainMapper.selectList(new LambdaQueryWrapper<DomainDO>().eq(DomainDO::getDomainType, DomainTypeEnum.OFFICIAL.name()));
    }

    @Override
    public List<DomainDO> listCustomDomain(Long accountNo) {
        return domainMapper.selectList(new LambdaQueryWrapper<DomainDO>()
                .eq(DomainDO::getDomainType, DomainTypeEnum.CUSTOM.name())
                .eq(DomainDO::getAccountNo, accountNo));
    }
}
