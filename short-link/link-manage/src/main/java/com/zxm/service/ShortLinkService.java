package com.zxm.service;

import com.zxm.bean.JsonData;
import com.zxm.controller.request.ShortLinkAddRequest;
import com.zxm.controller.request.ShortLinkDelRequest;
import com.zxm.controller.request.ShortLinkPageRequest;
import com.zxm.controller.request.ShortLinkUpdateRequest;
import com.zxm.vo.EventMessage;
import com.zxm.vo.ShortLinkVO;

import java.util.Map;


/**
 * 短链服务
 * @author zxm
 * @date 2022/10/20 23:07
 */
public interface ShortLinkService {


    /**
     * 创建短链
     */
    JsonData createShortLink(ShortLinkAddRequest request);

    /**
     * 解析短链
     */
    ShortLinkVO parseShortLinkCode(String shortLinkCode);

    /**
     * 处理新增短链消息
     */
    boolean handlerAddShortLink(EventMessage eventMessage);


    /**
     * 分页查找短链
     */
    Map<String,Object> pageByGroupId(ShortLinkPageRequest request);

    /**
     * 删除短链
     */
    JsonData del(ShortLinkDelRequest request);

    /**
     * 更新
     */
    JsonData update(ShortLinkUpdateRequest request);

    /**
     * 更新短链
     */
    boolean handleUpdateShortLink(EventMessage eventMessage);

    /**
     * 删除短链
     */
    boolean handleDelShortLink(EventMessage eventMessage);
}
