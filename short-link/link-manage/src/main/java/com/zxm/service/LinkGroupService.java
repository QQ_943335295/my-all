package com.zxm.service;

import com.zxm.controller.request.LinkGroupAddRequest;
import com.zxm.controller.request.LinkGroupUpdateRequest;
import com.zxm.vo.LinkGroupVO;

import java.util.List;


/**
 * 短链分组服务层接口
 * @author zxm
 * @date 2022/10/9 22:48
 */
public interface LinkGroupService {


    /**
     * 新增分组
     */
    int add(LinkGroupAddRequest addRequest);

    /**
     * 删除分组
     */
    int del(Long groupId);

    /**
     * 详情
     */
    LinkGroupVO detail(Long groupId);

    /**
     * 列出用户全部分组
     */
    List<LinkGroupVO> listAllGroup();

    /**
     * 更新组名
     */
    int updateById(LinkGroupUpdateRequest request);
}
