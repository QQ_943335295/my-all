package com.zxm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 账户服务启动类
 * @author zxm
 * @date 2022/8/25 22:05
 */
@EnableAsync
@MapperScan("com.zxm.mapper")
@EnableTransactionManagement
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class LinkManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(LinkManageApplication.class, args);
    }

}