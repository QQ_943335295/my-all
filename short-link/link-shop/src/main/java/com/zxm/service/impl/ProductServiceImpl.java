package com.zxm.service.impl;

import com.zxm.manager.ProductManager;
import com.zxm.model.ProductDO;
import com.zxm.service.ProductService;
import com.zxm.vo.ProductVO;
import groovy.util.logging.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author zxm
 * @date 2022/11/25 21:48
 */
@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductManager productManager;

    @Override
    public List<ProductVO> list() {
        List<ProductDO> list = productManager.list();
        List<ProductVO> collect = list.stream().map( obj -> beanProcess(obj) ).collect(Collectors.toList());
        return collect;
    }

    @Override
    public ProductVO findDetailById(long productId) {
        ProductDO productDO = productManager.findDetailById(productId);
        ProductVO productVO = beanProcess(productDO);
        return productVO;
    }


    private ProductVO beanProcess(ProductDO productDO) {
        ProductVO productVO = new ProductVO();
        BeanUtils.copyProperties(productDO, productVO);
        return productVO;
    }

}
