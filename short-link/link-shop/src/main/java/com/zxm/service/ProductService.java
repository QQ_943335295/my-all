package com.zxm.service;

import com.zxm.vo.ProductVO;

import java.util.List;

public interface ProductService {

    /**
     * 商品列表
     */
    List<ProductVO> list();

    /**
     * 商品详情
     */
    ProductVO findDetailById(long productId);
}
