package com.zxm.service;

import com.zxm.bean.JsonData;
import com.zxm.controller.request.ConfirmOrderRequest;

import java.util.Map;

public interface ProductOrderService {

    Map<String,Object> page(int page, int size, String state);

    String queryProductOrderState(String outTradeNo);

    JsonData confirmOrder(ConfirmOrderRequest orderRequest);
}
