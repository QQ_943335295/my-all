package com.zxm.manager;

import com.zxm.model.ProductDO;

import java.util.List;

public interface ProductManager {

    /**
     * 商品列表
     */
    List<ProductDO> list();

    /**
     * 查询商品详情
     */
    ProductDO findDetailById(long productId);
}
