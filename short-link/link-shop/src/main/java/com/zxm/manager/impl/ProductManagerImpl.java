package com.zxm.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxm.manager.ProductManager;
import com.zxm.mapper.ProductMapper;
import com.zxm.model.ProductDO;
import groovy.util.logging.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 管理层
 * @author zxm
 * @date 2022/11/25 21:47
 */
@Component
@Slf4j
public class ProductManagerImpl implements ProductManager {

    @Autowired
    private ProductMapper productMapper;


    @Override
    public List<ProductDO> list() {
        return productMapper.selectList(null);
    }

    @Override
    public ProductDO findDetailById(long productId) {
        return productMapper.selectOne(new LambdaQueryWrapper<ProductDO>().eq(ProductDO::getId, productId));
    }
}
