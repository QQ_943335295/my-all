package com.zxm.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxm.constants.CommonConstants;
import com.zxm.manager.ProductOrderManager;
import com.zxm.mapper.ProductOrderMapper;
import com.zxm.model.ProductOrderDO;
import com.zxm.vo.ProductOrderVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class ProductOrderManagerImpl implements ProductOrderManager {

    @Autowired
    private ProductOrderMapper productOrderMapper;

    @Override
    public int add(ProductOrderDO productOrderDO) {
        return productOrderMapper.insert(productOrderDO);
    }

    @Override
    public ProductOrderDO findByOutTradeNoAndAccountNo(String outTradeNo, Long accountNo) {
        ProductOrderDO productOrderDO = productOrderMapper.selectOne(new LambdaQueryWrapper<ProductOrderDO>()
                .eq(ProductOrderDO::getOutTradeNo, outTradeNo).eq(ProductOrderDO::getAccountNo, accountNo).eq(ProductOrderDO::getDel, CommonConstants.ZERO));
        return productOrderDO;
    }

    @Override
    public int updateOrderPayState(String outTradeNo, Long accountNo, String newState, String oldState) {
        int rows = productOrderMapper.update(null, new LambdaUpdateWrapper<ProductOrderDO>()
                        .eq(ProductOrderDO::getOutTradeNo, outTradeNo).eq(ProductOrderDO::getAccountNo, accountNo)
                .eq(ProductOrderDO::getState, oldState)
                .set(ProductOrderDO::getState, newState));
        return rows;
    }

    @Override
    public Map<String, Object> page(int page, int size, Long accountNo, String state) {
        Page<ProductOrderDO> pageInfo = new Page<>(page, size);
        IPage<ProductOrderDO> orderDOIPage;
        if (StringUtils.isBlank(state)) {
            orderDOIPage = productOrderMapper.selectPage(pageInfo, new LambdaQueryWrapper<ProductOrderDO>()
                    .eq(ProductOrderDO::getAccountNo, accountNo)
                    .eq(ProductOrderDO::getDel, CommonConstants.ZERO));
        } else {
            orderDOIPage = productOrderMapper.selectPage(pageInfo, new LambdaQueryWrapper<ProductOrderDO>()
                    .eq(ProductOrderDO::getAccountNo, accountNo)
                    .eq(ProductOrderDO::getDel, CommonConstants.ZERO)
                    .eq(ProductOrderDO::getState, state));
        }
        List<ProductOrderDO> orderDOIPageRecords = orderDOIPage.getRecords();
        List<ProductOrderVO> productOrderVOList = orderDOIPageRecords.stream().map(obj -> {
            ProductOrderVO productOrderVO = new ProductOrderVO();
            BeanUtils.copyProperties(obj, productOrderVO);
            return productOrderVO;
        }).collect(Collectors.toList());
        Map<String, Object> pageMap = new HashMap<>(3);
        pageMap.put("total_record", orderDOIPage.getTotal());
        pageMap.put("total_page", orderDOIPage.getPages());
        pageMap.put("current_data", productOrderVOList);
        return pageMap;
    }

    @Override
    public int del(Long productOrderId, Long accountNo) {
        int rows = productOrderMapper.update(null, new LambdaQueryWrapper<ProductOrderDO>()
                .eq(ProductOrderDO::getId, productOrderId).eq(ProductOrderDO::getAccountNo, accountNo).eq(ProductOrderDO::getDel, CommonConstants.ZERO));
        return rows;
    }

}
