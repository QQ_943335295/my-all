package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.ProductOrderDO;

public interface ProductOrderMapper extends BaseMapper<ProductOrderDO> {

}
