package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.ProductDO;

/**
 * 商品Mapper
 * @author zxm
 * @date 2022/11/25 21:52
 */
public interface ProductMapper extends BaseMapper<ProductDO> {

}
