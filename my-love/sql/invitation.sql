/*
 Navicat Premium Data Transfer

 Source Server         : local-3306
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : my-love

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 03/09/2023 23:54:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for invitation
-- ----------------------------
DROP TABLE IF EXISTS `invitation`;
CREATE TABLE `invitation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '映射路径',
  `content` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '内容',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `state` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态：1-zxm 2-ljy',
  PRIMARY KEY (`id`, `path`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invitation
-- ----------------------------
INSERT INTO `invitation` VALUES (1, 'zxm', 'zxm & lxy', '0', '', '2023-08-30 16:26:34', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (2, 'zyp', '钟先生', '0', '', '2023-09-03 22:10:03', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (3, 'wsy', '王女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (4, 'ty', '唐先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (5, 'tw', '谭女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (6, 'zhd', '朱先生 & 张女士', '0', '', '2023-09-03 22:10:03', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (7, 'cby', '陈先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (8, 'ljc', '李先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (9, 'lxn', '刘先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (10, 'wq', '王先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (11, 'hy', '洪先生 & 稂女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (12, 'zwm', '朱先生 & 朱夫人', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (13, 'oyk', '欧阳先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (14, 'ygz', '袁先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (15, 'llf', '黄先生 & 刘女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (16, 'zjs', '张先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (17, 'wlz', '王先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (18, 'xzx', '邹先生 & 谢女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (19, 'hmr', '黄先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (20, 'fsx', '冯先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (21, 'xbl', '夏先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (22, 'zjl', '曾先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (23, 'lzx', '李先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (24, 'lp', '李先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (25, 'cxy', '蔡女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (26, 'cd', '陈先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (27, 'lsf', '李先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (28, 'hzw', '胡先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (29, 'jgf', '简先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (30, 'gzx', '郭女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (31, 'zgd', '朱先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (32, 'zq', '张女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (33, 'ly', '廖女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (34, 'hmc', '黄女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (35, 'xh', '徐女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (36, 'zk', '曾女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (37, 'yys', '叶先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (38, 'zl', '曾先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (39, 'dbxd', '刘先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (40, 'lzy', '刘先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (41, 'yzw', '叶先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (42, 'hb', '胡先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (43, 'pj', '潘先生 & 刘女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (44, 'zxl', '朱女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (45, 'zxd', '曾女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (46, 'zy', '周先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (47, 'lj', '刘先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (48, 'wjs', '王先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (49, 'xzl', '徐先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (50, 'xlq', '徐先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (51, 'xwl', '徐先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (52, 'xgq', '徐先生 & 曾女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (53, 'zh', '张先生 & 张女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (54, 'zp', '周先生 & 周夫人', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (55, 'zyx', '朱先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (56, 'zm', '张女士', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (57, 'dzh', '段先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (58, 'ljx', '刘先生', '0', '', '2023-09-03 22:10:53', '', NULL, NULL, NULL);
INSERT INTO `invitation` VALUES (59, 'ccj', '陈先生', '0', '', '2023-09-03 22:10:53', '', NULL, '陈翅俊', NULL);
INSERT INTO `invitation` VALUES (60, 'hk', '胡先生', '0', '', '2023-09-03 22:10:53', '', NULL, '胡坤', NULL);
INSERT INTO `invitation` VALUES (61, 'plm', '彭先生', '0', '', '2023-09-03 22:10:53', '', NULL, '彭黎明', NULL);
INSERT INTO `invitation` VALUES (62, 'lyl', '刘女士', '0', '', '2023-09-03 22:10:53', '', NULL, '刘雅丽', NULL);
INSERT INTO `invitation` VALUES (63, 'sfq', '沈女士', '0', '', '2023-09-03 22:10:53', '', NULL, '沈符栖', NULL);
INSERT INTO `invitation` VALUES (64, 'lyy', '刘女士', '0', '', '2023-09-03 22:10:53', '', NULL, '刘圆圆', NULL);
INSERT INTO `invitation` VALUES (65, 'jjm', '蒋女士', '0', '', '2023-09-03 22:10:53', '', NULL, '将佳敏', NULL);
INSERT INTO `invitation` VALUES (66, 'ylj', '唐女士', '0', '', '2023-09-03 22:10:53', '', NULL, '唐翎洁', NULL);
INSERT INTO `invitation` VALUES (67, 'pk', '彭女士', '0', '', '2023-09-03 22:10:53', '', NULL, '唐翎洁', NULL);
INSERT INTO `invitation` VALUES (68, 'ly', '李女士', '0', '', '2023-09-03 22:10:53', '', NULL, '彭锟', NULL);
INSERT INTO `invitation` VALUES (69, 'jjm', '蒋女士', '0', '', '2023-09-03 22:10:53', '', NULL, '将佳敏', NULL);
INSERT INTO `invitation` VALUES (70, 'yjj', '杨女士&文先生', '0', '', '2023-09-03 22:10:53', '', NULL, '杨佳瑾&文吉凡', NULL);
INSERT INTO `invitation` VALUES (71, 'xhq', '薛女士&刘先生', '0', '', '2023-09-03 22:10:53', '', NULL, '薛慧琦', NULL);
INSERT INTO `invitation` VALUES (72, 'lh', '刘女士', '0', '', '2023-09-03 22:10:53', '', NULL, '刘欢', NULL);
INSERT INTO `invitation` VALUES (73, 'jjm', '蔡女士', '0', '', '2023-09-03 22:10:53', '', NULL, '蔡巧珍', NULL);
INSERT INTO `invitation` VALUES (74, 'djh', '邓女士', '0', '', '2023-09-03 22:10:53', '', NULL, '邓锦红', NULL);
INSERT INTO `invitation` VALUES (75, 'gzy', '龚女士', '0', '', '2023-09-03 22:10:53', '', NULL, '龚紫莹', NULL);
INSERT INTO `invitation` VALUES (76, 'hb', '黄先生', '0', '', '2023-09-03 22:10:53', '', NULL, '黄奔', NULL);
INSERT INTO `invitation` VALUES (77, 'ph', '彭女士', '0', '', '2023-09-03 22:10:53', '', NULL, '彭虹', NULL);
INSERT INTO `invitation` VALUES (78, 'ym', '袁女士', '0', '', '2023-09-03 22:10:53', '', NULL, '袁梦', NULL);
INSERT INTO `invitation` VALUES (79, 'gss', '龚女士&李先生', '0', '', '2023-09-03 22:10:53', '', NULL, '龚思丝&李东明', NULL);
INSERT INTO `invitation` VALUES (80, 'jjm', '曾女士', '0', '', '2023-09-03 22:10:53', '', NULL, '曾攀', NULL);
INSERT INTO `invitation` VALUES (81, 'gfj', '郭女士', '0', '', '2023-09-03 22:10:53', '', NULL, '郭福娇', NULL);
INSERT INTO `invitation` VALUES (82, 'yn', '杨女士', '0', '', '2023-09-03 22:10:53', '', NULL, '杨妮', NULL);
INSERT INTO `invitation` VALUES (83, 'syq', '宋先生', '0', '', '2023-09-03 22:10:53', '', NULL, '宋昱乾', NULL);
INSERT INTO `invitation` VALUES (84, 'hzw', '黄先生', '0', '', '2023-09-03 22:10:53', '', NULL, '黄志文', NULL);
INSERT INTO `invitation` VALUES (85, 'lwh', '刘先生', '0', '', '2023-09-03 22:10:53', '', NULL, '刘伟辉', NULL);
INSERT INTO `invitation` VALUES (86, 'zjy', '朱先生', '0', '', '2023-09-03 22:10:53', '', NULL, '朱嘉运', NULL);
INSERT INTO `invitation` VALUES (87, 'zwc', '朱先生', '0', '', '2023-09-03 22:10:53', '', NULL, '朱文超', NULL);
INSERT INTO `invitation` VALUES (88, 'dzl', '邓先生', '0', '', '2023-09-03 22:10:53', '', NULL, '邓子乐', NULL);
INSERT INTO `invitation` VALUES (89, 'hjf', '黄先生', '0', '', '2023-09-03 22:10:53', '', NULL, '黄佳福', NULL);
INSERT INTO `invitation` VALUES (90, 'gcs', '郭先生', '0', '', '2023-09-03 22:10:53', '', NULL, '郭承铄', NULL);
INSERT INTO `invitation` VALUES (91, 'dkf', '邓先生', '0', '', '2023-09-03 22:10:53', '', NULL, '邓凯峰哥哥', NULL);
INSERT INTO `invitation` VALUES (92, 'lht', '李先生', '0', '', '2023-09-03 22:10:53', '', NULL, '李洪涛哥哥', NULL);
INSERT INTO `invitation` VALUES (93, 'lht', '李先生', '0', '', '2023-09-03 22:10:53', '', NULL, '李洪涛哥哥', NULL);
INSERT INTO `invitation` VALUES (94, 'zsy', '朱先生', '0', '', '2023-09-03 22:10:53', '', NULL, '朱思远', NULL);

SET FOREIGN_KEY_CHECKS = 1;
