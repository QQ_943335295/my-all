<#assign basePath=request.contextPath />
<!DOCTYPE html>
<html class=" js-no-overflow-scrolling">
<head>
<meta charset="utf-8">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<title>婚礼邀请函-钟萌&罗俊雅</title>
<link rel="stylesheet" type="text/css" href="${basePath}/first/css/page-animation.css">
<link rel="stylesheet" type="text/css" href="${basePath}/first/css/H5FullscreenPage.css">
<link rel="icon" href="${basePath}/first/img/favicon.png" type="image/gif">
<style type="text/css">
    
    html, body {
        margin: 0;
        width: 100%;
        height: 100%;
        font-family: arial;
        text-align: center;
    }
    
    * {
        margin:0;
        padding: 0;
    }
    ul {
        list-style: none;
    }
    body {
        max-width: 640px;
        margin: auto;
    }
</style>

</head>
<script>
    // alert("温馨提示：手机访问方可滚动查看下一页！")
</script>
<body class="H5FullscreenPage">
<#--    <div class="niben_banc">
        <a href="#"  onclick=" Tanchuc()">基本信息</a>
        <a href="#">保存</a>
    </div>-->
    <!--  -->
     <!--弹框  -->
 <#--    <div class="tanchu_xx_web1 Tan_c">
        <div class="tcc_beij_x_web" onclick=" off()"></div>
        <div class="kuang_li">
            <div class="off" onclick=" off()"></div>
            <img class="img_bj" src="${basePath}/first/img/img_web3_03.png" alt="">
            &lt;#&ndash;<div class="bt">
                <h2>请输入您和您爱侣的名字</h2>
                <p>还有你们喜结连理的时间哦</p>
            </div>&ndash;&gt;
            <div class="bdkuan">
                <input type="text" placeholder="新郎">
                <input type="text" placeholder="新娘">
                <input type="text" placeholder="新郎电话">
                <input type="text" placeholder="新娘电话">
                <input type="text" placeholder="结婚日期（如：2020年5月20日（星期六）12:00）">
                <input type="text" placeholder="地址">
                <button>确定</button>
            </div>
        </div>
        <script>
        function Tanchuc(){
            $(".Tan_c").show();
        }
        function off(){
            $(".Tan_c").hide();
        }

        </script>
    </div> -->
    <!--  --> 
    <div class="H5FullscreenPage-wrap">
        <!--  -->
        <div class="item item5">
            <div class="part rollInLeft shang_img">
                <img src="${basePath}/first/img/2.png" alt="">
            </div>
            <div class="part rollInRight shang_img shang_img_b">
                <img src="${basePath}/first/img/21.png" alt="">
            </div>
            <!-- 线条 -->
            <div class="part top_Xial slideUp" data-delay="400"></div>
            <div class="part top_Xial top_Xial_b slideDown" data-delay="400"></div>
            <!--hua  金花  -->
            <div class="part fadeIn jinse_hua jinse_hua_no" data-delay="1000"> </div>
            <div class="part fadeIn jinse_hua jinse_hua_x jinse_hua_no" data-delay="1000"></div>
            <!-- 叶子 -->
            <div class="part slideLeft yezi_zuo"></div>
            <div class="part slideLeft yezi_zuo yezi_zuo_zq"></div>
            <div class="part slideDown yezi_zuo_xx"></div>
            <div class="part slideDown yezi_zuo_yrx"></div>

            <!-- 中间叶子 -->
            <div class="part fadeIn yezi_zhongk yezi_zhongk_dier" data-delay="1300"></div>
            <!-- 字 -->
            <div class="part zhongjiek_Ge zhongjiek_Ge_er" data-delay="1800">
                <img src="${basePath}/first/img/9.png" alt="">
            </div>
            <div class="part zhongjiek_Ge zhongjiek_Ge_er" data-delay="1800">
                <img class="nio" src="${basePath}/first/img/1.png" alt="">
            </div>
            <!-- h婚纱照 -->
            <div class="part nimen_img nimen_img_text slideDown"  data-delay="1800">
                <div class="hou_tupian">
                    <p>&nbsp;</p>
                    <p><b>尊敬的${content}:</b></p>
                    <p>诚邀您与您的家人参加我们的婚礼</p>
                    <p>共同见证我们人生中的一个重要时刻</p>
                    <p>婚礼将于<b>公历2023年10月4日</b>在<b>寒舍</b>举行</p>
                    <p>您的到来将是我们最大的荣幸和祝福</p>
                    <p>&nbsp;</p>
                    <p>地址：<b>江西省赣州市龙南市桃江乡窑头村委会往前150米</b></p>
                    <p>联系方式：<b>18370847419</b></p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>15170659864</b></p>
                    <p><b>钟萌 & 罗俊雅 敬邀</b></p>
                </div>
            </div>
            <!-- 修改 -->
            <!-- <div class="xiugai_nige"></div> -->
            <!--  -->
        </div>
        <!--  -->
        <div class="item item2">
            <span class="topArrow"></span>
            <!-- 金色 -->
            <div class="part rollInLeft shang_img"> 
                <img src="${basePath}/first/img/2.png" alt="">

            </div>
            <div class="part rollInRight shang_img shang_img_b">
                <img src="${basePath}/first/img/21.png" alt="">
            </div>
            <!-- 线条 -->
            <div class="part top_Xial slideUp" data-delay="400"></div>
            <div class="part top_Xial top_Xial_b slideDown" data-delay="400"></div>
            <!--hua  金花  -->
            <div class="part fadeIn jinse_hua" data-delay="1000"> </div>
            <div class="part fadeIn jinse_hua jinse_hua_x" data-delay="1000"></div> 
            <!-- 叶子 --> 
            <div class="part slideLeft yezi_zuo"></div>
            <div class="part slideLeft yezi_zuo yezi_zuo_zq"></div>
            <div class="part slideDown yezi_zuo_xx"></div>
            <div class="part slideDown yezi_zuo_yrx"></div>
            <div class="part zoomIn piao_gif"  data-delay="1500" style="    width: 100%;
            height: 100%;
            top: 0;">
                <img src="${basePath}/first/img/jingy.gif" alt="">
            </div>
            <!-- 中间 -->
            <div class="part fadeIn kuang_nieg" data-delay="800">
                <img src="${basePath}/first/img/19.png" alt="">
            </div>
            <div class="part fadeIn kuang_nieg" data-delay="1000">
                <img class="nie" src="${basePath}/first/img/19.png" alt="">
            </div>
            <!-- 中间叶子 -->
            <div class="part fadeIn yezi_zhongk" data-delay="1300"></div>
            <div class="part fadeIn yezi_zhongk_x" data-delay="1300"></div>
            <!-- 字 -->
            <div class="part zhongjiek_Ge rentx_kuang" data-delay="1800">
                <div class="rentx">
                    <img src="${basePath}/first/img/xinlang${mediumRandomNum}.jpg" alt="">
                </div>
            </div> 
            <!-- 文字 -->
            <div class="part xinniang_xinlang" data-delay="1800"   >
                <p>新郎：钟萌</p>
                <dd style="    line-height: .4rem; padding-top:.2rem;">要你拥有会长大的幸福<br>一天比一天像公主，梦都被满足</dd>
                <!-- <span>You two are the prince and princess in a fairy tale and may you live happily everafter!</span> -->
            </div>
            <!--  --> 
            <!-- 修改 -->
            <#--<div class="xiugai_nige" style="    left: .3rem;top: 5.8rem;"></div>-->
            <!--  -->
        </div>
        <!--  -->
        <div class="item item3">
            <span class="topArrow"></span>
            <!-- 金色 -->
            <div class="part rollInLeft shang_img"> 
                <img src="${basePath}/first/img/2.png" alt="">

            </div>
            <div class="part rollInRight shang_img shang_img_b">
                <img src="${basePath}/first/img/21.png" alt="">
            </div>
            <!-- 线条 -->
            <div class="part top_Xial slideUp" data-delay="400"></div>
            <div class="part top_Xial top_Xial_b slideDown" data-delay="400"></div>
            <!--hua  金花  -->
            <div class="part fadeIn jinse_hua" data-delay="1000"> </div>
            <div class="part fadeIn jinse_hua jinse_hua_x" data-delay="1000"></div> 
            <!-- 叶子 --> 
            <div class="part slideLeft yezi_zuo"></div>
            <div class="part slideLeft yezi_zuo yezi_zuo_zq"></div>
            <div class="part slideDown yezi_zuo_xx"></div>
            <div class="part slideDown yezi_zuo_yrx"></div>
            <div class="part zoomIn piao_gif"  data-delay="1500" style="    width: 100%;
            height: 100%;
            top: 0;">
                <img src="${basePath}/first/img/jingy.gif" alt="">
            </div>
            <!-- 中间 -->
            <div class="part fadeIn kuang_nieg" data-delay="800">
                <img src="${basePath}/first/img/19.png" alt="">
            </div>
            <div class="part fadeIn kuang_nieg" data-delay="1000">
                <img class="nie" src="${basePath}/first/img/19.png" alt="">
            </div>
            <!-- 中间叶子 -->
            <div class="part fadeIn yezi_zhongk" data-delay="1300"></div>
            <div class="part fadeIn yezi_zhongk_x" data-delay="1300"></div>
            <!-- 字 -->
            <div class="part zhongjiek_Ge rentx_kuang" data-delay="1800">
                <div class="rentx">
                    <img src="${basePath}/first/img/xinniang${mediumRandomNum}.jpg" alt="">
                </div>
            </div> 
            <!-- 文字 -->
            <div class="part xinniang_xinlang" data-delay="1800"   >
                <p>新娘：罗俊雅</p>
                <dd style="    line-height: .4rem; padding-top:.2rem;">全世界都知道，我不在乎付出多少<br>我想这是幸福的写照</dd>
                <!-- <span>You two are the prince and princess in a fairy tale and may you live happily everafter!</span> -->
            </div>
            <!--  --> 
            <!-- 修改 -->
            <#--<div class="xiugai_nige" style="    left: .3rem;top: 5.8rem;"></div>-->
            <!--  -->
        </div>
        <!--  -->
        <div class="item item4">
            <span class="topArrow"></span>
            <div class="part rollInLeft shang_img"> 
                <img src="${basePath}/first/img/2.png" alt="">
            </div>
            <div class="part rollInRight shang_img shang_img_b">
                <img src="${basePath}/first/img/21.png" alt="">
            </div>
            <!-- 线条 -->
            <div class="part top_Xial slideUp" data-delay="400"></div>
            <div class="part top_Xial top_Xial_b slideDown" data-delay="400"></div>
            <!--hua  金花  -->
            <div class="part fadeIn jinse_hua jinse_hua_no" data-delay="1000"> </div>
            <div class="part fadeIn jinse_hua jinse_hua_x jinse_hua_no" data-delay="1000"></div> 
            <!-- 叶子 --> 
            <div class="part slideLeft yezi_zuo"></div>
            <div class="part slideLeft yezi_zuo yezi_zuo_zq"></div>
            <div class="part slideDown yezi_zuo_xx"></div>
            <div class="part slideDown yezi_zuo_yrx"></div>
       
            <!-- 中间叶子 -->
            <div class="part fadeIn yezi_zhongk yezi_zhongk_dier" data-delay="1300"></div> 
          
            <!-- h婚纱照 --> 
            <div class="part nimen_img slideDown"  data-delay="1800">
                <img src="${basePath}/first/img/ours${mediumRandomNum}.jpg" alt="">
            </div>
            <!-- 文字 -->
            <div class="part xinniang_xinlang" data-delay="1800"   >
                <!-- <p>新娘：罗俊雅</p> -->
                <dd style="line-height: .4rem; padding-top:.2rem;">深爱之人藏心不挂嘴<br>久念之人在梦不在眼</dd>
                <!-- <span>You two are the prince and princess in a fairy tale and may you live happily everafter!</span> -->
            </div>
        </div>
        <!--  -->
<#--        <div class="item item1">
            <!-- 金色 &ndash;&gt;
            <div class="part rollInLeft shang_img">
                <img src="${basePath}/first/img/2.png" alt="">
            </div>
            <div class="part rollInRight shang_img shang_img_b">
                <img src="${basePath}/first/img/21.png" alt="">
            </div>
            <!-- 线条 &ndash;&gt;
            <div class="part top_Xial slideUp" data-delay="400"></div>
            <div class="part top_Xial top_Xial_b slideDown" data-delay="400"></div>
            <!--hua  金花  &ndash;&gt;
            <div class="part fadeIn jinse_hua" data-delay="1000"> </div>
            <div class="part fadeIn jinse_hua jinse_hua_x" data-delay="1000"></div>
            <!-- 叶子 &ndash;&gt;
            <div class="part slideLeft yezi_zuo"></div>
            <div class="part slideLeft yezi_zuo yezi_zuo_zq"></div>
            <div class="part slideDown yezi_zuo_xx"></div>
            <div class="part slideDown yezi_zuo_yrx"></div>
            <div class="part zoomIn piao_gif"  data-delay="1500">
                <img src="${basePath}/first/img/jingy.gif" alt="">
            </div>
            <!-- 中间 &ndash;&gt;
            <div class="part fadeIn kuang_nieg" data-delay="800">
                <img src="${basePath}/first/img/19.png" alt="">
            </div>
            <div class="part fadeIn kuang_nieg" data-delay="1000">
                <img class="nie" src="${basePath}/first/img/19.png" alt="">
            </div>
            <!-- 中间叶子 &ndash;&gt;
            <div class="part fadeIn yezi_zhongk" data-delay="1300"></div>
            <div class="part fadeIn yezi_zhongk_x" data-delay="1300"></div>
            <!-- 字 &ndash;&gt;
            <div class="part zhongjiek_Ge" data-delay="1800">
                <img src="${basePath}/first/img/9.png" alt="">
            </div>
            <div class="part zhongjiek_Ge" data-delay="1800">
                <img class="nio" src="${basePath}/first/img/1.png" alt="">
            </div>
            <!-- 文字 &ndash;&gt;
            <div class="part xinniang_xinlang" data-delay="1800">
                <p>Mr.志明<b>&</b>春娇.Miss</p>
                <dd>2020/05/20</dd>
                <span>You two are the prince and princess in a fairy tale and may you live happily everafter!</span>
            </div>
            <!-- 修改 &ndash;&gt;
            <!-- <div class="xiugai_nige"></div> &ndash;&gt;

            <!--  &ndash;&gt;
            <!-- <div class="part part1 fadeIn" data-delay="1000"> </div>
            <div class="part part2 fadeIn"></div> &ndash;&gt;
        </div>-->
    </div>
    <script type="text/javascript" src="${basePath}/first/js/index_Zepto.js"></script>
    <script type="text/javascript" src="${basePath}/first/js/H5FullscreenPage.js"></script>
    <script type="text/javascript">

    H5FullscreenPage.init({'type':2,'pageShow' : function(dom){
        },'pageHide' : function(dom){
            //console.log(dom);
    }});
    </script>
</body>
</html>