<#assign basePath = request.contextPath />
<!DOCTYPE html>
<html class="x-admin-sm" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>邀请管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="${basePath}/second/css/font.css">
    <link rel="stylesheet" href="${basePath}/second/css/xadmin.css">
    <script type="text/javascript" src="${basePath}/second/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${basePath}/second/ie9/jquery.min.js"></script>
    <script type="text/javascript" src="${basePath}/second/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <script src="${basePath}/second/ie9/html5.min.js"></script>
    <script src="${basePath}/second/ie9/respond.min.js"></script>
<body>
<form class="layui-form">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">姓名</label>
            <div class="layui-input-inline">
                <input type="text" name="remark" value="${data.remark!''}" lay-verify=""  autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">内容</label>
            <div class="layui-input-inline"  >
                <input type="text" name="content" value="${data.content!''}" lay-verify=""  autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">路径</label>
            <div class="layui-input-inline"  >
                <input type="text" name="path" value="${data.path!''}" lay-verify=""  autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">归属</label>
            <div class="layui-input-inline"  >
                <select name="belong" id="belong" lay-search>
                    <option value="">直接选择或搜索归属信息</option>
                    <option value="zxm"  ${((data.belong!'') == 'zxm')?string("selected","")}>zxm</option>
                    <option value="lxy" ${((data.belong!'') == 'lxy')?string("selected","")}>lxy</option>
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline"  >
                <select name="state" id="state" lay-search>
                    <option value="">直接选择或搜索状态信息</option>
                    <option value="0" ${((data.state!'') == '0')?string("selected","")}>未发送</option>
                    <option value="1" ${((data.state!'') == '1')?string("selected","")}>已发送</option>
                    <option value="2" ${((data.state!'') == '2')?string("selected","")}>不到场</option>
                    <option value="3" ${((data.state!'') == '3')?string("selected","")}>新干-1号住宿</option>
                    <option value="4" ${((data.state!'') == '4')?string("selected","")}>新干-2号住宿</option>
                    <option value="5" ${((data.state!'') == '5')?string("selected","")}>新干-1号2号住宿</option>
                    <option value="6" ${((data.state!'') == '6')?string("selected","")}>新干-不住宿</option>
                    <option value="7" ${((data.state!'') == '7')?string("selected","")}>龙南-3号住宿</option>
                    <option value="8" ${((data.state!'') == '8')?string("selected","")}>龙南-4号住宿</option>
                    <option value="9" ${((data.state!'') == '9')?string("selected","")}>龙南-3-4号住宿</option>
                    <option value="10" ${((data.state!'') == '10')?string("selected","")}>龙南-不住宿</option>
                </select>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="${data.id!-1}" lay-verify=""  autocomplete="off" class="layui-input">
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit  type="button" lay-filter="add">立即提交</button>
        </div>
    </div>
</form>

<script>
    //自定义日期格式
    layui.use(['form', 'layer', 'jquery','table'], function () {
        $ = layui.jquery;
        var form = layui.form, layer = layui.layer;
        //监听提交
        form.on('submit(add)', function (data) {
            let remark = data.field.remark;
            let content = data.field.content;
            let path = data.field.path;
            let belong = data.field.belong;
            let state = data.field.state;
            let id = data.field.id
            let url = '/invitation/update';
/*            if (!manufacturer) {
                layer.msg("请选择厂商！");
                return;
            }*/
            const params = JSON.stringify({
                "remark": remark,
                "content": content,
                "path": path,
                "belong": belong,
                "state": state,
                "id": id
            });
            $.post({
                url: '${basePath}' + url,
                contentType: 'application/json',
                data: params,
                success:function(req){
                    console.log(req.code)
                    if(req.code === 0){
                        layer.alert("修改成功", { icon: 6 }, function () {
                            //关闭当前frame
                            xadmin.close();
                            // 可以对父窗口进行刷新
                            xadmin.father_reload();
                        });
                        return false;
                    } else {
                        layer.msg(req.msg);
                        return;
                    }
                },
                error:function(){
                    layer.msg("操作失败");
                    return;
                }
            });
        });
    });

</script>
</body>
</html>