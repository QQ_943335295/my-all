<#assign basePath = request.contextPath />
<!DOCTYPE html>
<html class="x-admin-sm" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>邀请管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="${basePath}/second/css/font.css">
    <link rel="stylesheet" href="${basePath}/second/css/xadmin.css">
    <script type="text/javascript" src="${basePath}/second/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${basePath}/second/ie9/jquery.min.js"></script>
    <script type="text/javascript" src="${basePath}/second/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <script src="${basePath}/second/ie9/html5.min.js"></script>
    <script src="${basePath}/second/ie9/respond.min.js"></script>
<body>
<form class="layui-form">
    <table class="layui-table">
        <colgroup>
            <col width="150">
            <col width="200">
            <col>
        </colgroup>
        <thead>
        <tr>
            <th>路径</th>
            <th>重复数量</th>
        </tr>
        </thead>
        <tbody>
        <#list list as item>
            <tr>
                <td>${item.path!''}</td>
                <td>${item.num!-1}</td>
            </tr>
        </#list>
        </tbody>
        <tr>
            <th>状态</th>
            <th>数量</th>
        </tr>
        </thead>
        <tbody>
        <#list reportList as item>
            <tr>
                <td>
                    <#if item.state == 1>
                        已发送
                    <#elseif item.state == 2>
                        不到场
                    <#elseif item.state == 3>
                        新干-1号住宿
                    <#elseif item.state == 4>
                        新干-2号住宿
                    <#elseif item.state == 5>
                        新干-1号2号住宿
                    <#elseif item.state == 6>
                        新干-不住宿
                    <#elseif item.state == 7>
                        龙南-3号住宿
                    <#elseif item.state == 8>
                        龙南-4号住宿
                    <#elseif item.state == 9>
                        龙南-3-4号住宿
                    <#elseif item.state == 10>
                        龙南-不住宿
                    <#else>
                        未发送
                    </#if>
                </td>
                <td>${item.num!-1}</td>
            </tr>
        </#list>
        </tbody>
    </table>
</form>

<script>
</script>
</body>
</html>
