<#assign basePath = request.contextPath />
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>邀请管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="${basePath}/second/css/font.css">
    <link rel="stylesheet" href="${basePath}/second/css/xadmin.css">
    <script src="${basePath}/second/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${basePath}/second/js/xadmin.js"></script>
    <script src="${basePath}/second/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${basePath}/second/js/xadmin.js"></script>
</head>
<body>
<div class="x-nav">
    <#--    <span class="layui-breadcrumb">-->
    <#--        <a href="">首页</a>-->
    <#--        <a href="">集中器管理</a>-->
    <#--        <a href=""><cite>集中器关联</cite></a>-->
    <#--&lt;#&ndash;        <a href="">表计管理</a>&ndash;&gt;-->
    <#--&lt;#&ndash;        <a><cite>表计模板管理</cite></a>&ndash;&gt;-->
    <#--    </span>-->
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"
       onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
    </a>
</div>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body ">
                    <form class="layui-form layui-col-space5">
                        姓名：
                        <div class="layui-input-inline layui-show-xs-block">
                            <input type="text" id="remark" placeholder="请输入姓名" autocomplete="off" class="layui-input">
                        </div>
                        内容：
                        <div class="layui-input-inline layui-show-xs-block">
                            <input type="text" id="content" placeholder="请输入内容" autocomplete="off" class="layui-input">
                        </div>
                        路径：
                        <div class="layui-input-inline layui-show-xs-block">
                            <input type="text" id="path" placeholder="请输入路径" autocomplete="off" class="layui-input">
                        </div>
                        归属：
                        <div class="layui-input-inline layui-show-xs-block">
                            <select name="belong" id="belong" lay-search>
                                <option value="">直接选择或搜索归属信息</option>
                                <option value="zxm">zxm</option>
                                <option value="lxy">lxy</option>
                            </select>
                        </div>
                        状态：
                        <div class="layui-input-inline layui-show-xs-block">
                            <select name="state" id="state" lay-search>
                                <option value="">直接选择或搜索状态信息</option>
                                <option value="0">未发送</option>
                                <option value="1">已发送</option>
                                <option value="2">不到场</option>
                                <option value="3">新干-1号住宿</option>
                                <option value="4">新干-2号住宿</option>
                                <option value="5">新干-1号2号住宿</option>
                                <option value="6">新干-不住宿</option>
                                <option value="7">龙南-3号住宿</option>
                                <option value="8">龙南-4号住宿</option>
                                <option value="9">龙南-3-4号住宿</option>
                                <option value="10">龙南-不住宿</option>
                            </select>
                        </div>
                        <div class="layui-input-inline layui-show-xs-block">
                            <button class="layui-btn" data-type="reload" type="button" id="active-report-search">
                                <i class="layui-icon">&#xe615;</i>
                            </button>
                        </div>
                        <div class="layui-row" style="padding-top: 15px;">
                            <div class="layui-input-inline layui-show-xs-block">
                                <button class="layui-btn" type="button" id="addTaskBtn">
                                    <i class="layui-icon">&#xe654;添加</i>
                                </button>
                                <button class="layui-btn" type="button" id="repeatItem">
                                    <i class="layui-icon">重复</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="layui-card-body layui-table-body layui-table-main">
                    <table class="layui-table layui-form" id="dataTableList" lay-filter="dataTableList">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<#--操作按钮-->
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-bg-blue layui-btn-xs" lay-event="see">修改</a>
    <a class="layui-btn layui-bg-red layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
    layui.use(['table', 'layer', 'upload'], function () {
        var laydate = layui.laydate, layer = layui.layer, $jquery = layui.jquery;
        var table = layui.table;
        //方法级渲染
        table.render({
            elem: '#dataTableList'
            , url: '${basePath}/invitation/list'
            , method: 'POST'
            //, contentType: 'application/json'
            , cols: [[{field: 'id', title: 'ID', sort: true, width: '5%'}
                , {field: 'remark', title: '姓名', width: '10%'}
                , {field: 'path', title: '路径', width: '10%'}
                , {field: 'content', title: '内容', width: '15%'}
                , {field: 'belong', title: '归属', width: '10%', templet: function (d) {
                        let r = d.belong;
                        if (r == 'zxm') {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">zxm</label>'
                        } else {
                            return '<label style="color: #FF5722;border: 1px solid #FF5722; padding: 4px 6px; border-radius: 5px">lxy</label>'
                        }
                    }}
                , {
                    field: 'state', title: '状态', width: '10%', templet: function (d) {
                        let r = d.state;
                        if (r == 1) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">已发送</label>'
                        }  else if (r == 2) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">不到场</label>'
                        } else if (r == 3) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">新干-1号住宿</label>'
                        }else if (r == 4) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">新干-2号住宿</label>'
                        }else if (r == 5) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">新干-1号2号住宿</label>'
                        }else if (r == 6) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">新干-不住宿</label>'
                        }else if (r == 7) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">龙南-3号住宿</label>'
                        }else if (r == 8) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">龙南-4号住宿</label>'
                        }else if (r == 9) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">龙南-3-4号住宿</label>'
                        }else if (r == 10) {
                            return '<label style="color: #1E9FFF;border: 1px solid #1E9FFF; padding: 4px 6px; border-radius: 5px">龙南-不住宿</label>'
                        } else {
                            return '<label style="color: #FF5722;border: 1px solid #FF5722; padding: 4px 6px; border-radius: 5px">未发送</label>'
                        }
                    }
                }
                , {field: 'url', title: '链接', width: '23%'}
                , {title: '操作', toolbar: '#barDemo', width: '15%'}
            ]]
            , page: true
        });
        var $ = layui.$, active = {
            reload: function () {
                //执行重载
                table.reload('dataTableList', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    , where: {
                        remark: $('#remark').val(),
                        path: $('#path').val(),
                        content: $('#content').val(),
                        belong: $('#belong').val(),
                        state: $('#state').val(),
                    }
                });
            }
        };
        $('#active-report-search').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        //监听行操作工具事件
        table.on('tool(dataTableList)', function (obj) {
            var data = obj.data;
            if (obj.event === 'see') {
                xadmin.open('修改', '${basePath}/invitation/update/' + data.id, 0.8, 0.75);
            }
            if (obj.event === 'del') {
                $jquery.post({
                    url: '${basePath}/invitation/delete/' + data.id,
                    success: function (req) {
                        if (req.code === 0) {
                            layer.msg("删除成功", {time: 1000}, function () {
                                //重新加载列表
                                location.reload();
                            });
                        } else {
                            layer.msg(req.msg);
                            return;
                        }
                    },
                    error: function () {
                        layer.msg("操作失败");
                        return;
                    }
                });
            }
        });
        $('#addTaskBtn').on('click', function () {
            xadmin.open('添加', '${basePath}/invitation/add', 0.8, 0.75);
        });

        $('#repeatItem').on('click', function () {
            xadmin.open('重复', '${basePath}/invitation/repeat', 0.8, 0.75);
        });
    });
</script>

</html>