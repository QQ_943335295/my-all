package com.my.love.service;

import com.my.love.domain.InvitationBaseUrl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *  服务类
 *
 * @author zxm
 * @since 2023-09-04
 */
public interface IInvitationBaseUrlService extends IService<InvitationBaseUrl> {

}
