package com.my.love.service.impl;

import com.my.love.controller.InvitationReport;
import com.my.love.domain.Invitation;
import com.my.love.domain.InvitationRepeat;
import com.my.love.mapper.InvitationMapper;
import com.my.love.service.IInvitationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  服务实现类
 *
 * @author zxm
 * @since 2023-08-30
 */
@Service
public class InvitationServiceImpl extends ServiceImpl<InvitationMapper, Invitation> implements IInvitationService {

    @Override
    public List<InvitationRepeat> listRepeatPath() {
        return baseMapper.listRepeatPath();
    }

    @Override
    public List<InvitationReport> stateReport() {
        return baseMapper.stateReport();
    }
}
