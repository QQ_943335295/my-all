package com.my.love.service;

import com.my.love.controller.InvitationReport;
import com.my.love.domain.Invitation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.my.love.domain.InvitationRepeat;

import java.util.List;

/**
 *  服务类
 *
 * @author zxm
 * @since 2023-08-30
 */
public interface IInvitationService extends IService<Invitation> {

    List<InvitationRepeat> listRepeatPath();

    List<InvitationReport> stateReport();
}
