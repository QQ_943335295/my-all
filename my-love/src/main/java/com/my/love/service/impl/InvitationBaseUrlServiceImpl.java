package com.my.love.service.impl;

import com.my.love.domain.InvitationBaseUrl;
import com.my.love.mapper.InvitationBaseUrlMapper;
import com.my.love.service.IInvitationBaseUrlService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 *  服务实现类
 *
 * @author zxm
 * @since 2023-09-04
 */
@Service
public class InvitationBaseUrlServiceImpl extends ServiceImpl<InvitationBaseUrlMapper, InvitationBaseUrl> implements IInvitationBaseUrlService {

}
