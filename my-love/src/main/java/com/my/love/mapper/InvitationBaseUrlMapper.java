package com.my.love.mapper;

import com.my.love.domain.InvitationBaseUrl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper接口
 *
 * @author zxm
 * @since 2023-09-04
 */
public interface InvitationBaseUrlMapper extends BaseMapper<InvitationBaseUrl> {

}
