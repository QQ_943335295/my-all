package com.my.love.mapper;

import com.my.love.controller.InvitationReport;
import com.my.love.domain.Invitation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.my.love.domain.InvitationRepeat;

import java.util.List;

/**
 *  Mapper接口
 *
 * @author zxm
 * @since 2023-08-30
 */
public interface InvitationMapper extends BaseMapper<Invitation> {

    List<InvitationRepeat> listRepeatPath();

    List<InvitationReport> stateReport();
}
