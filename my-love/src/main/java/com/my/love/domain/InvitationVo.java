package com.my.love.domain;

import lombok.Data;

/**
 * 
 *
 * @author zxm
 * @since 2023-08-30
 */
@Data
public class InvitationVo extends Invitation {

    private static final long serialVersionUID = 1L;

    private String url;

}
