package com.my.love.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 表格分页数据对象
 *
 * @author lapis
 */
public class TableDataInfo implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     * 消息状态码
     */
    private int code;

    /**
     * 消息内容
     */
    private String msg;

    /**
     * 总记录数
     */
    private long count;

    /**
     * 列表数据
     */
    private List<?> data;


    /**
     * 表格数据对象
     */
    public TableDataInfo() {
    }

    /**
     * 分页
     * @param data  列表数据
     * @param count 总记录数
     */
    public TableDataInfo(long count, List<?> data) {
        this.count = count;
        this.data = data;
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}