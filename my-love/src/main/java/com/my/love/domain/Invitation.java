package com.my.love.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.my.love.bean.BaseEntity;
import lombok.Data;

/**
 * 
 *
 * @author zxm
 * @since 2023-08-30
 */
@Data
public class Invitation extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 映射路径
     */
    private String path;

    /**
     * 内容
     */
    private String content;

    /**
     * 状态：1-发送 0-未发送
     */
    private String state;

    /**
     * 归属：zxm、lxy
     */
    private String belong;

}
