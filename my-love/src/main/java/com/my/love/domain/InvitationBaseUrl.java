package com.my.love.domain;

import com.my.love.bean.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author zxm
 * @since 2023-09-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InvitationBaseUrl extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态：zxm、lxy
     */
    private String belong;
}
