package com.my.love.domain;

import lombok.Data;

/**
 * @author zxm
 * @since 2023-08-30
 */
@Data
public class InvitationRepeat {

    private String path;

    /**
     * 映射路径
     */
    private Integer num;
}
