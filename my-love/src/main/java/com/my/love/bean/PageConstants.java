package com.my.love.bean;

/**
 * 分页参数
 * @author zxm
 * @date 2023/9/4
 */
public interface PageConstants {

    long DEFAULT_CURRENT = 1;

    long DEFAULT_SIZE = 10;

}
