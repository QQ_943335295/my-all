package com.my.love.utils;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.my.love.bean.BaseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：MybatisPlus代码生成器
 *
 * @author wangyaxing
 * @date 2021/10/25
 */
public class MybatisPlusCodeGenerator {

    /** 需要修改的配置 */
    // 数据库名
    private static final String DB_NAME = "my-love";
    // 表名：可多个表批量生成
    private static final String[] TABLE_NAME = new String[] {"invitation_base_url"};
    // 是否中间表
    private static final Boolean MIDDLE_TABLE = false;
    // 代码编写者如：wangyaxing
    private static final String AUTHOR = "zxm";

    /** 固定配置 */
    // 数据源配置
    private static final String URL = "jdbc:mysql://localhost:3306/" + DB_NAME + "?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8&allowMultiQueries=true";
    private static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "lc123456";
    private static final String OUTPUT_DIR_CLASS = "D:\\project\\code\\my-all\\my-love\\src\\main\\java\\";
    private static final String OUTPUT_DIR_XML = "D:\\project\\code\\my-all\\my-love\\src\\main\\java\\com\\my\\love\\mapper\\";

    public static void main(String[] args) {
        // 创建代码生成器并使用freemarker模板
        AutoGenerator generator = new AutoGenerator();
        generator.setTemplateEngine(new FreemarkerTemplateEngine());

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(OUTPUT_DIR_CLASS);
        gc.setAuthor(AUTHOR);
        gc.setOpen(false);
        gc.setFileOverride(true);
        if (!MIDDLE_TABLE) {
            gc.setIdType(IdType.AUTO);
        }
        gc.setDateType(DateType.ONLY_DATE);
        gc.setBaseResultMap(true);
        generator.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(URL);
        dsc.setDriverName(DRIVER_NAME);
        dsc.setUsername(USERNAME);
        dsc.setPassword(PASSWORD);
        dsc.setDbType(DbType.MYSQL);
        generator.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.my.love");
        pc.setController("controller");
        pc.setEntity("domain");
        pc.setService("service");
        pc.setMapper("mapper");
        pc.setModuleName(null);
        generator.setPackageInfo(pc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude(TABLE_NAME);
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        if (!MIDDLE_TABLE) {
            strategy.setSuperEntityClass(BaseEntity.class);
            strategy.setSuperEntityColumns("create_by", "create_time", "update_by", "update_time", "del_flag");
        }
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);
        generator.setStrategy(strategy);

        // 模板配置
        TemplateConfig tc = new TemplateConfig();
        tc.setEntity("template/entity.java");
        tc.setMapper("template/mapper.java");
        tc.setXml(null);
        tc.setService(MIDDLE_TABLE ? null : "template/service.java");
        tc.setServiceImpl(MIDDLE_TABLE ? null : "template/serviceImpl.java");
        tc.setController(MIDDLE_TABLE ? null : "template/controller.java");
        generator.setTemplate(tc);

        // xml模板位置和生成路径
        if (!MIDDLE_TABLE) {
            InjectionConfig cfg = new InjectionConfig() {
                @Override
                public void initMap() { }
            };
            List<FileOutConfig> focList = new ArrayList<>();
            focList.add(new FileOutConfig("template/mapper.xml.ftl") {
                @Override
                public String outputFile(TableInfo tableInfo) {
                    return OUTPUT_DIR_XML + tableInfo.getEntityName() + "Mapper.xml";
                }
            });
            cfg.setFileOutConfigList(focList);
            generator.setCfg(cfg);
        }

        // 生成代码
        generator.execute();
    }
}
