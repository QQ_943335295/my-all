package com.my.love.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *  前端控制器
 *
 * @author zxm
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/invitationBaseUrl")
public class InvitationBaseUrlController {

}
