package com.my.love.controller;

import lombok.Data;

/**
 * @author zxm
 * @date 2023/9/8
 */
@Data
public class InvitationReport {

    private Integer state;

    private Integer num;
}
