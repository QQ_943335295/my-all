package com.my.love.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.my.love.bean.JsonData;
import com.my.love.bean.PageConstants;
import com.my.love.domain.*;
import com.my.love.service.IInvitationBaseUrlService;
import com.my.love.service.IInvitationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *  前端控制器
 *
 * @author zxm
 * @since 2023-08-30
 */
@Controller
@RequestMapping("/invitation")
public class InvitationController {

    @Autowired
    private IInvitationService invitationService;
    @Autowired
    private IInvitationBaseUrlService invitationBaseUrlService;

    @GetMapping("/lxy/{path}")
    public String lxyInvitation(@PathVariable("path") String path, HttpServletRequest request) {
        LambdaQueryWrapper<Invitation> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Invitation::getPath, path);
        Invitation one = invitationService.getOne(wrapper);
        if (one == null){
            request.setAttribute("content", "朋友");
        } else {
            request.setAttribute("content", one.getContent());
        }
        int mediumRandomNum = RandomUtil.randomInt(1, 4);
        // int mediumRandomNum = 3;
        request.setAttribute("mediumRandomNum", mediumRandomNum);
        return "first/lxy";
    }

    @GetMapping("/zxm/{path}")
    public String list(@PathVariable("path") String path, HttpServletRequest request) {
        LambdaQueryWrapper<Invitation> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Invitation::getPath, path);
        Invitation one = invitationService.getOne(wrapper);
        if (one == null){
            request.setAttribute("content", "朋友");
        } else {
            request.setAttribute("content", one.getContent());
        }
        int mediumRandomNum = RandomUtil.randomInt(1, 4);
        // int mediumRandomNum = 3;
        request.setAttribute("mediumRandomNum", mediumRandomNum);
        return "first/zxm";
    }


    @GetMapping("/list")
    public String list(HttpServletRequest request) {
        return "second/invitation/list";
    }

    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Invitation invitation, HttpServletRequest request) {
        // 分页查询列表
        LambdaQueryWrapper<Invitation> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StrUtil.isNotBlank(invitation.getRemark()), Invitation::getRemark, invitation.getRemark());
        wrapper.eq(StrUtil.isNotBlank(invitation.getPath()), Invitation::getPath, invitation.getPath());
        wrapper.eq(StrUtil.isNotBlank(invitation.getContent()), Invitation::getContent, invitation.getContent());
        wrapper.eq(StrUtil.isNotBlank(invitation.getState()), Invitation::getState, invitation.getState());
        wrapper.eq(StrUtil.isNotBlank(invitation.getBelong()), Invitation::getBelong, invitation.getBelong());
        Page<Invitation> pageInfo = new Page<>(invitation.getPage() == null ? PageConstants.DEFAULT_CURRENT : invitation.getPage(),
                invitation.getLimit() == null ? PageConstants.DEFAULT_SIZE : invitation.getLimit());
        IPage<Invitation> invitationPage =  invitationService.page(pageInfo, wrapper);
        // 获取baseUrl与归属者Map
        List<InvitationBaseUrl> baseUrls = invitationBaseUrlService.list();
        Map<String, String> baseUrlMap = baseUrls.stream().collect(Collectors.toMap(InvitationBaseUrl::getBelong, InvitationBaseUrl::getRemark));
        // 合并数据
        List<InvitationVo> list = new ArrayList<>();
        invitationPage.getRecords().forEach(item -> {
            InvitationVo vo = new InvitationVo();
            BeanUtil.copyProperties(item, vo);
            vo.setUrl(baseUrlMap.get(item.getBelong()) + item.getPath());
            list.add(vo);
        });
        return new TableDataInfo(invitationPage.getTotal(), list);
    }

    @GetMapping("/add")
    public String add(HttpServletRequest request) {
        return "second/invitation/add";
    }

    @PostMapping("/add")
    @ResponseBody
    public JsonData add(@RequestBody Invitation invitation) {
        invitation.setCreateTime(new Date());
        try {
            invitationService.save(invitation);
        } catch (Exception e){
            return JsonData.buildError("添加失败,请检查路径是否重复！");
        }
        return JsonData.buildSuccess();
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") Long id, HttpServletRequest request) {
        Invitation invitation = invitationService.getById(id);
        request.setAttribute("data", invitation);
        return "second/invitation/update";
    }

    @PostMapping("/update")
    @ResponseBody
    public JsonData update(@RequestBody Invitation invitation) {
        invitation.setUpdateTime(new Date());
        try {
            invitationService.updateById(invitation);
        } catch (Exception e){
            return JsonData.buildError("修改失败,请检查路径是否重复！");
        }
        return JsonData.buildSuccess();
    }

    @PostMapping(value = "/delete/{id}")
    @ResponseBody
    public JsonData delete(@PathVariable("id") Long id) {
        try {
            invitationService.removeById(id);
        } catch (Exception e){
            return JsonData.buildError("删除失败！");
        }
        return JsonData.buildSuccess();
    }


    @GetMapping("/repeat")
    public String repeat(HttpServletRequest request) {
        List<InvitationRepeat> invitationRepeatList = invitationService.listRepeatPath();
        request.setAttribute("list", invitationRepeatList);
        List<InvitationReport> reportList = invitationService.stateReport();
        request.setAttribute("reportList", reportList);
        return "second/invitation/repeat";
    }

}
