package com.zxm.controller;

import com.zxm.sms.service.ISmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendController {

    @Autowired
    private ISmsService smsService;

    @GetMapping("/send")
    public String send(){
        smsService.send("18370847419", "hello !");
        return "OK";
    }

}
