package com.zxm.service;

import com.zxm.sms.service.ISmsService;
import org.springframework.stereotype.Service;

@Service
public class MySmsServiceImpl implements ISmsService {
    @Override
    public void send(String phone, String msg) {
        System.out.println("my sms starter 发送短信：" + phone + "---" + msg);
    }
}
