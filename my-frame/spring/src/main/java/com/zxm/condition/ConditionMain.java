package com.zxm.condition;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

public class ConditionMain {

    /**
     * 获取全部某个类型全部对象
     */
    public static void testGetAllCondition(AnnotationConfigApplicationContext context){
        Map<String, DataSource> beansOfType = context.getBeansOfType(DataSource.class);
        System.out.println(beansOfType);
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //扫描指定的包，包括子包
        context.scan("com.zxm.condition");
        //里面完成初始化操作，核心方法
        context.refresh();
        testGetAllCondition(context);
    }


}
