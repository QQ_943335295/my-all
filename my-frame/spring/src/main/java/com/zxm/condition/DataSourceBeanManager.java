package com.zxm.condition;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceBeanManager {
    @Bean("source-1")
    public DataSource dataSource1(){
        return new DataSource("source-1","xdclass.net");
    }

    /**
     * 根据DatasourceCondition中的条件判断是否加载当前bean
     */
    @Bean("source-2")
    @Conditional({DatasourceCondition.class})
    public DataSource dataSource2(){
        return new DataSource("source-2","baidu.com");
    }

}
