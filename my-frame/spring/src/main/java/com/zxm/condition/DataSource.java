package com.zxm.condition;

public class DataSource {
    public String name;
    public String url;

    public DataSource(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
