package com.zxm.imports;

import com.zxm.domain.Black;
import com.zxm.domain.Blue;
import com.zxm.domain.Red;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(Black.class)
@Configuration
public class ImportController {

    @Bean("import_blue")
    public Blue getBlue(){
        return new Blue("import_blue");
    }

    @Bean("import_red")
    public Red getRed(){
        return new Red("import_red");
    }


}
