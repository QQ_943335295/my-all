package com.zxm.imports;

import com.zxm.domain.Color;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class ImportMain {

    /**
     * Spring内置的Processor类和Factory类不受过滤规则限制，其他Bean定义信息都注册到IOC容器中
     */
    public static void testImport(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //扫描指定的包，包括子包
        context.scan("com.zxm.imports");
        //里面完成初始化操作，核心方法
        context.refresh();
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(System.out::println);
        System.out.println("-----------------------");
        //获取容器里面全部color类的bean名称
        String[] beanNamesForType = context.getBeanNamesForType(Color.class);
        Arrays.stream(beanNamesForType).forEach(System.out::println);
    }
    public static void main(String[] args) {
        testImport();
    }

}
