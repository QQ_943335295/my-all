package com.zxm.imports;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;

@Order(100)
@Import({ColorImportBeanDefinitionRegistry.class})
@Configuration
public class ImportBeanDefinitionRegisterController {

}
