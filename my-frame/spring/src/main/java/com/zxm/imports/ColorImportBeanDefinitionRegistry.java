package com.zxm.imports;

import com.zxm.domain.Gray;
import com.zxm.domain.Yellow;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class ColorImportBeanDefinitionRegistry implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        /**
         * 可以通过该方法进行手动注册
         * 判断容器中是否有黑色的对象
         */
        boolean containsWhite = registry.containsBeanDefinition("com.zxm.domain.White");
        boolean containsWhites = registry.containsBeanDefinition("net.xdclass.color.Withes");
        if (containsWhite){
            //IOC容器加个混合对象
            BeanDefinition beanDefinition = new RootBeanDefinition(Yellow.class);
            registry.registerBeanDefinition("yellow",beanDefinition);
        }
        if (containsWhites){
            //IOC容器加个混合对象
            BeanDefinition beanDefinition = new RootBeanDefinition(Gray.class);
            registry.registerBeanDefinition("gray",beanDefinition);
        }
    }
}
