package com.zxm.imports;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;

@Order(120)
@Import({ColorImportSelector.class})
@Configuration
public class ImportSelectorController {

}
