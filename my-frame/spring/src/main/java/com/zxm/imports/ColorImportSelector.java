package com.zxm.imports;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Set;

public class ColorImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        System.out.println("-------------------------------------------------");
        Set<String> annotationTypes = importingClassMetadata.getAnnotationTypes();
        annotationTypes.stream().forEach(System.out::println);
        System.out.println("-------------------------------------------------");
        return new String[]{"com.zxm.domain.Green", "com.zxm.domain.White"};
    }
}
