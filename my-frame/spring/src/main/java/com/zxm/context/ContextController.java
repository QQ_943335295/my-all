package com.zxm.context;

import com.zxm.domain.Blue;
import com.zxm.domain.Red;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContextController {

    @Bean("blue_obj")
    public Blue getBlue(){
        return new Blue("blue_obj");
    }

    @Bean("red_obj")
    public Red getRed(){
        return new Red("red_obj");
    }


}
