package com.zxm.context;

import com.zxm.domain.Blue;
import com.zxm.domain.Color;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class ContextMain {

    /**
     * Spring内置的Processor类和Factory类不受过滤规则限制，其他Bean定义信息都注册到IOC容器中
     */
    public static void testContext(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //扫描指定的包，包括子包
        context.scan("com.zxm.context");
        //里面完成初始化操作，核心方法
        context.refresh();
        //获取获取的对象
        System.err.println("beanDefinitionNames......");
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(System.err::println);
        //获取指定类型的对象
        System.err.println("beanNamesForType......");
        String[] beanNamesForType = context.getBeanNamesForType(Color.class);
        Arrays.stream(beanNamesForType).forEach(System.err::println);
        System.err.println("blueBean......");
        String[] blueBean = context.getBeanNamesForType(Blue.class);
        Arrays.stream(blueBean).forEach(System.err::println);
    }
    public static void main(String[] args) {
        testContext();
    }

}
