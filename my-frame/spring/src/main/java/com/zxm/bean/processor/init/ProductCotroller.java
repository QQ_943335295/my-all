package com.zxm.bean.processor.init;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

@Configuration
public class ProductCotroller {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public MyProductService myProductService(){
        return new MyProductService();
    }


}
