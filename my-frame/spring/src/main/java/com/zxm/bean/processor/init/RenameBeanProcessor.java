package com.zxm.bean.processor.init;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RenameBeanProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof  MyProductService){
            System.err.println("postProcessBeforeInitialization ....");
        }
        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof  MyProductService){
            System.err.println("postProcessAfterInitialization ....");
        }
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
