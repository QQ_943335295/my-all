package com.zxm.bean.processor.init;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BeanProcessorInitMain {


    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //扫描指定的包，包括子包
        context.scan("com.zxm.bean.processor.init");
        //里面完成初始化操作，核心方法
        context.refresh();

        // MyProductService myProductService = (MyProductService) context.getBean("myProductService");
    }

}
