package com.zxm.bean.processor.init;

import org.springframework.beans.factory.InitializingBean;

public class MyProductService implements InitializingBean {

    private String name;

    public MyProductService() {
        System.err.println("MyProductService constructor ...");
        this.name = "MyProductService";
    }

    public void myProductServiceMethod(){
        System.err.println("MyProductService Method ...");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.err.println("MyProductService afterPropertiesSet ...");
    }

    private void init() {
        System.err.println("MyProductService init ...");
    }

    private void destroy() {
        System.err.println("MyProductService destroy ...");
    }
}
