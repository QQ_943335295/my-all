package com.zxm.factory.bean.processor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class LogInvokerProxy implements InvocationHandler {

    private Object target;

    public LogInvokerProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.err.println("start log ...");
        Object invoke = method.invoke(target, args);
        System.err.println("end log ...");
        return invoke;
    }
}
