package com.zxm.factory.bean.processor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class FactoryBeanMain {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //扫描指定的包，包括子包
        context.scan("com.zxm.factory.bean.processor");
        //里面完成初始化操作，核心方法
        context.refresh();
        ICar myCar = (ICar) context.getBean("myCar");
        myCar.run();
        myCar.bi();

        ICar audiCar = (ICar) context.getBean("audiCar");
        audiCar.run();
        audiCar.bi();
    }

}
