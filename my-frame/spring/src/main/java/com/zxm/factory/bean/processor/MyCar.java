package com.zxm.factory.bean.processor;

import org.springframework.stereotype.Service;

@Service
public class MyCar implements ICar{
    @Override
    public void run() {
        System.err.println("run ...");
    }

    @Override
    public void bi() {
        System.err.println("bi ...");
    }

    public MyCar() {
    }

}
