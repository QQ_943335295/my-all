package com.zxm.factory.bean.processor;

import org.springframework.stereotype.Service;

@Service
public class AudiCar implements ICar{
    @Override
    public void run() {
        System.err.println("ad run ...");
    }

    @Override
    public void bi() {
        System.err.println("ad bi ...");
    }
}
