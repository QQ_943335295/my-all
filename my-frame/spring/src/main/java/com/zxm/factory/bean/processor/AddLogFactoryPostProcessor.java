package com.zxm.factory.bean.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.stereotype.Component;

@Component
public class AddLogFactoryPostProcessor implements BeanFactoryPostProcessor {
    @lombok.SneakyThrows
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        String[] beanDefinitionNames = configurableListableBeanFactory.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames){
            // 获取对应的bean并判断类型
            Object bean = configurableListableBeanFactory.getBean(beanDefinitionName);
            if (bean instanceof ICar){
                BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(AddLogFactoryBean.class);
                builder.setInitMethodName("init");
                builder.addPropertyValue("target", bean.getClass().newInstance());
                builder.addPropertyValue("interfaceClass", ICar.class);
                // 通过getBeanDefinition方法获取BeanDefinition对象。
                BeanDefinition beanDefinition = builder.getBeanDefinition();
                BeanDefinitionRegistry registry = (BeanDefinitionRegistry) configurableListableBeanFactory;
                registry.removeBeanDefinition(beanDefinitionName);
                registry.registerBeanDefinition(beanDefinitionName, beanDefinition);
            }
        }
    }

}
