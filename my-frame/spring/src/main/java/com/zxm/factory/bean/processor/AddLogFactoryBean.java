package com.zxm.factory.bean.processor;

import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Proxy;

public class AddLogFactoryBean implements FactoryBean<Object> {

    private Class<?> interfaceClass;

    private Object object;

    private Object target;

    @Override
    public Object getObject() {
        return object;
    }

    public void init(){
        this.object = Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass},
                new LogInvokerProxy(target));
    }

    @Override
    public Class<?> getObjectType() {
        return interfaceClass;
    }

    public Class<?> getInterfaceClass() {
        return interfaceClass;
    }

    public void setInterfaceClass(Class<?> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }
}
