package com.zxm.sms.service;

public interface ISmsService {

    /**
     * 发送短信
     * @param phone
     * @param msg
     * @return
     */
    void send(String phone,String msg);
}
