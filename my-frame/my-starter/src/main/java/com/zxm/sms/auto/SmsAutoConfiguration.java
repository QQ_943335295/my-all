package com.zxm.sms.auto;

import com.zxm.sms.config.SmsProperties;
import com.zxm.sms.service.ISmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;


@AutoConfiguration
//类路径需要存在SmsService才能被spring装载
@ConditionalOnClass(ISmsService.class)
//@EnableConfigurationProperties负责导入这个SmsProperties已经绑定了属性的bean到spring容器中
@EnableConfigurationProperties(value = SmsProperties.class)
public class SmsAutoConfiguration {

    @Autowired
    private SmsProperties smsProperties;
    @Bean
    //容器里面缺少这个bean才会被执行
    @ConditionalOnMissingBean
    public ISmsService smsService() {
        System.out.println("——————————————自定义starter启动——————————————");
        return (phone, msg) -> {
            System.out.println("默认 sms starter 发送短信：" + phone + "---" + msg + "---"  + smsProperties.getTemplateId());
        };
    }

}
