package com.zxm.sms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 通过 @ConfigurationProperties("xdclass.sms") 注解可以绑定application.yml配置文件
 */
//@EnableConfigurationProperties负责导入这个SmsProperties已经绑定了属性的bean到spring容器中
@ConfigurationProperties(prefix = "zxm.sms")
public class SmsProperties {
    private String templateId;

    private String secret;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
