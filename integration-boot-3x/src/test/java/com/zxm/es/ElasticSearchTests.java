package com.zxm.es;

import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.aggregations.BucketSortAggregation;
import co.elastic.clients.elasticsearch._types.aggregations.Buckets;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import com.zxm.domain.VideoDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchAggregation;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchAggregations;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.core.query.StringQuery;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * ElasticSearch测试类
 *
 * @author zxm
 * @date 2023/12/13
 * <p>
 * * CriteriaQuery
 * * 创建Criteria来搜索数据，而无需了解 Elasticsearch 查询的语法或基础知识
 * * 允许用户通过简单地连接和组合，指定搜索文档必须满足的对象来构建查询
 * * StringQuery
 * * 将Elasticsearch查询作为JSON字符串，更适合对Elasticsearch查询的语法比较了解的人
 * * 也更方便使用kibana或postman等客户端工具行进调试
 * * NativeQuery
 * * 复杂查询或无法使用CriteriaAPI 表达的查询时使用的类，例如在构建查询和使用聚合的场景
 */
@SpringBootTest
class ElasticSearchTests {

    @Autowired
    private ElasticsearchTemplate restTemplate;

    /**
     * 判断索引是否存在索引
     */
    @Test
    void existsIndex() {
        IndexOperations indexOperations = restTemplate.indexOps(VideoDTO.class);
        boolean exists = indexOperations.exists();
        System.out.println(exists);
    }

    /**
     * 创建索引
     */
    @Test
    void createIndex() {
        // spring data es所有索引操作都在这个接口
        IndexOperations indexOperations = restTemplate.indexOps(VideoDTO.class);
        // 是否存在，存在则删除
        if (indexOperations.exists()) {
            indexOperations.delete();
        }
        // 创建索引
        indexOperations.create();
        //设置映射: 在正式开发中，几乎不会使用框架创建索引或设置映射，这是架构或者管理员的工作，不适合使用代码实现
        restTemplate.indexOps(VideoDTO.class).putMapping();
    }

    /**
     * 删除索引
     */
    @Test
    void deleteIndex() {
        IndexOperations indexOperations = restTemplate.indexOps(VideoDTO.class);
        boolean delete = indexOperations.delete();
        System.out.println(delete);
    }

    /**
     * 插入文档
     */
    @Test
    void insert() {
        VideoDTO videoDTO = new VideoDTO();
        videoDTO.setId(1L);
        videoDTO.setTitle("小滴课堂架构大课和Spring Cloud");
        videoDTO.setCreateTime(LocalDateTime.now());
        videoDTO.setDuration(100);
        videoDTO.setCategory("后端");
        videoDTO.setDescription("这个是综合大型课程，包括了jvm，redis，新版spring boot3.x，架构，监控，性能优化，算法，高并发等多方面内容");
        VideoDTO video = restTemplate.save(videoDTO);
        System.err.println(video);
    }

    /**
     * 更新文档
     */
    @Test
    void update() {
        VideoDTO videoDTO = new VideoDTO();
        videoDTO.setId(1L);
        videoDTO.setTitle("小滴课堂架构大课和Spring Cloud V2");
        videoDTO.setCreateTime(LocalDateTime.now());
        videoDTO.setDuration(102);
        videoDTO.setCategory("后端");
        videoDTO.setDescription("这个是综合大型课程，包括了jvm，redis，新版spring boot3.x，架构，监控，性能优化，算法，高并发等多方面内容");
        VideoDTO saved = restTemplate.save(videoDTO);
        System.out.println(saved);
    }

    /**
     * 批量插入
     */
    @Test
    void batchInsert() {
        List<VideoDTO> list = new ArrayList<>();
        list.add(new VideoDTO(2L, "老王录制的按摩课程", "主要按摩和会所推荐", 123, "后端"));
        list.add(new VideoDTO(3L, "冰冰的前端性能优化", "前端高手系列", 100042, "前端"));
        list.add(new VideoDTO(4L, "海量数据项目大课", "D哥的后端+大数据综合课程", 5432345, "后端"));
        list.add(new VideoDTO(5L, "小滴课堂永久会员", "可以看海量专题课程，IT技术持续充电平台", 6542, "后端"));
        list.add(new VideoDTO(6L, "大钊-前端低代码平台", "高效开发底层基础平台，效能平台案例", 53422, "前端"));
        list.add(new VideoDTO(7L, "自动化测试平台大课", "微服务架构下的spring cloud架构大课，包括jvm,效能平台", 6542, "后端"));
        Iterable<VideoDTO> result = restTemplate.save(list);
        System.out.println(result);
    }

    /**
     * 根据主键查询
     */
    @Test
    void searchById() {
        VideoDTO videoDTO = restTemplate.get("6", VideoDTO.class);
        // 检查表达式是否true，如果为false，抛出一个AssertionError异常。
        assert videoDTO != null;
        System.out.println(videoDTO);
    }

    /**
     * 根据主键删除
     */
    @Test
    void deleteById() {
        String delete = restTemplate.delete("4", VideoDTO.class);
        System.out.println(delete);
    }

    /**
     * 查询全部
     */
    @Test
    void listAll() {
        SearchHits<VideoDTO> search = restTemplate.search(Query.findAll(), VideoDTO.class);
        List<SearchHit<VideoDTO>> searchHits = search.getSearchHits();
        List<VideoDTO> list = new ArrayList<>();
        searchHits.forEach(item -> list.add(item.getContent()));
        System.err.println(list);
    }

    /* 复杂查询或无法使用CriteriaAPI 表达的查询时使用的类，例如在构建查询和使用聚合的场景 */
    /**
     * match查询
     */
    @Test
    void matchQuery() {
        Query query = NativeQuery.builder().withQuery(q -> q
                .match(m -> m.field("description") //字段
                        .query("spring") //值
                )).build();
        SearchHits<VideoDTO> searchHits = restTemplate.search(query, VideoDTO.class);

        // 获得searchHits,进行遍历得到content
        List<VideoDTO> videoDTOS = new ArrayList<>();
        searchHits.forEach(hit -> videoDTOS.add(hit.getContent()));
        System.out.println(videoDTOS);
    }

    /**
     * 分页查询
     */
    @Test
    void pageSearch() {
        // 从0开始算分页的
        Query query = NativeQuery.builder().withPageable(Pageable.ofSize(2).withPage(1)).build();
        SearchHits<VideoDTO> searchHits = restTemplate.search(query, VideoDTO.class);
        // 获得searchHits,进行遍历得到content
        List<VideoDTO> videoDTOS = new ArrayList<>();
        searchHits.forEach(hit -> videoDTOS.add(hit.getContent()));
        System.out.println(videoDTOS);
    }


    /**
     * 排序查询
     */
    @Test
    void sortSearch(){
        Query query = NativeQuery
                .builder()
                .withPageable(Pageable.ofSize(2).withPage(0))
                .withSort(Sort.by("id").descending())
                .build();
        // 降序：descending  顺序：ascending
        SearchHits<VideoDTO> searchHits = restTemplate.search(query, VideoDTO.class);
        // 获得searchHits,进行遍历得到content
        List<VideoDTO> videoDTOS = new ArrayList<>();
        searchHits.forEach(hit -> videoDTOS.add(hit.getContent()));
        System.out.println(videoDTOS);
    }

    /* 将Elasticsearch查询作为JSON字符串，更适合对Elasticsearch查询的语法比较了解的人 */
    @Test
    void stringQuery(){
        String elasticSql = """
                {
                    "bool":{
                        "must":[
                            {
                                "match":{
                                    "description":"jvm"
                                }
                            },
                            {
                                "term":{
                                    "category":{
                                        "value":"后端"
                                    }
                                }
                            },
                            {
                                "range":{
                                    "duration":{
                                        "gte":0,
                                        "lte":150
                                    }
                                }
                            }
                        ]
                    }
                }
                """;
//        String elasticSql = """
//                   {"bool":{"must":[{"match":{"title":"架构"}},{"match":{"description":"spring"}},{"range":{"duration":{"gte":10,"lte":6000}}}]}}
//                """;
        Query query = new StringQuery(elasticSql);
        SearchHits<VideoDTO> search = restTemplate.search(query, VideoDTO.class);
        List<SearchHit<VideoDTO>> searchHits = search.getSearchHits();
        // 获得searchHits,进行遍历得到content
        List<VideoDTO> videoDTOS = new ArrayList<>();
        searchHits.forEach(hit -> videoDTOS.add(hit.getContent()));
        System.out.println(videoDTOS);
    }

    /**
     * 聚合查询
     * 方案一：可以使用原始DSL进行处理
     * 方案二：使用NativeQuery完成聚合搜索
     */
    @Test
    void aggSearch(){
        // 统计不同分类下的视频数量
        Query query = NativeQuery.builder()
                .withAggregation("category_group", Aggregation.of(a -> a
                        .terms(ta -> ta.field("category").size(2))))
                .build();
        SearchHits<VideoDTO> searchHits = restTemplate.search(query, VideoDTO.class);
        //获取聚合数据
        ElasticsearchAggregations aggregationsContainer = (ElasticsearchAggregations) searchHits.getAggregations();
        Map<String, ElasticsearchAggregation> aggregations = Objects.requireNonNull(aggregationsContainer).aggregationsAsMap();
        //获取对应名称的聚合
        ElasticsearchAggregation aggregation = aggregations.get("category_group");
        Buckets<StringTermsBucket> buckets = aggregation.aggregation().getAggregate().sterms().buckets();
        //打印聚合信息
        buckets.array().forEach(bucket -> {
            System.out.println("组名："+bucket.key().stringValue() + ", 值" + bucket.docCount());
        });
        // 获得searchHits,进行遍历得到content
        List<VideoDTO> videoDTOS = new ArrayList<>();
        searchHits.forEach(hit -> videoDTOS.add(hit.getContent()));
        System.out.println(videoDTOS);
    }


}
