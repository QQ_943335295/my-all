package com.zxm.bean;

/**
 * 数字常量
 * @author zxm
 * @date 2022/8/28 18:10
 */
public interface NumberConstants {
    /**
     * -1
     */
    int NEGATIVE = -1;

    /**
     * 0
     */
    int ZERO = 0;

}
