package com.zxm.bean;

import lombok.Getter;

/**
 * 信息提示码
 * 比如 商品服务210,购物车是220、用户服务230，403代表权限
 * @author zxm
 * @date 2022/8/20 21:40
 */
public enum BizCodeEnum {


    /** 基本错误 */
    OPERATION_FAIL(10001,"操作失败！"),
    /** 数据库错误 */
    DB_ROUTE_NOT_FOUND(20001,"数据库找不到"),
    /** MQ消费异常 */
    MQ_CONSUME_EXCEPTION(900101,"消费者消费异常");



    @Getter
    private String message;

    @Getter
    private int code;

    BizCodeEnum(int code, String message){
        this.code = code;
        this.message = message;
    }
}
