package com.zxm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.zxm.boot.mapper.*")
@SpringBootApplication
public class IntegrationBoot3xApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegrationBoot3xApplication.class, args);
    }

}
