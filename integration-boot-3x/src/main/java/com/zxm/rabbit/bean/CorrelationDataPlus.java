package com.zxm.rabbit.bean;

import org.springframework.amqp.rabbit.connection.CorrelationData;

/**
 * 回调增强类
 * @author zxm
 * @date 2024/1/9
 */
public class CorrelationDataPlus extends CorrelationData {
    private Object message;
    private String exchange;
    private String routingKey;

    public void setMessage(Object message) {
        this.message = message;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }
}
