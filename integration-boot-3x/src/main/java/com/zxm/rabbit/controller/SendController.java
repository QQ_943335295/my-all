package com.zxm.rabbit.controller;

import com.alibaba.fastjson.JSONObject;
import com.zxm.bean.JsonData;
import com.zxm.rabbit.bean.CorrelationDataPlus;
import com.zxm.rabbit.config.RabbitMqConfig;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发送控制器
 * @author zxm
 * @date 2023/12/19
 */
@RestController
@RequestMapping("rabbit")
public class SendController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 普通集成测试
     */
    @RequestMapping("/integration/{message}")
    public JsonData integration(@PathVariable("message") String message){
        rabbitTemplate.convertAndSend(RabbitMqConfig.INTEGRATION_EXCHANGE, RabbitMqConfig.INTEGRATION_ROUTE_KEY, message);
        return JsonData.buildSuccess();
    }

    /**
     * 消息投递到交换机回调：
     * rabbitTemplate是单例模式，因此所有消息投递后，都会回调到该回调函数钟
     * 并且，只能对该单例模式的rabbitTemplate设置一个回调，否则会报错
     */
    @RequestMapping("/integration/callback/{message}")
    public JsonData integrationCallback(@PathVariable("message") String message){
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             * CorrelationData：配置信息
             * acK：成功true 错误false
             * cause：错误原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean acK, String cause) {
                System.out.println("integration callback ......");
                System.out.println("acK:" + acK);
                System.out.println("cause:" + cause);
                System.out.println("config:" + JSONObject.toJSONString(correlationData));
                System.out.println("integration callback......");
            }
        });
        rabbitTemplate.convertAndSend(RabbitMqConfig.INTEGRATION_EXCHANGE, RabbitMqConfig.INTEGRATION_ROUTE_KEY, message);
        return JsonData.buildSuccess();
    }

    /**
     * rabbitmq开启可靠性消息回调确认：
     *   rabbitmq:
     *     publisher-confirm-type: correlated
     * rabbitTemplate是单例模式，只能配置一个可靠性消息回调，所有队列与交换机如果属于同一个rabbitTemplate，则公用回调参数
     */
    @RequestMapping("/submit/confirm/{message}")
    public JsonData submitConfirm(@PathVariable("message") String message){
        rabbitTemplate.convertAndSend(RabbitMqConfig.SUBMIT_CONFIRM_EXCHANGE, RabbitMqConfig.SUBMIT_CONFIRM_ROUTE_KEY, message);
        return JsonData.buildSuccess();
    }


    /**
     * 消息投递到交换机回调：
     * 如果已经存在消息投递到交换机中，那该方法报错
     */
    @RequestMapping("/submit/confirm/callback/{message}")
    public JsonData submitCallback(@PathVariable("message") String message){
        /**
         * 消息投递到broker，broker会回复一个确认帧给生产者
         */
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             * CorrelationData：配置信息
             * acK：成功true 错误false
             * cause：错误原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean acK, String cause) {
                System.out.println("submit confirm callback ......");
                System.out.println("acK:" + acK);
                System.out.println("cause:" + cause);
                System.out.println("correlationData:" + correlationData);
                System.out.println("submit confirm callback ......");
            }
        });
        return JsonData.buildSuccess();
    }

    /**
     * 使用错误交换机验证消息可靠性投递
     */
    @RequestMapping("/submit/confirm/error_exchange/{message}")
    public JsonData submitConfirmErrorExchange(@PathVariable("message") String message){
        rabbitTemplate.convertAndSend(RabbitMqConfig.SUBMIT_CONFIRM_ERROR_EXCHANGE, RabbitMqConfig.SUBMIT_CONFIRM_ROUTE_KEY, message);
        return JsonData.buildSuccess();
    }

    /**
     * 通过包装CorrelationDataPlus可以使得回调函数中可以获取到消息的信息
     */
    @RequestMapping("/submit/confirm/error_exchange/WithCorrelationData/{message}")
    public JsonData submitConfirmWithCorrelationData(@PathVariable("message") String message){
        CorrelationDataPlus plus = new CorrelationDataPlus();
        plus.setExchange(RabbitMqConfig.SUBMIT_CONFIRM_EXCHANGE);
        plus.setRoutingKey(RabbitMqConfig.SUBMIT_CONFIRM_ROUTE_KEY);
        plus.setMessage(message);
        rabbitTemplate.convertAndSend(RabbitMqConfig.SUBMIT_CONFIRM_EXCHANGE, RabbitMqConfig.SUBMIT_CONFIRM_ROUTE_KEY, message, plus);
        return JsonData.buildSuccess();
    }



    /**
     * 使用错误路由key验证消息可靠性投递
     */
    @RequestMapping("/submit/confirm/error_route_key/{message}")
    public JsonData submitConfirmErrorRouteKey(@PathVariable("message") String message){
        rabbitTemplate.convertAndSend(RabbitMqConfig.SUBMIT_CONFIRM_EXCHANGE, RabbitMqConfig.SUBMIT_CONFIRM_ERROR_ROUTE_KEY, message);
        return JsonData.buildSuccess();
    }

    /**
     * 消息从交换机投递到队列中时的回调
     * ReturnedMessage中包含了消息的具体信息
     */
    @RequestMapping("/submit/confirm/return/{message}")
    public JsonData submitConfirmReturn(@PathVariable("message") String message){
        // 为true,则交换机处理消息到路由失败，则会返回给生产者
        // 开启强制消息投递（mandatory为设置为true），但消息未被路由至任何一个queue，则回退一条消息
        rabbitTemplate.setReturnsCallback(new RabbitTemplate.ReturnsCallback() {
            @Override
            public void returnedMessage(ReturnedMessage returned) {
                System.out.println("submit confirm returned ......");
                System.out.println("replyCode:" + returned.getReplyCode());
                System.out.println("returned:" + returned.toString());
                System.out.println("submit confirm returned ......");
            }
        });
        rabbitTemplate.convertAndSend(RabbitMqConfig.SUBMIT_CONFIRM_EXCHANGE, RabbitMqConfig.SUBMIT_CONFIRM_ROUTE_KEY, message);
        return JsonData.buildSuccess();
    }


    /**
     * 死信队列
     * （1）死信队列其本质上就是一个普通队列
     * （2）给正常队列配置消息过期时间，当消息过期后，就会根据队列绑定的死信队列的key发送到对应死信队列中
     */
    @RequestMapping("/dead_queue/{message}")
    public JsonData deadQueue(@PathVariable("message") String message){
        rabbitTemplate.convertAndSend(RabbitMqConfig.NEW_MERCHANT_EXCHANGE, RabbitMqConfig.NEW_MERCHANT_ROUTE_KEY, message);
        return JsonData.buildSuccess();
    }

}
