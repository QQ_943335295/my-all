package com.zxm.rabbit.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * RabbitMq配置类
 * @author zxm
 * @date 2023/12/19
 */
@Configuration
public class RabbitMqConfig {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /******************   集成测试交换机与队列   ******************/

    /**
     * 交换机名称
     */
    public static final String INTEGRATION_EXCHANGE = "integration_exchange";
    /**
     * 队列名称
     */
    public static final String INTEGRATION_QUEUE = "integration_queue";

    public static final String INTEGRATION_ROUTE_KEY = "integration.error";

    /**
     * 定义交换机
     */
    @Bean
    public Exchange integrationExchange() {
        // 持久化
        return ExchangeBuilder.topicExchange(INTEGRATION_EXCHANGE).durable(true).build();
        // return new TopicExchange(EXCHANGE_NAME, true, false);
    }

    /**
     * 定义队列
     */
    @Bean
    public Queue integrationQueue() {
        return QueueBuilder.durable(INTEGRATION_QUEUE).build();
        //return new Queue(QUEUE_NAME, true, false, false, null);
    }

    /**
     * 交换机和队列绑定关联关系
     */
    @Bean
    public Binding integrationRouteKey(Queue integrationQueue, Exchange integrationExchange) {
        return BindingBuilder.bind(integrationQueue).to(integrationExchange).with(INTEGRATION_ROUTE_KEY).noargs();
        //return new Binding(QUEUE_NAME, Binding.DestinationType.QUEUE, EXCHANGE_NAME, "order.#", null);
    }

    /******************   集成测试交换机与队列   ******************/


    /******************   可靠性消息投递交换机与队列   ******************/

    /**
     * 交换机名称
     */
    public static final String SUBMIT_CONFIRM_EXCHANGE = "submit_confirm_exchange";

    /**
     * 交换机名称
     */
    public static final String SUBMIT_CONFIRM_ERROR_EXCHANGE = "submit_confirm_error_exchange";

    /**
     * 队列名称
     */
    public static final String SUBMIT_CONFIRM_QUEUE = "submit_confirm_queue";

    /**
     * 路由键
     */
    public static final String SUBMIT_CONFIRM_ROUTE_KEY = "submit_confirm_route_key";

    /**
     * 错误的路由键
     */
    public static final String SUBMIT_CONFIRM_ERROR_ROUTE_KEY = "submit_confirm_error_route_key";

    /**
     * 定义交换机
     */
    @Bean
    public Exchange submitConfirmExchange() {
        // 持久化
        return ExchangeBuilder.topicExchange(SUBMIT_CONFIRM_EXCHANGE).durable(true).build();
    }

    /**
     * 定义队列
     */
    @Bean
    public Queue submitConfirmQueue() {
        return QueueBuilder.durable(SUBMIT_CONFIRM_QUEUE).build();
    }

    /**
     * 交换机和队列绑定关联关系
     */
    @Bean
    public Binding submitConfirmBinding(Queue submitConfirmQueue, Exchange submitConfirmExchange) {
        return BindingBuilder.bind(submitConfirmQueue).to(submitConfirmExchange).with(SUBMIT_CONFIRM_ROUTE_KEY).noargs();
    }

    /******************   可靠性消息投递交换机与队列   ******************/



    /******************   死信队列   ******************/

    /**
     * 交换机名称
     */
    public static final String LOCK_MERCHANT_DEAD_EXCHANGE = "lock_merchant_dead_exchange";

    /**
     * 队列名称
     */
    public static final String LOCK_MERCHANT_DEAD_QUEUE = "lock_merchant_dead_queue";

    /**
     * 路由键
     */
    public static final String LOCK_MERCHANT_DEAD_ROUTE_KEY = "lock_merchant_dead_route_key";

    /**
     * 定义交换机
     */
    @Bean
    public Exchange lockMerchantExchange() {
        // 持久化
        return ExchangeBuilder.topicExchange(LOCK_MERCHANT_DEAD_EXCHANGE).durable(true).build();
    }

    /**
     * 定义队列
     */
    @Bean
    public Queue lockMerchantQueue() {
        return QueueBuilder.durable(LOCK_MERCHANT_DEAD_QUEUE).build();
    }

    /**
     * 交换机和队列绑定关联关系
     */
    @Bean
    public Binding lockMerchantBinding(Queue lockMerchantQueue, Exchange lockMerchantExchange) {
        return BindingBuilder.bind(lockMerchantQueue).to(lockMerchantExchange).with(LOCK_MERCHANT_DEAD_ROUTE_KEY).noargs();
    }



    /**
     * 交换机名称
     */
    public static final String NEW_MERCHANT_EXCHANGE = "new_merchant_exchange";

    /**
     * 队列名称
     */
    public static final String NEW_MERCHANT_QUEUE = "new_merchant_queue";

    /**
     * 路由键
     */
    public static final String NEW_MERCHANT_ROUTE_KEY = "new_merchant_route_key";

    /**
     * 定义交换机
     */
    @Bean
    public Exchange newMerchantExchange() {
        return new TopicExchange(NEW_MERCHANT_EXCHANGE, true, false);
    }

    /**
     * 定义队列
     */
    @Bean
    public Queue newMerchantQueue() {
/*        Map<String,Object> args = new HashMap<>(3);
        //消息过期后，进⼊到死信交换机
        args.put("x-dead-letter-exchange",LOCK_MERCHANT_DEAD_EXCHANGE);
        //消息过期后，进⼊到死信交换机的路由key
        args.put("x-dead-letter-routing-key",LOCK_MERCHANT_DEAD_ROUTE_KEY);
        //过期时间，单位毫秒
        args.put("x-message-ttl",20000);
        return QueueBuilder.durable(NEW_MERCHANT_QUEUE).withArguments(args).build();*/
        return QueueBuilder.durable(NEW_MERCHANT_QUEUE)
                // 消息过期后，进⼊到死信交换机
                .deadLetterExchange(LOCK_MERCHANT_DEAD_EXCHANGE)
                // 过期时间，单位毫秒
                .ttl(20 * 1000)
                // 消息过期后，进⼊到死信交换机的路由key
                .deadLetterRoutingKey(LOCK_MERCHANT_DEAD_ROUTE_KEY).build();

    }

    /**
     * 交换机和队列绑定关联关系
     */
    @Bean
    public Binding newMerchantBinding(Queue newMerchantQueue, Exchange newMerchantExchange) {
        return BindingBuilder.bind(newMerchantQueue).to(newMerchantExchange).with(NEW_MERCHANT_ROUTE_KEY).noargs();
    }



    /******************   死信队列   ******************/


    /******************   重试次数消到了之后消息路由到该队列   ******************/

    public static final String REPUBLISH_EXCHANGE = "republish.exchange";

    public static final String REPUBLISH_QUEUE = "republish.queue";

    public static final String REPUBLISH_ROUTE_KEY= "republish.route.key";

    /**
     * 异常交换机
     * @return
     */
    @Bean
    public TopicExchange republishExchange(){
        return new TopicExchange(REPUBLISH_EXCHANGE,true,false);
    }

    /**
     * 异常队列
     * @return
     */
    @Bean
    public Queue republishQueue(){
        return new Queue(REPUBLISH_QUEUE,true);
    }

    /**
     * 队列与交换机进行绑定
     */
    @Bean
    public Binding BindingErrorQueueAndExchange(Queue republishQueue, TopicExchange republishExchange){
        return BindingBuilder.bind(republishQueue).to(republishExchange).with(REPUBLISH_ROUTE_KEY);
    }

    /**
     * 配置 RepublishMessageRecoverer
     * 用途：消息重试一定次数后，用特定的routingKey转发到指定的交换机中，方便后续排查和告警
     * 顶层是 MessageRecoverer接口，多个实现类
     */
    @Bean
    public MessageRecoverer messageRecoverer(){
        return new RepublishMessageRecoverer(rabbitTemplate, REPUBLISH_EXCHANGE, REPUBLISH_ROUTE_KEY);
    }

    /******************   重试次数消到了之后消息路由到该队列   ******************/

}
