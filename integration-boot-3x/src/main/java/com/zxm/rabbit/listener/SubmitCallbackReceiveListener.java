package com.zxm.rabbit.listener;

import com.rabbitmq.client.Channel;
import com.zxm.constant.StrConstants;
import com.zxm.rabbit.config.RabbitMqConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;

/**
 * 监听者
 * @author zxm
 * @date 2023/12/19
 */
@Component
@RabbitListener(queues = RabbitMqConfig.SUBMIT_CONFIRM_QUEUE)
public class SubmitCallbackReceiveListener {

    /**
     * RabbitHandler 会⾃动匹配消息类型（消息⾃动确认）
     */
    @RabbitHandler
    public void integrationHandler(String msg, Message message, Channel channel) throws IOException {
        long msgTag = message.getMessageProperties().getDeliveryTag();
        System.out.println("Submit Callback Receive Listener route key："+RabbitMqConfig.SUBMIT_CONFIRM_QUEUE);
        System.out.println("Submit Callback Receive Listener msg="+msg);
        System.out.println("Submit Callback Receive Listener msgTag="+msgTag);
        System.out.println("Submit Callback Receive Listener message="+ message);
        System.out.println("Submit Callback Receive Listener 监听到消息：消息内容:"+ new String(msg.getBytes(StandardCharsets.UTF_8)));
        System.out.println("Submit Callback Receive Listener route key："+RabbitMqConfig.SUBMIT_CONFIRM_QUEUE);
        if (msg.contains(StrConstants.ERROR)){
            throw new RemoteException("consumer error ...");
        } else {
            // 如果关闭，则新的监听者监听后则会不断消费该消息
            channel.basicAck(msgTag, false);
        }
    }

}
