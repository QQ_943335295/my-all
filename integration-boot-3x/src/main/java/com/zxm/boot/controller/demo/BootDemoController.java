package com.zxm.boot.controller.demo;

import com.zxm.bean.JsonData;
import com.zxm.boot.domain.demo.User;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

/**
 * @author zxm
 * @date 2023/11/7
 */
@RestController
@RequestMapping("/boot/demo")
public class BootDemoController {

    @GetMapping("/getUserInfo")
    public JsonData getUserInfo() {
        User user = new User("getUserInfo" + new Random().nextLong(), "getUserInfo");
        return JsonData.buildSuccess(user);
    }

    @PostMapping("/queryUserByParam")
    public JsonData queryUserByParam(@RequestBody User user) {
        user = new User("queryUserByParam", user.getUsername());
        return JsonData.buildSuccess(user);
    }

    @GetMapping("/queryByName")
    public JsonData queryByName(String username) {
        User user = new User("queryByName", username);
        return JsonData.buildSuccess(user);
    }

}
