package com.zxm.boot.controller.mysql.part;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.zxm.bean.JsonData;
import com.zxm.boot.domain.part.PartInfo;
import com.zxm.boot.mapper.mysql.part.MysqlPartDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 动态分区测试
 * @author zxm
 * @date 2024/2/1
 */
@RestController
public class MysqlPartController {

    @Autowired
    private MysqlPartDao mysqlPartDao;

    @RequestMapping("/create/part")
    public JsonData createPart(){
        mysqlPartDao.createPart();
        return JsonData.buildSuccess();
    }

    @RequestMapping("/add/part")
    public JsonData addPart(){
        List<PartInfo> partInfos = mysqlPartDao.listPartInfo();
        PartInfo partInfo = partInfos.get(partInfos.size() - 1);
        String curPartitionDescription = partInfo.getPARTITIONDESCRIPTION().replace("'", "");
        Date date = DateUtil.parseDate(curPartitionDescription);
        DateTime dateTime = DateUtil.offsetMonth(date, 1);
        String partitionDescription = DateUtil.format(dateTime, DatePattern.NORM_DATETIME_FORMATTER);
        partInfo.setPARTITIONDESCRIPTION(partitionDescription);
        mysqlPartDao.addPart(partInfo);
        return JsonData.buildSuccess();
    }

    @RequestMapping("/get/part/info")
    public JsonData getPartInfo(){
        mysqlPartDao.listPartInfo();
        return JsonData.buildSuccess(mysqlPartDao.listPartInfo());
    }

}
