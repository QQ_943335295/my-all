package com.zxm.boot.domain.part;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * mysql_part
 * @author
 */
@Data
public class MysqlPart implements Serializable {
    private Long id;

    private Date recordDate;

    private Integer usedAmount;

    private static final long serialVersionUID = 1L;
}
