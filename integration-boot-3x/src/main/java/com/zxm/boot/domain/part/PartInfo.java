package com.zxm.boot.domain.part;

import lombok.Data;

/**
 * 分区信息
 * @author zxm
 * @date 2024/2/1
 */
@Data
public class PartInfo {

    /* 库名 */
    private String TABLESCHEMA;
    /* 表名 */
    private String TABLENAME;
    /* 分区名 */
    private String PARTITIONNAME;
    /* 分区排序 */
    private String  PARTITIONORDINALPOSITION;
    /* 分区方法 */
    private String  PARTITIONMETHOD;
    /* 分区键 */
    private String  PARTITIONEXPRESSION;
    /* 分区限制数 */
    private String  PARTITIONDESCRIPTION;
    /* 分区数据行数 */
    private String  TABLEROWS;


}
