package com.zxm.boot.mapper.mysql.part;

import com.zxm.boot.domain.part.MysqlPart;
import com.zxm.boot.domain.part.PartInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MysqlPartDao {
    int deleteByPrimaryKey(Long id);

    int insert(MysqlPart record);

    int insertSelective(MysqlPart record);

    MysqlPart selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MysqlPart record);

    int updateByPrimaryKey(MysqlPart record);

    /**
     * 创建分区
     */
    void createPart();

    /**
     * 获取分区信息
     */
    List<PartInfo> listPartInfo();

    void addPart(PartInfo partInfo);
}
