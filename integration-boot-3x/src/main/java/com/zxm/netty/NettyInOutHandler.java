package com.zxm.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.CharsetUtil;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 出栈入栈
 * @author zxm
 * @date 2023/10/11
 */
@Slf4j
@Component
public class NettyInOutHandler {

    private static AtomicLong atomicLong = new AtomicLong();

    //@PostConstruct
    public void init() {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        try {
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ch.pipeline()
                                    .addLast(new SampleInBoundHandler(SampleInBoundHandler.class.getName(), true));
                        }
                    });
            ChannelFuture channelFuture = serverBootstrap.bind(38888).sync();
            log.info("Tcp服务启动：{}", 38888);
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public static class SampleInBoundHandler extends ChannelInboundHandlerAdapter {
        private final String name;
        private final boolean flush;

        public SampleInBoundHandler(String name, boolean flush) {
            this.name = name;
            this.flush = flush;
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            String data = ((ByteBuf) msg).toString(CharsetUtil.UTF_8);
            log.info("收到报文消息：{}, 数量：{}", data, atomicLong.incrementAndGet());
            if (flush) {
                ctx.channel().writeAndFlush(Unpooled.copiedBuffer("ok".getBytes()));
            } else {
                super.channelRead(ctx, msg);
            }
        }

        @Override
        public void channelInactive(ChannelHandlerContext ctx) throws Exception {
            log.info("断开连接, 数量：{}", atomicLong.decrementAndGet());
            super.channelInactive(ctx);
        }
    }
}
