package my.all.boot.controller;

import bean.JsonData;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import constants.CommonConstants;
import my.all.filter.LoginFilter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 首页控制器
 * @author zxm
 * @date 2022/2/8
 */
@Controller
public class IndexController {

    /**
     * 首页
     * @return String
     */
    @GetMapping(value = {"/","/index"})
    public String login(){
        return "/index";
    }


    /**
     * 欢迎页
     * @return String
     */
    @GetMapping("/welcome")
    public String welcome(){
        return "/welcome";
    }


    @PostMapping("/loginOut")
    @ResponseBody
    public JsonData loginOut(HttpServletRequest request){
        Object attribute = request.getSession().getAttribute(CommonConstants.TOKEN);
        if (attribute != null){
            String token = attribute.toString();
            LoginFilter.LOGIN_USERS.remove(token);
            String result = HttpUtil.createPost(CommonConstants.SSO_LOGIN_OUT_URL +
                    String.format(CommonConstants.TOKEN_PARAM, token)).execute().body();
            JsonData data = JSONObject.parseObject(result, JsonData.class);
            if(data.getCode() == 0){
                return JsonData.buildSuccess();
            }
        }
        return JsonData.buildError();
    }

}