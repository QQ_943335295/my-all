package my.all;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 单点登录-登录服务
 * @author zxm
 * @date 2022/1/26
 */
@SpringBootApplication
public class AppSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSsoApplication.class, args);
    }

}
