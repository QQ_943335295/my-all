package my.all.boot.controller;

import bean.JsonData;
import cn.hutool.core.util.StrUtil;
import constants.CommonConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import domian.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 登录控制器
 * @author zxm
 * @date 2022/2/8
 */
@Controller
public class LoginController {

    /**
     * 用户信息
     * key：username
     * value：password
     */
    public static final Map<String, String> USERS;
    static {
        USERS = new HashMap<>();
        USERS.put("zxm", "123456");
        USERS.put("lxy", "123456");
        USERS.put("admin", "123456");
    }

    /**
     * SSO的会话登录状态
     * key：session
     * value：登录状态
     */
    public static final Map<String, String> LOGIN_SESSIONS = new HashMap<>();

    /**
     * 登录用户
     * key：token
     * value：info
     */
    public static final Map<String, User> LOGIN_USERS = new HashMap<>();


    /**
     * 登录页
     * @return String
     */
    @GetMapping("/login")
    public String login(HttpServletRequest request){
        String sessionId = request.getSession().getId();
        System.err.println("进入登录页面SessionId:" + sessionId);
        String redirectUrl = request.getParameter(CommonConstants.LOGIN_REDIRECT_URL);
        if(redirectUrl == null){
            redirectUrl = CommonConstants.BLANK;
        }
         request.setAttribute("redirectUrl", redirectUrl);
        return "/login";
    }

    /**
     * 登录接口
     * @return String
     */
    @PostMapping("/sso/login")
    @ResponseBody
    public JsonData login(String username, String password, HttpServletRequest request){
        String sessionId = request.getSession().getId();
        System.err.println("调用登录接口SessionId:" + sessionId);
        String redirectUrl = request.getParameter(CommonConstants.LOGIN_REDIRECT_URL);
        if(checkLoginInfo(username, password)){
            String token = UUID.randomUUID().toString();
            User user = new User();
            user.setUsername(username);
            user.setSessionId(sessionId);
            LOGIN_USERS.put(token, user);
            String result = "/index";
            if(StrUtil.isNotBlank(redirectUrl)){
                result = redirectUrl + String.format(CommonConstants.TOKEN_PARAM, token);
            }
            LOGIN_SESSIONS.put(request.getSession().getId(), token);
            return JsonData.buildSuccess(result);
        } else {
            return JsonData.buildError("用户名或密码错误！");
        }
    }

    private boolean checkLoginInfo(String username, String password) {
        String value = USERS.get(username);
        if(value != null && value.equals(password)){
            return true;
        }
        return false;
    }



    @PostMapping("/sso/check/sys/login")
    @ResponseBody
    public JsonData checkSysLogin(HttpServletRequest request){
        System.err.println("sso/check/sys/login:" + request.getSession().getId());
        String token =  LOGIN_SESSIONS.get(request.getSession().getId());
        if(StrUtil.isNotBlank(token)){
            User user = LOGIN_USERS.get(token);
            if(user != null){
                return JsonData.buildSuccess(token);
            }
        }
        return JsonData.buildError();
    }


    @PostMapping("/sso/check/login")
    @ResponseBody
    public JsonData checkLogin(String token){
        if(StrUtil.isNotBlank(token)){
            User user = LOGIN_USERS.get(token);
            if(user != null){
                return JsonData.buildSuccess(user);
            }
        }
        return JsonData.buildError();
    }

    @PostMapping("/sso/loginOut")
    @ResponseBody
    public JsonData loginOut(String token){
        User user = LOGIN_USERS.remove(token);
        if(user != null){
            LOGIN_SESSIONS.remove(user.getSessionId());
        }
        return JsonData.buildSuccess();
    }

    /**
     * 首页
     * @return String
     */
    @GetMapping(value = {"/","/index"})
    public String index(){
        return "/index";
    }
}
