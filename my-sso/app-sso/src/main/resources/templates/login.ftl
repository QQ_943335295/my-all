<#assign basePath = request.contextPath />
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SSO登录</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <script type="text/javascript" src="${basePath}/static/ie9/jquery.min.js"></script>
    <script src="${basePath}/static/ie9/html5.min.js"></script>
    <script src="${basePath}/static/ie9/respond.min.js"></script>
</head>
<body>
<div>
    <form>
        <div class="item-row">SSO登录</div>
        <div class="item-row">
            <div>账号：</div>
            <div><input name="username" id="username"></div>
        </div>
        <div class="item-row">
            <div>密码：</div>
            <div><input name="password" id="password"></div>
        </div>
        <div class="item-row">
            <button type="button" id="submitBtn">提交</button>
            <button type="reset">重置</button>
        </div>
    </form>

</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#submitBtn").click(function(){
        console.log("111111111111111111")
        //请求参数
        let username = $("#username").val();
        let password = $("#password").val();
        let paramUrl = "&username=" + username + "&password=" + password;
        let url = '${basePath}' + "/sso/login?redirectUrl=" + '${redirectUrl}' + paramUrl;
        $.ajax({
            //请求方式
            type : "POST",
            //请求的媒体类型
            contentType: "application/json;charset=UTF-8",
            //请求地址
            url : url,
            //请求成功
            success : function(result) {
                console.log(result)
                if(result.code == 0){
                    window.location.href = result.data;
                } else {
                    alert(result.msg)
                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
            }
        });
    });
    checkSysLogin();
    function checkSysLogin(){
        let url = '${basePath}' + "/sso/check/sys/login";
        $.ajax({
            //请求方式
            type : "POST",
            //请求的媒体类型
            contentType: "application/json;charset=UTF-8",
            //请求地址
            url : url,
            //请求成功
            success : function(result) {
                console.log(result)
                let redirectUrl = '${redirectUrl}';
                if(result.code == 0){
                    if (redirectUrl){
                        window.location.href = redirectUrl + "?token=" + result.data;
                    } else {
                        window.location.href = 'http://localhost:63000/app-sso/'
                    }

                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
            }
        })
    }
});
</script>
<style>
    .item-row{
        display: flex;
        justify-content: center;
        padding: 7px 14px;
    }
</style>
</body>
</html>