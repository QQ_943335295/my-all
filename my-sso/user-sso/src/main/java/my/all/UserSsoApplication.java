package my.all;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 单点登录——用户服务
 * @author zxm
 * @date 2022/1/26
 */
@SpringBootApplication
public class UserSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserSsoApplication.class, args);
    }

}
