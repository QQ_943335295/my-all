package my.all.filter;


import bean.JsonData;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import constants.CommonConstants;
import org.springframework.stereotype.Component;
import utils.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登录拦截器
 * @author zxm
 * @date 2022/2/8
 */
@Component
public class LoginFilter implements Filter {

    public static List<String> NOT_FILTER_LIST = new ArrayList<>();
    static {
        NOT_FILTER_LIST.add("/static/*");
        NOT_FILTER_LIST.add("*.html");
        NOT_FILTER_LIST.add("*.js");
        NOT_FILTER_LIST.add("*.ico");
    }

    /**
     * 登录用户
     * key：token
     * value：info
     */
    public static final Map<String, String> LOGIN_USERS = new HashMap<>();;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String currentUrl = request.getRequestURL().toString();

        System.err.println("sessionId:" + request.getSession().getId());

        if(StringUtils.matches(currentUrl, NOT_FILTER_LIST)){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String token = getToken(request);
        String user = null;
        if(StrUtil.isNotBlank(token)){
            user = LOGIN_USERS.get(token);
        }
        // 如果TOKEN为空或者user为空，则需要调
        if(StrUtil.isBlank(token) || StrUtil.isBlank(user)){
            String result = HttpUtil.createPost(CommonConstants.SSO_LOGIN_CHECK_URL +
                    String.format(CommonConstants.TOKEN_PARAM, token)).execute().body();
            JsonData data = JSONObject.parseObject(result, JsonData.class);
            if(data.getCode() == 0){
                user = data.getData(new TypeReference<String>(){});
                LOGIN_USERS.put(token, user);
                request.getSession().setAttribute(CommonConstants.TOKEN, token);
            }
        }
        if (StrUtil.isBlank(token) || StrUtil.isBlank(user)){
            String redirectUrl = String.format(CommonConstants.LOGIN_REDIRECT_URL_PARAM, currentUrl);
            response.sendRedirect(CommonConstants.LOGIN_SYS_URL + redirectUrl);
            return;
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
    }

    public static String getToken(HttpServletRequest request){
        String token = CommonConstants.BLANK;
        if(ObjectUtil.isNotNull(request)){
            token = request.getParameter(CommonConstants.TOKEN);
            if(StrUtil.isBlank(token)){
                token = request.getHeader(CommonConstants.TOKEN);
            }
            if(StrUtil.isBlank(token)){
                token = String.valueOf(request.getSession().getAttribute(CommonConstants.TOKEN));
            }
        }
        return token;
    }
}
