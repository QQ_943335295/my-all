<#assign basePath = request.contextPath />
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SSO用户</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <script type="text/javascript" src="${basePath}/static/ie9/jquery.min.js"></script>
    <script src="${basePath}/static/ie9/html5.min.js"></script>
    <script src="${basePath}/static/ie9/respond.min.js"></script>
</head>
<body>
<div>
    <div class="item-row">SSO用户</div>
    <div class="item-row"><a href="javaScript:void(0);" onclick="loginOut()">退出</a></div>
</div>
<script>
function loginOut(){
    let url = '${basePath}' + "/loginOut";
    $.ajax({
        //请求方式
        type : "POST",
        //请求的媒体类型
        contentType: "application/json;charset=UTF-8",
        //请求地址
        url : url,
        //请求成功
        success : function(result) {
            if(result.code == 0){
                window.location.href = 'http://localhost:63000/app-sso/login';
            } else {
                alert(result.msg)
            }
        },
        //请求失败，包含具体的错误信息
        error : function(e){
            console.log(e.status);
        }
    });
}
</script>
<style>
    .item-row{
        display: flex;
        justify-content: center;
        padding: 7px 14px;
    }
</style>
</body>
</html>