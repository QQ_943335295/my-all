package bean;

import lombok.Getter;

/**
 * 状态码定义约束，共6位数，前三位代表服务，后4位代表接口
 * 比如 通用 - 100，权限 - 403
 * @author zxm
 * @date 2022/2/8
 */
public enum BizCodeEnum {

    /** 操作成功 */
    OPS_SUCCESS(1000000,"操作成功"),
    /** 操作失败 */
    OPS_FAIL(1000001,"操作失败"),
    /** 无权限 */
    NO_PERMISSION(4030001,"无权限");

    @Getter
    private String message;

    @Getter
    private int code;

    BizCodeEnum(int code, String message){
        this.code = code;
        this.message = message;
    }



}
