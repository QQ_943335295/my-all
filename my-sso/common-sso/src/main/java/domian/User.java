package domian;

import lombok.Data;

/**
 * @author zxm
 * @date 2022/4/27
 */
@Data
public class User{
    private String username;
    private String sessionId;
}