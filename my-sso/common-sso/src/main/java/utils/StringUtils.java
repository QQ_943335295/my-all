package utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;

import java.util.List;

/**
 * 字符串工具类
 * @author zxm
 * @date 2022/2/15
 */
public class StringUtils extends StrUtil {

    /**
     * 星号
     */
    private static final char START = '*';


    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param str  指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, List<String> strs) {
        if (CharSequenceUtil.isEmpty(str) || CollectionUtil.isEmpty(strs)) {
            return false;
        }
        for (String testStr : strs) {
            if (matches(str, testStr)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找指定字符串是否匹配指定字符串数组中的任意一个字符串
     *
     * @param str  指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, String... strs) {
        if (CharSequenceUtil.isEmpty(str) || ArrayUtil.isEmpty(strs)) {
            return false;
        }
        for (String testStr : strs) {
            if (matches(str, testStr)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找指定字符串是否匹配
     *
     * @param str     指定字符串
     * @param pattern 需要检查的字符串
     * @return 是否匹配
     */
    public static boolean matches(String str, String pattern) {
        if (CharSequenceUtil.isEmpty(pattern) || CharSequenceUtil.isEmpty(str)) {
            return false;
        }
        // 替换空格
        pattern = pattern.replaceAll("\\s*", "");
        // pattern截取开始位置
        int beginOffset = 0;
        // 前星号的偏移位置
        int formerStarOffset = -1;
        // 后星号的偏移位置
        int latterStarOffset = -1;

        String remainingURI = str;
        String prefixPattern = "";
        String suffixPattern = "";

        boolean result = false;
        do {
            formerStarOffset = CharSequenceUtil.indexOf(pattern, START, beginOffset);
            prefixPattern = CharSequenceUtil.sub(pattern, beginOffset, formerStarOffset > -1 ? formerStarOffset : pattern.length());

            // 匹配前缀Pattern
            result = remainingURI.contains(prefixPattern);
            // 已经没有星号，直接返回
            if (formerStarOffset == -1) {
                return result;
            }

            // 匹配失败，直接返回
            if (!result) {
                return false;
            }

            if (!CharSequenceUtil.isEmpty(prefixPattern)) {
                remainingURI = CharSequenceUtil.subAfter(str, prefixPattern, false);
            }

            // 匹配后缀Pattern
            latterStarOffset = CharSequenceUtil.indexOf(pattern, START, formerStarOffset + 1);
            suffixPattern = CharSequenceUtil.sub(pattern, formerStarOffset + 1, latterStarOffset > -1 ? latterStarOffset : pattern.length());

            result = remainingURI.contains(suffixPattern);
            // 匹配失败，直接返回
            if (!result) {
                return false;
            }

            if (!CharSequenceUtil.isEmpty(suffixPattern)) {
                remainingURI = CharSequenceUtil.subAfter(str, suffixPattern, false);
            }

            // 移动指针
            beginOffset = latterStarOffset + 1;

        }
        while (!CharSequenceUtil.isEmpty(suffixPattern) && !CharSequenceUtil.isEmpty(remainingURI));

        return true;
    }

}
