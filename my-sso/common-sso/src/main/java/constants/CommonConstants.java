package constants;

/**
 * 通用常量
 * @author zxm
 * @date 2022/2/8
 */
public interface CommonConstants {

    /**
     * -------------通用常量------------------
     */
    String BLANK = "";


    /**
     * -------------登录相关常量------------------
     */

    /** SSO登录地址 */
    String LOGIN_SYS_URL = "http://localhost:63000/app-sso/login";

    /** 登录后回调地址 */
    String LOGIN_REDIRECT_URL = "redirectUrl";

    /** 登录回调参数 */
    String LOGIN_REDIRECT_URL_PARAM = "?redirectUrl=%s";

    /** token */
    String TOKEN = "token";

    /** token */
    String TOKEN_PARAM = "?token=%s";

    /** SSO验证登录地址 **/
    String SSO_LOGIN_CHECK_URL = "http://localhost:63000/app-sso/sso/check/login";

    /** SSO退出登录地址 **/
    String SSO_LOGIN_OUT_URL = "http://localhost:63000/app-sso/sso/loginOut";

}
