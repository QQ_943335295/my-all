/*
Navicat MySQL Data Transfer

Source Server         : local_mysql
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : ali-shop-demo

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2021-03-01 17:27:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `default_status` int(1) DEFAULT NULL COMMENT '是否默认收货地址：0->否；1->是',
  `receive_name` varchar(64) DEFAULT NULL COMMENT '收发货人姓名',
  `phone` varchar(64) DEFAULT NULL COMMENT '收货人电话',
  `province` varchar(64) DEFAULT NULL COMMENT '省/直辖市',
  `city` varchar(64) DEFAULT NULL COMMENT '市',
  `region` varchar(64) DEFAULT NULL COMMENT '区',
  `detail_address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='电商-公司收发货地址表';

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('1', '1', '1', '钟小萌', '18370847419', '江西省', '赣州市', '龙南市', '桃江乡窑头村', '2021-02-24 16:02:02');
INSERT INTO `address` VALUES ('4', '1', '0', '钟小萌', '18370847419', '江西省', '赣州市', '龙南市', '窑头-20号', '2021-02-26 17:53:59');
INSERT INTO `address` VALUES ('5', '1', '0', '钟小萌', '18370847419', '江西省', '赣州市', '龙南市', '窑头-20号', '2021-02-26 17:54:35');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `category` varchar(11) DEFAULT NULL COMMENT '优惠卷类型[NEW_USER注册赠券，TASK任务卷，PROMOTION促销劵]',
  `publish` varchar(11) DEFAULT NULL COMMENT '发布状态, PUBLISH发布，DRAFT草稿，OFFLINE下线',
  `coupon_img` varchar(524) DEFAULT NULL COMMENT '优惠券图片',
  `coupon_title` varchar(128) DEFAULT NULL COMMENT '优惠券标题',
  `price` decimal(16,2) DEFAULT NULL COMMENT '抵扣价格',
  `user_limit` int(11) DEFAULT NULL COMMENT '每人限制张数',
  `start_time` datetime DEFAULT NULL COMMENT '优惠券开始有效时间',
  `end_time` datetime DEFAULT NULL COMMENT '优惠券失效时间',
  `publish_count` int(11) DEFAULT NULL COMMENT '优惠券总量',
  `stock` int(11) DEFAULT '0' COMMENT '库存',
  `create_time` datetime DEFAULT NULL,
  `condition_price` decimal(16,2) DEFAULT NULL COMMENT '满多少才可以使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('18', 'NEW_USER', 'PUBLISH', 'https://file.xdclass.net/video/2020/alibabacloud/zt-alibabacloud.png', '永久有效-新人注册-0元满减-5元抵扣劵-限领取2张-不可叠加使用', '5.00', '2', '2000-01-01 00:00:00', '2099-01-29 00:00:00', '100000000', '99999991', '2020-12-26 16:33:02', '0.00');
INSERT INTO `coupon` VALUES ('19', 'PROMOTION', 'PUBLISH', 'https://file.xdclass.net/video/2020/alibabacloud/zt-alibabacloud.png', '有效中-21年1月到25年1月-20元满减-5元抵扣劵-限领取2张-不可叠加使用', '5.00', '2', '2000-01-29 00:00:00', '2025-01-29 00:00:00', '10', '3', '2020-12-26 16:33:03', '20.00');
INSERT INTO `coupon` VALUES ('20', 'PROMOTION', 'PUBLISH', 'https://file.xdclass.net/video/2020/alibabacloud/zt-alibabacloud.png', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '8.80', '40', '2020-08-01 00:00:00', '2021-09-29 00:00:00', '60', '0', '2020-12-26 16:33:03', '0.00');
INSERT INTO `coupon` VALUES ('21', 'PROMOTION', 'PUBLISH', 'https://file.xdclass.net/video/2020/alibabacloud/zt-alibabacloud.png', '有效中-20年8月到21年9月-商品id2-9.9元抵扣劵-限领取2张-可叠加使用', '8.80', '2', '2020-08-01 00:00:00', '2021-09-29 00:00:00', '100', '96', '2020-12-26 16:33:03', '0.00');
INSERT INTO `coupon` VALUES ('22', 'PROMOTION', 'PUBLISH', 'https://file.xdclass.net/video/2020/alibabacloud/zt-alibabacloud.png', '过期-20年8月到20年9月-商品id3-6元抵扣劵-限领取1张-可叠加使用', '6.00', '1', '2020-08-01 00:00:00', '2020-09-29 00:00:00', '100', '100', '2020-12-26 16:33:03', '0.00');

-- ----------------------------
-- Table structure for coupon_record
-- ----------------------------
DROP TABLE IF EXISTS `coupon_record`;
CREATE TABLE `coupon_record` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint(11) DEFAULT NULL COMMENT '优惠券id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间获得时间',
  `use_state` varchar(32) DEFAULT NULL COMMENT '使用状态  可用 NEW,已使用USED,过期 EXPIRED;',
  `user_id` bigint(11) DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(128) DEFAULT NULL COMMENT '用户昵称',
  `coupon_title` varchar(128) DEFAULT NULL COMMENT '优惠券标题',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `order_id` bigint(11) DEFAULT NULL COMMENT '订单id',
  `price` decimal(16,2) DEFAULT NULL COMMENT '抵扣价格',
  `condition_price` decimal(16,2) DEFAULT NULL COMMENT '满多少才可以使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of coupon_record
-- ----------------------------
INSERT INTO `coupon_record` VALUES ('1', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('2', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('3', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('4', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('5', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('6', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('7', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('8', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('9', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('10', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('11', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('12', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('13', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('14', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('15', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('16', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('17', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('18', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('19', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');
INSERT INTO `coupon_record` VALUES ('20', '20', '2021-03-01 16:57:00', 'NEW', '1', 'zxm', '有效中-20年8月到21年9月-商品id1-8.8元抵扣劵-限领取2张-不可叠加使用', '2020-08-01 00:00:00', '2021-09-29 00:00:00', null, '8.80', '0.00');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL COMMENT '昵称',
  `pwd` varchar(124) DEFAULT NULL COMMENT '密码',
  `head_img` varchar(524) DEFAULT NULL COMMENT '头像',
  `slogan` varchar(524) DEFAULT NULL COMMENT '用户签名',
  `sex` tinyint(2) DEFAULT '1' COMMENT '0表示女，1表示男',
  `points` int(10) DEFAULT '0' COMMENT '积分',
  `create_time` datetime DEFAULT NULL,
  `mail` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `secret` varchar(12) DEFAULT NULL COMMENT '盐，用于个人敏感信息处理',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail_idx` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'zxm', '$1$c0vgjnQo$IT.TqN6.7okDgRXxPuZsO.', 'https://xdclass-1024shop-img.oss-cn-shenzhen.aliyuncs.com/user/2021/02/03/39473aa1029a430298ac2620dd819962.jpeg', '小菜鸡历险记', '1', '0', '2021-02-26 15:33:21', '943335295@qq.com', '$1$c0vgjnQo');
