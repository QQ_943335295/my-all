#登录阿里云镜像仓
docker login --username=18370847419 registry.cn-hangzhou.aliyuncs.com --password=zmjy@2016.
#构建整个项目，或者单独构建common项目,避免依赖未被构建上去
cd ali-common
call mvn install

#构建网关
cd ../ali-gateway
call mvn install -Dmaven.test.skip=true dockerfile:build
docker tag ali-shop/ali-gateway:latest registry.cn-hangzhou.aliyuncs.com/ali-shop/api-gateway:v1.2
docker push registry.cn-hangzhou.aliyuncs.com/ali-shop/api-gateway:v1.2
echo "网关构建推送成功"

#用户服务
cd ../ali-user-service
call mvn install -Dmaven.test.skip=true dockerfile:build
docker tag ali-shop/ali-user-service:latest registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-user-service:v1.2
docker push registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-user-service:v1.2
echo "用户服务构建推送成功"

#商品服务
cd ../ali-product-service
call mvn install -Dmaven.test.skip=true dockerfile:build
docker tag ali-shop/ali-product-service:latest registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-product-service:v1.2
docker push registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-product-service:v1.2
echo "商品服务构建推送成功"

#订单服务
cd ../ali-order-service
call mvn install -Dmaven.test.skip=true dockerfile:build
docker tag ali-shop/ali-order-service:latest registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-order-service:v1.2
docker push registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-order-service:v1.2
echo "订单服务构建推送成功"

#优惠券服务
cd ../ali-coupon-service
call mvn install -Dmaven.test.skip=true dockerfile:build
docker tag ali-shop/ali-coupon-service:latest registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-coupon-service:v1.2
docker push registry.cn-hangzhou.aliyuncs.com/ali-shop/ali-coupon-service:v1.2
echo "优惠券服务构建推送成功"

echo "=======构建脚本执行完毕====="

