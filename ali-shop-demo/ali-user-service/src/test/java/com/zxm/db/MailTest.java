package com.zxm.db;

import com.zxm.AliUserApplication;
import com.zxm.service.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zm
 * @date 2021/2/25 14:41
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AliUserApplication.class)
public class MailTest {
    @Autowired
    private MailService mailService;

    @Test
    public void testUser(){
        mailService.sendSimpleMail("943335295@qq.com", "测试邮箱", "哈哈哈哈");
    }
}