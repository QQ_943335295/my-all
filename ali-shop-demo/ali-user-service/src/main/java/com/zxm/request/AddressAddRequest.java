package com.zxm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 地址对象
 * @author zm
 * @date 2021/2/26 11:24
 */
@Data
@ApiModel(value = "地址对象",description = "新增收货地址对象")
public class AddressAddRequest {

    /**
     * 是否默认收货地址：0->否；1->是
     */
    @ApiModelProperty(value = "是否是否默认收货地址，0->否；1->是",example = "0")
    //@JsonProperty("default_status")
    private Integer defaultStatus;

    /**
     * 收发货人姓名
     */
    @ApiModelProperty(value = "收发货人姓名",example = "钟小萌")
    //@JsonProperty("receive_name")
    private String receiveName;

    /**
     * 收货人电话
     */
    @ApiModelProperty(value = "收货人电话",example = "18370847419")
    private String phone;

    /**
     * 省/直辖市
     */
    @ApiModelProperty(value = "省/直辖市",example = "江西省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "城市",example = "赣州市")
    private String city;

    /**
     * 区
     */
    @ApiModelProperty(value = "区",example = "龙南市")
    private String region;

    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址",example = "窑头-20号")
    //@JsonProperty("detail_address")
    private String detailAddress;


}
