package com.zxm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 用户注册对象
 * @author zm
 * @date 2021/2/26 11:24
 */
@ApiModel(value = "用户注册对象",description = "用户注册请求对象")
@Data
public class UserRegisterRequest {

    @ApiModelProperty(value = "昵称",example = "zxm")
    private String name;

    @ApiModelProperty(value = "密码",example = "12345")
    private String pwd;

    @ApiModelProperty(value = "头像",example = "https://xdclass-1024shop-img.oss-cn-shenzhen.aliyuncs.com/user/2021/02/03/39473aa1029a430298ac2620dd819962.jpeg")
    //@JsonProperty("head_img")
    private String headImg;

    @ApiModelProperty(value = "用户个人性签名",example = "小菜鸡历险记")
    private String slogan;

    @ApiModelProperty(value = "0表示女，1表示男",example = "1")
    private Integer sex;

    @ApiModelProperty(value = "邮箱",example = "943335295@qq.com")
    private String mail;

    @ApiModelProperty(value = "验证码",example = "232343")
    private String code;

}
