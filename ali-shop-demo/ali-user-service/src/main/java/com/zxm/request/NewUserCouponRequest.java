package com.zxm.request;

import lombok.Data;

/**
 * 
 * @author zm
 * @date 2021/3/3 17:58
 */
@Data
public class NewUserCouponRequest {
    
    private long userId;
    
    private String name;
    
}
