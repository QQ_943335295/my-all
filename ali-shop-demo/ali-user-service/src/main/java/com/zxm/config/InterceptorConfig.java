package com.zxm.config;

import com.zxm.interceptor.LoginInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author zm
 * @date 2021/2/26 16:46
 */
@Configuration
@Slf4j
public class InterceptorConfig  implements WebMvcConfigurer {

    public LoginInterceptor loginInterceptor(){
        return new LoginInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(loginInterceptor())
                //拦截的路径
                .addPathPatterns("/api/user/*/**","/api/address/*/**")
                //排查不拦截的路径
                .excludePathPatterns("/api/user/*/register","/api/user/*/upload/header",
                        "/api/user/*/login",
                        "/api/notify/*/captcha","/api/notify/*/send/register/code");
    }

}
