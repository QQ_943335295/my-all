package com.zxm.mapper;

import com.zxm.model.AddressDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 电商-公司收发货地址表 Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2021-02-24
 */
public interface AddressMapper extends BaseMapper<AddressDO> {

}
