package com.zxm.service;

import com.zxm.enums.SendCodeEnum;
import com.zxm.util.JsonData;

/**
 * @author zm
 * @date 2021/2/25 15:20
 */
public interface NotifyService {
    JsonData sendRegisterCode(String to, SendCodeEnum sendCodeEnum);

    boolean checkMailCode(String mail, String code);
}
