package com.zxm.service.impl;

import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.SendCodeEnum;
import com.zxm.service.MailService;
import com.zxm.service.NotifyService;
import com.zxm.util.CommonUtils;
import com.zxm.util.JsonData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author zm
 * @date 2021/2/25 15:34
 */
@Service
@Slf4j
public class NotifyServiceImpl implements NotifyService {
    @Autowired
    private MailService mailService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public JsonData sendRegisterCode(String to, SendCodeEnum sendCodeEnum) {
        String registerCodeKey = String.format(sendCodeEnum.getKey(), to, sendCodeEnum.name());
        String oldRegisterCode = stringRedisTemplate.opsForValue().get(registerCodeKey);
        // 判断是否60秒内重复发送
        if(StringUtils.isNotBlank(oldRegisterCode)){
            long oldTime = Long.parseLong(oldRegisterCode.split("_")[1]);
            long betweenTime = System.currentTimeMillis() - oldTime;
            if(betweenTime < sendCodeEnum.getTimeLimit()){
                log.info("重复发送验证码,时间间隔:{} 秒", betweenTime / 1000);
                return JsonData.buildResult(BizCodeEnum.CODE_LIMITED);
            }
        }
        String registerCode = CommonUtils.getRandCode(4);
        String value = registerCode + "_" + System.currentTimeMillis();
        stringRedisTemplate.opsForValue().set(registerCodeKey, value, sendCodeEnum.getTimeOut(), TimeUnit.MILLISECONDS);
        mailService.sendSimpleMail(to,sendCodeEnum.getTitle(),String.format(sendCodeEnum.getContent(), registerCode));
        log.info("注册验证码发送成功:{} 秒", value);
        return JsonData.buildSuccess();
    }

    @Override
    public boolean checkMailCode(String mail, String code) {
        String registerCodeKey = String.format(SendCodeEnum.SEND_REGISTER_CODE.getKey(), mail, SendCodeEnum.SEND_REGISTER_CODE.name());
        String cacheValue = stringRedisTemplate.opsForValue().get(registerCodeKey);
        if(StringUtils.isNotBlank(cacheValue) && StringUtils.isNotBlank(code)){
            String cacheCode = cacheValue.split("_")[0];
            if(code.equalsIgnoreCase(cacheCode)){
                stringRedisTemplate.delete(registerCodeKey);
                return true;
            }
        }
        return false;
    }
}
