package com.zxm.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author zm
 * @date 2021/2/26 10:48
 */
public interface FileService {
    String uploadFileToOSS(MultipartFile file, String type);
}
