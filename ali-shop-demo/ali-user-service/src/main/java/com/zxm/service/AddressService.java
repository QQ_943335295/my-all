package com.zxm.service;

import com.zxm.model.AddressDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.request.AddressAddRequest;
import com.zxm.vo.AddressVO;

import java.util.List;

/**
 * <p>
 * 电商-公司收发货地址表 服务类
 * </p>
 *
 * @author zxm
 * @since 2021-02-24
 */
public interface AddressService extends IService<AddressDO> {

    /**
     * 查找指定地址详情
     * @param id
     * @return
     */
    AddressVO detail(Long id);

    /**
     * 新增收货地址
     * @param addressAddRequest
     */
    void add(AddressAddRequest addressAddRequest);

    /**
     * 根据id删除地址
     * @param addressId
     * @return
     */
    int del(int addressId);

    /**
     * 查找用户全部收货地址
     * @return
     */
    List<AddressVO> listUserAllAddress();
}
