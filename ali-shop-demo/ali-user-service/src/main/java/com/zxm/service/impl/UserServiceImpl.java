package com.zxm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxm.enums.BizCodeEnum;
import com.zxm.feign.CouponFeignService;
import com.zxm.interceptor.LoginInterceptor;
import com.zxm.mapper.UserMapper;
import com.zxm.model.LoginUser;
import com.zxm.model.UserDO;
import com.zxm.request.NewUserCouponRequest;
import com.zxm.request.UserLoginRequest;
import com.zxm.request.UserRegisterRequest;
import com.zxm.service.NotifyService;
import com.zxm.service.UserService;
import com.zxm.util.CommonUtils;
import com.zxm.util.JsonData;
import com.zxm.util.JwtUtils;
import com.zxm.vo.UserVO;
//import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2021-02-24
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, UserDO> implements UserService {

    @Autowired
    private NotifyService notifyService;
    @Autowired
    private CouponFeignService couponFeignService;

    /**
     * 1、邮箱验证码验证
     * 2、账号唯一性验证
     * 3、密码加密
     * 4、插入数据库
     * 5、发送新人福利
     * @param userRegisterRequest 用户注册福利
     * @return JsonData
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    //@GlobalTransactional
    @Override
    public JsonData register(UserRegisterRequest userRegisterRequest) {
        boolean checkFlag = false;
        if(StringUtils.isNotBlank(userRegisterRequest.getMail())){
            checkFlag = notifyService.checkMailCode(userRegisterRequest.getMail(), userRegisterRequest.getCode());
        }
        if(!checkFlag){
            return JsonData.buildResult(BizCodeEnum.CODE_ERROR);
        }
        checkFlag = checkMailUnique(userRegisterRequest.getMail());
        // 唯一性校验（数据库设置唯一索引 + 查库）
        if(checkFlag){
            UserDO userDO = new UserDO();
            BeanUtils.copyProperties(userRegisterRequest, userDO);
            userDO.setCreateTime(new Date());
            //生成秘钥
            userDO.setSecret("$1$" + CommonUtils.getStringNumRandom(8));
            //密码 + 加盐处理
            String cryptPwd = Md5Crypt.md5Crypt(userRegisterRequest.getPwd().getBytes(), userDO.getSecret());
            userDO.setPwd(cryptPwd);
            int rows = baseMapper.insert(userDO);
            log.info("rows:{},注册成功:{}", rows, userDO.toString());
            // 初始化信息，发放福利等 TODO
            userRegisterInitTask(userDO);
            //int i = 1 / 0;
            return JsonData.buildSuccess();
        } else {
            return JsonData.buildResult(BizCodeEnum.ACCOUNT_REPEAT);
        }
    }
    /**
     * 用户注册，初始化福利信息 TODO
     *
     * @param userDO
     */

    private void userRegisterInitTask(UserDO userDO) {
        NewUserCouponRequest request = new NewUserCouponRequest();
        request.setName(userDO.getName());
        request.setUserId(userDO.getId());
        JsonData jsonData = couponFeignService.getNewUserCoupon(request);
        log.info("发放新用户注册优惠券：{},结果:{}",request.toString(),jsonData.toString());

    }

    private boolean checkMailUnique(String mail) {
        LambdaQueryWrapper<UserDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserDO::getMail, mail);
        return count(wrapper) < 1;
    }

    @Override
    public JsonData login(UserLoginRequest userLoginRequest) {
        if(StringUtils.isNotBlank(userLoginRequest.getMail()) && StringUtils.isNotBlank(userLoginRequest.getPwd())){
            LambdaQueryWrapper<UserDO> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(UserDO::getMail, userLoginRequest.getMail());
            UserDO userDO = getOne(wrapper);
            if(userDO != null){
                String oldPwd = userDO.getPwd();
                String newPwd = Md5Crypt.md5Crypt(userLoginRequest.getPwd().getBytes(), userDO.getSecret());
                if(oldPwd.equals(newPwd)){
                    //生成token令牌
                    LoginUser userDTO = new LoginUser();
                    BeanUtils.copyProperties(userDO, userDTO);
                    String token = JwtUtils.geneJsonWebToken(userDTO);
                    return JsonData.buildSuccess(token);
                }
            }
        }
        return JsonData.buildResult(BizCodeEnum.ACCOUNT_PWD_ERROR);
    }


    @Override
    public JsonData findUserDetail() {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        UserDO userDO = getById(loginUser.getId());
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userDO, userVO);
        return JsonData.buildSuccess(userVO);
    }


}
