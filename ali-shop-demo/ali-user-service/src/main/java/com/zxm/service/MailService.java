package com.zxm.service;

/**
 * 邮件发送
 * @author zm
 * @date 2021/2/25 14:37
 */
public interface MailService {
    void sendSimpleMail(String to, String subject, String content);
}
