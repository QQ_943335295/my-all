package com.zxm.service;

import com.zxm.model.UserDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.request.UserLoginRequest;
import com.zxm.request.UserRegisterRequest;
import com.zxm.util.JsonData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2021-02-24
 */
public interface UserService extends IService<UserDO> {

    JsonData register(UserRegisterRequest userRegisterRequest);

    JsonData login(UserLoginRequest userLoginRequest);

    JsonData findUserDetail();
}
