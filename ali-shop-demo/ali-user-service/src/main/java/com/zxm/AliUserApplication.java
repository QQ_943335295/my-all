package com.zxm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author zm
 * @date 2021/2/24 15:55
 */
@MapperScan("com.zxm.mapper")
@EnableCaching
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@EnableTransactionManagement
public class AliUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(AliUserApplication.class, args);
    }
}
