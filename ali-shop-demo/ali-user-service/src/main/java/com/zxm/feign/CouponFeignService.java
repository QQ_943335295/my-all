package com.zxm.feign;

import com.zxm.request.NewUserCouponRequest;
import com.zxm.util.JsonData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "ali-coupon-service")
public interface CouponFeignService {

    /**
     * 新用户注册发放优惠券
     * @param newUserCouponRequest
     * @return
     */
    @PostMapping("/api/coupon/v1/get_new_user_coupon")
    JsonData getNewUserCoupon(@RequestBody NewUserCouponRequest newUserCouponRequest);
}