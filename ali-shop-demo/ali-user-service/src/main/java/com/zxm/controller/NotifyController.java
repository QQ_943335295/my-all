package com.zxm.controller;

import com.google.code.kaptcha.Producer;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.SendCodeEnum;
import com.zxm.service.NotifyService;
import com.zxm.util.CheckUtils;
import com.zxm.util.CommonUtils;
import com.zxm.util.JsonData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

/**
 * 通知模块
 * @author zm
 * @date 2021/2/25 10:28
 */
@Slf4j
@Api(tags = "通知模块")
@RestController
@RequestMapping("/api/notify/v1")
public class NotifyController {

    @Autowired
    private Producer captchaProducer;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private NotifyService notifyService;


    @ApiOperation("获取图形验证码")
    @GetMapping("captcha")
    public void getCaptcha(HttpServletRequest request, HttpServletResponse response){
        String code = captchaProducer.createText();
        stringRedisTemplate.opsForValue().set(getCaptchaKey(request), code, SendCodeEnum.SEND_SIMPLE_CAPTCHA_CODE.getTimeOut(), TimeUnit.MILLISECONDS);
        log.info("验证码为：{}", code);
        BufferedImage bufferedImage = captchaProducer.createImage(code);
        try(OutputStream outputStream = response.getOutputStream()) {
            ImageIO.write(bufferedImage, "jpg", outputStream);
            outputStream.flush();
        } catch (IOException e) {
            log.error("验证码生成出错：{}", e);
        }
    }

    private String getCaptchaKey(HttpServletRequest request){
        String ip = CommonUtils.getIpAddr(request);
        String userAgent = request.getHeader("User-Agent");
        String key =  String.format(SendCodeEnum.SEND_SIMPLE_CAPTCHA_CODE.getKey(), CommonUtils.MD5(ip + userAgent));
        log.info("getCaptchaKey={}", key);
        return key;
    }

    @ApiOperation("发送用户注册码邮件")
    @GetMapping("send/register/code")
    public JsonData sendRegisterCode(@ApiParam(value = "收件人", required = true) String to,
                                     @ApiParam(value = "图形验证码", required = true) String captcha,
                                     HttpServletRequest request){
        if(CheckUtils.isEmail(to)){
            String key = getCaptchaKey(request);
            String captchaCode = stringRedisTemplate.opsForValue().get(key);
            if(captchaCode != null && captchaCode.equalsIgnoreCase(captcha)){
                stringRedisTemplate.delete(key);
                return notifyService.sendRegisterCode(to, SendCodeEnum.SEND_REGISTER_CODE);
            }
        }
        return JsonData.buildResult(BizCodeEnum.CODE_TO_ERROR);

    }

}
