package com.zxm.controller;


import com.zxm.enums.BizCodeEnum;
import com.zxm.request.UserLoginRequest;
import com.zxm.request.UserRegisterRequest;
import com.zxm.service.FileService;
import com.zxm.service.UserService;
import com.zxm.util.JsonData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zxm
 * @since 2021-02-24
 */
@Api(tags = "用户管理模块")
@RestController
@RequestMapping("/api/user/v1")
public class UserController {
    @Autowired
    private FileService fileService;
    @Autowired
    private UserService userService;

    @ApiOperation("用户注册")
    @PostMapping("register")
    public JsonData register(@ApiParam(value = "用户注册信息实体", required = true)@RequestBody UserRegisterRequest userRegisterRequest){
        JsonData result = userService.register(userRegisterRequest);
        return result;
    }


    @ApiOperation("用户头像上传")
    @PostMapping("upload/header")
    public JsonData uploadHeader(@ApiParam(value = "用户头像文件", required = true)@RequestPart("file") MultipartFile file){
        String url = fileService.uploadFileToOSS(file, "header");
        return  url != null? JsonData.buildSuccess(url):JsonData.buildResult(BizCodeEnum.FILE_UPLOAD_USER_IMG_FAIL);
    }

    @ApiOperation("用户登陆")
    @PostMapping("login")
    public JsonData login(@ApiParam(value = "用户登陆实体", required = true)@RequestBody UserLoginRequest userLoginRequest){
        JsonData result = userService.login(userLoginRequest);
        return result;
    }

    @ApiOperation("用户详情")
    @PostMapping("detail")
    public JsonData findUserDetail(){
        JsonData result = userService.findUserDetail();
        return result;
    }

}

