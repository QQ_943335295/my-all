package com.zxm.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author zm
 * @date 2021/3/3 13:55
 */
public class CartVO {

    /**
     * 购物项
     */
    private List<CartItemVO> cartItems;

    /**
     * 购买总件数
     */
    private Integer totalNum;

    /**
     * 购物车总价格
     */
    private BigDecimal totalAmount;

    /**
     * 购物车实际支付价格
     */
    private BigDecimal realPayAmount;

    /**
     * 总件数
     * @return
     */
    public Integer getTotalNum() {
        if(this.cartItems!=null){
            int total = cartItems.stream().mapToInt(CartItemVO::getBuyNum).sum();
            return total;
        }
        return 0;
    }
    /**
     * 总价格
     * @return
     */
    public BigDecimal getTotalAmount() {
        BigDecimal amount = new BigDecimal("0");
        if(this.cartItems!=null){
            for(CartItemVO cartItemVO : cartItems){
                BigDecimal itemTotalAmount =  cartItemVO.getTotalAmount();
                amount = amount.add(itemTotalAmount);
            }
        }
        return amount;
    }

    /**
     * 购物车里面实际支付的价格
     * @return
     */
    public BigDecimal getRealPayAmount() {
        BigDecimal amount = new BigDecimal("0");
        if(this.cartItems!=null){
            for(CartItemVO cartItemVO : cartItems){
                BigDecimal itemTotalAmount =  cartItemVO.getTotalAmount();
                amount = amount.add(itemTotalAmount);
            }
        }
        return amount;
    }

    public List<CartItemVO> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItemVO> cartItems) {
        this.cartItems = cartItems;
    }
}
