package com.zxm.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @author zm
 * @date 2021/3/3 13:59
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BannerVO  {


    private Integer id;

    /**
     * 图片
     */
    private String img;

    /**
     * 跳转地址
     */
    private String url;

    /**
     * 权重
     */
    private Integer weight;


}
