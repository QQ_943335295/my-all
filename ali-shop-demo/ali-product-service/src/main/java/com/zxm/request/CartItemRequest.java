package com.zxm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class CartItemRequest {

    @ApiModelProperty(value = "商品id",example = "11")
    private long productId;

    @ApiModelProperty(value = "购买数量",example = "1")
    private int buyNum;
}
