package com.zxm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author zm
 * @date 2021/3/23 16:18
 */
@ApiModel(value = "商品子项")
@Data
public class OrderItemRequest {


    @ApiModelProperty(value = "商品id",example = "1")
    private long productId;

    @ApiModelProperty(value = "购买数量",example = "2")
    private int buyNum;
}
