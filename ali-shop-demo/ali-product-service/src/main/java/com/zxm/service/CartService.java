package com.zxm.service;

import com.zxm.request.CartItemRequest;
import com.zxm.vo.CartItemVO;
import com.zxm.vo.CartVO;

import java.util.List;

/**
 * @author zm
 * @date 2021/3/3 14:14
 */
public interface CartService {
    void addToCart(CartItemRequest cartItemRequest);

    void changeItemNum(CartItemRequest cartItemRequest);

    void clear();

    void deleteItem(long productId);

    CartVO getMyCart();

    List<CartItemVO> confirmOrderCartItem(List<Long> productIdList);
}
