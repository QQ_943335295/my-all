package com.zxm.service.impl;

import com.alibaba.fastjson.JSON;
import com.zxm.constant.CacheKey;
import com.zxm.enums.BizCodeEnum;
import com.zxm.exception.BizException;
import com.zxm.interceptor.LoginInterceptor;
import com.zxm.model.LoginUser;
import com.zxm.request.CartItemRequest;
import com.zxm.service.CartService;
import com.zxm.service.ProductService;
import com.zxm.vo.CartItemVO;
import com.zxm.vo.CartVO;
import com.zxm.vo.ProductVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zm
 * @date 2021/3/3 14:56
 */
@Service
@Slf4j
public class CartServiceImpl implements CartService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ProductService productService;
    @Override
    public void addToCart(CartItemRequest cartItemRequest) {
        //获取购物车
        BoundHashOperations<String, Object, Object> myCart = getCartCacheOps();
        // 判断购物车是否有这件商品
        Long productId = cartItemRequest.getProductId();
        int buyNum = cartItemRequest.getBuyNum();
        String productObj = (String) myCart.get(productId);
        if(productObj != null){
            CartItemVO cartItem = JSON.parseObject(productObj, CartItemVO.class);
            cartItem.setBuyNum(cartItem.getBuyNum() + buyNum);
            myCart.put(productId, JSON.toJSONString(cartItem));
        } else {
            //不存在则新建一个购物项
            CartItemVO cartItem = new CartItemVO();
            ProductVO productVO = productService.findDetailById(productId);
            cartItem.setAmount(productVO.getPrice());
            cartItem.setBuyNum(buyNum);
            cartItem.setProductId(productId);
            cartItem.setProductImg(productVO.getCoverImg());
            cartItem.setProductTitle(productVO.getTitle());
            myCart.put(productId, JSON.toJSONString(cartItem));
        }
    }

    @Override
    public void changeItemNum(CartItemRequest cartItemRequest) {
        //获取购物车
        BoundHashOperations<String, Object, Object> myCart = getCartCacheOps();
        // 判断购物车是否有这件商品
        long productId = cartItemRequest.getProductId();
        int buyNum = cartItemRequest.getBuyNum();
        String productObj = (String) myCart.get(productId);
        if(productObj != null){
            CartItemVO cartItem = JSON.parseObject(productObj, CartItemVO.class);
            cartItem.setBuyNum(buyNum);
            myCart.put(productId, JSON.toJSONString(cartItem));
        } else {
           throw new BizException(BizCodeEnum.CART_FAIL);
        }

    }

    @Override
    public CartVO getMyCart() {
        //获取购物车，并更新价格
        List<CartItemVO> cartItemVOList = parseCart();
        CartVO cartVO = new CartVO();
        cartVO.setCartItems(cartItemVOList);
        return cartVO;
    }

    private List<CartItemVO> parseCart() {
        BoundHashOperations<String, Object, Object> myCart = getCartCacheOps();
        List<Object> carts = myCart.values();
        List<CartItemVO> cartItemVOList = new ArrayList<>();
        List<Long> productIds = new ArrayList<>();
        carts.forEach(obj ->{
            CartItemVO cartItemVO = JSON.parseObject((String) obj, CartItemVO.class);
            cartItemVOList.add(cartItemVO);
            productIds.add(cartItemVO.getProductId());
        });
        // 更新购物车信息
        List<ProductVO> productVOList = productService.findProductsByIdBatch(productIds);
        //分组
        Map<Long,ProductVO> maps = productVOList.stream().collect(Collectors.toMap(ProductVO::getId, Function.identity()));
        cartItemVOList.stream().forEach(item->{
            ProductVO productVO = maps.get(item.getProductId());
            item.setProductTitle(productVO.getTitle());
            item.setProductImg(productVO.getCoverImg());
            item.setAmount(productVO.getPrice());
        });
        return cartItemVOList;
    }

    @Override
    public void clear() {
        //获取购物车
        redisTemplate.delete(getCartKey());
    }

    @Override
    public void deleteItem(long productId) {
        getCartCacheOps().delete(productId);
    }

    @Override
    public List<CartItemVO> confirmOrderCartItem(List<Long> productIdList) {
        //获取全部购物车的购物项
        List<CartItemVO> cartItemVOList = parseCart();
        //根据需要的商品id进行过滤，并清空对应的购物项
        cartItemVOList.stream().filter(cartItemVO -> {
            if(productIdList.contains(cartItemVO.getProductId())){
                deleteItem(cartItemVO.getProductId());
                return true;
            }
            return false;
        }).collect(Collectors.toList());
        return cartItemVOList;
    }

    private BoundHashOperations<String, Object, Object> getCartCacheOps() {
        return redisTemplate.boundHashOps(getCartKey());
    }

    private String getCartKey() {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        return String.format(CacheKey.CART_KEY, loginUser.getId());
    }


}
