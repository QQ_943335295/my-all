package com.zxm.service;

import com.zxm.model.ProductDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.model.ProductMessage;
import com.zxm.request.LockProductRequest;
import com.zxm.util.JsonData;
import com.zxm.vo.ProductVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2021-03-03
 */
public interface ProductService extends IService<ProductDO> {

    Map<String, Object> pageProductList(int page, int size);

    ProductVO findDetailById(long productId);

    /**
     * 根据id批量查询商品
     * @param productIdList
     * @return
     */
    List<ProductVO> findProductsByIdBatch(List<Long> productIdList);

    /**
     * 锁定商品库存
     * @param lockProductRequest
     * @return
     */
    JsonData lockProductStock(LockProductRequest lockProductRequest);

    /**
     * 释放商品库存
     * @param productMessage
     * @return
     */
    boolean releaseProductStock(ProductMessage productMessage);
}
