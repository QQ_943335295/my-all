package com.zxm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxm.model.BannerDO;
import com.zxm.mapper.BannerMapper;
import com.zxm.service.BannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxm.vo.BannerVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2021-03-03
 */
@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, BannerDO> implements BannerService {

    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public List<BannerVO> getBannerList() {
        List<BannerDO> bannerDOList =  bannerMapper.selectList(new LambdaQueryWrapper<BannerDO>().orderByAsc(BannerDO::getWeight));
        List<BannerVO> bannerVOList =  bannerDOList.stream().map(obj->{
            BannerVO bannerVO = new BannerVO();
            BeanUtils.copyProperties(obj,bannerVO);
            return bannerVO;
        }).collect(Collectors.toList());
        return bannerVOList;
    }

}
