package com.zxm.service;

import com.zxm.model.BannerDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.vo.BannerVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2021-03-03
 */
public interface BannerService extends IService<BannerDO> {

    List<BannerVO> getBannerList();

}
