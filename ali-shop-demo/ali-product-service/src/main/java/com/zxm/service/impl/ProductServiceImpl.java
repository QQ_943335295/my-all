package com.zxm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxm.config.RabbitMQConfig;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.ProductOrderStateEnum;
import com.zxm.enums.StockTaskStateEnum;
import com.zxm.exception.BizException;
import com.zxm.feign.ProductOrderFeignSerivce;
import com.zxm.mapper.ProductMapper;
import com.zxm.mapper.ProductTaskMapper;
import com.zxm.model.ProductDO;
import com.zxm.model.ProductMessage;
import com.zxm.model.ProductTaskDO;
import com.zxm.request.LockProductRequest;
import com.zxm.request.OrderItemRequest;
import com.zxm.service.ProductService;
import com.zxm.util.JsonData;
import com.zxm.vo.ProductVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import springfox.documentation.spring.web.json.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2021-03-03
 */
@Slf4j
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, ProductDO> implements ProductService {

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductTaskMapper productTaskMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private RabbitMQConfig rabbitMQConfig;
    @Autowired
    private ProductOrderFeignSerivce orderFeignSerivce;

    /**
     * 商品分页
     * @param page
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> pageProductList(int page, int size) {
        Page<ProductDO> pageInfo = new Page<>(page,size);
        IPage<ProductDO> productDOIPage =  productMapper.selectPage(pageInfo,null);
        Map<String,Object> pageMap = new HashMap<>(3);
        pageMap.put("records",productDOIPage.getTotal());
        pageMap.put("pages",productDOIPage.getPages());
        pageMap.put("data",productDOIPage.getRecords().stream().map(obj->beanProcess(obj)).collect(Collectors.toList()));
        return pageMap;
    }


    /**
     * 根据id找商品详情
     * @param productId
     * @return
     */
    @Override
    public ProductVO findDetailById(long productId) {
        ProductDO productDO = productMapper.selectById(productId);
        return beanProcess(productDO);
    }

    /**
     * 批量查询
     */
    @Override
    public List<ProductVO> findProductsByIdBatch(List<Long> productIdList) {
        // TODO 如果没有商品
        List<ProductVO> productVOList = new ArrayList<>();
        if(productIdList.size() > 0){
            List<ProductDO> productDOList =  productMapper.selectList(new LambdaQueryWrapper<ProductDO>().in(ProductDO::getId, productIdList));
            productVOList = productDOList.stream().map(obj->beanProcess(obj)).collect(Collectors.toList());
        }
        return productVOList;
    }

    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    @Override
    public JsonData lockProductStock(LockProductRequest lockProductRequest) {
        String outTradeNo = lockProductRequest.getOrderOutTradeNo();
        List<OrderItemRequest> itemList  = lockProductRequest.getOrderItemList();
        // 获取订单项商品详细信息
        List<Long> orderItemIds = itemList.stream().map( orderItemRequest -> orderItemRequest.getProductId()).collect(Collectors.toList());
        List<ProductDO> productDOList = productMapper.selectBatchIds(orderItemIds);
        Map<Long, ProductDO> productDOMap = productDOList.stream().collect(Collectors.toMap(ProductDO::getId,Function.identity()));
        itemList.forEach(orderItemRequest -> {
            // 锁定商品库存
            int rows = productMapper.lockProductStock(orderItemRequest.getProductId(), orderItemRequest.getBuyNum());
            if(rows != 1){
                throw new BizException(BizCodeEnum.ORDER_CONFIRM_LOCK_PRODUCT_FAIL);
            } else {
                //插入商品product_task
                ProductDO productDO = productDOMap.get(orderItemRequest.getProductId());
                ProductTaskDO productTaskDO = new ProductTaskDO();
                productTaskDO.setBuyNum(orderItemRequest.getBuyNum());
                productTaskDO.setLockState(StockTaskStateEnum.LOCK.name());
                productTaskDO.setProductId(orderItemRequest.getProductId());
                productTaskDO.setProductName(productDO.getTitle());
                productTaskDO.setOutTradeNo(outTradeNo);
                productTaskMapper.insert(productTaskDO);
                log.info("商品库存锁定-插入商品product_task成功:{}",productTaskDO);

                // 投递消息，锁定库存，以便数据回滚
                // 发送MQ延迟消息，介绍商品库存
                ProductMessage productMessage = new ProductMessage();
                productMessage.setOutTradeNo(outTradeNo);
                productMessage.setTaskId(productTaskDO.getId());

                rabbitTemplate.convertAndSend(rabbitMQConfig.getEventExchange(),rabbitMQConfig.getStockReleaseDelayRoutingKey(),productMessage);
                log.info("商品库存锁定信息延迟消息发送成功:{}",productMessage);
            }
        });
        return JsonData.buildSuccess();
    }

    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    @Override
    public boolean releaseProductStock(ProductMessage productMessage) {
        // 查询是否锁住了库存
        String orderNum = productMessage.getOutTradeNo();
        ProductTaskDO taskDO = productTaskMapper.selectOne(new LambdaQueryWrapper<ProductTaskDO>().eq(ProductTaskDO::getId,productMessage.getTaskId()));
        if(taskDO == null){
            log.warn("工作单不存在，消息体为:{}",productMessage);
        }

        if(taskDO.getLockState().equalsIgnoreCase(StockTaskStateEnum.LOCK.name())){
            // 如果库存被锁住，回查订单状态
            JsonData result  = orderFeignSerivce.queryProductOrderState(orderNum);
            if(result.getCode() == 0){
                String state = result.getData().toString();
                if(ProductOrderStateEnum.NEW.name().equalsIgnoreCase(state)){
                    //订单没被确认状态是NEW新建状态，则返回给消息队，列重新投递
                    log.warn("订单状态是NEW,返回给消息队列，重新投递:{}",productMessage);
                    return false;
                }
                //如果是已经支付
                if(ProductOrderStateEnum.PAY.name().equalsIgnoreCase(state)){
                    //如果已经支付，修改task状态为finish
                    taskDO.setLockState(StockTaskStateEnum.FINISH.name());
                    productTaskMapper.update(taskDO,new LambdaQueryWrapper<ProductTaskDO>().eq(ProductTaskDO::getId,productMessage.getTaskId()));
                    log.info("订单已经支付，修改库存锁定工作单FINISH状态:{}",productMessage);
                    return true;
                }
            }
            // 订单不存在，订单被取消，则需要更新优惠券状态变为NEW，把订单状态设置为CANCEL
            log.warn("订单不存在，或者订单被取消，确认消息,修改task状态为CANCEL,恢复商品库存,message:{}",productMessage);
            taskDO.setLockState(StockTaskStateEnum.CANCEL.name());
            productTaskMapper.update(taskDO,new QueryWrapper<ProductTaskDO>().eq("id",productMessage.getTaskId()));

            //恢复商品库存，集锁定库存的值减去当前购买的值
            productMapper.unlockProductStock(taskDO.getProductId(),taskDO.getBuyNum());
            return true;
        } else {
            log.warn("工作单状态不是LOCK,state={},消息体={}",taskDO.getLockState(),productMessage);
            return true;
        }
    }


    private ProductVO beanProcess(ProductDO productDO) {
        ProductVO productVO = new ProductVO();
        BeanUtils.copyProperties(productDO,productVO);
        productVO.setStock( productDO.getStock() - productDO.getLockStock());
        return productVO;
    }
}
