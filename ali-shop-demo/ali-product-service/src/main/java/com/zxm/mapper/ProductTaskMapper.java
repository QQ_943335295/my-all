package com.zxm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.ProductTaskDO;

/**
 *
 * @author zm
 * @date 2021/3/23 16:10
 */
public interface ProductTaskMapper extends BaseMapper<ProductTaskDO> {


}
