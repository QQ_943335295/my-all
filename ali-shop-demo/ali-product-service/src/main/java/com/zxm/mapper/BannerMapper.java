package com.zxm.mapper;

import com.zxm.model.BannerDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2021-03-03
 */
public interface BannerMapper extends BaseMapper<BannerDO> {

}
