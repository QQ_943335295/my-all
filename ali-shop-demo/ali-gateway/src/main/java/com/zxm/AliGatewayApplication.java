package com.zxm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 
 * @author zm
 * @date 2021/3/3 16:49
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AliGatewayApplication {
    public static void main(String [] args){
        SpringApplication.run(AliGatewayApplication.class,args);
    }
}
