package com.zxm.model;

import lombok.Data;

/**
 * 
 * @author zm
 * @date 2021/3/23 16:58
 */

@Data
public class OrderMessage {

    /**
     * 消息id
     */
    private Long messageId;

    /**
     * 订单号
     */
    private String outTradeNo;

}
