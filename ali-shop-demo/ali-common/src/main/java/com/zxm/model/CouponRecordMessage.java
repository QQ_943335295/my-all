package com.zxm.model;

import lombok.Data;

/**
 * 
 * @author zm
 * @date 2021/3/23 16:35
 */

@Data
public class CouponRecordMessage {


    /**
     * 消息id
     */
    private String messageId;

    /**
     * 订单号
     */
    private String outTradeNo;


    /**
     * 库存锁定任务id
     */
    private Long taskId;


}
