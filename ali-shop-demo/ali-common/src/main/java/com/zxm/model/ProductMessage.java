package com.zxm.model;

import lombok.Data;

/**
 * 
 * @author zm
 * @date 2021/3/23 17:06
 */

@Data
public class ProductMessage {


    /**
     * 消息队列id
     */
    private long messageId;

    /**
     * 订单号
     */
    private String outTradeNo;

    /**
     * 库存锁定taskId
     */
    private long taskId;
}
