package com.zxm.exception;

import com.zxm.util.JsonData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * "@ControllerAdvice"：注解定义全局异常处理类
 * "@ExceptionHandler"：指定自定义错误处理方法拦截的异常类型
 * 全局异常处理器
 * @author zm
 * @date 2021/2/24 17:26
 */
//@ControllerAdvice
@Slf4j
public class MyExceptionHandle {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public JsonData Handle(Exception e) {
        if (e instanceof BizException) {
            BizException bizException = (BizException) e;
            log.info("[业务异常]{}", e);
            return JsonData.buildCodeAndMsg(bizException.getCode(), bizException.getMsg());
        } else {
            log.info("[系统异常]{}", e);
            return JsonData.buildError("全局异常，未知错误");
        }
    }
}