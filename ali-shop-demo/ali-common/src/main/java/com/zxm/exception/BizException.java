package com.zxm.exception;

import com.zxm.enums.BizCodeEnum;
import lombok.Data;

/**
 * 自定义异常
 * @author zm
 * @date 2021/2/24 17:24
 */
@Data
public class BizException extends RuntimeException {

    private Integer code;
    private String msg;

    public BizException(Integer code, String message) {
        super(message);
        this.code = code;
        this.msg = message;
    }

    public BizException(BizCodeEnum bizCodeEnum) {
        super(bizCodeEnum.getMessage());
        this.code = bizCodeEnum.getCode();
        this.msg = bizCodeEnum.getMessage();
    }

}
