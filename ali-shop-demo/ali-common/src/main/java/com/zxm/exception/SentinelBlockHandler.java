package com.zxm.exception;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.zxm.enums.BizCodeEnum;
import com.zxm.util.JsonData;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author zm
 * @date 2021/3/25 21:02
 */
@Component
public class SentinelBlockHandler implements BlockExceptionHandler {


    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, BlockException e) throws Exception {
        JsonData jsonData = null;
        if(e instanceof FlowException){
            jsonData = JsonData.buildResult(BizCodeEnum.CONTROL_FLOW);
        }else if(e instanceof DegradeException){
            jsonData = JsonData.buildResult(BizCodeEnum.CONTROL_DEGRADE);
        } else if(e instanceof AuthorityException){
            jsonData = JsonData.buildResult(BizCodeEnum.CONTROL_AUTH);
        }
        httpServletResponse.setStatus(200);
        httpServletResponse.setContentType("application/json; charset=utf-8");
        httpServletResponse.getWriter().println(jsonData);
    }
}
