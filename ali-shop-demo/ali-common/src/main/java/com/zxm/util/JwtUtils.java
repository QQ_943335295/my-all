package com.zxm.util;

import com.zxm.model.LoginUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * @author zm
 * @date 2021/2/26 15:46
 */
public class JwtUtils {

    /**
     * token 过期时间，正常是7天，方便测试我们改为70
     */
    private static final long EXPIRE = 1000 * 60 * 60 * 24 * 7 * 10;

    /**
     * 加密的秘钥
     */
    private static final String SECRET = "ali-shop-demo";

    /**
     * 令牌前缀
     */
    private static final String TOKEN_PREFIX = "ali-shop-demo";

    /**
     * subject
     */
    private static final String SUBJECT = "ali-shop-demo";

    /**
     * 根据用户信息，生成令牌
     */
    public static String geneJsonWebToken(LoginUser user) {
        Long userId = user.getId();
        String token = Jwts.builder().setSubject(SUBJECT)
                .claim("headImg", user.getHeadImg())
                .claim("id", userId)
                .claim("name", user.getName())
                .claim("mail", user.getMail())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRE))
                .signWith(SignatureAlgorithm.HS256, SECRET).compact();
        token = TOKEN_PREFIX + token;
        return token;
    }

    /**
     * 校验token的方法
     */
    public static Claims checkJWT(String token) {
        try {
            final Claims claims = Jwts.parser().setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody();
            return claims;
        } catch (Exception e) {
            return null;
        }

    }
}
