package com.zxm.enums;

/**
 * @author zm
 * @date 2021/2/25 16:15
 */
public enum SendCodeEnum {
    /**
     * 通用图形验证码
     */
    SEND_SIMPLE_CAPTCHA_CODE("普通验证码", "", "user-service:captcha:%s", 0, 10 * 1000 * 1000),
    /**
     * 注册验证码信息
     */
    SEND_REGISTER_CODE("注册验证码", "ali-shop-demo的用户注册验证码为：%s，15分钟内有效，嘘！千万不要告诉别人哦！",
            "user-service:register:code:%s:%s", 60 * 1000, 15 * 1000 * 1000);

    private String title;
    private String content;
    private String key;
    private long timeLimit;
    private long timeOut;

    SendCodeEnum(String title, String content, String key, long timeLimit, long timeOut) {
        this.title = title;
        this.content = content;
        this.key = key;
        this.timeLimit = timeLimit;
        this.timeOut = timeOut;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getKey() {
        return key;
    }

    public long getTimeLimit() {
        return timeLimit;
    }

    public long getTimeOut() {
        return timeOut;
    }
}
