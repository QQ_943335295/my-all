package com.zxm.constant;

/**
 * @author zm
 * @date 2021/3/3 15:22
 */
public class CacheKey {
    /**
     * 购物车 hash 结果，key是用户唯一标识
     */
    public static final String CART_KEY = "CART:%s";

    /**
     * 提交表单的token key
     */
    public static final String SUBMIT_ORDER_TOKEN_KEY = "order:submit:%s";
}
