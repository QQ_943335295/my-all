package com.zxm.constant;

public class TimeConstant {

    /**
     * 订单过期时间：3分钟
     */
    public final static int ORDER_PAY_TIME_OUT = 1000 * 60 * 3;

    /**
     * 订单支付临界点：30秒
     */
    public final static int ORDER_PAY_TIME_LIMIT = 1000 * 30;

    /**
     * 锁定消息过期时间
     */
    public final static int ORDER_PAY_QUEUE_DELAY_TIME = 1000 * 60;

    /**
     * 分钟
     */
    public final static int TIME_MINUTE = 1000 * 60;

    /**
     * 表单令牌提交过期时间
     */
    public static final long SUBMIT_TIME = 1000 * 60 * 30;
}
