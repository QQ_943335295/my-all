package com.zxm.interceptor;

import com.zxm.model.LoginUser;
import com.zxm.util.JsonData;
import com.zxm.util.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author zm
 * @date 2021/2/26 16:17
 */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    public static ThreadLocal<LoginUser> threadLocal = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        response.setContentType("application/json; charset=utf-8");
        try {
            String accessToken = request.getHeader("token");
            if (accessToken == null) {
                accessToken = request.getParameter("token");
            }
            if (StringUtils.isNotBlank(accessToken)) {
                Claims claims = JwtUtils.checkJWT(accessToken);
                if (claims == null) {
                    //告诉登录过期，重新登录
                    response.getWriter().println(JsonData.buildError("登录过期，重新登录"));
                    return false;
                }
                Long id = Long.valueOf( claims.get("id").toString());
                String headImg = (String) claims.get("headImg");
                String mail = (String) claims.get("mail");
                String name = (String) claims.get("name");
                //TODO 用户信息传递(可以用request.setAttribute，可以用threadLocal)
                LoginUser loginUser = new LoginUser();
                loginUser.setId(id);
                loginUser.setName(name);
                loginUser.setMail(mail);
                loginUser.setHeadImg(headImg);
                threadLocal.set(loginUser);
                return true;
            }

        } catch (Exception e) {
            log.error("拦截器错误:{}",e);
        }
        response.getWriter().println(JsonData.buildError("登录过期，重新登录"));
        return false;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        threadLocal.remove();
    }
}