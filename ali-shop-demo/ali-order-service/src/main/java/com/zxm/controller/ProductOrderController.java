package com.zxm.controller;


import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import com.zxm.config.AliPayConfig;
import com.zxm.config.PayUrlConfig;
import com.zxm.constant.CacheKey;
import com.zxm.constant.TimeConstant;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.ClientType;
import com.zxm.enums.ProductOrderPayTypeEnum;
import com.zxm.interceptor.LoginInterceptor;
import com.zxm.model.LoginUser;
import com.zxm.request.ConfirmOrderRequest;
import com.zxm.request.RepayOrderRequest;
import com.zxm.service.ProductOrderService;
import com.zxm.util.CommonUtils;
import com.zxm.util.JsonData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author zm
 * @date 2021/3/3 16:40
 */
@Api("订单模块")
@RestController
@RequestMapping("/api/order/v1")
@Slf4j
public class ProductOrderController {

    @Autowired
    private ProductOrderService productOrderService;
    @Autowired
    private PayUrlConfig payUrlConfig;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/query_state")
    JsonData queryProductOrderState(@RequestParam("outTradeNo") String outTradeNo){
        String state = productOrderService.queryProductOrderState(outTradeNo);
        return StringUtils.isEmpty(state) ? JsonData.buildResult(BizCodeEnum.ORDER_CONFIRM_NOT_EXIST) :JsonData.buildSuccess(state);
    }

    @ApiOperation("提交订单")
    @PostMapping("/confirm")
    public void confirmOrder(@ApiParam("订单对象") @RequestBody ConfirmOrderRequest orderRequest, HttpServletResponse response){
        JsonData jsonData = productOrderService.confirmOrder(orderRequest);
        if(jsonData.getCode() == 0){
            String client = orderRequest.getClientType();
            String payType = orderRequest.getPayType();

            //如果是支付宝网页支付，都是跳转网页，APP除外
            if(payType.equalsIgnoreCase(ProductOrderPayTypeEnum.ALIPAY.name())){
                log.info("创建支付宝订单成功:{}",orderRequest.toString());
                if(client.equalsIgnoreCase(ClientType.H5.name())){
                    writeData(response,jsonData);
                }else if(client.equalsIgnoreCase(ClientType.APP.name())){
                    //APP SDK支付  TODO
                }
            } else if(payType.equalsIgnoreCase(ProductOrderPayTypeEnum.WECHAT.name())){
                //微信支付 TODO
            }
        } else {
            log.error("创建订单失败{}",jsonData.toString());
        }
    }


    @ApiOperation("提交订单")
    @PostMapping("/callback_pay")
    public String callbackPay(HttpServletRequest request){
        //将异步通知中收到的所有参数存储到map中
        Map<String,String> paramsMap = convertRequestParamsToMap(request);
        log.info("支付宝回调通知结果:{}",paramsMap);
        //调用SDK验证签名
        try {
            boolean signVerified = AlipaySignature.rsaCheckV1(paramsMap, AliPayConfig.ALIPAY_PUB_KEY, AliPayConfig.CHARSET, AliPayConfig.SIGN_TYPE);
            if(signVerified){
                JsonData jsonData = productOrderService.handlerOrderCallbackMsg(ProductOrderPayTypeEnum.ALIPAY,paramsMap);
                if(jsonData.getCode() == 0){
                    //通知结果确认成功，不然会一直通知，八次都没返回success就认为交易失败
                    return "success";
                }
            }
        } catch (AlipayApiException e) {
            log.info("支付宝回调验证签名失败:异常：{}，参数:{}",e,paramsMap);
        }
        return "failure";
    }


    /**
     * 分页查询我的订单列表
     * @param page
     * @param size
     * @param state
     * @return
     */
    @ApiOperation("分页查询我的订单列表")
    @GetMapping("/page")
    public JsonData pagePOrderList(
            @ApiParam(value = "当前页")  @RequestParam(value = "page", defaultValue = "1") int page,
            @ApiParam(value = "每页显示多少条") @RequestParam(value = "size", defaultValue = "10") int size,
            @ApiParam(value = "订单状态") @RequestParam(value = "state",required = false) String  state){
        Map<String,Object> pageResult = productOrderService.page(page,size,state);
        return JsonData.buildSuccess(pageResult);
    }



    @ApiOperation("重新支付订单")
    @PostMapping("/repay")
    public void repay(@ApiParam("订单对象") @RequestBody RepayOrderRequest repayOrderRequest, HttpServletResponse response){
        JsonData jsonData = productOrderService.repay(repayOrderRequest);
        if(jsonData.getCode() == 0){
            String client = repayOrderRequest.getClientType();
            String payType = repayOrderRequest.getPayType();
            //如果是支付宝网页支付，都是跳转网页，APP除外
            if(payType.equalsIgnoreCase(ProductOrderPayTypeEnum.ALIPAY.name())){
                log.info("重新支付订单成功:{}",repayOrderRequest.toString());
                if(client.equalsIgnoreCase(ClientType.H5.name())){
                    writeData(response,jsonData);
                }else if(client.equalsIgnoreCase(ClientType.APP.name())){
                    //APP SDK支付  TODO
                }
            } else if(payType.equalsIgnoreCase(ProductOrderPayTypeEnum.WECHAT.name())){
                //微信支付 TODO
            }

        } else {
            log.error("重新支付订单失败{}",jsonData.toString());
            try {
                response.setContentType("application/json; charset=utf-8");
                response.getWriter().println(jsonData);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 测试支付方法
     */
    @GetMapping("/test_pay")
    public void testAlipay(HttpServletResponse response) throws AlipayApiException, IOException {

        HashMap<String,String> content = new HashMap<>();
        //商户订单号,64个字符以内、可包含字母、数字、下划线；需保证在商户端不重复
        String no = UUID.randomUUID().toString();
        log.info("订单号:{}",no);
        content.put("out_trade_no", no);
        content.put("product_code", "FAST_INSTANT_TRADE_PAY");
        //订单总金额，单位为元，精确到小数点后两位
        content.put("total_amount", String.valueOf("111.99"));
        //商品标题/交易标题/订单标题/订单关键字等。 注意：不可使用特殊字符，如 /，=，&amp; 等。
        content.put("subject", "杯子");
        //商品描述，可空
        content.put("body", "好的杯子");
        // 该笔订单允许的最晚付款时间，逾期将关闭交易。取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。
        content.put("timeout_express", "5m");

        AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
        request.setBizContent(JSON.toJSONString(content));
        request.setNotifyUrl(payUrlConfig.getAlipayCallbackUrl());
        request.setReturnUrl(payUrlConfig.getAlipaySuccessReturnUrl());

        AlipayTradeWapPayResponse alipayResponse  = AliPayConfig.getAlipayClient().pageExecute(request);
        if(alipayResponse.isSuccess()){
            System.out.println("调用成功");
            String form = alipayResponse.getBody();
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(form);
            response.getWriter().flush();
            response.getWriter().close();
        } else {
            System.out.println("调用失败");
        }
    }

    @ApiOperation("获取提交订单令牌")
    @GetMapping("/get_token")
    public JsonData getOrderToken(){
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        String key = String.format(CacheKey.SUBMIT_ORDER_TOKEN_KEY,loginUser.getId());
        String token = CommonUtils.getStringNumRandom(32);
        stringRedisTemplate.opsForValue().set(key,token,TimeConstant.SUBMIT_TIME,TimeUnit.MINUTES);
        return JsonData.buildSuccess(token);
    }


    /**
     * 将request中的参数转换成Map
     * @param request
     * @return
     */
    private static Map<String, String> convertRequestParamsToMap(HttpServletRequest request) {
        Map<String, String> paramsMap = new HashMap<>(16);
        Set<Map.Entry<String, String[]>> entrySet = request.getParameterMap().entrySet();
        for (Map.Entry<String, String[]> entry : entrySet) {
            String name = entry.getKey();
            String[] values = entry.getValue();
            int size = values.length;
            if (size == 1) {
                paramsMap.put(name, values[0]);
            } else {
                paramsMap.put(name, "");
            }
        }
        return paramsMap;
    }


    private void writeData(HttpServletResponse response, JsonData jsonData) {
        try {
            response.setContentType("text/html;charset=UTF8");
            response.getWriter().write(jsonData.getData().toString());
            response.getWriter().flush();
            response.getWriter().close();
        }catch (IOException e){
            log.error("写出Html异常：{}",e);
        }

    }


}

