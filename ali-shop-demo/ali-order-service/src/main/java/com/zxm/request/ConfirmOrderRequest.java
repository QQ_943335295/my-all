package com.zxm.request;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author zm
 * @date 2021/3/3 16:48
 */
@Data
public class ConfirmOrderRequest {

    /**
     * 购物车使用的优惠券，集满减劵
     *
     * 注意：如果传空或者小于0,则不用优惠券
     */
    private Long couponRecordId;


    /**
     * 最终购买的商品列表
     * 传递id，购买数量从购物车中读取
     */
    private List<Long> productIdList;


    /**
     * 支付方式
     */
    private String payType;


    /**
     * 端类型
     */
    private String clientType;


    /**
     * 收货地址id
     */
    private long addressId;


    /**
     * 总价格，前端传递，后端需要验价
     */
    private BigDecimal totalAmount;


    /**
     * 实际支付的价格，
     * 如果用了优惠劵，则是减去优惠券后端价格，如果没的话，则是totalAmount一样
     */
    private BigDecimal realPayAmount;


    /**
     * 防重令牌
     */
    private String token;

}
