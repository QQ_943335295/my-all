package com.zxm.mq;

import com.rabbitmq.client.Channel;
import com.zxm.model.OrderMessage;
import com.zxm.service.ProductOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 *
 * @author zm
 * @date 2021/3/23 17:46
 */

@Slf4j
@Component
@RabbitListener(queues = "${mqconfig.order_close_queue}")
public class OrderMQListener {

    @Autowired
    private ProductOrderService productOrderService;

    /**
     * 重复消费-幂等性
     * <p>
     * 消费失败，重新入队后最大重试次数：
     * 如果消费失败，不重新入队，可以记录日志，然后插到数据库人工排查
     * <p>
     * 消费者这块还有啥问题，大家可以先想下，然后给出解决方案
     *
     * @param message
     * @param channel
     * @throws IOException
     */
    @RabbitHandler
    public void releaseCouponRecord(OrderMessage orderMessage, Message message, Channel channel) throws IOException {
        // 到了订单关闭时间，需要关闭订单
        log.info("监听到消息：releaseCouponRecord消息内容：{}", orderMessage);
        long msgTag = message.getMessageProperties().getDeliveryTag();
        boolean flag = productOrderService.closeProductOrder(orderMessage);
        try {
            if (flag) {
                //确认消息消费成功
                channel.basicAck(msgTag, false);
            } else {
                log.error("释放优惠券失败 flag=false,{}", orderMessage);
                channel.basicReject(msgTag, true);
            }

        } catch (IOException e) {
            log.error("释放优惠券记录异常:{},msg:{}", e, orderMessage);
            channel.basicReject(msgTag, true);
        }
    }
}