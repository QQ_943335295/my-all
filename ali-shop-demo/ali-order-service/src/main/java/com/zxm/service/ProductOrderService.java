package com.zxm.service;

import com.zxm.enums.ProductOrderPayTypeEnum;
import com.zxm.model.OrderMessage;
import com.zxm.request.ConfirmOrderRequest;
import com.zxm.request.RepayOrderRequest;
import com.zxm.util.JsonData;

import java.util.Map;

/**
 * 
 * @author zm
 * @date 2021/3/3 16:43
 */
public interface ProductOrderService {

    /**
     * 创建订单
     * @param orderRequest
     * @return
     */
    JsonData confirmOrder(ConfirmOrderRequest orderRequest);

    String queryProductOrderState(String outTradeNo);

    boolean closeProductOrder(OrderMessage orderMessage);

    JsonData handlerOrderCallbackMsg(ProductOrderPayTypeEnum alipay, Map<String, String> paramsMap);

    Map<String,Object> page(int page, int size, String state);

    JsonData repay(RepayOrderRequest repayOrderRequest);
}
