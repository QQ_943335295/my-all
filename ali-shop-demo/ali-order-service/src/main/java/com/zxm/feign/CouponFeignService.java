package com.zxm.feign;

import com.zxm.request.LockCouponRecordRequest;
import com.zxm.util.JsonData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author zm
 * @date 2021/3/24 11:09
 */
@FeignClient(name = "ali-coupon-service")
public interface CouponFeignService {
    /**
     * 查询用户的优惠券是否可用，防止水平权限
     */
    @GetMapping("/api/coupon_record/v1/detail/{record_id}")
    JsonData getCouponRecordDetail(@PathVariable("record_id") long recordId);

    /**
     * 锁定优惠券记录
     * @param lockCouponRecordRequest
     * @return
     */
    @PostMapping("/api/coupon_record/v1/lock_coupon_records")
    JsonData lockCouponRecords(@RequestBody LockCouponRecordRequest lockCouponRecordRequest);
}

