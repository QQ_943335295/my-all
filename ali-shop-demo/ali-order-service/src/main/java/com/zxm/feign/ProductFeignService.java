package com.zxm.feign;

import com.zxm.request.LockProductRequest;
import com.zxm.util.JsonData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "ali-product-service")
public interface ProductFeignService {

    /**
     * 根据商品id列表确定购物车信息
     * @return
     */
    @GetMapping("/api/cart/v1/confirm_order_cart_item")
    JsonData confirmOrderCartItem(@RequestBody List<Long> productIdList);

    /**
     * 锁定商品购物项库存
     * @param lockProductRequest
     * @return
     */
    @PostMapping("/api/product/v1/lock_products")
    JsonData lockProductStock(@RequestBody LockProductRequest lockProductRequest);


}
