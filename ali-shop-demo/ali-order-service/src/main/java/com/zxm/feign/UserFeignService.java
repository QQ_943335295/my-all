package com.zxm.feign;

import com.zxm.util.JsonData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ali-user-service")
public interface UserFeignService{
    /**
     * 根据id查找地址详情
     * @param addressId
     * @return
     */
    @GetMapping("/api/address/v1/find/{address_id}")
    JsonData detail(@PathVariable("address_id") long addressId);
}
