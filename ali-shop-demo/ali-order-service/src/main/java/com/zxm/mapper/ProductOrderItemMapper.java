package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.ProductOrderItemDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * @author zm
 * @date 2021/3/3 16:52
 */
public interface ProductOrderItemMapper extends BaseMapper<ProductOrderItemDO> {

    /**
     * 批量插入
     * @param list
     */
    void insertBatch( @Param("orderItemList") List<ProductOrderItemDO> list);
}
