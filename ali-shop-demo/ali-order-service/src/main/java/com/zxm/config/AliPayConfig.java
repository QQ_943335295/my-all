package com.zxm.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;

/**
 * @author zm
 * @date 2021/3/24 15:17
 */
public class AliPayConfig {
    /**
     * 支付宝网关地址  TODO
     */
    public static final  String PAY_GATEWAY="https://openapi.alipaydev.com/gateway.do";
    /**
     * 支付宝 APPID TODO
     */
    public static final  String APPID="2021000117626537";
    /**
     * 应用私钥 TODO
     */
    public static final String APP_PRI_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCe0pxFU8/DccApBu4XRYcHqpP6Ab7sNCbiE8a7BarQBInx8MYjMwBkQyEQv7mAQ/Dkh7PBLipuuwyY9M5LDQAPAFVbAbAeG7cyGwxarlOwuoaro3m6WaFNgWLjd6erXE2LjIBkowJoQZ2jbprlxG2yTXQ95sCbevvzd8a1aH4YHHjImDdIwMQWf4PivrLJBMLsKgpuN973m48xaIHDRjegF6678z6OzApT4gROIJRUOBYjz2KEV+6LBP5HkKcNi9K2m7NRMnTOdRiUTrQejl/xQqHcuHY8uZ8eLkgYdujVAu0t47HowcZF+jDtdklWKkbVSJzp230Z0sFFpUvQx3hrAgMBAAECggEAQTf7M/zWtEthvXc7DhNe/6bsXR2aaSSBUxUL5oDNQ96ZRuC4fHvVDVcDnP0eD63WngGbrxhmFFJ7OdRNYkRUYdShOF3ZFIQNFmZFDHfJpijyZEkdE4j60/jtwrq//35H2aY4kxrdQjKvqQ0iTnf7E9z6fvDi5vX5wGDPGG9wWDWvwarMADzyk2dExzkN2WfDzuR1ef07XtIY1y0uAURyuJk2qh3v/U8YZ8ImY1QqTmr5wFdh9RukVRxBH+mLg4rs2MQeyD+9H3ZkqJsM4m+hizuIf48DtzhmD7+6mUiRNPJIcLSZgogyXX5w5FPOQUHZ1o0bdfjGrofmgBYsASL7AQKBgQD3x9MPhD5x8tpVQFEA79M3FlRNcTQI9ONtdZuaPqd7ET1M0wCHDT07gtsKCPbhaKvpOC8G5RPm73+ujdreRgctZPEnpmoiOlS3El2ImjVw8VpGcE66UeKxlNrMpeWP2dwVKTCyy2E506tAOVE+o64mLs+7XWQlqXvFWVSgIacRnwKBgQCkF1j2p4GIeKGajvexEGN+h9R6pN2VNgjggP544jMX5+24Bfl4PHfFd5XpK/MU6YTKy85M5GMkD8u4ea9IgK8bFiWadwx0+723W5cq7p2wpYde0Y3tK7av+GB5FCx91klEDUAkCA2NnCfXG5iJbnNYu2jhhPfUej8KdVOZgLcdtQKBgBWnCaOee9J7VWcDwtBhJMT4l49Y6CIsPD+RenKFLR+NBx1ShwZGOuabbca3t9NE0VkFBt8EX7Pbk/3N+aVY20G1Q3miij3JeSOyYCgSyb6/6JKlN8wCYdwA1QnzKVdjw166Bb5qFWn3PEzVdWsS5KIDjzWhWKIN6Hn7b3C64B2jAoGASjKMdau0dMMnP74Z3iSs+I/u0i4uS8QNK4boijyuIccrxKRe87FT7yRt0uuelPwqoZCajkZPVgUWdyn+ymf1xeezxS7DoTobxQdTSYRwJWoT1hondPDpQJSs+tLWc2gr5Oug7yn9BuVvVmMSDEK3QY4slJ0NezRzwQTgcm6zXmkCgYB2wkbkwwR8bFRBGQjzkylCz9D1izkwsBEnhvB6jUcVnMjH7aTNwWsi6e9Spp3nfu9FtogqU/KO6zR4WgAHnVdXFsQTKUjen/S2xK0aav8DYbBuRINP8KYYZqCoYiZdHLHyMNaiC90WPOnNZcOvqnK30LK6QysiY3xRIlkYV90oYA==";
    /**
     * 支付宝公钥 TODO
     */
    public static final String ALIPAY_PUB_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkT1fs3XvC537FsDxygtW3lpeuXWqkP1K+td+mPhh9z1Em1WCd15P2NOAtQP755BTGfpWgfpAczSN+rfxZYvSERYsMiwYiOzEWFAwYDE5nPTEqv9gfolAiPDYXkUkYmSz/goNCswAXkQeoWZMM299yAI3ga0ggPZ7j8rB3XLxscjzUXz9/NANGN3ZOQBOTT5RUGo8jlMRBnbQgQAKLlrapeGxGckusiZpoeMS04vGjhmMl8+ROhBEV7gweWdo87J5YIptZwAlEPKkWLbPj1NvgdI2piz2p9v+3KH3X59z/RPFlYUwrN8K6Vr95lp9VxuqPuUpTh4z89i+RfyQmUAHaQIDAQAB";
    /**
     * 签名类型
     */
    public static final  String SIGN_TYPE="RSA2";
    /**
     * 字符编码
     */
    public static final  String CHARSET="UTF-8";
    /**
     * 返回参数格式
     */
    public static final  String FORMAT="json";
    /**
     * 构造函数私有化
     */
    private AliPayConfig(){
    }

    private volatile static AlipayClient instance = null;

    public static AlipayClient getAlipayClient(){
        if(instance == null){
            synchronized (AlipayClient.class){
                if(instance == null){
                    instance = new DefaultAlipayClient(PAY_GATEWAY,APPID,APP_PRI_KEY,FORMAT,CHARSET,ALIPAY_PUB_KEY,SIGN_TYPE);
                }
            }
        }
        return instance;
    }

}