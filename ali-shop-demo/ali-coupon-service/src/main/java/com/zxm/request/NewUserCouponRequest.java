package com.zxm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 小滴课堂,愿景：让技术不再难学
 *
 * @Description
 * @Author 二当家小D
 * @Remark 有问题直接联系我，源码-笔记-技术交流群
 * @Version 1.0
 **/
@ApiModel
@Data
public class NewUserCouponRequest {

    @ApiModelProperty(value = "用户Id",example = "19")
    private long userId;

    @ApiModelProperty(value = "名称",example = "zxm")
    private String name;

}
