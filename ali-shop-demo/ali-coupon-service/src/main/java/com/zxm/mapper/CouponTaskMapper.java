package com.zxm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxm.model.CouponTaskDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 
 * @author zm
 * @date 2021/3/23 16:06
 */
public interface CouponTaskMapper extends BaseMapper<CouponTaskDO> {

    /**
     * 批量插入
     * @param couponTaskDOList
     * @return
     */
    int insertBatch(@Param("couponTaskList") List<CouponTaskDO> couponTaskDOList);
}
