package com.zxm.mapper;

import com.zxm.model.CouponRecordDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2021-02-27
 */
public interface CouponRecordMapper extends BaseMapper<CouponRecordDO> {


    /**
     * 批量更新优惠券使用记录
     * @param lockCouponRecordIds
     * @return
     */
    int lockUseStateBatch(@Param("userId") Long userId, @Param("useState") String useState, @Param("lockCouponRecordIds") List<Long> lockCouponRecordIds);

    /**
     * 更新优惠券使用记录
     */
    void updateState(@Param("couponRecordId") Long couponRecordId, @Param("useState") String useState);

}
