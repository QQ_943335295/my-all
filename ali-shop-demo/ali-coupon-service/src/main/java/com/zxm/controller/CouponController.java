package com.zxm.controller;


import com.zxm.enums.CouponCategoryEnum;
import com.zxm.request.NewUserCouponRequest;
import com.zxm.service.CouponService;
import com.zxm.util.JsonData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zxm
 * @since 2021-02-27
 */
@Api(tags = "优惠券服务接口")
@RestController
@RequestMapping("/api/coupon/v1")
public class CouponController {

    @Autowired
    private CouponService couponService;

    @ApiOperation("分页查询优惠券")
    @GetMapping("page_coupon")
    public JsonData pageCouponList(@ApiParam(value = "当前页")
                                       @RequestParam(value = "page", defaultValue = "1") int page,
                                   @ApiParam(value = "每页显示多少条")
                                   @RequestParam(value = "size", defaultValue = "10") int size){
        Map<String,Object> pageMap = couponService.pageCouponList(page,size);
        return JsonData.buildSuccess(pageMap);
    }

    @ApiOperation("领取优惠券")
    @PostMapping("get/coupon/{coupon_id}")
    public JsonData getCoupon(@ApiParam(value = "优惠券id")
                                   @PathVariable(value = "coupon_id") long couponId){
        return couponService.getCouponByRedisson(couponId, CouponCategoryEnum.PROMOTION);
    }

    @ApiOperation("新人优惠券发放")
    @PostMapping("get_new_user_coupon")
    public JsonData getNewUserCoupon(@ApiParam(value = "新用户优惠券领取信息") @RequestBody NewUserCouponRequest newUserCouponRequest){
        return couponService.getNewUserCoupon(newUserCouponRequest);
    }

}

