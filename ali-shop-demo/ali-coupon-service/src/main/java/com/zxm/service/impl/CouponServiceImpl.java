package com.zxm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.CouponCategoryEnum;
import com.zxm.enums.CouponPublishEnum;
import com.zxm.enums.CouponStateEnum;
import com.zxm.exception.BizException;
import com.zxm.interceptor.LoginInterceptor;
import com.zxm.mapper.CouponMapper;
import com.zxm.mapper.CouponRecordMapper;
import com.zxm.model.CouponDO;
import com.zxm.model.CouponRecordDO;
import com.zxm.model.LoginUser;
import com.zxm.request.NewUserCouponRequest;
import com.zxm.service.CouponService;
import com.zxm.util.CommonUtils;
import com.zxm.util.JsonData;
import com.zxm.vo.CouponVO;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2021-02-27
 */
@Service
@Slf4j
public class CouponServiceImpl extends ServiceImpl<CouponMapper, CouponDO> implements CouponService {
    @Autowired
    private CouponRecordMapper couponRecordMapper;
    @Autowired
    private CouponMapper couponMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedissonClient redissonClient;

    @Override
    public Map<String, Object> pageCouponList(int page, int size) {
        Page<CouponDO> pageInfo = new Page<>(page, size);
        IPage<CouponDO> couponDOIPage = baseMapper.selectPage(pageInfo, new LambdaQueryWrapper<CouponDO>()
                .eq(CouponDO::getPublish, CouponPublishEnum.PUBLISH)
                .eq(CouponDO::getCategory, CouponCategoryEnum.PROMOTION)
                .orderByDesc(CouponDO::getCreateTime));
        Map<String, Object> pageMap = new HashMap<>(3);
        //总条数
        pageMap.put("records", couponDOIPage.getTotal());
        //总页数
        pageMap.put("pages", couponDOIPage.getPages());
        pageMap.put("data", couponDOIPage.getRecords().stream()
                .map(obj -> beanProcess(obj)).collect(Collectors.toList()));
        return pageMap;
    }

    /**
     * 领劵接口
     * 1、获取优惠券是否存在
     * 2、校验优惠券是否可以领取：时间、库存、超过限制
     * 3、扣减库存
     * 4、保存领劵记录
     * 始终要记得，羊毛党思维很厉害，社会工程学 应用的很厉害
     */
    @Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
    @Override
    public JsonData getCouponByRedisLua(long couponId, CouponCategoryEnum couponCategoryEnum) {
        CouponDO couponDO = getOne(new LambdaQueryWrapper<CouponDO>()
                .eq(CouponDO::getId, couponId).eq(CouponDO::getCategory, couponCategoryEnum));
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        /*** 分布式事务锁 ***/
        String key = "coupon:lock:" + couponId;
        String value = CommonUtils.generateUUID();
        // redis设置一个包含过期时间的值，判断其是否存在（存在则表示已加锁、设置过期是为了方式程序中断后，锁无法释放）
        boolean couponLock = stringRedisTemplate.opsForValue().setIfAbsent(key, value, Duration.ofSeconds(60));
        if(couponLock){
            log.info("加锁：{}", key);
            try {
                doGetCoupon(couponDO, loginUser);
            } finally {
                // 释放锁时，需要判断是否是当前线程加的锁
                String script = "if redis.call('get', KEYS[1]) == KEYS[2] then return redis.call('del', KEYS[1]) else return 0 end";
                long result = stringRedisTemplate.execute(new DefaultRedisScript<>(script, Long.class), Arrays.asList(key, value));
                log.info("解锁：{}, {}", key, result);
            }
        } else {
            log.info("加锁失败，睡眠100毫秒，自旋重试");
            //加锁失败，睡眠100毫秒，自旋重试 TODO TODO
//            try {
//                TimeUnit.MILLISECONDS.sleep(100L);
//            } catch (InterruptedException e) {}
//            return getCoupon( couponId);
        }
        /*** 分布式事务锁 ***/
        return JsonData.buildSuccess();
    }

    @Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
    @Override
    public JsonData getCouponByRedisson(long couponId, CouponCategoryEnum couponCategoryEnum) {
        CouponDO couponDO = getOne(new LambdaQueryWrapper<CouponDO>()
                .eq(CouponDO::getId, couponId).eq(CouponDO::getCategory, couponCategoryEnum));
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        /*** 分布式事务锁 ***/
        Lock lock = redissonClient.getLock("coupon:lock:" + couponId);
        log.info("加锁:{}", lock);
        lock.lock();
        try {
            doGetCoupon(couponDO, loginUser);
        } finally {
            lock.unlock();
        }
        int i = 1 / 0;
        /*** 分布式事务锁 ***/
        return JsonData.buildSuccess();
    }

    /**
     * 由于微服务调用未传入token，因此需要自己构造用户
     */
    @Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
    @Override
    public JsonData getNewUserCoupon(NewUserCouponRequest newUserCouponRequest) {
        LoginUser loginUser = new LoginUser();
        loginUser.setId(newUserCouponRequest.getUserId());
        loginUser.setName(newUserCouponRequest.getName());
        LoginInterceptor.threadLocal.set(loginUser);
        List<CouponDO> list = baseMapper.selectList(new LambdaQueryWrapper<CouponDO>().eq(CouponDO::getCategory, CouponCategoryEnum.NEW_USER));
        list.forEach(couponDO -> getCouponByRedisson(couponDO.getId(), CouponCategoryEnum.NEW_USER));
        return JsonData.buildSuccess();
    }

    public void doGetCoupon(CouponDO couponDO, LoginUser loginUser){
        //优惠券是否可以领取
        this.checkCoupon(couponDO,loginUser.getId());
        //构建领劵记录
        CouponRecordDO couponRecordDO = new CouponRecordDO();
        BeanUtils.copyProperties(couponDO,couponRecordDO);
        couponRecordDO.setCreateTime(new Date());
        couponRecordDO.setUseState(CouponStateEnum.NEW.name());
        couponRecordDO.setUserId(loginUser.getId());
        couponRecordDO.setUserName(loginUser.getName());
        couponRecordDO.setCouponId(couponDO.getId());
        couponRecordDO.setId(null);
        //扣减库存  TODO
        int rows = couponMapper.reduceStock(couponDO.getId());
        if(rows == 1){
            //库存扣减成功才保存记录
            couponRecordMapper.insert(couponRecordDO);
        }else {
            log.warn("发放优惠券失败:{},用户:{}",couponDO,loginUser);
            throw  new BizException(BizCodeEnum.COUPON_NO_STOCK);
        }
    }
    private void checkCoupon(CouponDO couponDO, Long userId) {
        if(couponDO == null){
            throw new BizException(BizCodeEnum.COUPON_NO_EXITS);
        }
        // TODO 高并发情况下可能出现库存减多的情况
        if(couponDO.getStock() < 1){
            throw new BizException(BizCodeEnum.COUPON_NO_STOCK);
        }
        //判断是否是否发布状态
        if(!couponDO.getPublish().equals(CouponPublishEnum.PUBLISH.name())){
            throw new BizException(BizCodeEnum.COUPON_GET_FAIL);
        }
        //是否在领取时间范围
        long time = System.currentTimeMillis();
        long start = couponDO.getStartTime().getTime();
        long end = couponDO.getEndTime().getTime();
        if(time<start || time>end){
            throw new BizException(BizCodeEnum.COUPON_OUT_OF_TIME);
        }
        // 判断是否已经超过领取限度
        int num = couponRecordMapper.selectCount(new LambdaQueryWrapper<CouponRecordDO>()
                .eq(CouponRecordDO::getUserId, userId).eq(CouponRecordDO::getCouponId, couponDO.getId()));
        if(num >= couponDO.getUserLimit()){
            throw new BizException(BizCodeEnum.COUPON_OUT_OF_LIMIT);
        }
    }

    private CouponVO beanProcess(CouponDO couponDO) {
        CouponVO couponVO = new CouponVO();
        BeanUtils.copyProperties(couponDO, couponVO);
        return couponVO;
    }
}
