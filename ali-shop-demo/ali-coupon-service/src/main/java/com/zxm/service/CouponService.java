package com.zxm.service;

import com.zxm.enums.CouponCategoryEnum;
import com.zxm.model.CouponDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.request.NewUserCouponRequest;
import com.zxm.util.JsonData;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2021-02-27
 */
public interface CouponService extends IService<CouponDO> {

    Map<String,Object> pageCouponList(int page, int size);

    JsonData getCouponByRedisLua(long couponId,  CouponCategoryEnum couponCategoryEnum);

    JsonData getCouponByRedisson(long couponId,  CouponCategoryEnum couponCategoryEnum);

    JsonData getNewUserCoupon(NewUserCouponRequest newUserCouponRequest);
}
