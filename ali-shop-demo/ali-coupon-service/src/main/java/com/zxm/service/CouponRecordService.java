package com.zxm.service;

import com.zxm.model.CouponRecordDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.model.CouponRecordMessage;
import com.zxm.request.LockCouponRecordRequest;
import com.zxm.util.JsonData;
import com.zxm.vo.CouponRecordVO;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2021-02-27
 */
public interface CouponRecordService extends IService<CouponRecordDO> {
    /**
     * 分页查询领劵记录
     */
    Map<String,Object> page(int page, int size);

    CouponRecordVO findById(long recordId);

    JsonData lockCouponRecords(LockCouponRecordRequest recordRequest);

    boolean releaseCouponRecord(CouponRecordMessage recordMessage);
}
