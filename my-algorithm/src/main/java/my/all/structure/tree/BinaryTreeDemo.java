package my.all.structure.tree;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

/**
 * 二叉树
 * @author zxm
 * @date 2023/2/3
 */
public class BinaryTreeDemo<E extends Comparable> {

    private TreeNode<E> root;


    public void addNode(E e){
        if (root == null){
            root = new TreeNode<>(e);
        } else {
            root.add(e);
        }
    }

    public E get(TreeNode treeNode, E e){
        if (treeNode == null){
            return null;
        }
        if (treeNode.element.compareTo(e) > 0){
            return get(treeNode.left, e);
        } else if (treeNode.element.compareTo(e) < 0){
            return get(treeNode.right, e);
        } else {
            return (E) treeNode.element;
        }
    }

    public int getDeep(TreeNode treeNode){
        if (treeNode == null){
            return 0;
        } else {
            int left = getDeep(treeNode.left);
            int right = getDeep(treeNode.right);
            return Math.max(left, right) + 1;
        }
    }




    public void showDeepNode(TreeNode treeNode){
        Queue<TreeNode> queue = new ArrayDeque<>();
        if (treeNode != null){
            queue.add(treeNode);
        }
        while (queue.size() > 0){
            TreeNode peek = queue.poll();
            System.err.println(peek.element);
            if (peek.left != null){
                queue.add(peek.left);
            }
            if (peek.right != null){
                queue.add(peek.right);
            }
        }
    }

    public void showReserveDeepNode(TreeNode treeNode){
        Queue<TreeNode> queue = new ArrayDeque<>();
        if (treeNode != null){
            queue.add(treeNode);
        }
        Stack<TreeNode> stack = new Stack<>();
        while (queue.size() > 0){
            TreeNode peek = queue.poll();
            stack.add(peek);
            if (peek.left != null){
                queue.add(peek.left);
            }
            if (peek.right != null){
                queue.add(peek.right);
            }
        }
        while (stack.size() > 0){
            System.err.println(stack.pop().element);
        }
    }

    /**
     * 前序遍历
     */
    public void preTraversal(){
        this.root.preTraversal();
    }

    /**
     * 中序遍历
     */
    public void infixTraversal(){
        this.root.infixTraversal();
    }

    /**
     * 后序遍历
     */
    public void postTraversal(){
        this.root.postTraversal();
    }


    public static void main(String[] args) {
        int[]arr={6,4,9,2,3,5};
        BinaryTreeDemo<Integer> binaryTree = new BinaryTreeDemo();
        for(int i=0;i<arr.length;i++){
            binaryTree.addNode(arr[i]);
        }
        System.err.println("root节点的信息是:"+ binaryTree.getRoot());
        System.err.println("广度遍历：" );
        binaryTree.showDeepNode(binaryTree.root);
        System.err.println("逆序广度遍历：" );
        binaryTree.showReserveDeepNode(binaryTree.root);
        System.err.println("前序遍历结果：");
        binaryTree.preTraversal(); //6 4 2 9
        System.err.println("中序遍历结果：");
        binaryTree.infixTraversal(); //2 4 6 9
        System.err.println("后序遍历结果：");
        binaryTree.postTraversal(); //2 4 9 6
        System.err.println("查找节点  "+binaryTree.get(binaryTree.getRoot(),8));
        System.err.println("二叉树最大深度为："+binaryTree.getDeep(binaryTree.root));
    }

    private TreeNode<E> getRoot() {
        return root;
    }

}

class TreeNode<E extends Comparable>{
    public E element;
    public TreeNode<E> left;
    public TreeNode<E> right;

    public TreeNode(E element) {
        this.element = element;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "element=" + element +
                ", left=" + left +
                ", right=" + right +
                '}';
    }

    public void add(E e){
        if (e == null){
            return;
        }
        if (this.element.compareTo(e) > 0){
            if (this.left == null){
                this.left = new TreeNode<>(e);
            } else {
                this.left.add(e);
            }
        }
        if (this.element.compareTo(e) < 0){
            if (this.right == null){
                this.right = new TreeNode<>(e);
            } else {
                this.right.add(e);
            }
        }
    }

    public void preTraversal() {
        System.err.println(this.element);
        if (this.left != null){
            this.left.preTraversal();
        }
        if (this.right != null){
            this.right.preTraversal();
        }
    }

    public void infixTraversal() {
        if (this.left != null){
            this.left.infixTraversal();
        }
        System.err.println(this.element);
        if (this.right != null){
            this.right.infixTraversal();
        }
    }

    public void postTraversal() {
        if (this.left != null){
            this.left.postTraversal();
        }
        if (this.right != null){
            this.right.postTraversal();
        }
        System.err.println(this.element);
    }
}