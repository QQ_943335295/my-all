package my.all.structure.list;

/**
 * 链表课程
 * @author zxm
 * @date 2023/1/12
 */
public class LinkListCourse {
    private int id;
    private String courseName;

    public LinkListCourse(int id, String courseName) {
        this.id = id;
        this.courseName = courseName;
    }

    @Override
    public String toString() {
        return "[ 课程ID：" + id +
                "课程名：" + courseName +" ]";
    }

    @Override
    public boolean equals(Object obj) {
        LinkListCourse linkListCourse = (LinkListCourse) obj;
        if (linkListCourse.id == this.id){
            return true;
        }
        return false;
    }
}
