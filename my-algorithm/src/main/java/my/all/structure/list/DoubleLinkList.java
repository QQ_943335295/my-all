package my.all.structure.list;

/**
 * 单向链表
 * @author zxm
 * @date 2023/1/12
 */
public class DoubleLinkList<T> {

    private DoubleListNode<T> head;
    private int length;

    public DoubleLinkList() {
        this.head = new DoubleListNode<>(null);
        this.length = 0;
    }

    /** 添加节点 */
    public void add(T data){
        // 辅助指针，用于记录遍历时的当前位置
        DoubleListNode<T> cur = this.head;
        while (cur.next != null){
            cur = cur.next;
        }
        cur.next = new DoubleListNode<T>(data, cur);
        this.length++;
    }

    /** 修改节点 */
    public void update(T oldData, T newData){
        // 辅助指针，用于记录遍历时的当前位置
        DoubleListNode<T> cur = this.head;
        boolean isExist = false;
        while (cur.next != null){
            cur = cur.next;
            if (cur.data.equals(oldData)){
                isExist = true;
                break;
            }
        }
        if (isExist){
            cur.data = newData;
        } else {
            throw new RuntimeException("node not exist !");
        }
    }

    /** 删除节点 */
    public void remove(T removeData){
        // 辅助指针，用于记录遍历时的当前位置
        DoubleListNode<T> cur = this.head;
        boolean isExist = false;
        while (cur.next != null){
            cur = cur.next;
            if (cur.data.equals(removeData)){
                isExist = true;
                break;
            }
        }
        if (isExist){
            cur.pre.next = cur.next;
            if (cur.next != null){
                cur.next.pre = cur.pre;
            }
            this.length --;
        } else {
            throw new RuntimeException("node not exist !");
        }
    }

    /** 遍历输出 */
    public void showCourse(){
        // 辅助指针，用于记录遍历时的当前位置
        DoubleListNode<T> cur = this.head;
        while (cur.next != null){
            cur = cur.next;
            System.err.println(cur);
        }
    }

    /** 获取倒数第K个节点 */
    public DoubleListNode<T> getLastNode(int index){
        if (index <= 0 || index > this.length){
            throw new RuntimeException("index error ！");
        }
        // 辅助指针，用于记录遍历时的当前位置
        DoubleListNode<T> cur = this.head;
        for (int i = 0; i < this.length - index + 1; i ++){
            cur = cur.next;
        }
        return cur;
    }

    /** 获取长度 */
    public int getLength() {
        return this.length;
    }

    static class DoubleListNode<T>{
        private T data;
        private DoubleListNode<T> pre;
        private DoubleListNode<T> next;

        public DoubleListNode(T data) {
            this.data = data;
        }

        public DoubleListNode(T data, DoubleListNode<T> pre) {
            this.data = data;
            this.pre = pre;
        }

        @Override
        public String toString() {
            return "DoubleListNode{" +
                    "data=" + data +
                    ", next=" + next +
                    '}';
        }
    }


    public static void main(String[] args) {
        DoubleLinkList<LinkListCourse> list = new DoubleLinkList<>();
        LinkListCourse data1 = new LinkListCourse(1, "算法");
        LinkListCourse data2 = new LinkListCourse(2, "操作系统");
        LinkListCourse data3 = new LinkListCourse(3, "网络");
        LinkListCourse data4 = new LinkListCourse(4, "编码");
        list.add(data1);
        list.add(data2);
        list.add(data3);
        list.add(data4);
        list.showCourse();
        System.err.println("=============================================================================");
        DoubleListNode<LinkListCourse> lastNode = list.getLastNode(1);
        System.err.println(lastNode);
        System.err.println("=============================================================================");
        LinkListCourse oldData2 = new LinkListCourse(2, "操作系统");
        LinkListCourse newData2 = new LinkListCourse(2, "操作系统原理");
        list.update(oldData2, newData2);
        list.showCourse();
        System.err.println("=============================================================================");
        LinkListCourse removeData = new LinkListCourse(4, "操作系统");
        list.remove(removeData);
        list.showCourse();
        System.err.println("=============================================================================");
        System.err.println(list.getLength());
    }

}




