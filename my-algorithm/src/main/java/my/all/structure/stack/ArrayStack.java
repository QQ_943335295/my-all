package my.all.structure.stack;

/**
 * 数组实现栈——先进后出
 * @author zxm
 * @date 2023/2/2
 */
public class ArrayStack<E> {

    /** 顶部指针 */
    private int top;
    /** 当前大小 */
    private int maxSize;
    /** 数据 */
    private Object[] stack;

    public ArrayStack(int maxSize) {
        this.maxSize = maxSize;
        this.top = 0;
        this.stack = new Object[maxSize];
    }

    /**
     * 判空
     */
    public boolean isEmpty(){
        return this.top == 0;
    }

    /**
     * 判满
     */
    public boolean isFull(){
        return this.top == maxSize;
    }

    /**
     * 入栈
     */
    public void push(E e){
        if (isFull()){
            throw new RuntimeException("stack full !");
        }
        stack[top] = e;
        top++;
    }

    /**
     * 出栈
     */
    public E pop(){
        if (isEmpty()){
            throw new RuntimeException("stack full !");
        }
        top--;
        return (E) stack[top];
    }

    /**
     * 显示
     */
    public void show(){
        if (isEmpty()){
            throw new RuntimeException("stack is empty !");
        }
        System.err.print("size:" + top + "，元素：");
        for (int i = 0; i < top; i++){
            System.err.print(this.stack[i] + " ");
        }
        System.err.println();
    }

    public static void main(String[] args) {
        ArrayStack<Integer>  arrayStack = new ArrayStack<>(8);
        // arrayStack.show();
        arrayStack.push(1);
        arrayStack.push(2);
        arrayStack.push(3);
        arrayStack.push(4);
        arrayStack.push(5);
        arrayStack.show();
        arrayStack.push(5);
        arrayStack.push(6);
        System.err.println(arrayStack.pop());
        System.err.println(arrayStack.pop());
        arrayStack.show();
        arrayStack.push(5);
        arrayStack.push(6);
        arrayStack.push(5);
        arrayStack.push(6);
    }

}
