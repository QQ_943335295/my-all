package my.all.structure.queue;

/**
 * 线性队列-先进先出
 * @author zxm
 * @date 2023/2/2
 */
public class ListQueue<E> {

    /** 当前大小 */
    private int maxSize;
    /** 当前大小 */
    private int size;
    /** 头部指针 */
    private int head;
    /** 队尾指针 */
    private int tail;
    /** 数据 */
    private Object[] queue;

    public ListQueue(int maxSize) {
        if (maxSize < 0 || maxSize > Integer.MAX_VALUE - Byte.MAX_VALUE){
            throw new RuntimeException("maxSize illegal !");
        }
        this.maxSize = maxSize;
        this.size = 0;
        this.head = 0;
        this.tail = 0;
        this.queue = new Object[maxSize];
    }

    /**
     * 判断队列是否为空
     */
    public boolean isEmpty(){
        return this.head == this.tail;
    }

    /**
     * 判断队满
     */
    public boolean isFull(){
        return this.tail == this.maxSize;
    }

    /**
     * 添加元素到队列中
     */
    public void add(E e){
        if (isFull()){
            throw new RuntimeException("queue is full !");
        }
        this.queue[tail] = e;
        this.tail++;
        this.size++;
    }

    /**
     * 出队列 并返回出队列的元素
     */
    public E get(){
        if (isEmpty()){
            throw new RuntimeException("queue is empty !");
        }
        E e = (E) this.queue[head];
        this.head++;
        this.size--;
        return e;
    }

    /**
     * 遍历队列
     */
    public void show(){
        if (isEmpty()){
            throw new RuntimeException("queue is empty !");
        }
        System.err.print("size:" + size + "，元素：");
        for (int i = head; i < tail; i++){
            System.err.print(this.queue[i] + " ");
        }
        System.err.println();
    }

    public static void main(String[] args) {
        ListQueue<String> listQueue = new ListQueue<>(8);
        listQueue.add("zxm");
        listQueue.add("lxy");
        listQueue.add("jy");
        listQueue.add("zm");
        listQueue.add("my");
        listQueue.show();
        System.err.println(listQueue.get());
        System.err.println(listQueue.get());
        listQueue.show();
        listQueue.add("me");
        listQueue.add("lxp");  listQueue.add("me");
        listQueue.add("lxp");  listQueue.add("me");
        listQueue.add("lxp");
        listQueue.show();
    }

}
