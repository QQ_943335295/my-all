package my.all.structure.sequence;

/**
 * 顺序表基本实现
 * @author zxm
 * @date 2022/12/20
 */
public class SequenceList<E> {

    /** 数据项 */
    private Object[] elementData;

    /** 包含数据个数 */
    private int size;

    /** 构造函数 */
    public SequenceList(int initialCapacity) {
        this.elementData = new Object[initialCapacity];
        this.size = 0;
    }

    /** 判空 */
    public boolean isEmpty(){
        return this.size == 0;
    }

    /** 判满 */
    public boolean isFull(){
        return this.size == this.elementData.length;
    }

    /** 添加元素 */
    public void add(E e){
        if (isFull()){
            // 链表已满，需要扩容
            growCapacity(this.elementData.length * 2);
        }
        this.elementData[this.size] = e;
        this.size++;
    }

    /** 指定位置插入元素 */
    public void add(int index, E e){
        checkIndex(index);
        if (isFull()){
            // 链表已满，需要扩容
            growCapacity(this.elementData.length * 2);
        }
        for (int i = this.size; i > index; i--){
            this.elementData[i] = this.elementData[i - 1];
        }
        this.elementData[index] = e;
        this.size++;
    }

    /** 删除指定位置的元素 */
    public void remove(int index){
        checkIndex(index);
        for (int i = index ; i < this.size; i++){
            this.elementData[i] = this.elementData[i + 1];
        }
        this.size--;
    }

    /** 修改指定位置的元素 */
    public void set(int index, E e){
        checkIndex(index);
        this.elementData[index] = e;
    }

    /** 获取指定位置的元素 */
    public E get(int index){
        checkIndex(index);
        return (E) this.elementData[index];
    }

    /** 显示 */
    public void show(){
        System.err.print( "sze:" + size + "   ");
        System.err.print( "[    ");
        for (int i = 0; i < this.size; i++){
            System.err.print(i + "---" + get(i) + "   ");
        }
        System.err.print( "] ");
        System.err.println();
    }

    /** 长度 */
    public int getLength(){
        return this.size;
    }

    /** 扩容 */
    private void growCapacity(int newCapacity){
        Object[] temp = this.elementData;
        Object[] newSequenceList = new Object[newCapacity];
        for (int i = 0; i < this.size; i++){
            newSequenceList[i] = temp[i];
        }
        this.elementData = newSequenceList;
    }

    /** 校验索引 */
    private void checkIndex(int index){
        if (index < 0 || index > this.size){
            throw new RuntimeException("invalid index");
        }
    }




    public static void main(String[] args) {
//        List<Integer> arrayList = new ArrayList<>(5);
//        arrayList.add(3, 5);
//        System.err.println(arrayList);

        SequenceList<String> list = new SequenceList<>(4);
        list.add("a");
        list.add("b");
        list.add(2, "c");
        list.add("c");
        list.add("d");
        list.add(5, "c");
        list.show();
        list.remove(0);
        list.remove(2);
        list.set(3, "e");
        list.show();

    }

}
