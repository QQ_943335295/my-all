package my.all.structure.table;


/**
 * 哈希mapper
 * @author zxm
 * @date 2023/2/3
 */
public class HashTableDemo<K, V> {
    public HashTableNodeList<K, V>[] table;
    public int maxSize;

    public HashTableDemo(int maxSize) {
        this.maxSize = maxSize;
        // 初始化数组
        this.table = new HashTableNodeList[maxSize];
        //对数组中的每一条链表都要初始化
        for(int i=0; i<maxSize; i++){
            table[i]= new HashTableNodeList<K, V>();
        }
    }

    public void add(K key, V value){
        if (key == null){
            throw new RuntimeException("key is null");
        }
        int index = key.hashCode() % maxSize;
        table[index].add(key, value);
    }

    public V get(K key){
        int index = key.hashCode() % maxSize;
        return table[index].get(key);
    }

    public void show(){
        for (int i = 0; i < maxSize; i++){
            table[i].show();
        }
    }


    public static void main(String[] args) {
        HashTableDemo<String, String> table = new HashTableDemo<>(3);
        for (int i = 0; i < 10; i++){
            table.add(String.valueOf(i), "zxm" + String.valueOf(i));
        }
        table.show();
        System.err.println(table.get("6"));
        System.err.println(table.get("7"));
    }

}

class HashTableNodeList<K, V>{
    private HashTableNode<K, V> head;

    public void add(K key, V value){
        if (head == null){
            head = new HashTableNode<>(key, value);
            return;
        }
        HashTableNode<K, V> cur= head;
        while (cur.next != null){
            cur = cur.next;
        }
        cur.next = new HashTableNode<>(key, value);
    }


    public V get(K key){
        if (head == null){
            throw new RuntimeException("link empty !");
        }
        HashTableNode<K, V> cur= head;
        while (cur != null){
            if (cur.key.equals(key)){
                return cur.value;
            }
            cur = cur.next;
        }
        throw new RuntimeException("data not exist !");
    }

    public void show(){
        HashTableNode<K, V> cur= head;
        System.err.println("--------------------");
        while (cur != null){
            System.err.println(cur.key + "----" + cur.value);
            cur = cur.next;
        }
        System.err.println("--------------------");
    }


}

class HashTableNode<K, V>{
    public K key;
    public V value;
    public HashTableNode<K, V> next;

    public HashTableNode(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "HashTableNode{" +
                "key=" + key +
                ", value=" + value +
                ", next=" + next +
                '}';
    }
}
