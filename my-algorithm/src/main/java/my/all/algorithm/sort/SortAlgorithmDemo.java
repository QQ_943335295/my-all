package my.all.algorithm.sort;

import java.util.Arrays;

/**
 * 排序算法案例
 * @author zxm
 * @date 2023/2/2
 */
public class SortAlgorithmDemo {

    public static void main(String[] args) {
        int[] arr1 = new int[]{1, 3, 9, 2, 7, 4};
        int [] arr2 = {80,50,60,40,70,20};
        int[] arr3 = {5,4,8,2,1,6,7};

//        System.err.println("================冒泡==================");
//        System.err.println(Arrays.toString(bubbleSort(arr1)));
//        System.err.println(Arrays.toString(bubbleSort(arr2)));
//        System.err.println(Arrays.toString(bubbleSort(arr3)));

//        System.err.println("================选择==================");
//        System.err.println(Arrays.toString(selectSort(arr1)));
//        System.err.println(Arrays.toString(selectSort(arr2)));
//        System.err.println(Arrays.toString(selectSort(arr3)));

//        System.err.println("================插入==================");
//        System.err.println(Arrays.toString(insertSort(arr1)));
//        System.err.println(Arrays.toString(insertSort(arr2)));
//        System.err.println(Arrays.toString(insertSort(arr3)));

//        System.err.println("================快速排序==================");
//        quickSort(arr1, 0, arr1.length - 1);
//        System.err.println(Arrays.toString(arr1));
//        quickSort(arr2, 0, arr2.length - 1);
//        System.err.println(Arrays.toString(arr2));
        quickSort(arr3, 0, arr3.length - 1);
        System.err.println(Arrays.toString(arr3));

//        System.err.println("================递归阶层==================");
//        System.err.println(factorial(5));

    }


    /**
     * 冒泡排序——每一轮都会找到一个最大的数字冒泡到数组数组末尾
     */
    public static int[] bubbleSort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++){
            for (int j = 0; j < arr.length - i - 1;  j++){
                if (arr[j] > arr[j + 1]){
                    int temp = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    /**
     * 选择排序——首先在没有排序的数列里找出最小的元素，并把它放到这一组数列最开始的位置
     */
    public static int[] selectSort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++){
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++){
                if (arr[j] < arr[j -1]){
                    minIndex = j;
                }
            }
            // 如果最小数值的索引不为i，
            if (minIndex != i){
                int temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
        }
        return arr;
    }

    /**
     * 插入排序——每一步将一个待排序的数据插入到前面已经排序好的有序序列里，直到插完所有的元素为止。
     */
    public static int[] insertSort(int[] arr){
        for (int i = 1; i < arr.length; i++){
            for (int j = i; j > 0; j--){
                if (arr[j -1] > arr[j]){
                    int temp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = temp;
                } else {
                    break;
                }
            }
        }
        return arr;
    }

    /**
     * 快速排序——首先快速排序要选取一个基准值，以基准值为分割，**把比该基准值小的放左边，基准值大的放右边**。然后得出来的序列再进行快速排序。
     */
    public static void quickSort(int[] arr, int leftIndex, int rightIndex){
        if (leftIndex >= rightIndex){
            return;
        }
        int left = leftIndex;
        int right = rightIndex;
        // 基准值
        int key = arr[left];
        while (left < right){
            // 右边的必须大于基准值
            while(right > left && arr[right] >= key){
                right--;
            }
            arr[left] = arr[right];
            // 左边的都必须小于基准值
            while (left < right && arr[left] <= key){
                left++;
            }
            arr[right] = arr[left];
        }
        //基准值 归位 left=right
        arr[left] = key;
        quickSort(arr, leftIndex, left - 1);
        quickSort(arr, right + 1, rightIndex);
    }

    /**
     * 递归阶层
     */
    public static int factorial(int num){
        if (num > 1){
            return num * factorial(num - 1);
        }
        return 1;
    }

}
