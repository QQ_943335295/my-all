package my.all.algorithm.search;

/**
 * 排序算法案例
 * @author zxm
 * @date 2023/2/2
 */
public class SearchAlgorithmDemo {

    public static void main(String[] args) {
        int [] arr1 = {80,50,60,40,70,20};
        System.err.println("===========线性搜索===========");
        System.err.println(lineSearch(arr1, 10));
        System.err.println(lineSearch(arr1, 70));
        System.err.println("===========二分查找===========");
        int[] arr2={4,5,6,7,9,21,24};
        System.err.println(middleSearch(arr2, 21, 0, arr2.length - 1));
        System.err.println(middleSearch(arr2, 24, 0, arr2.length - 1));
        System.err.println(middleSearch(arr2, 0, 0, arr2.length - 1));
        System.err.println("===========插值查找===========");
        System.err.println(insertSearch(arr2, 21, 0, arr2.length - 1));
        System.err.println(insertSearch(arr2, 24, 0, arr2.length - 1));
        System.err.println(insertSearch(arr2, 0, 0, arr2.length - 1));
    }

    /**
     * 线性搜索：无序
     */
    public static int lineSearch(int[] arr, int findNum){
        for (int i = 0; i < arr.length; i++){
            if (arr[i] == findNum){
                return i;
            }
        }
        return -1;
    }

    /**
     * 二分查找：有序
     */
    public static int middleSearch(int[] arr, int findNum, int left, int right){
        if (left > right){
            return -1;
        }
        int middleIndex = (left + right) / 2;
        if (arr[middleIndex] > findNum){
            return middleSearch(arr, findNum, left, middleIndex - 1);
        } else if (arr[middleIndex] < findNum){
            return middleSearch(arr, findNum, middleIndex + 1, right);
        } else {
            return middleIndex;
        }
    }
    //              int[] arr2={4,5,6,7,9,21,24};
    // 0 + 6 / 2 => 3 => 7 < 24
    // 3 + 6 / 2 => 4 => 9 < 24
    // 4 + 6 / 2 => 5 => 21 < 24
    // 5 + 6 / 2 => 5 => 21 < 24

    /**
     * 插值查找：有序
     */
    public static int insertSearch(int[] arr, int findNum, int left, int right){
        if (left > right || findNum<arr[left] ||findNum > arr[right]){
            return -1;
        }
        int middleIndex = left + (right - left) * (findNum - arr[left]) / (arr[right] - arr[left]);
        if (arr[middleIndex] > findNum){
            return insertSearch(arr, findNum, left, middleIndex - 1);
        } else if (arr[middleIndex] < findNum){
            return insertSearch(arr, findNum, middleIndex + 1, right);
        } else {
            return middleIndex;
        }
    }





}
