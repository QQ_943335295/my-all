package my.all.question;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

/**
 * 2. 两数相加
 * 给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 * 你可以假设除了数字 0 之外，这两个数都不会以 0 开头。
 *
 * 示例 1：
 * 输入：l1 = [2,4,3], l2 = [5,6,4]
 * 输出：[7,0,8]
 * 解释：342 + 465 = 807.
 *
 * 示例 2：
 * 输入：l1 = [0], l2 = [0]
 * 输出：[0]
 *
 * 示例 3：
 * 输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * 输出：[8,9,9,9,0,0,0,1]
 *
 *
 * 提示：
 * 每个链表中的节点数在范围 [1, 100] 内
 * 0 <= Node.val <= 9
 * 题目数据保证列表表示的数字不含前导零
 * @author zxm
 * @date 2023/2/14
 */
public class AddTwoNumbers {


    public static ListNode addTwoNumbers1(ListNode l1, ListNode l2) {
        Queue<Integer> stack1 = new ArrayDeque<>();
        ListNode cur = l1;
        stack1.add(cur.val);
        while (cur.next != null){
            stack1.add(cur.next.val);
            cur = cur.next;
        }
        Queue<Integer> stack2 = new ArrayDeque<>();
        ListNode cur2 = l2;
        stack2.add(cur2.val);
        while (cur2.next != null){
            stack2.add(cur2.next.val);
            cur2 = cur2.next;
        }
        int stack1Size = stack1.size();
        int stack2Size = stack2.size();
        int maxSize = Math.max(stack1Size, stack2Size);
        int nextBit = 0;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < maxSize; i++){
            Integer pop1 = i < stack1Size ? stack1.poll() : 0;
            Integer pop2 = i < stack2Size ? stack2.poll() : 0;
            int result = pop1 + pop2 + nextBit;
            stack.add(result % 10);
            nextBit = result >= 10 ? 1 : 0;
        }
        if (nextBit > 0){
            stack.add(nextBit);
        }
        ListNode resultNode = null;
        for (int i = stack.size(); i > 0; i--){
            resultNode = new ListNode(stack.pop(), resultNode);
        }
        return resultNode;
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = null;
        ListNode cur1 = l1;
        ListNode cur2 = l2;
        int val = 0;
        int nextValue = 0;
        boolean cur1Flag;
        boolean cur2Flag;
        ListNode nextNode = null;
        while ((cur1Flag = (cur1 != null)) & (cur2Flag = (cur2 != null))){
            val = cur1.val + cur2.val + nextValue;
            if(result == null){
                result = new ListNode(val % 10);
                nextNode = result;
            } else {
                nextNode.next = new ListNode(val % 10);
                nextNode = nextNode.next;
            }
            // result = new ListNode(val % 10, result);
            nextValue = val >= 10 ? 1 : 0;
            val = 0;
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        ListNode cur = null;
        if (cur1Flag){
            cur = cur1;
        }
        if (cur2Flag){
            cur = cur2;
        }
        while (cur != null){
            val = cur.val + nextValue;
            nextNode.next = new ListNode(val % 10);
            nextNode = nextNode.next;
            nextValue = val >= 10 ? 1 : 0;
            val = 0;
            cur = cur.next;
        }
        if (nextValue > 0){
            nextNode.next = new ListNode(nextValue);
            nextNode = nextNode.next;
        }
        return result;
    }


    public static void main(String[] args) {
        int[] array1 = new int[]{2,4,3};
        int[] array2 = new int[]{5,6,4};
        ListNode listNode1 = null;
        ListNode listNode2 = null;
        for (int i = array1.length - 1; i >= 0; i--){
            listNode1 = new ListNode(array1[i], listNode1);
        }
        for (int i = array2.length - 1; i >= 0; i--){
            listNode2 = new ListNode(array2[i], listNode2);
        }
        System.err.println("=======================================");
        System.err.println(listNode1);
        System.err.println(listNode2);
        System.err.println(addTwoNumbers(listNode1, listNode2));

        int[] array3 = new int[]{0};
        int[] array4 = new int[]{0};
        ListNode listNode3 = null;
        ListNode listNode4 = null;
        for (int i = array3.length - 1; i >= 0; i--){
            listNode3 = new ListNode(array3[i], listNode3);
        }
        for (int i = array4.length - 1; i >= 0; i--){
            listNode4 = new ListNode(array4[i], listNode4);
        }
        System.err.println("=======================================");
        System.err.println(listNode3);
        System.err.println(listNode4);
        System.err.println(addTwoNumbers(listNode3, listNode4));

        int[] array5 = new int[]{9,9,9,9,9,9,9};
        int[] array6 = new int[]{9,9,9,9};
        ListNode listNode5 = null;
        ListNode listNode6 = null;
        for (int i = array5.length - 1; i >= 0; i--){
            listNode5 = new ListNode(array5[i], listNode5);
        }
        for (int i = array6.length - 1; i >= 0; i--){
            listNode6 = new ListNode(array6[i], listNode6);
        }
        System.err.println("=======================================");
        System.err.println(listNode5);
        System.err.println(listNode6);
        System.err.println(addTwoNumbers(listNode5, listNode6));

        int[] array7 = new int[]{2,4,9};
        int[] array8 = new int[]{5,6,4,9};
        ListNode listNode7 = null;
        ListNode listNode8 = null;
        for (int i = array7.length - 1; i >= 0; i--){
            listNode7 = new ListNode(array7[i], listNode7);
        }
        for (int i = array8.length - 1; i >= 0; i--){
            listNode8 = new ListNode(array8[i], listNode8);
        }
        System.err.println("=======================================");
        System.err.println(listNode7);
        System.err.println(listNode8);
        System.err.println(addTwoNumbers(listNode7, listNode8));
    }

}

class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "val=" + val +
                ", next=" + next +
                '}';
    }
}
