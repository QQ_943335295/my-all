package my.all.question;

import java.util.HashMap;
import java.util.Map;

/**
 * 13. 罗马数字转整数
 * 罗马数字包含以下七种字符: I， V， X， L，C，D 和 M。
 * 字符          数值
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * 例如， 罗马数字 2 写做 II ，即为两个并列的 1 。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。
 * 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：
 * I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
 * X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。
 * C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
 * 给定一个罗马数字，将其转换成整数。
 *
 * 示例 1:
 * 输入: s = "III"
 * 输出: 3
 *
 * 示例 2:
 * 输入: s = "IV"
 * 输出: 4
 * 示例 3:
 *
 * 输入: s = "IX"
 * 输出: 9
 * 示例 4:
 *
 * 输入: s = "LVIII"
 * 输出: 58
 * 解释: L = 50, V= 5, III = 3.
 * 示例 5:
 *
 * 输入: s = "MCMXCIV"
 * 输出: 1994
 * 解释: M = 1000, CM = 900, XC = 90, IV = 4.
 *
 * 提示：
 *
 * 1 <= s.length <= 15
 * s 仅含字符 ('I', 'V', 'X', 'L', 'C', 'D', 'M')
 * 题目数据保证 s 是一个有效的罗马数字，且表示整数在范围 [1, 3999] 内
 * 题目所给测试用例皆符合罗马数字书写规则，不会出现跨位等情况。
 * IL 和 IM 这样的例子并不符合题目要求，49 应该写作 XLIX，999 应该写作 CMXCIX 。
 * 关于罗马数字的详尽书写规则，可以参考 罗马数字 - Mathematics 。
 * @author zxm
 * @date 2023/2/13
 */
public class RomanToInt {
    public static int romanToInt(String s) {
        Map<String, Integer> leftMap = new HashMap<>(16);
        leftMap.put("I", 1);
        leftMap.put("IV", 4);
        leftMap.put("V", 5);
        leftMap.put("IX", 9);
        leftMap.put("X", 10);
        leftMap.put("XL", 40);
        leftMap.put("L", 50);
        leftMap.put("XC", 90);
        leftMap.put("C", 100);
        leftMap.put("CD", 400);
        leftMap.put("D", 500);
        leftMap.put("CM", 900);
        leftMap.put("M", 1000);
        char[] array = s.toCharArray();
        int result = 0;
        for (int i = 0; i < array.length; i++){
            int left = leftMap.get(String.valueOf(array[i]));
            int right = i + 1 == array.length ? 0 : leftMap.get(String.valueOf(array[i + 1]));
            if (left < right){
                result += leftMap.get(array[i] + String.valueOf(array[i + 1]));
                i++;
            } else {
                result += leftMap.get(String.valueOf(array[i]));
            }
        }
        return result;
    }

    public static int romanToInt2(String s) {
        char[] array = s.toCharArray();
        int result = 0;
        for (int i = 0; i < array.length; i++){
            int left = getIntByRoman(String.valueOf(array[i]));
            int right = 0;
            if ( i + 1 != array.length){
                right = getIntByRoman(String.valueOf(array[i + 1]));
            }
            if (left < right){
                result += (right - left);
                i++;
            } else {
                result += left;
            }
        }
        return result;
    }

    public static int getIntByRoman(String roman){
        switch (roman){
            case "I": return 1;
            case "IV": return 4;
            case "V": return 5;
            case "IX": return 9;
            case "X": return 10;
            case "XL": return 40;
            case "L": return 50;
            case "XC": return 90;
            case "C": return 100;
            case "CD": return 400;
            case "D": return 500;
            case "CM": return 900;
            case "M": return 1000;
            default: return 0;
        }
    }

    public static void main(String[] args) {
        String s1 = "III";
        System.err.println(romanToInt2(s1));
        String s2 = "IV";
        System.err.println(romanToInt2(s2));
        String s3 = "IX";
        System.err.println(romanToInt2(s3));
        String s4 = "LVIII";
        System.err.println(romanToInt2(s4));
        String s5 = "MCMXCIV";
        System.err.println(romanToInt2(s5));
    }

}
