package my.all.question;

/**
 * 14. 最长公共前缀
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 * 如果不存在公共前缀，返回空字符串 ""。
 *
 * 示例 1：
 * 输入：strs = ["flower","flow","flight"]
 * 输出："fl"
 * 示例 2：
 *
 * 输入：strs = ["dog","racecar","car"]
 * 输出：""
 * 解释：输入不存在公共前缀。
 *
 *
 * 提示：
 *
 * 1 <= strs.length <= 200
 * 0 <= strs[i].length <= 200
 * strs[i] 仅由小写英文字母组成
 * @author zxm
 * @date 2023/2/13
 */
public class LongestCommonPrefix {

    public static String longestCommonPrefix(String[] strs) {
        int minIndex = 0;
        for (int i = 0; i < strs.length; i++){
            if (strs[i].length() < strs[minIndex].length()){
                minIndex = i;
            }
        }
        String minLenStr = strs[minIndex];
        for (int i = 0; i < strs.length; i++){
            while (!strs[i].startsWith(minLenStr)){
                minLenStr = minLenStr.substring(0, minLenStr.length() -1);
            }
        }
        return minLenStr;
    }

    public static void main(String[] args) {
        String[] strs1 = {"flower","flow","flight"};
        System.err.println(longestCommonPrefix(strs1));
        String[] strs2 = {"dog","racecar","car"};
        System.err.println(longestCommonPrefix(strs2));
    }

}
