package my.all.question;

/**
 * 383. 赎金信
 * 给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。
 * 如果可以，返回 true ；否则返回 false 。
 *
 * magazine 中的每个字符只能在 ransomNote 中使用一次。
 *
 * 解释：ransomNote中字符a为2，那么magazine中出现字符a的次数必须大于2
 *
 * 示例 1：
 * 输入：ransomNote = "a", magazine = "b"
 * 输出：false
 *
 * 示例 2：
 * 输入：ransomNote = "aa", magazine = "ab"
 * 输出：false
 *
 * 示例 3：
 * 输入：ransomNote = "aa", magazine = "aab"
 * 输出：true
 *
 * 提示：
 * 1 <= ransomNote.length, magazine.length <= 105
 * ransomNote 和 magazine 由小写英文字母组成
 *
 * 链接：https://leetcode.cn/problems/ransom-note/description/
 * @author zxm
 * @date 2023/2/8
 */
public class CanConstruct {

    public static boolean canConstruct(String ransomNote, String magazine){
        if (ransomNote.length() > magazine.length()){
            return false;
        }
        char[] ransomNoteArray = ransomNote.toCharArray();
        char[] magazineArray = magazine.toCharArray();
        int[] temp = new int[26];
        for (char c : magazineArray){
            temp[c - 'a']++;
        }
        for (char c : ransomNoteArray){
            temp[c - 'a']--;
            if (temp[c - 'a'] < 0){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String ransomNote = "aa", magazine = "ab";
        System.err.println(canConstruct(ransomNote, magazine));
        String ransomNote1 = "aa", magazine1 = "aab";
        System.err.println(canConstruct(ransomNote1, magazine1));
        String ransomNote2 = "bg", magazine2 = "efjbdfbdgfjhhaiigfhbaejahgfbbgbjagbddfgdiaigdadhcfcj";
        System.err.println(canConstruct(ransomNote2, magazine2));
        String ransomNote3 = "aab", magazine3 = "baa";
        System.err.println(canConstruct(ransomNote3, magazine3));
    }
}
