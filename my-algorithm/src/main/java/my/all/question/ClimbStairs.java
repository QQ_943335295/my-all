package my.all.question;

/**
 * 70. 爬楼梯
 * 简单
 * 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
 * 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
 *
 * 示例 1：
 * 输入：n = 2
 * 输出：2
 * 解释：有两种方法可以爬到楼顶。
 * 1. 1 阶 + 1 阶
 * 2. 2 阶
 *
 * 示例 2：
 * 输入：n = 3
 * 输出：3
 * 解释：有三种方法可以爬到楼顶。
 * 1. 1 阶 + 1 阶 + 1 阶
 * 2. 1 阶 + 2 阶
 * 3. 2 阶 + 1 阶
 *
 * （1）如果第一阶段走的步数为1，则实际可走方式为f(n-1)
 * （2）如果第一阶段走的步数为2，则实际可走方式为f(n-2)
 * （3）因此n个阶梯的可走次数为 f(n-1) + f(n-2)
 * 1 - 1
 * 2 - 2
 * 3 - 2 + 1 -> 第一个位置走一步：f(2)  第一个位置走两步：f(1)
 * 4 - 3 + 2 -> 第一个位置走一步：f(3)  第一个位置走两步：f(2)
 * 5 - 5 + 3 -> 第一个位置走一步：f(4)  第一个位置走两步：f(3)
 * 6 - 8 + 5 -> 第一个位置走一步：f(5)  第一个位置走两步：f(4)
 * 提示：
 * 1 <= n <= 45
 *
 * 地址：https://leetcode.cn/problems/climbing-stairs/
 * @author zxm
 * @date 2023/2/9
 */
public class ClimbStairs {

    /**
     * 备忘录方式
     */
    public static int climbStairs(int n) {
        int[] temp = new int[n];
        return climbStairsTemp(n, temp);
    }

    private static int climbStairsTemp(int n, int[] temp) {
        if (temp[n - 1] != 0){
            return temp[n -1];
        }
        if (n < 4) {
            temp[n -1] = n;
            return n;
        }
        int next = climbStairsTemp(n - 2, temp);
        temp[n - 3] = next;
        return next + climbStairsTemp(n - 1, temp);
    }

    /**
     * 动态规划
     */
    public static int climbStairs2(int n) {
        if (n < 3) {
            return n;
        }
        int[] temp = new int[n];
        int result = 0;
        temp[0] = 1;
        temp[1] = 2;
        for (int i = 3; i <= n; i++){
            result = temp[i - 2] + temp[i - 3];
            temp[i - 1] = result;
        }
        return result;
    }


    public static void main(String[] args) {
        System.err.println(climbStairs2(1));
        System.err.println(climbStairs2(2));
        System.err.println(climbStairs2(3));
        System.err.println(climbStairs2(4));
        System.err.println(climbStairs2(5));
        System.err.println(climbStairs2(6));
        System.err.println(climbStairs2(45));
    }
}
