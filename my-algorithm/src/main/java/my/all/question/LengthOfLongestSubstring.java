package my.all.question;

import java.util.HashMap;
import java.util.Map;

/**
 * 3. 无重复字符的最长子串
 * 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
 *
 * 示例 1:
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 *
 * 示例 2:
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 *
 * 示例 3:
 * 输入: s = "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
 *      请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
 *
 * 提示：
 * 0 <= s.length <= 5 * 104
 * s 由英文字母、数字、符号和空格组成
 *
 * @author zxm
 * @date 2023/2/15
 */
public class LengthOfLongestSubstring {

    public static int lengthOfLongestSubstring2(String s) {
        char[] array = s.toCharArray();
        Map<Character, Integer> map = new HashMap<>();
        int length = 0;
        for (int i = 0; i < array.length; i++){
            Integer temp = map.get(array[i]);
            if (temp != null){
                i = temp;
                if (map.size() > length){
                    length = map.size();
                }
                map = new HashMap<>();
            } else {
                map.put(array[i], i);
            }
        }
        return Math.max(map.size(), length);
    }


    // 滑动窗口
    public static int lengthOfLongestSubstring(String s) {
        int length = 0;
        for (int i = 0; i < s.length(); i++){
            int tempNextIndex = s.indexOf(s.charAt(i), i + 1);
            tempNextIndex = tempNextIndex > 0 ? tempNextIndex : s.length();
            boolean flag = false;
            for (int j = i; j < tempNextIndex; j++){
                int innerNextIndex = s.indexOf(s.charAt(j), j + 1);
                innerNextIndex = innerNextIndex > 0 ? innerNextIndex : s.length();
                if (tempNextIndex > innerNextIndex){
                    flag = true;
                    length = Math.max(length, innerNextIndex - i);
                    break;
                }
            }
            if (!flag){
                length = Math.max(length, tempNextIndex - i);
            }
        }
        return length;
    }


    public static void main(String[] args) {

        // 4
        String s0 = "uqinntq";
        System.err.println(lengthOfLongestSubstring(s0));
        // 3
        String s1 = "abcabcbb";
        System.err.println(lengthOfLongestSubstring(s1));
        // 1
        String s2 = "bbbbb";
        System.err.println(lengthOfLongestSubstring(s2));
        // 3
        String s3 = "pwwkew";
        System.err.println(lengthOfLongestSubstring(s3));
        // 6
        String s4 = "pweazwfjwkew";
        System.err.println(lengthOfLongestSubstring(s4));
        // 1
        String s5 = " ";
        System.err.println(lengthOfLongestSubstring(s5));
        // 2
        String s6 = "au";
        System.err.println(lengthOfLongestSubstring(s6));
        //2
        String s7 = "aab";
        System.err.println(lengthOfLongestSubstring(s7));
    }

}
