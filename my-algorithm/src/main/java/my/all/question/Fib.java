package my.all.question;

/**
 * 509. 斐波那契数
 * 斐波那契数 （通常用 F(n) 表示）形成的序列称为 斐波那契数列 。该数列由 0 和 1 开始，后面的每一项数字都是前面两项数字的和。也就是：
 * F(0) = 0，F(1) = 1
 * F(n) = F(n - 1) + F(n - 2)，其中 n > 1
 * 给定 n ，请计算 F(n) 。
 *
 * 示例 1：
 * 输入：n = 2
 * 输出：1
 * 解释：F(2) = F(1) + F(0) = 1 + 0 = 1
 *
 * 示例 2：
 * 输入：n = 3
 * 输出：2
 * 解释：F(3) = F(2) + F(1) = 1 + 1 = 2
 *
 * 示例 3：
 * 输入：n = 4
 * 输出：3
 * 解释：F(4) = F(3) + F(2) = 2 + 1 = 3
 *
 * 提示：
 * 0 <= n <= 30
 *
 * 地址：https://leetcode.cn/problems/fibonacci-number/description/
 * @author zxm
 * @date 2023/2/10
 */
public class Fib {

    public static int fib(int n){
        if (n < 2){
            return n;
        }
        return fib(n -1) + fib(n - 2);
    }


    /**
     * 动态规划
     */
    public static int fib2(int n){
        if (n < 2){
            return n;
        }
        int[] temp = new int[n + 1];
        temp[0] = 0;
        temp[1] = 1;
        int result = 0;
        for (int i = 2; i <= n; i++){
            result = temp[i - 1] + temp[i - 2];
            temp[i] = result;
        }
        return result;
    }

    /**
     * 备忘录
     */
    public static int fib3(int n){
        if (n < 2){
            return n;
        }
        int[] temp = new int[n + 1];
        temp[0] = 0;
        temp[1] = 1;
        return fibTemp(n -1, temp) + fibTemp(n - 2, temp);
    }

    private static int fibTemp(int n, int[] temp) {
        if (temp[n] != 0){
            return temp[n];
        }
        if (n < 2){
            return n;
        }
        return fibTemp(n -1, temp) + fibTemp(n - 2, temp);
    }


    public static void main(String[] args) {
        System.err.println(fib3(2));
        System.err.println(fib3(3));
        System.err.println(fib3(4));
        System.err.println(fib3(5));
        System.err.println(fib3(6));
    }

}
