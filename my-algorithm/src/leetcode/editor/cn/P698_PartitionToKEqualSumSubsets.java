//给定一个整数数组 nums 和一个正整数 k，找出是否有可能把这个数组分成 k 个非空子集，其总和都相等。 
//
// 
//
// 示例 1： 
//
// 
//输入： nums = [4, 3, 2, 3, 5, 2, 1], k = 4
//输出： True
//说明： 有可能将其分成 4 个子集（5），（1,4），（2,3），（2,3）等于总和。 
//
// 示例 2: 
//
// 
//输入: nums = [1,2,3,4], k = 3
//输出: false 
//
// 
//
// 提示： 
//
// 
// 1 <= k <= len(nums) <= 16 
// 0 < nums[i] < 10000 
// 每个元素的频率在 [1,4] 范围内 
// 
//
// Related Topics 位运算 记忆化搜索 数组 动态规划 回溯 状态压缩 👍 686 👎 0


package leetcode.editor.cn;

/**
 * 划分为k个相等的子集
 * @author zxm
 * @date 2022-09-20 09:18:27
 */
public class P698_PartitionToKEqualSumSubsets{
	 public static void main(String[] args) {
	 	 //测试代码
	 	 Solution solution = new P698_PartitionToKEqualSumSubsets().new Solution();
	 }
	 
//力扣代码
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean canPartitionKSubsets(int[] nums, int k) {
    	int sum = 0;
    	int max = 0;
    	int min = 0;
    	for (int num : nums){
    		sum += num;
    		max = num > max ? num : max;
    		min = num > min ? min : num;
    	}
    	if (sum % k == 0){
    		int ave = sum / k;
    		if (max > ave || max + min >ave){
    			return false;
			}
    		return checkAve(nums);
		}
    	return false;
    }

    public boolean checkAve(int[] nums){
		for (int i = 0; i < nums.length; i++){
			int[] indexArray;
			int indexArrayLen = -1;
			if (nums[i] == ave){
				indexArray[indexArrayLen + 1] = i;
				System.err.println("(" + nums[i] + ")");
				break;
			}

			int temp = -1;
			for (int j = i+1; j < nums.length; j++){
				if (nums[i] + nums[j] == ave){
					temp = j;
					break;
				}
			}
			if (temp != -1){
				System.err.println("(" + nums[i]  + "," + nums[j] + ")");
				break;
			} else {
				return false
			}
		}
	}

}
//leetcode submit region end(Prohibit modification and deletion)

}