package my.all.bt.code;

/**
 * 循环中的x++
 */
public class WhileXAdd {

    public static void main(String[] args) {
        int i = 0;
        int x = 1;
        while (i < 10){
            i++;
            x = x++;
        }
        System.err.println(x);
    }

}
