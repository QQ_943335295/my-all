package my.all.bt.code;

/**
 * 图解方法执行流程
 */
public class MapMethodRunProcess {

    /**
     * 字节码指令、操作数栈、常量池关系
     */
    public static void main(String[] args) {
        int a = 10;
        int b = Short.MAX_VALUE + 1;
        int c = a + b;
        System.err.println(c);
    }

}
