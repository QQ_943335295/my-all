package my.all.cache.line;


/**
 * CPU不共享同一个存储块数据运行时效率测试
 * @author zxm
 * @date 2022/4/28 22:20
 */
public class CacheLinePaddingUnSharing {
    public static volatile long[] arr = new long[32];
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 2000000000; i++) {
                arr[0] = i;
            }});
        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 2000000000; i++) {
                arr[8] = i;
            }
        });
        long startTime = System.currentTimeMillis();
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.err.println(System.currentTimeMillis() - startTime);
    }
}
