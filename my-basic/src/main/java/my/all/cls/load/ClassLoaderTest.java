package my.all.cls.load;

/**
 * 测试使用了什么类加载器
 */
public class ClassLoaderTest {

    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> aClass = Class.forName("my.all.cls.load.LoadClass");
        System.err.println(aClass.getClassLoader());
        ClassLoaderTest.class.getClassLoader().loadClass("123");

    }

}
