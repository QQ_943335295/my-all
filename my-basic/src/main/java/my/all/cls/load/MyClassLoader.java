package my.all.cls.load;

import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * 自定义类加载器
 * 需要去除package my.all.cls.load;才能被加载
 *
 * 通过逃逸分析后的对象直接在栈上进行分配，极大降低了GC次数
 * 栈未溢出时，对象分配在站上而不是对上，对象随栈消亡
 */
public class MyClassLoader {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        MyselfClassLoader classLoader = new MyselfClassLoader();
        Class<?> constantPool = classLoader.loadClass("TestGCMemory");
        Object o = constantPool.getDeclaredConstructor().newInstance();
        Object o2 = constantPool.getDeclaredConstructor().newInstance();
        System.err.println(o == o2);
        Class<?> constantPool2 = classLoader.loadClass("TestGCMemory");
        Object o3 = constantPool2.getDeclaredConstructor().newInstance();
        System.err.println(o == o3);
    }

}

class MyselfClassLoader extends ClassLoader{
    @SneakyThrows
    @Override
    protected Class<?> findClass(String name) {
        String path = "C:\\Users\\94333\\Desktop\\笔记\\" + name + ".class";
       /* FileInputStream fis = new FileInputStream(path);
        byte[] bytes = fis.readAllBytes();*/
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Files.copy(Path.of(path), baos);
        byte[] bytes = baos.toByteArray();
        return defineClass(name, bytes, 0, bytes.length);
    }
}
