package my.all.jvm.reference;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * 引用的使用
 */
public class ReferenceUsed {

    static final int _4M = 1024 * 1024 * 4;

    public static void main(String[] args) {
        // hardReference();
        // softReference();
        softReferenceWithReferenceQueue();
    }

    public static void hardReference(){
        List<byte[]> list = new ArrayList<>();
        int i = 0;
        try {
            for (; i < 6; i++){
                list.add(new byte[_4M]);
                System.err.println(i);
            }
        } catch (Exception e) {
            System.err.println(i);
        }
    }


    public static void softReference(){
        // list强引用SoftReference， SoftReference软引用byte数组
        List<SoftReference<byte[]>> list = new ArrayList<>();
        int i = 0;
        for (; i < 6; i++){
            SoftReference<byte[]> reference = new SoftReference<>(new byte[_4M]);
            list.add(reference);
            System.err.println(i);
        }

        for (int j = 0; j < list.size(); j++){
            System.err.println(list.get(j).get());
        }
    }

    public static void softReferenceWithReferenceQueue(){
        // list强引用SoftReference， SoftReference软引用byte数组
        List<SoftReference<byte[]>> list = new ArrayList<>();
        ReferenceQueue<byte[]> queue = new ReferenceQueue<>();
        int i = 0;
        for (; i < 6; i++){
            SoftReference<byte[]> reference = new SoftReference<>(new byte[_4M], queue);
            list.add(reference);
            System.err.println(i);
        }
        Reference<? extends byte[]> poll = queue.poll();
        while (poll != null){
            list.remove(poll);
            poll = queue.poll();
        }
        for (int j = 0; j < list.size(); j++){
            System.err.println(list.get(j).get());
        }
    }



}
