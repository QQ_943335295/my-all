package my.all.jvm.method.area;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

/**
 * 方法区内存溢出
 * -XXMaxPermSize=8m
 * -XXMaxMetaspace=8m
 * 可用来加载类的二进制字节码
 */
public class MethodAreaOutMemory extends ClassLoader{

    public static void main(String[] args) {
        try {
            MethodAreaOutMemory methodAreaOutMemory = new MethodAreaOutMemory();
            for(int i = 0; i < 1000000; i++){
                // CsvWriter 用于生成类的二进制字节码
                ClassWriter cw = new ClassWriter(1);
                // 版本号，public 类型 包名 父类 接口
                cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, "Class" + i, null, "java/lang/Object", null);
                // 返回byte[]
                byte[] code = cw.toByteArray();
                // 执行了类的加载
                methodAreaOutMemory.defineClass("Class" + i, code, 0, code.length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
