package my.all.jvm.method.area.string.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * StringTable具体的过程
 */
public class StringTable {

    public static void main(String[] args) {
        String s1 = "a";  // a 放入常量池
        String s2 = "b";  // b 放入常量池
        String s3 = "a" + "b";  // 编译器直接代码优化 ab 放入常量池
        String s4 = s1 + s2; // 生成新对象放入堆
/*        String s5 = "ab";   // ab 放入常量池
        String s6 = s4.intern();  // jdk1.8放入常量池并返回常量池中常量，jdk1.6放入常量池并返回当前字符串对象
        System.err.println(s3 == s4);  // false
        System.err.println(s3 == s5);  // true
        System.err.println(s3 == s6);  // 1.8->true  1.6->false*/
    }

}


class StringComparable implements Comparator<StringComparable>, Comparable<String>{
    @Override
    public int compare(StringComparable o1, StringComparable o2) {
        return 0;
    }
    @Override
    public int compareTo(String o) {
        return 0;
    }
    public static void main(String[] args) {
        StringComparable stringComparable1 = new StringComparable();
        StringComparable stringComparable2 = new StringComparable();
        List<StringComparable> myList = new ArrayList<>();
        // 一个类可以实现不同比较方式的comparator接口
        Collections.sort(myList, stringComparable1);
        Collections.sort(myList, stringComparable2);
        // 当前对象与另一个对象进行比较
        int i = stringComparable1.compareTo("123");
    }
}