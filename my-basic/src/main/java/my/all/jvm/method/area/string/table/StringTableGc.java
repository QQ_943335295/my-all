package my.all.jvm.method.area.string.table;

/**
 * StringTable GC演示
 * -Xmx10m -XX:+PrintStringTableStatistics -XX:+PrintGCDetails -verbose:gc
 * 虚拟机堆最大值  打印字符串表字符信息  打印垃圾回收详细信息
 *
 * -XX:+PrintStringTableStatistics -XX:+PrintGCDetails -XX:StringTableSize=1024
 * 花费时间:13274
 *131072
 *
 */
public class StringTableGc {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        try{
            for (int i = 0; i < 1000000; i++) {
                String.valueOf(i).intern();
            }
        } catch (Exception e){

        }
        System.err.println("花费时间:" + (System.currentTimeMillis() - startTime));
    }

}
