package my.all.jvm.method.area.string.table;

public class Intern {

    public static void main(String[] args) {
        String str2 = new StringBuffer("我是").toString();
        String intern2 = str2.intern();
        System.out.println(intern2 == str2);
/*        String s3 = "abcdefg";
        System.err.println(s1 == s3);*/


        // jdk1.6
        // 1      2     3       4    5     6
        // false、true、false、true、false、true
        // jdk1.7
        // false、true、true、true、false、true
        // jdk1.8
        // false、true、true、true、false、true
/*

        String str1 = new StringBuffer("ja").append("va").toString();
        String intern1 = str1.intern();

        System.out.println(intern1 == str1);    // false     false
        System.out.println(intern1.equals(str1));  // true    true


        String str2 = new StringBuffer("我是").append("中国人").toString();
        String intern2 = str2.intern();

        System.out.println(intern2 == str2);    // false    true
        System.out.println(intern2.equals(str2));  // true   true


        String str3 = new StringBuffer("我是").append("中国人").toString();
        String intern3 = str3.intern();

        System.out.println(intern3 == str3);   // false      false
        System.out.println(intern3.equals(str3));   // true   true


        */
        /**
         * "java"字符串常量比较特殊，它是固定存在字符串常量池中，所以都是返回了false
         * Java6中字符串常量池是在永久代区，intern方法会将首次遇到的字符串复制到那里，返回的也是永久代中这个字符串的引用。不建议使用这个方法。
         * Java7中字符串常量池被放到了堆中，intern的实现也不再是复制实例，只是在常量池中记录首次出现的实例引用。
         * Java8同7一致
         */

    }
}
