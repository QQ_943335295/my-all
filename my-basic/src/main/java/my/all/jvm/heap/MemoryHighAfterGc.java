package my.all.jvm.heap;

import java.util.ArrayList;
import java.util.List;

/**
 * 垃圾回收后，内存占用仍然很高
 */
public class MemoryHighAfterGc {

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(30000);
        List<Student> list = new ArrayList<>();
        for (int i = 0; i < 10 ; i++){
            list.add(new Student());
        }
        Thread.sleep(1000000000L);
    }

    static class Student{
        byte[] bigBytes = new byte[10 * 1024 * 1024];
    }

}
