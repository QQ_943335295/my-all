package my.all.jvm.stack;

/**
 * JVM 虚拟机栈长时间未输出结果
 * jstack 4544 --查看死锁
 */
public class LongTimeNoResultLocationDemo {

    static Object lock1 = new Object();
    static Object lock2 = new Object();

    public static void main(String[] args) {
        new Thread(()->{
            synchronized (lock1){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock2){
                    System.err.println("线程一 获得了锁 a 和 b");
                }
            }
        }).start();
        new Thread(()->{
            synchronized (lock2){
                synchronized (lock1){
                    System.err.println("线程二 获得了锁 a 和 b");
                }
            }
        }).start();
    }

}
