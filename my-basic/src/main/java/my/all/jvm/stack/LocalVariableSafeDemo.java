package my.all.jvm.stack;

/**
 * JVM 虚拟机栈局部变量安全问题
 */
public class LocalVariableSafeDemo {

    // 局部变量安全
    public void test01(){
        int i = 0;
        while (i < 500){
            i++;
        }
    }

    // Sb局部变量安全
    public void test02(){
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < 500){
            sb.append(i);
        }
    }

    // Sb局部变量不安全
    public void test03(StringBuilder sb){
        int i = 0;
        while (i < 500){
            sb.append(i);
        }
    }

    // Sb局部不变量安全
    public StringBuilder test04(){
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < 500){
            sb.append(i);
        }
        return sb;
    }



}
