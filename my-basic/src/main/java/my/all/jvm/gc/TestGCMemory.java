package my.all.jvm.gc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 测试GC回收
 */
public class TestGCMemory {

    public static void main(String[] args) throws IOException {
        testLiveRootGc();
    }

    private static void testLiveRootGc() throws IOException {
        List<Object> list1 = new ArrayList<>();
        list1.add("a");
        list1.add("b");
        System.out.println(1);
        System.in.read();

        list1 = null;
        System.out.println(2);
        System.in.read();
        System.out.println("end...");
    }

}
