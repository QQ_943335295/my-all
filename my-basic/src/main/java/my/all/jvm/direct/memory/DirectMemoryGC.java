package my.all.jvm.direct.memory;

import sun.misc.Unsafe;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * 直接堆内存回收实验
 * -XX:+DisableExplicitGC
 */
public class DirectMemoryGC {

    static final int _1GB = 1024 * 1024 * 1024;

    public static void main(String[] args) throws IOException, NoSuchFieldException, IllegalAccessException {
        // outOfMemory();
        // gc();
         manualGC();
    }


    /**
     * 手动释放内存
     */
    private static void manualGC() throws IOException, NoSuchFieldException, IllegalAccessException {
        // 获取Unsafe对象
        Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
        theUnsafe.setAccessible(true);
        Unsafe unsafe = (Unsafe) theUnsafe.get(null);
        // 申请内存，并返回内存地址
        long base = unsafe.allocateMemory(_1GB);
        unsafe.setMemory(base, _1GB, (byte) 0);
        System.in.read();
        // 根据内存地址释放内存
        unsafe.freeMemory(base);
        System.in.read();
    }

    /**
     * Gc实验
     */
    private static void gc() throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(_1GB);
        System.err.println("分配完毕....");
        System.in.read();
        System.err.println("开始释放....");
        byteBuffer = null;
        System.gc();
    }

    /**
     * 内存溢出
     */
    public static void outOfMemory(){
        List<ByteBuffer> byteBuffers = new ArrayList<>();
        int i = 0;
        try {
            for (; i < 100; i++){
                byteBuffers.add(ByteBuffer.allocateDirect(_1GB));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.err.println(i);
        }
    }
}
