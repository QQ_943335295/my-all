package my.all.jvm.memory.allocation;

import java.util.ArrayList;

/**
 * 内存分配演示
 */
public class MemoryAllocation {

    private static final int _1MB = 1024 * 1024;
    private static final int _6MB = 6 * 1024 * 1024;
    private static final int _7MB = 7 * 1024 * 1024;
    private static final int _8MB = 8 * 1024 * 1024;

    // -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -XX:+PrintGCDetails -verbose:gc
    // -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -Xlog:gc* -verbose:gc
    // -XX:-ScavengeBeforeFullGC
    public static void main(String[] args) throws InterruptedException {

    }

    private static void memory8mb() throws InterruptedException {
        new Thread(() -> {
            ArrayList<byte[]> list = new ArrayList<>();
            list.add(new byte[_8MB]);
            list.add(new byte[_8MB]);
        }).start();
        System.out.println("sleep....");
        Thread.sleep(1000L);
    }

}
