package my.all.gof.action.b.template;

public class BingbingReviewTemplate extends AbstractReviewTemplate{


    @Override
    protected void writePaper() {
        System.err.println("paper ...");
    }

    @Override
    protected void studyJvm() {
        System.err.println("see jvm ...");
    }

    @Override
    protected void studyJdk() {
        System.err.println("see jdk ...");
    }

}
