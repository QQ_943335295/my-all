package my.all.gof.action.b.template;

public class AnnaReviewTemplate extends AbstractReviewTemplate{


    @Override
    protected void writePaper() {
        System.err.println("write paper ...");
    }

    @Override
    protected void studyJvm() {
        System.err.println("jvm ...");
    }

    @Override
    protected void studyJdk() {
        System.err.println("jdk ...");
    }

}
