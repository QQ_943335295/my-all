package my.all.gof.action.d.strategy;

public class DiscountComputeAmontStrategy extends ComputeAmountStrategy{
    @Override
    public void computeAmount(int total) {
        System.err.println("打6折，最终价格为：" + total * 0.6);
    }
}
