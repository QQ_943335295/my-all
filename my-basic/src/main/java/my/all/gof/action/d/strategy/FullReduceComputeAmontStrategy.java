package my.all.gof.action.d.strategy;

public class FullReduceComputeAmontStrategy extends ComputeAmountStrategy{
    @Override
    public void computeAmount(int total) {
        System.err.println("满200减100，最终价格为：" + (total > 200 ?  total - 100 : total));
    }
}
