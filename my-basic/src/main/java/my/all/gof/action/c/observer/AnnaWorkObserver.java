package my.all.gof.action.c.observer;

public class AnnaWorkObserver implements WorkObserver{
    @Override
    public void updateState(int state) {
        if (state == 0){
            System.err.println("touch fish ...");
        } else {
            System.err.println("hard work ...");
        }
    }
}
