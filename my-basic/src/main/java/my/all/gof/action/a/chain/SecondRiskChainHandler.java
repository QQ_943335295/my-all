package my.all.gof.action.a.chain;

public class SecondRiskChainHandler extends AbstractRiskChainHandler{

    public SecondRiskChainHandler(String name) {
        super(name);
    }

    @Override
    public void handlerRisk(int num) {
        if (num >= 500){
            System.err.println(num + "元，" + getName() + "：输入指纹");
        }
        if (nextRiskChainHandler != null){
            nextRiskChainHandler.handlerRisk(num);
        }
    }
}
