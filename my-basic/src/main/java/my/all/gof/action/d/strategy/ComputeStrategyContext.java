package my.all.gof.action.d.strategy;

public class ComputeStrategyContext {

    private ComputeAmountStrategy computeAmountStrategy;

    public ComputeStrategyContext(ComputeAmountStrategy computeAmountStrategy) {
        this.computeAmountStrategy = computeAmountStrategy;
    }

    public void executeComputeAmount(int total){
        computeAmountStrategy.computeAmount(total);
    }

    public static void main(String[] args) {
        new ComputeStrategyContext(new DiscountComputeAmontStrategy()).executeComputeAmount(200);
        System.err.println("-------------------------");
        new ComputeStrategyContext(new FullReduceComputeAmontStrategy()).executeComputeAmount(200);
    }

}
