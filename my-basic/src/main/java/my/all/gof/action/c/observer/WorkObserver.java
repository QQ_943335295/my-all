package my.all.gof.action.c.observer;

/**
 * 抽象观察者-定义观察者更新方法
 */
public interface WorkObserver {

    void updateState(int state);

}
