package my.all.gof.action.c.observer;

public class BossWorkSubject extends WorkSubject{

    @Override
    public void notifyAllObserver(int state) {
        if (state == 0){
            System.err.println("boss out ...");
        } else {
            System.err.println("boss work ...");
        }
        for (WorkObserver workObserver : workObservers){
            workObserver.updateState(state);
        }
    }

    public static void main(String[] args) {
        WorkSubject workSubject = new BossWorkSubject();
        workSubject.addWorkObservers(new AnnaWorkObserver());
        workSubject.addWorkObservers(new BingbingWorkObserver());
        workSubject.notifyAllObserver(1);
        System.err.println("----------------------");
        workSubject.notifyAllObserver(0);
    }
}
