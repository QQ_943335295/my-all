package my.all.gof.action.a.chain;

import lombok.Data;

/**
 * 抽象处理者：定义一个抽象处理接口
 */
@Data
public abstract class AbstractRiskChainHandler {

    protected AbstractRiskChainHandler nextRiskChainHandler;

    protected String name;

    public AbstractRiskChainHandler(String name) {
        this.name = name;
    }

    public abstract void handlerRisk(int num);

    public static void main(String[] args) {
        FirstRiskChainHandler firstRiskChainHandler = new FirstRiskChainHandler("第一层风控");
        SecondRiskChainHandler secondRiskChainHandler = new SecondRiskChainHandler("第二层风控");
        ThirdRiskChainHandler thirdRiskChainHandler = new ThirdRiskChainHandler("第三层风控");
        firstRiskChainHandler.setNextRiskChainHandler(secondRiskChainHandler);
        secondRiskChainHandler.setNextRiskChainHandler(thirdRiskChainHandler);
        System.err.println("-----------------------------");
        firstRiskChainHandler.handlerRisk(100);
        System.err.println("-----------------------------");
        firstRiskChainHandler.handlerRisk(500);
        System.err.println("-----------------------------");
        firstRiskChainHandler.handlerRisk(10000);
        System.err.println("-----------------------------");
    }
}
