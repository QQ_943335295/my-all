package my.all.gof.action.c.observer;

public class BingbingWorkObserver implements WorkObserver{
    @Override
    public void updateState(int state) {
        if (state == 0){
            System.err.println("wc ...");
        } else {
            System.err.println("hard work ...");
        }
    }
}
