package my.all.gof.action.a.chain;

public class FirstRiskChainHandler extends AbstractRiskChainHandler{

    public FirstRiskChainHandler(String name) {
        super(name);
    }

    @Override
    public void handlerRisk(int num) {
        if (num < 500){
            System.err.println(num + "元，" + getName() + "：输入密码即可");
        }
        if (nextRiskChainHandler != null){
            nextRiskChainHandler.handlerRisk(num);
        }
    }
}
