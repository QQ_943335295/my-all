package my.all.gof.action.d.strategy;

/**
 * 抽象类：定义策略抽象方法
 */
public abstract class ComputeAmountStrategy {

    public abstract void computeAmount(int total);

}
