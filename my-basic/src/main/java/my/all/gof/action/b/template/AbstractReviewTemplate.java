package my.all.gof.action.b.template;

public abstract class AbstractReviewTemplate {

    protected void review(){
        writePaper();
        studyJvm();
        studyJdk();
    }

    protected abstract void writePaper();

    protected abstract void studyJvm();

    protected abstract void studyJdk();


    public static void main(String[] args) {
        AbstractReviewTemplate annaReviewTemplate = new AnnaReviewTemplate();
        annaReviewTemplate.review();
        System.err.println("------------------------");
        AbstractReviewTemplate reviewTemplate = new BingbingReviewTemplate();
        reviewTemplate.review();
    }
}
