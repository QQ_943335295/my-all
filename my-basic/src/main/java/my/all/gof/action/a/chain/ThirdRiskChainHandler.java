package my.all.gof.action.a.chain;

public class ThirdRiskChainHandler extends AbstractRiskChainHandler{

    public ThirdRiskChainHandler(String name) {
        super(name);
    }

    @Override
    public void handlerRisk(int num) {
        if (num > 1000){
            System.err.println(num + "元，" + getName() + "：刷脸");
        }
        if (nextRiskChainHandler != null){
            nextRiskChainHandler.handlerRisk(num);
        }
    }
}
