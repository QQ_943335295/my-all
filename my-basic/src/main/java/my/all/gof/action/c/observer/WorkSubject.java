package my.all.gof.action.c.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽象主题-持有多个观察者的引用，实现增删观察则及发布消息的功能
 */
public abstract class WorkSubject {

    protected List<WorkObserver> workObservers = new ArrayList<>();

    public void addWorkObservers(WorkObserver workObserver){
        workObservers.add(workObserver);
    }

    public void removeWorkObservers(WorkObserver workObserver){
        workObservers.add(workObserver);
    }

    public abstract void notifyAllObserver(int state);

}
