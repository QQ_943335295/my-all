package my.all.gof.org.f.facade;

public class PushMessageFacade {

    private DingDingPushMessage dingDingPushMessage = new DingDingPushMessage();

    private WechatPushMessage wechatPushMessage = new WechatPushMessage();

    public void pushMessage(){
        dingDingPushMessage.pushMessage();
        wechatPushMessage.pushMessage();
    }

    public static void main(String[] args) {
        new PushMessageFacade().pushMessage();
    }
}
