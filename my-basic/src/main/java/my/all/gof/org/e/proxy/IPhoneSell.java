package my.all.gof.org.e.proxy;

/**
 * 主题：主题和代理公共的方法
 */
public interface IPhoneSell {

    void sell();

}
