package my.all.gof.org.b.brige;

public class HwPhone extends AbstractPhone{

    public HwPhone(IColor color) {
        this.color = color;
    }

    @Override
    public void run() {
        System.err.println("hw run ...");
        color.displayColor();
    }
}
