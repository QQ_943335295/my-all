package my.all.gof.org.c.composite;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 组合部件：根节点，抽象接口，表示共性
 */
@AllArgsConstructor
@Data
public abstract class AbstractFileRoot {

    private String name;

    public abstract void addFile(AbstractFileRoot root);

    public abstract void removeFile(AbstractFileRoot root);

    public abstract void showFile(int depth);

    public static void main(String[] args) {
        AbstractFileRoot root = new FolderNode("zxm");
        root.addFile(new FileLeaf("1.txt"));
        root.addFile(new FileLeaf("2.txt"));
        AbstractFileRoot folder3 = new FolderNode("3");
        AbstractFileRoot folder4 = new FolderNode("4");
        folder4.addFile(new FileLeaf("6"));
        folder3.addFile(folder4);
        folder3.addFile(new FileLeaf("5"));
        root.addFile(folder3);
        root.addFile(new FileLeaf("4.sh"));
        root.showFile(0);
    }
}
