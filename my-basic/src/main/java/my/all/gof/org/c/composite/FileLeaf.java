package my.all.gof.org.c.composite;

public class FileLeaf extends AbstractFileRoot{

    public FileLeaf(String name) {
        super(name);
    }

    @Override
    public void addFile(AbstractFileRoot root) {

    }

    @Override
    public void removeFile(AbstractFileRoot root) {

    }

    @Override
    public void showFile(int depth) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < depth; i++){
            sb.append("-");
        }
        System.err.println(sb + getName());
    }
}
