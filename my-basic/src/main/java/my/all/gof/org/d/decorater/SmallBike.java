package my.all.gof.org.d.decorater;

/**
 * 被装饰者：被装饰的具体对象，原始核心功能
 */
public class SmallBike implements IBike{

    @Override
    public void run() {
        System.err.println("Small bike run ...");
    }
}
