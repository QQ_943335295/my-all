package my.all.gof.org.d.decorater;

/**
 * 装饰着：实现具体的装饰功能
 */
public class RedBikeDecorator extends BikeDecorator{

    public RedBikeDecorator(IBike iBike) {
        super(iBike);
    }

    @Override
    public void run() {
        super.run();
        System.err.println("is a red bike ...");
    }
}
