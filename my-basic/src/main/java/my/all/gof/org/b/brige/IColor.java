package my.all.gof.org.b.brige;

/**
 * 桥接抽象类
 */
public interface IColor {

    void displayColor();

}