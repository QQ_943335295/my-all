package my.all.gof.org.b.brige;

/**
 * 抽象类
 */
public abstract class AbstractPhone {

    protected IColor color;

    abstract public void run();

    public static void main(String[] args) {
        AbstractPhone hw = new HwPhone(new Blue());
        hw.run();
        AbstractPhone ap = new ApPhone(new Red());
        ap.run();
    }
}
