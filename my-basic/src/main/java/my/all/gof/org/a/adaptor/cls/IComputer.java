package my.all.gof.org.a.adaptor.cls;

/**
 * 新的接口-拥有更多的实现
 */
public interface IComputer {

    void work();

    void draw();

    void play();

}
