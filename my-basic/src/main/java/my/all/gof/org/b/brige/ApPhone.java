package my.all.gof.org.b.brige;

public class ApPhone extends AbstractPhone{

    public ApPhone(IColor color) {
        this.color = color;
    }

    @Override
    public void run() {
        System.err.println("ap run ...");
        color.displayColor();
    }
}
