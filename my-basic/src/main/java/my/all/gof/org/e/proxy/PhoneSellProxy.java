package my.all.gof.org.e.proxy;

public class PhoneSellProxy implements IPhoneSell{

    private HwPhoneSell hwPhoneSell = new HwPhoneSell();

    @Override
    public void sell() {
        System.err.println("make address ...");
        hwPhoneSell.sell();
        System.err.println("share money ...");
    }

    public static void main(String[] args) {
        new PhoneSellProxy().sell();
    }

}

