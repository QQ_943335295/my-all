package my.all.gof.org.a.adaptor.api;

/**
 * 具体类，实现自己需要的方法
 */
public class HighComputer extends ComputerAdapter{

    @Override
    public void work() {
        System.err.println("work ...");
    }

    @Override
    public void draw() {
        System.err.println("draw ...");
    }

    @Override
    public void play() {
        System.err.println("play ...");
    }
}
