package my.all.gof.org.a.adaptor.api;

/**
 * 抽象接口
 */
public interface IComputer {

    void work();

    void draw();

    void play();

}
