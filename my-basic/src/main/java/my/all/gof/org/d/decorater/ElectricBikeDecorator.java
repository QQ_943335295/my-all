package my.all.gof.org.d.decorater;

/**
 * 装饰着：实现具体的装饰功能
 */
public class ElectricBikeDecorator extends BikeDecorator{

    public ElectricBikeDecorator(IBike iBike) {
        super(iBike);
    }

    @Override
    public void run() {
        super.run();
        System.err.println("is a electric bike ...");
    }
}
