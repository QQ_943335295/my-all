package my.all.gof.org.a.adaptor.api;

/**
 * 具体类，实现自己需要的方法
 */
public class LowComputer extends ComputerAdapter{

    @Override
    public void work() {
        System.err.println("only work ...");
    }
}
