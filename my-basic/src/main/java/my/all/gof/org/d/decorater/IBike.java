package my.all.gof.org.d.decorater;

/**
 * 抽象组件-定义装饰方法规范
 */
public interface IBike {

    void run();

}
