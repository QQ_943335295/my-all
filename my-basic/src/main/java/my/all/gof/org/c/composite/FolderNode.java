package my.all.gof.org.c.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * 合成部件：节点，有自己的检点，与组合部件类似
 */
public class FolderNode extends AbstractFileRoot{

    List<AbstractFileRoot> list = new ArrayList<>();

    public FolderNode(String name) {
        super(name);
    }

    @Override
    public void addFile(AbstractFileRoot root) {
        list.add(root);
    }

    @Override
    public void removeFile(AbstractFileRoot root) {
        list.remove(root);
    }

    @Override
    public void showFile(int depth) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < depth; i++){
            sb.append("-");
        }
        System.err.println(sb + getName());
        for (AbstractFileRoot root : list){
            root.showFile(depth + 1);
        }
    }
}
