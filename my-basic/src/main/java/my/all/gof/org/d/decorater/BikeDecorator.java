package my.all.gof.org.d.decorater;

import java.io.*;

/**
 * 被装饰者：被装饰的具体对象，原始核心功能
 */
public abstract class BikeDecorator implements IBike{

    protected IBike iBike;

    public BikeDecorator(IBike iBike) {
        this.iBike = iBike;
    }

    @Override
    public void run() {
        iBike.run();
    }

    public static void main(String[] args) throws FileNotFoundException {
        IBike smallBike = new RedBikeDecorator(new ElectricBikeDecorator(new SmallBike()));
        smallBike.run();
        System.err.println("--------------------------------------");
        IBike small2eBike = new ElectricBikeDecorator(smallBike);
        small2eBike.run();

        BufferedInputStream is = new BufferedInputStream(new FileInputStream("1"));
    }
}
