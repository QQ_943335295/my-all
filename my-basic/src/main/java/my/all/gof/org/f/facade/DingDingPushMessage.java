package my.all.gof.org.f.facade;

/**
 * 子系统角色：负责处理各自的任务，不知道门面存在，被门面调用
 */
public class DingDingPushMessage implements IPushMessage{
    @Override
    public void pushMessage() {
        System.err.println("ding ding msg ...");
    }
}
