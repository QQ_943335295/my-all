package my.all.gof.org.e.proxy;

/**
 * 真实的对象：需要被代理的对象
 */
public class HwPhoneSell implements IPhoneSell{

    @Override
    public void sell() {
        System.err.println("hw phone sell ...");
    }
}
