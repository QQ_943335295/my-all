package my.all.gof.org.a.adaptor.api;

/**
 * 接口适配-默认实现接口所有方法
 */
public class ComputerAdapter implements IComputer{

    @Override
    public void work() {

    }

    @Override
    public void draw() {

    }

    @Override
    public void play() {

    }
}
