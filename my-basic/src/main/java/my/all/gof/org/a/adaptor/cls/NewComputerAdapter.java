package my.all.gof.org.a.adaptor.cls;

/**
 * 适配器-肌层原有类实现新的接口方法
 */
public class NewComputerAdapter extends OldComputer implements IComputer{
    @Override
    public void draw() {
        System.err.println("draw ...");
    }

    @Override
    public void play() {
        System.err.println("play ...");
    }
}
