package my.all.gof.create.b.factory.abst;

/**
 * 实际工厂-实现超级工厂，负责创建具体产品
 */
public class MobileFactory implements IOrderFactory{
    @Override
    public IPay getPay() {
        return new MobilePay();
    }

    @Override
    public IRefund getRefund() {
        return new MobileRefund();
    }
}
