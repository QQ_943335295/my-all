package my.all.gof.create.a.single;

/**
 * 单例-饿汉模式
 * @author zxm
 * @date 2023/6/1 21:36
 */
public class SingletonLazy {

    private static SingletonLazy instance = new SingletonLazy();

    private SingletonLazy() {
    }

    public static SingletonLazy getInstance(){
        return instance;
    }

    public static void main(String[] args) {
        System.err.println(getInstance() == getInstance());
    }
}
