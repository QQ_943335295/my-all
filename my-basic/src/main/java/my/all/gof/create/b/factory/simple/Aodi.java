package my.all.gof.create.b.factory.simple;

/**
 * 具体产品-工厂创建目标
 */
public class Aodi implements ICar{
    @Override
    public void run() {
        System.err.println("Aodi run ...");
    }
}
