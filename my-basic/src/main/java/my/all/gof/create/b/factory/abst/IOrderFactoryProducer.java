package my.all.gof.create.b.factory.abst;

import cn.hutool.core.util.StrUtil;

/**
 * 超级工厂生产者
 */
public class IOrderFactoryProducer {
    public static IOrderFactory getOrderFactory(String type){
        if (StrUtil.isNotBlank(type)){
            if ("CardFactory".equals(type)){
                return new CardFactory();
            } else if ("MobileFactory".equals(type)){
                return new MobileFactory();
            }
        }
        return null;
    }

    public static void main(String[] args) {
        IOrderFactory cardFactory = getOrderFactory("CardFactory");
        cardFactory.getPay().Pay();
        IOrderFactory mobileFactory = getOrderFactory("MobileFactory");
        mobileFactory.getPay().Pay();
        mobileFactory.getRefund().refund();
    }
}
