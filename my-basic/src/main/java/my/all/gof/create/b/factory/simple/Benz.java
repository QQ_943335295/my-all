package my.all.gof.create.b.factory.simple;

public class Benz implements ICar{
    @Override
    public void run() {
        System.err.println("Benz run ...");
    }
}
