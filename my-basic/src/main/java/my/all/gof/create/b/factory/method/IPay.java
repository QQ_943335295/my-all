package my.all.gof.create.b.factory.method;

/**
 * 产品抽象类-描述实例共有接口
 */
public interface IPay {
    void pay();
}
