package my.all.gof.create.a.single;

/**
 * 单例静态初始化
 */
public class SingletonStaticInit {

    private static SingletonStaticInit singletonStaticInit;

    private SingletonStaticInit() {
    }

    static {
        singletonStaticInit = new SingletonStaticInit();
    }
}
