package my.all.gof.create.c.build;

/**
 * 指挥者-控制整个组合过程
 */
public class ComputerDirector {

    public Computer directComputer(ComputerBuilder builder){
        Computer computer = builder.buildCpu()
                .buildDisk()
                .buildMemory()
                .createComputer();
        return computer;
    }

    public static void main(String[] args) {
        ComputerDirector computerDirector = new ComputerDirector();
        Computer maxComputer = computerDirector.directComputer(new MaxComputerBuilder());
        maxComputer.run();

        Computer minComputer = computerDirector.directComputer(new MinComputerBuilder());
        minComputer.run();

    }

}
