package my.all.gof.create.c.build;

/**
 * 抽象建造者：定义创建对象公用过程
 */
public interface ComputerBuilder {

    ComputerBuilder buildCpu();

    ComputerBuilder buildMemory();

    ComputerBuilder buildDisk();

    Computer createComputer();
}
