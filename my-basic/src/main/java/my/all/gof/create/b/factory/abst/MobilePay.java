package my.all.gof.create.b.factory.abst;

/**
 * 具体产品-实现抽象产品接口，具体工厂类创建对象
 */
public class MobilePay implements IPay{
    @Override
    public void Pay() {
        System.err.println("MobilePay ...");
    }
}
