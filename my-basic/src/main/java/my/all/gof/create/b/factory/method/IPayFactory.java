package my.all.gof.create.b.factory.method;

/**
 * 抽象工厂-描述具体工厂类的几口
 */
public interface IPayFactory {
    IPay getPay();

    static void main(String[] args) {
        IPayFactory aliPayFactory = new AliPayFactory();
        aliPayFactory.getPay().pay();
        IPayFactory wechatPay = new WechatPayFactory();
        wechatPay.getPay().pay();
    }
}
