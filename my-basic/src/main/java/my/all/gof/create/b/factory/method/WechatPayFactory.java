package my.all.gof.create.b.factory.method;

/**
 * 具体工厂类-工厂具体创建对象，实现抽象产品的接口
 */
public class WechatPayFactory implements IPayFactory{
    @Override
    public IPay getPay() {
        return new WechatPay();
    }
}
