package my.all.gof.create.a.single;

/**
 * 单例-饿汉模式
 * @author zxm
 * @date 2023/6/1 21:36
 */
public class SingletonDoubleCheck {

    /**
     * volatile是Java关键字，它具有可见性和有序性，可防止指令重排
     * 指令重拍：JVM对语句执行的优化，只要语句之间没有相互依赖关系，JVM有权对语句进行优化
     */
    private static volatile SingletonDoubleCheck instance;

    private SingletonDoubleCheck() {
    }

    public static SingletonDoubleCheck getInstance(){
        if (instance == null){
            synchronized (SingletonDoubleCheck.class){
                if (instance == null){
                    instance = new SingletonDoubleCheck();
                }
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        System.err.println(getInstance() == getInstance());
    }
}
