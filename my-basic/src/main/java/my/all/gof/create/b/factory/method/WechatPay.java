package my.all.gof.create.b.factory.method;

/**
 * 具体产品-实现抽象产品接口，工厂创建的对象
 */
public class WechatPay implements IPay{
    @Override
    public void pay() {
        System.err.println("WechatPay ...");
    }
}
