package my.all.gof.create.b.factory.simple;

import cn.hutool.core.util.StrUtil;

/**
 * 工厂类-实现创建产品实例的所有内部逻辑
 */
public class CarFactory {

    public static ICar getCar(String type){
        if (StrUtil.isNotBlank(type)){
            if ("Benz".equals(type)){
                return new Benz();
            } else if ("Aodi".equals(type)){
                return new Aodi();
            }
        }
        return null;
    }

    public static void main(String[] args) {
        ICar benz = CarFactory.getCar("Benz");
        benz.run();
        ICar aodi = CarFactory.getCar("Aodi");
        aodi.run();
    }
}
