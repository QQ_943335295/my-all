package my.all.gof.create.b.factory.simple;

/**
 * 抽象产品-产品的父类
 */
public interface ICar {
    void run();
}
