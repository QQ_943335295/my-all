package my.all.gof.create.b.factory.abst;

/**
 * 具体产品-实现抽象产品接口，具体工厂类创建对象
 */
public class MobileRefund implements IRefund{
    @Override
    public void refund() {
        System.err.println("MobileRefund ...");
    }
}
