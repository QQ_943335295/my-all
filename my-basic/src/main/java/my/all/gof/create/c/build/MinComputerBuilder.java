package my.all.gof.create.c.build;

/**
 * 具体建造者-负责实现自己所创建产品的具体步骤
 */
public class MinComputerBuilder implements ComputerBuilder{
    private Computer computer = new Computer();
    @Override
    public ComputerBuilder buildCpu() {
        computer.setCpu("min cpu ...");
        return this;
    }

    @Override
    public ComputerBuilder buildMemory() {
        computer.setMemory("min memory ...");
        return this;
    }

    @Override
    public ComputerBuilder buildDisk() {
        computer.setDisk("min disk ...");
        return this;
    }

    @Override
    public Computer createComputer() {
        return computer;
    }
}
