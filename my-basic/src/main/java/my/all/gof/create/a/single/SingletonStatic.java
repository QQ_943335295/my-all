package my.all.gof.create.a.single;

/**
 * 单例模式-静态内部类
 */
public class SingletonStatic {

    private static class SingletonStaticHold{
        private static SingletonStatic INSTANCE = new SingletonStatic();
    }

    private SingletonStatic(){
    }

    public static SingletonStatic getInstance(){
        return SingletonStaticHold.INSTANCE;
    }

    public static void main(String[] args) {
        System.err.println(getInstance() == getInstance());
    }
}
