package my.all.gof.create.b.factory.abst;

/**
 * 超级工厂-负责定义抽象工厂的接口
 */
public interface IOrderFactory {

    IPay getPay();

    IRefund getRefund();

}
