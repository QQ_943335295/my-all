package my.all.gof.create.b.factory.abst;

/**
 * 抽象产品-负责定义产品共有接口
 */
public interface IPay {
    void Pay();
}
