package my.all.gof.create.c.build;

import lombok.Data;

/**
 * 具体产品
 */
@Data
public class Computer {

    private String cpu;

    private String memory;

    private String disk;

    public void run(){
        System.err.println(cpu);
        System.err.println(memory);
        System.err.println(disk);
    }

}
