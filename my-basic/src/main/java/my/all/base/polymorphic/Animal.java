package my.all.base.polymorphic;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author zxm
 * @date 2023/6/15
 */
@AllArgsConstructor
@Data
public class Animal {

    private String name;

    public static void main(String[] args) {
        Animal animal = new Dog("阿黄", "黄色");
        System.err.println(animal.name);
        Dog dog = (Dog) animal;
        System.err.println(dog.getName() + ":" + dog.getColor());
    }
}


class Dog extends Animal{
    private String color;

    public Dog(String name, String color) {
        super(name);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
