package my.all.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoop;
import io.netty.util.NettyRuntime;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zxm
 * @date 2023/10/11
 */
public class NettyTest {
    public static void main(String[] args) {
        // 网络通信层
        // netty NioEventLoopGroup 默认线程池数量 = 逻辑处理器数量 * 2
        System.err.println("netty NioEventLoopGroup 默认线程池数量 = 逻辑处理器数量 * 2 —— " + NettyRuntime.availableProcessors() * 2);

        // channel 继承 ChannelOutboundInvoker 实现   register、bind、connect、read、write、flush
        Channel channel;

        // ChannelOutboundInvoker 与 ChannelInboundInvoker 的区别

        // 事件调度层
        EventLoop eventLoop;

        // 服务编排层
        ChannelPipeline channelPipeline;

    }
}
