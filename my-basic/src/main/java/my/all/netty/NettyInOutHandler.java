package my.all.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.sctp.nio.NioSctpServerChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.NettyRuntime;
import lombok.extern.slf4j.Slf4j;

/**
 * 出栈入栈
 * @author zxm
 * @date 2023/10/11
 */
public class NettyInOutHandler {
    public static void main(String[] args) throws InterruptedException {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) {
                ch.pipeline()
                        .addLast(new SampleInBoundHandler("SampleInBoundHandlerA", false))
                        .addLast(new SampleInBoundHandler("SampleInBoundHandlerB", false))
                        .addLast(new SampleInBoundHandler("SampleInBoundHandlerC", true));
                ch.pipeline()
                        .addLast(new SampleOutBoundHandler("SampleOutBoundHandlerA"))
                        .addLast(new SampleOutBoundHandler("SampleOutBoundHandlerB"))
                        .addLast(new SampleOutBoundHandler("SampleOutBoundHandlerC"));
            }
        });
        serverBootstrap.bind(59999).sync();
    }
    public static class SampleInBoundHandler extends ChannelInboundHandlerAdapter {
        private final String name;
        private final boolean flush;

        public SampleInBoundHandler(String name, boolean flush) {
            this.name = name;
            this.flush = flush;
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            System.out.println("InBoundHandler: " + name);
            if (flush) {
                ctx.channel().writeAndFlush(msg);
            } else {
                super.channelRead(ctx, msg);
            }
        }

    }
    public static class SampleOutBoundHandler extends ChannelOutboundHandlerAdapter {
        private final String name;
        public SampleOutBoundHandler(String name) {
            this.name = name;
        }
        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.println("OutBoundHandler: " + name);
            super.write(ctx, msg, promise);
        }
    }
}
