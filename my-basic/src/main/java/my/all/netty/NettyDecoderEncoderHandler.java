package my.all.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.CharsetUtil;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author zxm
 * @date 2023/10/11
 */
public class NettyDecoderEncoderHandler {
    public static void main(String[] args) throws InterruptedException {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(new FixedLengthFrameDecoder(10));
                            ch.pipeline().addLast(new ResponseSampleEncoder());
                            ch.pipeline().addLast(new RequestSampleHandler());
                        }
                    });
            ChannelFuture f = b.bind(59999).sync();
            f.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public static class ResponseSampleEncoder extends MessageToByteEncoder<ResponseSample> {
        @Override
        protected void encode(ChannelHandlerContext ctx, ResponseSample msg, ByteBuf out) {
            if (msg != null) {
                out.writeBytes(msg.getCode().getBytes());
                out.writeBytes(msg.getData().getBytes());
                out.writeLong(msg.getTimestamp());
            }
        }
    }

    public static class RequestSampleHandler extends ChannelInboundHandlerAdapter {
        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) {
            String data = ((ByteBuf) msg).toString(CharsetUtil.UTF_8);
            ResponseSample response = new ResponseSample("OK", data, System.currentTimeMillis());
            ctx.channel().writeAndFlush(response);
        }
    }

    @AllArgsConstructor
    @Data
    public static class ResponseSample {
        private String code;
        private String data;
        private Long timestamp;
    }

}
