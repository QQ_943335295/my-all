package my.all.netty;

/**
 * @author zxm
 * @date 2023/10/17
 */
public class ThreadLocalTest {
    public static void main(String[] args) {
        ThreadLocal<String> t1 = new ThreadLocal<>();
        ThreadLocal<String> t2 = new ThreadLocal<>();
        ThreadLocal<Integer> t3 = new ThreadLocal<>();
        t1.set("zxm");
        t2.set("lxy");
        t3.set(123);
        System.err.println();
    }
}
