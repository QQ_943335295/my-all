package my.all.rabbitmq.robin;

import com.rabbitmq.client.*;
import lombok.SneakyThrows;

import java.io.IOException;

/**
 * 简单模式队列消费者
 *
 * @author zxm
 * @date 2023/12/19
 */
public class RobinReceive1 {
    private final static String QUEUE_NAME = "work_mq_rr";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("zxm");
        factory.setPassword("zxm");
        factory.setVirtualHost("zxm");
        factory.setPort(5672);
        //消费者一般不增加自动关闭
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 限制消费者一次只能消费一条消息，处理完成才能处理条消息
        channel.basicQos(1);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                // consumerTag 是固定的  可以做此会话的名字，    deliveryTag 每次接收消息+1
                System.out.println("consumerTag消息标识=" + consumerTag);
                //可以获取交换机，路由健等
                System.out.println("envelope元数据=" + envelope);
                System.out.println("properties配置信息=" + properties);
                System.out.println("body=" + new String(body, "utf-8"));
                // 手工确认，不是确认多条
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };
        // 取消自动确认
        channel.basicConsume(QUEUE_NAME, false, consumer);
    }
}


