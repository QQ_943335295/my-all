package my.all.rabbitmq.topic;

import com.rabbitmq.client.*;

/**
 * 广播订阅简单模式队列消费者
 * @author zxm
 * @date 2023/12/19
 */
public class TopicReceiveError {
    private final static String EXCHANGE_NAME = "exchange_topic";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("zxm");
        factory.setPassword("zxm");
        factory.setVirtualHost("zxm");
        factory.setPort(5672);
        //消费者一般不增加自动关闭
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        // 绑定交换机，FANOUT扇形，即广播类型
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        // 如果是队列名相同的消费者 - 只有队列和路由key相同时才能这样使用， 并且排他队列的意思是一个队列只能绑定一个消费者
        String queueName = channel.queueDeclare().getQueue();
        // 如果是队列名相同的消费者
        // String queueName = channel.queueDeclare("direct_queue", false, false, false, null).getQueue();
        // 绑定交换机和队列
        channel.queueBind(queueName, EXCHANGE_NAME, "error.*");
        // 自动确认消息
        DeliverCallback deliver = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            // 手工确认，不是确认多条
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        };
        channel.basicConsume(queueName, false, deliver, consumerTag -> {
        });
    }
}


