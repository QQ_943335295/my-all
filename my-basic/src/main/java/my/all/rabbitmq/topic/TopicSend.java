package my.all.rabbitmq.topic;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

/**
 * 广播订阅工厂发送者
 * 指定为扇形发布订阅模型
 * 不需要指定队列名，会自动把消息投递到绑定该交换机的对应的消息队列中
 * @author zxm
 * @date 2023/12/19
 */
public class TopicSend {
    private final static String EXCHANGE_NAME = "exchange_topic";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("zxm");
        factory.setPassword("zxm");
        factory.setVirtualHost("zxm");
        factory.setPort(5672);
        //JDK7语法  或自动关闭  connnection 和channel, 创建连接何信道
        // 生产者不需要过多操作，只需要与交换机绑定
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
            String error = "我是ERROR日志";
            String info = "我是INFO日志";
            String debug = "我是DEBUG日志";
            channel.basicPublish(EXCHANGE_NAME,"error.log", null, error.getBytes(StandardCharsets.UTF_8));
            channel.basicPublish(EXCHANGE_NAME,"info.log", null, info.getBytes(StandardCharsets.UTF_8));
            channel.basicPublish(EXCHANGE_NAME,"debug.log", null, debug.getBytes(StandardCharsets.UTF_8));
            System.out.println("消息发送成功...");
        }
    }
}

