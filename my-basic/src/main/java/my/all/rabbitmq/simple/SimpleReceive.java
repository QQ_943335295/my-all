package my.all.rabbitmq.simple;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * 简单模式队列消费者
 * 1、如果消息被消费了，但是没有确认，则消息会被重新投递到其他消费者中进行消费
 *
 * @author zxm
 * @date 2023/12/19
 */
public class SimpleReceive {
    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("zxm");
        factory.setPassword("zxm");
        factory.setVirtualHost("zxm");
        factory.setPort(5672);
        //消费者一般不增加自动关闭
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        //回调方法，下面两种都行
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // consumerTag 是固定的  可以做此会话的名字，    deliveryTag 每次接收消息+1
                System.out.println("consumerTag消息标识=" + consumerTag);
                //可以获取交换机，路由健等
                System.out.println("envelope元数据=" + envelope);
                System.out.println("properties配置信息=" + properties);
                System.out.println("body=" + new String(body, "utf-8"));
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };
        // 设置消息不自动确认
        channel.basicConsume(QUEUE_NAME, false, consumer);
        // 设置自动确认
        // channel.basicConsume(QUEUE_NAME, true, consumer);
        // 自动确认消息
//        DeliverCallback deliver = (consumerTag, delivery) -> {
//            String message = new String(delivery.getBody(), "UTF-8");
//            System.out.println(" [x] Received '" + message + "'");
//        };
//        channel.basicConsume(QUEUE_NAME, true, deliver, consumerTag -> { });
    }
}


