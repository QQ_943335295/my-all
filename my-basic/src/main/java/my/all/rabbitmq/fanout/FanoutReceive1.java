package my.all.rabbitmq.fanout;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * 广播订阅简单模式队列消费者
 * @author zxm
 * @date 2023/12/19
 */
public class FanoutReceive1 {
    private final static String EXCHANGE_NAME = "exchange_fanout";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("zxm");
        factory.setPassword("zxm");
        factory.setVirtualHost("zxm");
        factory.setPort(5672);
        //消费者一般不增加自动关闭
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        // 绑定交换机，FANOUT扇形，即广播类型
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        // 获取队列（排他队列）
        String queueName = channel.queueDeclare().getQueue();
        // 绑定交换机和队列
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("body-1=" + new String(body, "utf-8"));
                // 手工确认，不是确认多条
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };
        // 取消自动确认
        channel.basicConsume(queueName, false, consumer);
    }
}


