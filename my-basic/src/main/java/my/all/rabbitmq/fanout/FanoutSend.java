package my.all.rabbitmq.fanout;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

/**
 * 广播订阅工厂发送者
 * 指定为扇形发布订阅模型
 * 不需要指定队列名，会自动把消息投递到绑定该交换机的对应的消息队列中
 * @author zxm
 * @date 2023/12/19
 */
public class FanoutSend {
    private final static String EXCHANGE_NAME = "exchange_fanout";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("zxm");
        factory.setPassword("zxm");
        factory.setVirtualHost("zxm");
        factory.setPort(5672);
        //JDK7语法  或自动关闭  connnection 和channel, 创建连接何信道
        // 生产者不需要过多操作，只需要与交换机绑定
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            /**
             * 队列名称
             * 持久化配置：  mq重启后还在
             * 是否独占：只能有一个消费者监听队列；当connection关闭是否删除队列，一般是false，发布订阅是独占
             * 自动删除 : 当没有消费者的时候，自动删除掉，一般是false
             * 其他参数
             * 队列不存在则会自动创建，如果存在则不会覆盖，所以此时的时候需要注意属性
             */
            String message = "Hello World pub !";
            // 绑定交换机,fanout扇形，即广播类型
            channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes(StandardCharsets.UTF_8));
            System.out.println(" [x] Sent '" + message + "'");
        }
    }
}

