package my.all.test.lambda;

public class PrintStream implements FunctionHandler{

    @Override
    public String printString() {
        System.err.println("PrintStream printString");
        return "PrintStream printString";
    }

    @Override
    public String doSomething() {
        System.err.println("PrintStream doSomething");
        return "PrintStream doSomething";
    }
}
