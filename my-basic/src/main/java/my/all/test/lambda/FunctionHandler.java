package my.all.test.lambda;

@FunctionalInterface
public interface FunctionHandler {
    String printString();

    default String doSomething(){
        return "";
    }
    boolean equals(Object obj);
}
