package my.all.test.lambda;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class LambdaDemo {

    @Test
    public void testLambdaMap(){
        Map<String, FunctionHandler> functionHandlerMap = new HashMap<>();
        PrintStream printStream = new PrintStream();
        functionHandlerMap.put("1", printStream);
        PrintOutStream printOutStream = new PrintOutStream();
        functionHandlerMap.put("2", printOutStream);

        FunctionHandler functionHandler1 = functionHandlerMap.get("1");
        functionHandler1.printString();
        functionHandler1.doSomething();

        FunctionHandler functionHandler2 = functionHandlerMap.get("2");
        functionHandler2.printString();
        functionHandler2.doSomething();
    }

}
