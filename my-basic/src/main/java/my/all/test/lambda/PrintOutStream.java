package my.all.test.lambda;

public class PrintOutStream implements FunctionHandler{
    @Override
    public String printString() {
        System.err.println("PrintOutStream printString");
        return "PrintOutStream printString";
    }

    @Override
    public String doSomething() {
        System.err.println("PrintOutStream doSomething");
        return "PrintOutStream doSomething";
    }
}
