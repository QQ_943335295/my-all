package my.all.test.basic;

/**
 * 静态父类
 * @author zxm
 * @date 2024/3/15
 */
public class StaticChild extends StaticParent{

    /**
     * 静态方法不能被子类重写，因为方法是从属于类的
     */
    public static void staticMethodOverride(){
        System.err.println("StaticChild staticMethodOverride ...");
    }

    public static void main(String[] args) {
        StaticChild.staticMethodOverride();
    }

}
