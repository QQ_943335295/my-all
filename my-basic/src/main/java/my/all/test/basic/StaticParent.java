package my.all.test.basic;

/**
 * 静态父类
 * @author zxm
 * @date 2024/3/15
 */
public class StaticParent {


    public static void staticMethodOverride(){
        System.err.println("StaticParent staticMethodOverride ...");
    }

}
