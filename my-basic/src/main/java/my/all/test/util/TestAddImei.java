//package my.all.test.util;
//
//import cn.hutool.http.HttpRequest;
//import com.alibaba.fastjson.JSONObject;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//
//import java.io.BufferedReader;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author zxm
// * @date 2022/6/22
// */
//public class TestAddImei {
//
//    public static void main(String[] args) throws IOException {
//        BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\zxm\\Desktop\\1.txt"));
//        String line = null;
//        List<MeterImei> list = new ArrayList<>();
//        while ( (line = in.readLine()) != null){
//            String[] lines = line.split("\t");
//            MeterImei meterImei = new MeterImei("15063139", lines[0], lines[1]);
//            list.add(meterImei);
//        }
//        for (int i = 1; i < list.size(); i++){
//            String resultStr =  HttpRequest.post("https://external.laison.top/v1/openapi/v1/telecom/nb/product/add/meter")
//                    .header("Authorization","Bearer 72f2ce4c-e117-455f-9171-cd688b18c0b9")
//                    .body(JSONObject.toJSONString(list.get(i)))
//                    .timeout(20000)
//                    .execute().body();
//            System.err.println(i + "的结果" + resultStr);
//        }
//
//    }
//
//    @Data
//    @AllArgsConstructor
//    static class MeterImei{
//        private String productId;
//        private String meterNo;
//        private String imei;
//    }
//
//
//}
