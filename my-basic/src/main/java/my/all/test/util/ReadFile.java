package my.all.test.util;

import cn.hutool.core.util.StrUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zxm
 * @date 2022/7/1
 */
public class ReadFile {
    public static void main(String[] args) throws IOException {
        String dir = "D:\\project\\code\\lapis-meter-server-04\\meter-server\\src\\main\\java\\com\\laisontech\\lapis\\server\\open\\dispatcher\\";
        File file = new File(dir);
        List<String> files = new ArrayList<>();
        if (file.exists() && file.isDirectory()){
            String[] dirFiles = file.list();
            for (String str : dirFiles){
                if ((new File(dir + str)).isDirectory()){
                    System.err.println(str);
                } else {
                    files.add(dir + str);
                }
            }
        }
//        List<String> files = Arrays.asList(
//                "D:\\project\\code\\Lapis-Enterprise-06\\lapis-open\\src\\main\\resources\\log\\logback-file.xml"
//                );
        String outFile = "C:\\Users\\zxm\\Desktop\\file.txt";
        readFileDelEmptyLine(files, outFile);
    }

    public static void readFileDelEmptyLine(List<String> files, String outFile) throws IOException {
        File file = new File(outFile);
        if (file.exists()){
            file.delete();
        }
        for(int i = 0; i < files.size(); i++){
            BufferedReader in = new BufferedReader(new FileReader(files.get(i)));
            BufferedWriter out = new BufferedWriter(new FileWriter(outFile, true));
            String line = null;
            while ( (line = in.readLine()) != null){
                if (StrUtil.isNotBlank(line)){
                    out.write(line);
                    out.flush();
                    out.newLine();
                }
            }
        }
    }
}
