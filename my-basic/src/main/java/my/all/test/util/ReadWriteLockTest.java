//package my.all.test.util;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.locks.ReadWriteLock;
//import java.util.concurrent.locks.ReentrantReadWriteLock;
//
///**
// * @author zxm
// * @date 2022/5/13
// */
//public class ReadWriteLockTest {
//
//    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
//    private List<Integer> list = new ArrayList<>();
//
//
//    public void add(){
//        readWriteLock.writeLock().lock();
//        System.err.println("开始写...");
//        try {
//            Thread.sleep(100);
//            list.add((int) Math.random() * 100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } finally {
//            System.err.println("结束写..." + list.size());
//            readWriteLock.writeLock().unlock();
//        }
//    }
//
//    public void get(){
//        readWriteLock.readLock().lock();
//        System.err.println("开始读...");
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } finally {
//            System.err.println("结束读...：" + list.size());
//            readWriteLock.readLock().unlock();
//
//        }
//    }
//
//
//    public static void main(String[] args) {
//        ReadWriteLockTest readWriteLockTest = new ReadWriteLockTest();
//        new Thread(()->{
//            for (int i = 0; i < 10; i++ ){
//                new Thread(()-> readWriteLockTest.add()).start();
//            }
//        }).start();
//
//        new Thread(()->{
//            for (int i = 0; i < 10; i++ ) {
//                new Thread(() -> readWriteLockTest.get()).start();
//            }
//        }).start();
//
//
//    }
//
//}
