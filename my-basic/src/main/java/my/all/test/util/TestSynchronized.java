//package my.all.test.util;
//
///**
// * @author zxm
// * @date 2022/5/11
// */
//public class TestSynchronized {
//
//    private Object obj = new Object();
//
//    public synchronized void method1() {
//        try {
//            Thread.sleep(5000L);
//            System.err.println(11111);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public synchronized void method2() {
//        System.err.println(22222);
//    }
//
//    public void method11() {
//        synchronized (obj){
//            try {
//                Thread.sleep(5000L);
//                System.err.println(11111);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//    public void method22() {
//        synchronized (obj){
//            System.err.println(22222);
//        }
//    }
//
//
//
//    public void method13() {
//        synchronized (obj){
//            try {
//                Thread.sleep(5000L);
//                System.err.println(11111);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//    public void method23() {
//        obj = new Object();
//        System.err.println(22222);
//    }
//
//
//    public static void main(String[] args) throws InterruptedException {
////        new Thread(() -> new TestSynchronized().method1()).start();
////        Thread.sleep(1000L);
////        new Thread(() -> new TestSynchronized().method2()).start();
//
////        new Thread(() -> new TestSynchronized().method11()).start();
////        Thread.sleep(1000L);
////        new Thread(() -> new TestSynchronized().method22()).start();
//
//        new Thread(() -> new TestSynchronized().method13()).start();
//        Thread.sleep(1000L);
//        new Thread(() -> new TestSynchronized().method23()).start();
//
//    }
//
//
//
//}
