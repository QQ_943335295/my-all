package my.all.test.demo;

import org.junit.Test;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author zxm
 * @date 2023/10/27
 */
public class TestBasicDemo01 {

    /**
     * 位运算
     */
    @Test
    public void testBitWish() {
        // 异或：同为0，异为1
        System.err.println(Integer.toBinaryString(0b110 ^ 0b100));
        // 一个数与另外一个数异或两次是其本身，一个数和自身异或结果为0
        int a = 0b110;
        int b = 0b100;
        System.err.println(Integer.toBinaryString(a ^ b ^ a));
        System.err.println(Integer.toBinaryString(a ^ a));

        // 数字交换
        System.err.println(a);
        System.err.println(b);
        a = a + b;
        b = a - b;
        a = a - b;
        System.err.println("--");
        System.err.println(a);
        System.err.println(b);
    }

    /**
     * 基本数据类型：
     * boolean - 1
     * byte - 1
     * short - 2
     * char - 2
     * int - 4
     * long - 8
     * float - 4
     * double - 8
     */
    @Test
    public void basicDataType() {
        int i = 10;
        System.err.println(i++);
        int j = 10;
        System.err.println(++j);
    }


    /**
     * 测试返回测试
     * finally中的return语句一定会被执行
     */
    @Test
    public void testExceptionReturn() {
        System.err.println(exceptionReturn01());
        System.err.println(exceptionReturn02());
    }

    // 返回3
    private int exceptionReturn01() {
        int a = 1;
        try {
            System.out.println(a / 0);
            a = 2;
        } catch (ArithmeticException e) {
            a = 3;
            return a;
        } finally {
            a = 4;
        }
        return a;
    }

    // 返回2
    private int exceptionReturn02() {
        int a = 1;
        try {
            System.out.println(a / 0);
            a = 2;
        } catch (ArithmeticException e) {
            a = 3;
            return a;
        } finally {
            a = 4;
            return a;
        }
    }

    /**
     * 递归：测试输出目录下所有文件信息
     */
    @Test
    public void printDir() {
        List<String> files = new ArrayList<>();
        String path = "D:\\project\\code\\my-all\\mini-rpc\\rpc-consumer";
        findAllFileInPath(new File(path), files, new StringBuilder(""));
        files.forEach(System.err::println);

    }

    private void findAllFileInPath(File file, List<String> files, StringBuilder prefix) {
        files.add(prefix + file.getName());
        if (file.isDirectory()) {
            File[] temps = file.listFiles();
            if (temps != null) {
                prefix.append("-");
                for (File temp : temps) {
                    findAllFileInPath(temp, files, prefix);
                }
            }
        }
    }


    /**
     * String相关应用，包括intern
     */
    @Test
    public void testStringEqual() {
        // 常量池中创建一个对象
        String str1 = "hello lxy";
        // 创建一个新的对象
        String str2 = new String("hello lxy");
        // false
        System.err.println(str1 == str2);
        // true
        System.err.println(str1.equals(str2));
        System.err.println("-------------------------------------");

        String str3 = new String("hello lxy baby");
        String str4 = "hello lxy baby";
        // false
        System.err.println(str3 == str4);
        System.err.println("-------------------------------------");

        String str5 = "hello " + "lxy";
        // true
        System.err.println(str1 == str5);
        // false
        System.err.println(str2 == str5);
        System.err.println("-------------------------------------");

        // jdk1.6- 如果存在，intern方法 它将返回常量池中该字符串的引用。如果不存在，它会在常量池中创建一个新的字符串实例，并返回对该实例的引用。
        // jdk1.7+ 无论常量池中是否存在对象，intern 方法都会返回指向常量池中对象的引用。
        String str6 = "lxy".intern();
        // 返回常量池对象
        String str7 = "lxy";
        // true
        System.err.println(str6 == str7);
        System.err.println("-------------------------------------");

        String str8 = new String("lxy zxm");
        String str9 = "lxy zxm";
        String intern = str8.intern();
        // true
        System.err.println(intern == str9);
        // false
        System.err.println(intern == str8);
        System.err.println("-------------------------------------");


        String str10 = new String("lxy zxm baby");
        String intern2 = str10.intern();
        String str11 = "lxy zxm baby";
        // false
        System.err.println(str10 == intern2);
        // true
        System.err.println(intern2 == str11);
        System.err.println("-------------------------------------");
    }

    /**
     * String、StringBuffer与StringBuilder的区别
     * 1、都是final修饰，不允许被继承
     * 2、1.8-本质都是char[]字符数组实现
     * 3、string中的字符数组是被final修饰的，是不可变对象，其他两个是可变
     * 4、1.9+之后变成byte[]
     * （1）优化String节省jvm内存空间的必要性，JVM内存进空间中存放最多的就是字符串
     * （2）一个char字符占用2个字节，使用UTF-16编码
     * （3）增加coder字段来标志用的是UTF-16编码还是Latin-1编码，此编码是一个字符占用1个字节
     * （4）java会根据字符串的内容自动选择UTF-16编码还是Latin-1编码
     */
    @Test
    public void stringBuildBuffer() {
        // 线程不安全，适合操作少量且不易改变的情况下使用，否则容易产生大量中间对象
        String s = new String("str");
        // 线程不安全，适合单线程下操作大量易变的字符
        StringBuilder sb = new StringBuilder("sb");
        // 方法全加StringBuffer，线程安全
        StringBuffer sbf = new StringBuffer("sbf");
    }

    /**
     * 面向对象
     * 1、封装（Encapsulation）
     * （1）封装的主要目的是隐藏信息，保护数据访问。
     * （2）它通过暴露有限接口和属性，并使用访问权限控制，
     * （3）例如Java的访问修饰符private、public等关键字，来提高代码的可维护性，降低接口的复杂度，并提高类的易用性。
     * 2、抽象（Abstraction）
     * （1）抽象的主要目的是隐藏具体实现，使使用者只需关心功能，无需关心实现。
     * （2）它通过接口类或者抽象类实现，可以提高代码的扩展性、维护性，降低复杂度，减少细节负担。
     * 3、继承（Inheritance）
     * （1）继承的主要目的是表示is-a关系，分为单继承和多继承。
     * （2）它需要编程语言提供特殊语法机制，例如Java的“extends”，C++的“:”。继承可以解决代码复用问题。
     * 4、多态（Polymorphism）：多态的主要作用是子类可以替换父类，在运行时调用子类的实现。它需要编程语言提供特殊的语法机制，例如接口类。多态可以提高代码扩展性和复用性
     */
    @Test
    public void faceObject() {

    }

    abstract class TestAbstract {
        abstract void testAbstractMethod();
    }


    @Test
    public void collections() {
        /** list */
        // 底层是数组实现，线程不安全，查询和修改非常快，但是增加和删除慢
        ArrayList arrayList = new ArrayList();
        // 底层是双向链表，线程不安全，查询和修改速度慢，但是增加和删除速度快
        LinkedList linkedList = new LinkedList<>();
        // 底层是数组实现，线程安全的，操作的时候使用synchronized进行加锁
        Vector vector = new Vector();

        // 保证线程安全实现
        // 方式一：自己写个包装类，根据业务一般是add/update/remove加锁
        //方式二 使用synchronized加锁
        Collections.synchronizedList(new ArrayList<>());
        //方式三：CopyOnWriteArrayList<>()  使用ReentrantLock加锁
        // 设计思想采用读写分离+最终一致性，写时采用复制机制，使得内存中存有两份对象，如果对象过大，容易产生GC
        CopyOnWriteArrayList<Object> objects = new CopyOnWriteArrayList<>();

        /** map **/
        // 底层是基于数组+链表，非线程安全的，默认容量是16、允许有空的健和值
        // 散列桶(数组+链表)，可以实现快速的存储和检索，但是确实包含无序的元素，适用于在map中插入删除和定位元素
        /**
         * HashMap步骤
         * 1、判断数组是否为空，为空则扩容
         * 2、数组不为空，根据hash判断应该放在哪个桶位，这个桶位没有值，则直接插入节点
         * 3、如果对应桶位有值，则判断key值是否一样，一样则直接替换
         * 4、如果key值不一样，判断是否为树节点，如果是树节点，则按树的方式插入
         * 5、如果不是树节点，则直接链表遍历插入，并判断链表长度是否大于8，如果大于8，则直接转换为红黑树
         * 6、如果存放的值大于阈值，则进行扩容
         */
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(null, null);

        // 基于hash表实现，线程安全，通过synchronized嘉措，默认容量是11，不允许null的键与值
        Hashtable<String, String> hashtable = new Hashtable<>();
        // 不允许key为null，需要获取key的hashcode
        // hashtable.put(null, "str");


        // https://blog.csdn.net/u012860938/article/details/95613684
        // LinkedHashMap 底层实现是hashmap + 双向链表（）
        // 通过特有底层双向链表的支持，使得LinkedHashMap可以保存元素之间的顺序，例如插入顺序或者访问顺序
        // LinkedHashMap支持两种缓存策略。FIFO和LRU。大家应该也猜到控制策略的地方就是accessOrder。
        // 默认为false。就是FIFO。设置为true时就是LRU。
        // Entry<K,V> before, after, int hash, K key, V value, Node<K,V> next;
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("zxm", "lxy");

        LinkedHashMap<String, String> linkedHashMap01 = new LinkedHashMap<>();
        linkedHashMap01.put("1", "1");
        linkedHashMap01.put("2", "2");
        linkedHashMap01.put("6", "6");
        linkedHashMap01.put("5", "5");
        for (Map.Entry<String, String> entry : linkedHashMap01.entrySet()) {
            System.err.println(entry.getKey() + " : " + entry.getValue());
        }
        System.err.println("----------------------------");
        LinkedHashMap<String, String> linkedHashMap02 = new LinkedHashMap<>(16, 0.75f, true);
        linkedHashMap02.put("1", "1");
        linkedHashMap02.put("2", "2");
        linkedHashMap02.put("6", "6");
        linkedHashMap02.put("5", "5");
        linkedHashMap02.get("1");
        linkedHashMap02.get("6");
        linkedHashMap02.get("5");
        for (Map.Entry<String, String> entry : linkedHashMap02.entrySet()) {
            System.err.println(entry.getKey() + " : " + entry.getValue());
        }
        // 使用存储结构是一个平衡二叉树->红黑树，可以自定义排序规则，要实现Comparator接口
        // 能便捷的实现内部元素的各种排序，但是一般性能比HashMap差，适用于安装自然排序或者自定义排序规则
        // 写过微信支付签名工具类就用这个类)
        // 按照添加顺序使用LinkedHashMap，按照自然排序使用TreeMap，自定义排序 TreeMap(Comparetor c)
        TreeMap<String, String> treeMap = new TreeMap<>();

        // 核心就是不保存重复的元素，存储一组唯一的对象
        // set的每一种实现都是对应Map里面的一种封装，
        // HashSet对应的就是HashMap，treeSet对应的就是treeMap
        HashSet<String> hashSet = new HashSet<>();
        TreeSet<String> treeSet = new TreeSet<>();

        ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        concurrentHashMap.put("key", "value");
    }

    /**
     * hash和equal
     */
    @Test
    public void hashCodeAndEquals() {
        HashCodeAndEqualsDemo demo1 = new HashCodeAndEqualsDemo(26, "zxm");
        HashCodeAndEqualsDemo demo2 = new HashCodeAndEqualsDemo(26, "zxm");
        System.err.println(demo1.equals(demo2));

    }

    class HashCodeAndEqualsDemo {
        private Integer age;
        private String name;

        public HashCodeAndEqualsDemo(Integer age, String name) {
            this.age = age;
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        /**
         * Object顶级类中的方法，会根据一定的hash规则来计算出一个int类型的数据
         */
        @Override
        public int hashCode() {
            return super.hashCode();
        }

        /**
         * Object顶级类中的方法，会根据一定的规则判断两个对象是否相同
         * 一般都会自定义对象比较规则，规则如下
         * （1）比较内存地址是否相同
         * （2）判断是否为空与类型是否匹配
         * （3）强转比较每一个字段是否相同
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj != null && obj.getClass() == getClass()) {
                HashCodeAndEqualsDemo temp = (HashCodeAndEqualsDemo) obj;
                if (age != null && name != null) {
                    return age.equals(temp.getAge()) && name.equals(temp.getName());
                } else {
                    return (age == null && temp.getAge() == null) || (name == null && temp.getName() == null);
                }
            }
            return false;
        }
    }

}
