package my.all.test.demo;

import io.netty.channel.DefaultEventLoop;
import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.Promise;

import java.util.concurrent.*;

/**
 * @author zxm
 * @date 2023/11/14
 */
public class ThreadTestDemo {

    /**
     * netty promise 异步提交及响应
     */
    public static void testPromise(){
        Promise<String> promise = new DefaultPromise<>(new DefaultEventLoop());
        new Thread(() -> {
            try {
                Thread.sleep(10000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            promise.setSuccess("hello world !");
        }).start();

        String reuslt = null;
        try {
            reuslt = promise.get(30000L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        System.err.println(reuslt);
    }

    /**
     * 交互打印线程
     */
    static volatile Integer num = 0;
    static Integer MAX_NUM = 1000;
    static Object lock = new Object();

    public static void threadPrintNumAdd() {
        new Thread(()-> doPrint(), "A").start();
        new Thread(()-> doPrint(), "B").start();
        new Thread(()-> doPrint(), "C").start();
        new Thread(()-> doPrint(), "D").start();
    }

    static void doPrint(){
        int count = 0;
        while (num < MAX_NUM){
            count++;
            synchronized (lock){
                lock.notify();
                num++;
                System.err.println(Thread.currentThread().getName() + ":" + num);
                if (num >= MAX_NUM){
                    break;
                }
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.err.println(Thread.currentThread().getName() + "- count :" + count);
    }


    /**
     * 测试FutureTask + CallAble
     */
    public static void testFutureTask(){
        FutureTask<String> callAbleFutureTask = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "Callable";
            }
        });
        new Thread(callAbleFutureTask).start();
        try {
            System.err.println(callAbleFutureTask.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }


    public static void testSemaphore(){
        Semaphore semaphore = new Semaphore(1);
        new Thread(()-> doPrint(semaphore), "A").start();
        new Thread(()-> doPrint(semaphore), "B").start();
    }

    private static void doPrint(Semaphore semaphore) {
        while (num < MAX_NUM){
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.err.println(Thread.currentThread().getName() + ":" + num++);
            semaphore.release();
        }
    }

    public static void main(String[] args) {
        // 测试netty promise异步提交
        // testPromise();

        // 两个线程交替打印
        // threadPrintNumAdd();

        // 测试FutureTask
        // testFutureTask();

        //
        testSemaphore();
    }
}
