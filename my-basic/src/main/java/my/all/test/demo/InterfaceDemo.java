package my.all.test.demo;

/**
 * 接口测试案例
 * @author zxm
 * @date 2023/5/16
 */
public interface InterfaceDemo {

    default void doSometing(){
        System.err.println(111);
    }
    static void doOtherting(){
        System.err.println(222);
    }
}

class DemoInterfaceExt implements InterfaceDemo{

}