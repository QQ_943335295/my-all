package my.all.test.demo;

import cn.hutool.core.util.HexUtil;
import lombok.SneakyThrows;
import org.junit.Test;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Java基础测试
 *
 * @author zxm
 * @date 2023/5/16
 */
public class BasicTestDemo {

    /**
     * 测试异常捕捉返回值
     */
    @Test
    public void testTryCatchResult() {
        // 输出结果3
        System.err.println(testTryCatchResult01());
        // 输出结果4
        System.err.println(testTryCatchResult02());
    }

    /**
     * 测试异常捕捉返回值-catch中返回
     */
    public static int testTryCatchResult01() {
        int a = 1;
        try {
            System.out.println(a / 0);
            a = 2;
        } catch (ArithmeticException e) {
            a = 3;
            return a;
        } finally {
            a = 4;
        }
        return a;
    }

    /**
     * 测试异常捕捉返回值-finally中返回
     */
    public static int testTryCatchResult02() {
        int a = 1;
        try {
            System.out.println(a / 0);
            a = 2;
        } catch (ArithmeticException e) {
            a = 3;
            return a;
        } finally {
            a = 4;
            return a;
        }
    }

    /**
     * 测试异常捕捉新特性
     */
    public void testNewTryCatchIO() {
        try (
                FileInputStream fis = new FileInputStream("/Users/xdclass/Desktop/test.txt");
                BufferedInputStream bis = new BufferedInputStream(fis);
                FileOutputStream fos = new FileOutputStream("/Users/xdclass/Desktop/copy.txt");
                BufferedOutputStream bos = new BufferedOutputStream(fos);
        ) {
            int size;
            byte[] buf = new byte[1024];
            while ((size = bis.read(buf)) != -1) {
                bos.write(buf, 0, size);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getAllFiles(){
        //找出某目录下的所有子目录以及子文件并打印到控制台上
        List<String> paths = new ArrayList<>();
        getAllFilePaths(new File("C:\\Users\\zxm\\Desktop\\net-deep"),paths);
        for(String path : paths){
            System.out.println(path);
        }
    }

    /**
     * 测试获取目录下所有文件
     */
    private static void getAllFilePaths(File filePath, List<String> paths){
        File[] files = filePath.listFiles();
        if (files == null){
            return;
        }
        for (File file : files){
            paths.add(file.getName());
            if (file.isDirectory()){
                getAllFilePaths(file, paths);
            }
        }
    }


    /**
     * 测试Map
     */
    @Test
    public void testMap(){
        // 按排序顺序输出
         Map<Integer, String> map = new HashMap<>();
        // 按加入顺序输出
         Map<Integer, String> map2 = new LinkedHashMap<>();
        // 按排序顺序输出
        Map<Integer, String> map3 = new TreeMap<>();
        map.put(1, "java1");
        map.put(3, "java2");
        map.put(5, "java se");
        map.put(2, "java 88");
        map.put(4, "java frame");
        map.put(7, "java ok");
        Set<Integer> integers = map.keySet();
        System.err.println(integers);
        Set<Map.Entry<Integer, String>> set = map.entrySet();
        Iterator<Map.Entry<Integer, String>> iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            Integer key = (Integer) entry.getKey();
            String value = (String) entry.getValue();
            System.out.println("key:" + key + ",value:" + value);
        }
    }


    /**
     * ThreadLocal GC测试
     */
    @Test
    public void testThreadLocalGC() throws InterruptedException {
        WeakReference<Integer> wr = new WeakReference<Integer>(1);
        ThreadLocal<Long> threadLocal = new ThreadLocal<>();
        threadLocal.set(1L);
        System.err.println(threadLocal.get());
        ThreadLocal<String> threadLocal1 = new ThreadLocal<>();
        threadLocal1.set("str");
        System.err.println(threadLocal1.get());
        Thread thread = Thread.currentThread();
        System.err.println(thread.getId());
        System.err.println("wr:" + wr.get());
        System.gc();
        Thread.sleep(10000L);
        System.err.println("wr2:" + wr.get());
        System.runFinalization();
        Runtime.getRuntime().runFinalization();
        System.err.println("first:" + threadLocal1.get());
        System.err.println("second:" +threadLocal1.get());
        System.err.println("third:" +threadLocal1.get());
    }

    /**
     * 测试CompletableFuture常用API
     */
    @Test
    public void testCompletableFutureApi() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        // Future 的使用方式
        Future<Integer> executorServiceFuture = executorService.submit(()->{
            Thread.sleep(2000);
            return Math.round(10);
        });
        System.err.println(executorServiceFuture.isDone());
        //轮询获取结果，耗费的CPU资源
        while (true){
            if(executorServiceFuture.isDone()) {
                System.out.println(executorServiceFuture.get());
                break;
            }
        }
        // 无返回结果-等待线程执行的值
        CompletableFuture future0 = CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                System.err.println(1111);
            }
        });
        // 有返回结果-等待线程执行的值
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return 9;
            }
        });
        System.err.println(future.get());

        // 无返回值——作为下一个参数的的入参
        CompletableFuture<Void> future3 = future.thenAccept(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.err.println(integer + 1);
            }
        });

        // 有返回值——作为下一个参数的的入参
        CompletableFuture<Integer> future2 = future.thenApply(new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer lastStepValue) {
                return 3 + lastStepValue;
            }
        });
        System.err.println(future2.get());


    }

    /**
     *  16进制转换为有符号int
     *  大端模式：高字节在前，也就是高字节排在数组低位，数组低位指的是数组位置序号小的位置
     *  小端模式：低字节在前，也就是低字节排在数组低位，数组低位指的是数组位置序号小的位置
     */
    @Test
    public void testConvertHexToSignInt(){

        // byte[] hexBytes = HexUtil.decodeHex("FF98");

        byte[] hexBytes = HexUtil.decodeHex("7fff");

        int intValue = (hexBytes[0] << 8) | (hexBytes[1] & 0xFF);
        System.err.println(intValue);

    }

    @Test
    public void testHexEqualInt(){
        byte[] hexBytes = {0x68};
        System.err.println(hexBytes[0] == 0x68);
    }


    @Test
    public void testListStream(){
        // 从小到大排序
        List<Integer> list = Arrays.asList(9, 23, 4, 8, 7, 10, 14, 9, 100, 5);
        List<Integer> collect = list.stream().sorted(Integer::compare).collect(Collectors.toList());
        System.err.println(collect);
    }


    public static ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 4, 60, TimeUnit.SECONDS, new LinkedBlockingDeque<>(1000));

    @SneakyThrows
    @Test
    public void testFuture(){
        // Future Runnable 无参返回
        Future future = executor.submit(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                Thread.sleep(1000L);
                System.err.println("future Runnable ...");
            }
        });
        while (!future.isDone()){
            Thread.sleep(500L);
            System.err.println("future Runnable not done ...");
        }
        // Future Callable有参返回
        Future<Integer> future2 = executor.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 999;
            }
        });
        System.err.println("获取future2 callable的返回值：" + future2.get());
    }

    @SneakyThrows
    @Test
    public void testCompletableFuture(){
        CompletableFuture<Void> completableFutureRunnable = CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                System.err.println("CompletableFuture Runnable");
            }
        }, executor);
    }



}
