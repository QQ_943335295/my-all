package my.architeture.struct;

import org.junit.Test;

/**
 * 数据结构测试类
 */
public class MyStructTest {

    /**
     * 栈
     */
    @Test
    public void testMyStack(){
        MyStack myStack = new MyStack(5);
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);
        myStack.push(5);
        System.out.println(myStack.peek());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        System.out.println(myStack.peek());
    }

    /**
     * 队列
     */
    @Test
    public void testMyQueue(){
        MyQueue myQueue = new MyQueue(5);
        myQueue.enterQueue(1);
        myQueue.enterQueue(2);
        myQueue.enterQueue(3);
        System.out.println(myQueue.deleteQueue());
        System.out.println(myQueue.deleteQueue());
        myQueue.enterQueue(4);
        myQueue.enterQueue(5);
        System.out.println(myQueue.deleteQueue());
        System.out.println(myQueue.deleteQueue());
        System.out.println(myQueue.deleteQueue());
        System.out.println(myQueue.deleteQueue());
    }


    @Test
    public void testMyLinkList(){
        MyLinkList myLinkList = new MyLinkList();
        myLinkList.add(1);
        myLinkList.add(2);
        // 1 2
        myLinkList.printNode();
        myLinkList.delete(1);
        // 2
        myLinkList.printNode();
        myLinkList.add(3);
        myLinkList.add(4);
        myLinkList.add(5);
        myLinkList.delete(4);
        // 2  3  5
        myLinkList.printNode();
        myLinkList.delete(1);
        // 2  3  5  6
        myLinkList.add(6);
        myLinkList.printNode();
    }
}
