package my.architeture.struct;

/**
 * 单向链表
 * @author zxm
 * @date 2023/11/28 21:53
 */
public class MyLinkList {

    private Node head;

    public MyLinkList() {
        this.head = null;
    }

    public void add(int val){
        if (head == null){
            head = new Node(val);
            return;
        }
        Node temp = head;
        while (temp.next != null){
            temp = temp.next;
        }
        temp.next = new Node(val);
    }

    public void delete(int val){
        if (head == null){
            return;
        }
        if (head.value == val){
            head = head.next;
        }
        Node temp = head;
        while (temp.next != null && temp.next.value != val){
            temp = temp.next;
        }
        if (temp.next != null && temp.next.value == val){
            temp.next = temp.next.next;
        }
    }

    public void printNode(){
        System.out.println("-----------------");
        Node temp = head;
        while (temp != null){
            System.out.print(temp.value + "  ");
            temp = temp.next;
        }
        System.out.println("-----------------");
    }



    class Node{
        private int value;
        private Node next;

        public Node(int value) {
            this.value = value;
        }

    }
}
