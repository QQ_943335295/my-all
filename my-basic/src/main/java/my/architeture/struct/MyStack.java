package my.architeture.struct;

/**
 * 栈
 * @author zxm
 * @date 2023/11/28 21:20
 */
public class MyStack {

    /** 元素 */
    private int[] elements;
    /** 栈顶指针 */
    private int top;

    public MyStack(int capacity) {
        this.elements = new int[capacity];
        this.top = -1;
    }

    /**
     * 入栈
     * @param val 值
     */
    public void push(int val){
        if (top >= elements.length - 1){
            throw new RuntimeException("栈满...");
        }
        elements[++top] = val;
    }

    /**
     *  出栈
     */
    public int pop(){
        if (top < 0){
            throw new RuntimeException("栈空...");
        }
        return elements[top--];
    }

    /**
     * 查看栈顶
     */
    public int peek(){
        if (top < 0){
            throw new RuntimeException("栈空...");
        }
        return elements[top];
    }
}
