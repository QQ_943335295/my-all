package my.architeture.struct;

/**
 * 队列
 */
public class MyQueue {

    private int[] elements;

    private int head;

    private int tail;

    public MyQueue(int length) {
        this.elements = new int[length];
        head = -1;
        tail = -1;
    }

    /**
     * 入队
     */
    public boolean enterQueue(int val){
        if (tail >= elements.length){
            System.err.println("队满...");
            return false;
        }
        this.elements[++tail] = val;
        return true;
    }

    /**
     * 入队
     */
    public int deleteQueue(){
        if (head >= tail){
            throw new RuntimeException("队空...");
        }
        return this.elements[++head];
    }
}
