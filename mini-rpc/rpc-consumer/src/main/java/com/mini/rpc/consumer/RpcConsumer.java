package com.mini.rpc.consumer;

import com.mini.rpc.codec.MiniRpcDecoder;
import com.mini.rpc.codec.MiniRpcEncoder;
import com.mini.rpc.common.MiniRpcRequest;
import com.mini.rpc.common.MiniRpcResponse;
import com.mini.rpc.common.RpcServiceHelper;
import com.mini.rpc.common.ServiceMeta;
import com.mini.rpc.consumer.annotation.RpcReference;
import com.mini.rpc.handler.RpcResponseHandler;
import com.mini.rpc.protocol.MiniRpcProtocol;
import com.mini.rpc.protocol.MsgHeader;
import com.mini.rpc.provider.registry.RegistryService;
import com.mini.rpc.serialization.HessianSerialization;
import com.mini.rpc.serialization.RpcSerialization;
import com.mini.rpc.serialization.SerializationFactory;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;


/**
 * 1、概念
 * （1）{@link FactoryBean} 是Spring框架中的一个接口，它提供了一个创建bean的工厂模式。
 *      通过实现FactoryBean接口，可以自定义bean的实例化逻辑，并将实例化过程与Spring容器解耦，提高代码的灵活性和可维护性。
 *      FactoryBean接口定义了一个getObject方法，该方法用于返回创建的bean对象。
 *      当Spring容器需要创建一个bean对象时，会首先实例化一个FactoryBean对象，然后调用该对象的getObject方法来获取实际的bean对象。
 *      使用FactoryBean的主要优点是可以将复杂的bean实例化逻辑封装在单独的类中，而不是在配置文件中进行繁琐的配置。
 *      同时，通过实现FactoryBean接口，可以将bean对象的创建和使用解耦，提高代码的模块化和可维护性。
 *      例如，在使用CGLib或ASM等库来创建代理对象时，可以使用FactoryBean模式来封装复杂的创建逻辑，并在Spring容器中使用简单的bean定义来注入代理对象。
 *      这样，当需要更换代理库或修改代理逻辑时，只需要修改FactoryBean的实现即可，而不需要修改配置文件或重新编译其他使用该代理对象的代码。
 * （2）{@link BeanFactoryPostProcessor} 是 Spring 容器加载 Bean 的定义之后以及 Bean 实例化之前执行，
 *      所以 BeanFactoryPostProcessor 可以在 Bean 实例化之前获取 Bean 的配置元数据，并允许用户对其修改。
 *      而 {@link BeanPostProcessor} 是在 Bean 初始化前后执行，它并不能修改 Bean 的配置信息。
 * （3）{@link Promise} 是基于 JDK 的 Future扩展，在Netty的异步操作中，一旦异步操作完成，就可以使用Promise来通知操作的结果。
 *      如果异步操作成功完成，Promise可以被设置为成功状态，同时可以通过get()方法获取操作的结果。如果异步操作失败，
 *      Promise可以被设置为失败状态，同时可以通过get()方法获取操作的异常。
 *
 * 2、RPC远程调用消费者
 * （1）负责获取 RPC 服务
 *     a、服务消费者并不是一个常驻的服务，每次发起 RPC 调用时它才会去选择向哪个远端服务发送数据。
 *     b、对于声明 {@link RpcReference} 注解的成员变量，我们需要构造出一个可以真正进行 RPC 调用的 Bean，然后将它注册到 Spring 的容器中。
 *     c、对声明 {@link RpcReference} 注解的成员变量构造出 RpcReferenceBean，所以需要实现 BeanFactoryPostProcessor 修改 Bean 的定义
 * （2）使用动态代理发起 RPC 远程调用，帮助使用者来屏蔽底层网络通信的细节。
 *     a、流程
 *          服务消费者实现协议编码，向服务提供者发送调用数据。
 *          服务提供者收到数据后解码，然后向服务消费者发送响应数据，暂时忽略 RPC 请求是如何被调用的。
 *          服务消费者收到响应数据后成功返回。
 *     b、定义协议
 *          +---------------------------------------------------------------+
 *          | 魔数 2byte | 协议版本号 1byte | 序列化算法 1byte | 报文类型 1byte  |
 *          +---------------------------------------------------------------+
 *          | 状态 1byte |        消息 ID 8byte     |      数据长度 4byte     |
 *          +---------------------------------------------------------------+
 *          |                   数据内容 （长度不定）                          |
 *          +---------------------------------------------------------------+
 *          {@link MiniRpcProtocol} = {@link MsgHeader } + {@link MiniRpcRequest } / {@link MiniRpcResponse }
 *
 *     c、数据区序列化与序列化
 *          MiniRpcRequest 和 MiniRpcResponse 实体类表示的协议体内容都是不确定具体长度的，
 *          因此使用通用且高效的序列化算法将其转换成二进制数据，这样可以有效减少网络传输的带宽，提升 RPC 框架的整体性能。
 *          为了能够支持不同序列化算法，我们采用工厂模式来实现不同序列化算法之间的切换，使用相同的序列化接口指向不同的序列化算法
 *          对于使用者来说只需要知道序列化算法的类型即可，不用关心底层序列化是如何实现的
 *          {@link RpcSerialization}、{@link HessianSerialization  }、 {@link SerializationFactory  }
 *     d、报文编码
 *          Netty 提供编解码抽象基类 {@link MessageToByteEncoder} 和 {@link ByteToMessageDecoder} 方便地扩展实现自定义协议
 *          编码器  {@link MiniRpcEncoder} 需要继承  {@link MessageToByteEncoder}，并重写 encode() 方法
 *          在服务消费者或者服务提供者调用 writeAndFlush() 将数据写给对方前，都已经封装成  {@link MiniRpcRequest} 或者 MiniRpcResponse
 *          所以采用  {@link MiniRpcProtocol<Object>} 作为 MiniRpcEncoder 编码器能够支持的编码类型。
 *     e、报文解码
 *            {@link MiniRpcDecoder} 需要继承  {@link ByteToMessageDecoder}，并重写 decode() 方法
 *
 *
 */

@Slf4j
public class RpcConsumer {
    private final Bootstrap bootstrap;
    private final EventLoopGroup eventLoopGroup;

    public RpcConsumer() {
        bootstrap = new Bootstrap();
        eventLoopGroup = new NioEventLoopGroup(4);
        bootstrap.group(eventLoopGroup).channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline()
                                .addLast(new MiniRpcEncoder())
                                .addLast(new MiniRpcDecoder())
                                .addLast(new RpcResponseHandler());
                    }
                });
    }

    public void sendRequest(MiniRpcProtocol<MiniRpcRequest> protocol, RegistryService registryService) throws Exception {
        MiniRpcRequest request = protocol.getBody();
        Object[] params = request.getParams();
        String serviceKey = RpcServiceHelper.buildServiceKey(request.getClassName(), request.getServiceVersion());

        int invokerHashCode = params.length > 0 ? params[0].hashCode() : serviceKey.hashCode();
        ServiceMeta serviceMetadata = registryService.discovery(serviceKey, invokerHashCode);

        if (serviceMetadata != null) {
            ChannelFuture future = bootstrap.connect(serviceMetadata.getServiceAddr(), serviceMetadata.getServicePort()).sync();
            future.addListener((ChannelFutureListener) arg0 -> {
                if (future.isSuccess()) {
                    log.info("connect rpc server {} on port {} success.", serviceMetadata.getServiceAddr(), serviceMetadata.getServicePort());
                } else {
                    log.error("connect rpc server {} on port {} failed.", serviceMetadata.getServiceAddr(), serviceMetadata.getServicePort());
                    future.cause().printStackTrace();
                    eventLoopGroup.shutdownGracefully();
                }
            });
            future.channel().writeAndFlush(protocol);
        }
    }
}
