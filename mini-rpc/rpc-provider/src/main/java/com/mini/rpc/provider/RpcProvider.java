package com.mini.rpc.provider;

import com.mini.rpc.codec.MiniRpcDecoder;
import com.mini.rpc.codec.MiniRpcEncoder;
import com.mini.rpc.common.RpcServiceHelper;
import com.mini.rpc.common.ServiceMeta;
import com.mini.rpc.handler.RpcRequestHandler;
import com.mini.rpc.provider.annotation.RpcService;
import com.mini.rpc.provider.registry.RegistryService;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * 1、概念
 * （1）Bean的生命周期
 *      Bean定义：Bean的类定义，包括名称、属性和构造函数等。
 *      实例化：根据Bean的定义，通过Java的反射机制创建Bean的实例。
 *      属性赋值：对Bean的属性进行赋值操作。
 *      初始化：调用Bean的初始化方法，例如{@link #afterPropertiesSet()}方法。
 *      生存期：Bean被实例化、属性被赋值并且初始化之后，就可以被正常使用。
 *      销毁：当Bean不再需要时，进行销毁操作。
 * （2）{@link InitializingBean}接口定义了一个afterPropertiesSet()方法，该方法在当前的bean的属性被初始化之后执行。
 *      通常，可以在此方法中进行一些自定义的初始化逻辑，例如数据校验、资源准备等。
 * （3）{@link BeanPostProcessor}接口定义了两个方法：postProcessBeforeInitialization和postProcessAfterInitialization。
 *      这两个方法分别在所有bean的构造方法执行（bean神功周期实例化时）之前和之后执行。通过实现BeanPostProcessor接口，我们可以对bean进行一些前置和后置处理，例如修改bean的属性、执行额外的逻辑等。
 *
 * 2、RPC远程调用提供者
 * （1）服务提供者启动服务，并暴露服务端口；
 *      a、创建注册中心连接，并创建{@link RpcProvider}对象
 *      b、创建RpcProvider对象后，启动netty提供者服务，并暴露服务端口，接受和处理RPC请求
 * （2）启动时扫描需要对外发布的服务，并将服务元数据信息发布到注册中心；
 *      a、在所有bean实例化之后，检查bean是否带有{@link RpcService}注解
 *          - @RpcService 注解本质上就是 @Component，可以将服务实现类注册成 Spring 容器所管理的 Bean
 *      b、把带有rpc注解的bean的服务信息注册到注册中心中，并把name#version作为key，bean作为value放入到map中
 * （3）接收 RPC 请求，解码后得到请求消息；
 * （4）提交请求至自定义线程池进行处理，并将处理结果写回客户端。
 */
@Slf4j
public class RpcProvider implements InitializingBean, BeanPostProcessor {

    private String serverAddress;
    private final int serverPort;
    private final RegistryService serviceRegistry;

    private final Map<String, Object> rpcServiceMap = new HashMap<>();

    public RpcProvider(int serverPort, RegistryService serviceRegistry) {
        this.serverPort = serverPort;
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void afterPropertiesSet() {
        new Thread(() -> {
            try {
                startRpcServer();
            } catch (Exception e) {
                log.error("start rpc server error.", e);
            }
        }).start();
    }

    private void startRpcServer() throws Exception {
        this.serverAddress = InetAddress.getLocalHost().getHostAddress();

        EventLoopGroup boss = new NioEventLoopGroup();
        EventLoopGroup worker = new NioEventLoopGroup();
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(boss, worker)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline()
                                    .addLast(new MiniRpcEncoder())
                                    .addLast(new MiniRpcDecoder())
                                    .addLast(new RpcRequestHandler(rpcServiceMap));
                        }
                    })
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            ChannelFuture channelFuture = bootstrap.bind(this.serverAddress, this.serverPort).sync();
            log.info("server addr {} started on port {}", this.serverAddress, this.serverPort);
            channelFuture.channel().closeFuture().sync();
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        RpcService rpcService = bean.getClass().getAnnotation(RpcService.class);
        if (rpcService != null) {
            String serviceName = rpcService.serviceInterface().getName();
            String serviceVersion = rpcService.serviceVersion();

            try {
                ServiceMeta serviceMeta = new ServiceMeta();
                serviceMeta.setServiceAddr(serverAddress);
                serviceMeta.setServicePort(serverPort);
                serviceMeta.setServiceName(serviceName);
                serviceMeta.setServiceVersion(serviceVersion);

                serviceRegistry.register(serviceMeta);
                rpcServiceMap.put(RpcServiceHelper.buildServiceKey(serviceMeta.getServiceName(), serviceMeta.getServiceVersion()), bean);
            } catch (Exception e) {
                log.error("failed to register service {}#{}", serviceName, serviceVersion, e);
            }
        }
        return bean;
    }

}
