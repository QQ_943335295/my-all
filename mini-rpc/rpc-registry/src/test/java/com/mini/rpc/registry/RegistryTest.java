package com.mini.rpc.registry;

import com.mini.rpc.common.ServiceMeta;
import com.mini.rpc.provider.registry.RegistryService;
import io.netty.channel.DefaultEventLoop;
import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.lang.reflect.Proxy;
import java.util.concurrent.*;

@Slf4j
public class RegistryTest {

    private RegistryService registryService;
//
//    @Before
//    public void init() throws Exception {
//        registryService = RegistryFactory.getInstance("127.0.0.1:2181", RegistryType.ZOOKEEPER);
//    }
//
//    @After
//    public void close() throws Exception {
//        registryService.destroy();
//    }

    @Test
    public void testAll() throws Exception {
        ServiceMeta serviceMeta1 = new ServiceMeta();
        serviceMeta1.setServiceAddr("127.0.0.1");
        serviceMeta1.setServicePort(8080);
        serviceMeta1.setServiceName("test1");
        serviceMeta1.setServiceVersion("1.0.0");

        ServiceMeta serviceMeta2 = new ServiceMeta();
        serviceMeta2.setServiceAddr("127.0.0.2");
        serviceMeta2.setServicePort(8080);
        serviceMeta2.setServiceName("test2");
        serviceMeta2.setServiceVersion("1.0.0");

        ServiceMeta serviceMeta3 = new ServiceMeta();
        serviceMeta3.setServiceAddr("127.0.0.3");
        serviceMeta3.setServicePort(8080);
        serviceMeta3.setServiceName("test3");
        serviceMeta3.setServiceVersion("1.0.0");

        registryService.register(serviceMeta1);
        registryService.register(serviceMeta2);
        registryService.register(serviceMeta3);

        ServiceMeta discovery1 = registryService.discovery("test1#1.0.0", "test1".hashCode());
        ServiceMeta discovery2 = registryService.discovery("test2#1.0.0", "test2".hashCode());
        ServiceMeta discovery3 = registryService.discovery("test3#1.0.0", "test3".hashCode());

        assert discovery1 != null;
        assert discovery2 != null;
        assert discovery3 != null;

        registryService.unRegister(discovery1);
        registryService.unRegister(discovery2);
        registryService.unRegister(discovery3);
    }


    @Test
    public void testPromise() throws InterruptedException, ExecutionException, TimeoutException {
       Promise<String> promise = new DefaultPromise<>(new DefaultEventLoop());
       new Thread(() -> {
           try {
               Thread.sleep(3000L);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
           promise.setSuccess("hello world !");
       }).start();

        String reuslt = promise.get(30000L, TimeUnit.MILLISECONDS);
        System.err.println(reuslt);
    }


    @Test
    public void testThreadAndFuture() throws InterruptedException, ExecutionException, TimeoutException {
        // 实现Runnable
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.err.println("thread create ...");
            }
        }).start();

        // 继承Thread
        new MyThread().start();

        // 使用Callable与future创建线程池
        Future<String> future = Executors.newSingleThreadExecutor().submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(2000L);
                return "使用Callable与future创建线程池...";
            }
        });
        try {
            System.err.println(future.get(1000L, TimeUnit.MILLISECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.err.println("do next task ...");
        }

        // 使用用Callable与futureTask
        FutureTask<String> futureTask = (FutureTask<String>) Executors.newSingleThreadExecutor().submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "使用Callable与FutureTask创建线程池...";
            }
        });
        System.err.println(futureTask.get());
    }

    class MyThread extends Thread{
        @Override
        public void run() {
            System.err.println("extend thread ...");
        }
    }


    @Test
    public void testCompleteFuture() throws InterruptedException, ExecutionException, TimeoutException {
        System.err.println();
    }


    @Test
    public void jdkDynamicProxy(){
        UserDao userDao = new UserDaoImpl();
        TransactionProxy userDaoTransactionProxy = new TransactionProxy(userDao);
        UserDao proxyInstance = (UserDao) userDaoTransactionProxy.genProxyInstance();
        proxyInstance.insert();
    }

    public interface UserDao {
        void insert();
    }

    class UserDaoImpl implements UserDao {
        @Override
        public void insert() {
            System.err.println("insert user success.");
        }
    }

   class TransactionProxy {
        private Object obj;
        public TransactionProxy(Object obj) {
            this.obj = obj;
        }

       public Object genProxyInstance() {
            return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), (proxy, method, args) -> {
                System.err.println("start invoke ...");
                Object invoke = method.invoke(obj, args);
                System.err.println("end invoke ...");
                return invoke;
            });
       }
    }


}
