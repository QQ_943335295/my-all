package com.zxm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxm.config.rabbit.config.RabbitMqConfig;
import com.zxm.config.sharding.strategy.db.TenantDBPreciseShardingAlgorithm;
import com.zxm.config.sharding.strategy.table.EquipmentDataTablePreciseShardingAlgorithm;
import com.zxm.enums.MQShardingObjEnum;
import com.zxm.enums.MQShardingType;
import com.zxm.mapper.ShardingDbConfigMapper;
import com.zxm.model.ShardingDbConfigDO;
import com.zxm.model.ShardingMQMessage;
import com.zxm.request.ShardingDbRequest;
import com.zxm.service.ShardingDbConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  DB分片策略服务实现类
 *
 * @author zxm
 * @since 2023-06-07
 */
@Slf4j
@Service
public class ShardingDbConfigServiceImpl extends ServiceImpl<ShardingDbConfigMapper, ShardingDbConfigDO> implements ShardingDbConfigService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void addShardingDbConfig(ShardingDbRequest shardingDbRequest) {
        ShardingDbConfigDO shardingDbConfigDO = new ShardingDbConfigDO();
        BeanUtils.copyProperties(shardingDbRequest, shardingDbConfigDO);
        baseMapper.insert(shardingDbConfigDO);
        log.info("保存DB分库策略：{}", shardingDbConfigDO);
        ShardingMQMessage shardingMqMessage = new ShardingMQMessage();
        shardingMqMessage.setShardingType(MQShardingType.DB_SHARDING_CONFIG_REFRESH.name());
        shardingMqMessage.setShardingObj(MQShardingObjEnum.DB_SHARDING_BY_TENANT.name());
        rabbitTemplate.convertAndSend(RabbitMqConfig.DB_TABLE_SHARDING_CONFIG_EVENT_EXCHANGE, RabbitMqConfig.DB_TABLE_SHARDING_CONFIG_ROUTING_KEY, shardingMqMessage);
    }

    @Override
    public void refreshShardingDBConfig(String shardingObj) {
        if (MQShardingObjEnum.DB_SHARDING_BY_TENANT.name().equals(shardingObj)){
            initTenantDBPreciseShardingAlgorithm();
        }
    }

    /**
     * 初始化租户分库标准策略
     */
    @Override
    public void initTenantDBPreciseShardingAlgorithm() {
        List<ShardingDbConfigDO> list = baseMapper.selectList(null);
        list.forEach(item -> TenantDBPreciseShardingAlgorithm.DB_PRECISE_SHARDING_CONFIG.put(item.getTenantId(), item.getDbName()));
        log.error("TenantDBPreciseShardingAlgorithm.DB_PRECISE_SHARDING_CONFIG:{}", TenantDBPreciseShardingAlgorithm.DB_PRECISE_SHARDING_CONFIG);
    }
}
