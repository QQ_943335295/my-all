package com.zxm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxm.model.EquipmentDataDO;
import com.zxm.mapper.EquipmentDataMapper;
import com.zxm.model.TestUserDO;
import com.zxm.request.EquipmentDataRequest;
import com.zxm.service.EquipmentDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
@Slf4j
@Service
public class EquipmentDataServiceImpl extends ServiceImpl<EquipmentDataMapper, EquipmentDataDO> implements EquipmentDataService {

    @Override
    public void add(EquipmentDataRequest equipmentDataRequest) {
        EquipmentDataDO equipmentDataDO = new EquipmentDataDO();
        BeanUtils.copyProperties(equipmentDataRequest, equipmentDataDO);
        int rows = baseMapper.insert(equipmentDataDO);
        log.info("rows:{}, 插入设备数据成功：{}", rows, equipmentDataDO);
    }

    @Override
    public List<EquipmentDataDO> list(EquipmentDataRequest equipmentDataRequest) {
        EquipmentDataDO equipmentDataDO = new EquipmentDataDO();
        BeanUtils.copyProperties(equipmentDataRequest, equipmentDataDO);
        LambdaQueryWrapper<EquipmentDataDO> wrapper = new LambdaQueryWrapper<>(equipmentDataDO);
        return baseMapper.selectList(wrapper);
    }
}
