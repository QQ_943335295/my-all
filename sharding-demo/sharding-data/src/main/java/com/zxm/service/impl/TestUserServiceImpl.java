package com.zxm.service.impl;

import com.zxm.model.TestUserDO;
import com.zxm.mapper.TestUserMapper;
import com.zxm.request.TestUserRequest;
import com.zxm.service.TestUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
@Slf4j
@Service
public class TestUserServiceImpl extends ServiceImpl<TestUserMapper, TestUserDO> implements TestUserService {

    @Override
    public void add(TestUserRequest testUserRequest) {
        TestUserDO testUserDO = new TestUserDO();
        BeanUtils.copyProperties(testUserRequest, testUserDO);
        int rows = baseMapper.insert(testUserDO);
        log.info("rows:{}，添加测试用户成功:{}", rows, testUserDO);
    }
}
