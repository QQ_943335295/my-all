package com.zxm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxm.config.rabbit.config.RabbitMqConfig;
import com.zxm.config.sharding.strategy.table.EquipmentDataTablePreciseShardingAlgorithm;
import com.zxm.config.sharding.strategy.table.EquipmentDataTableRangeShardingAlgorithm;
import com.zxm.constants.DBTableConstants;
import com.zxm.enums.MQShardingObjEnum;
import com.zxm.enums.MQShardingType;
import com.zxm.mapper.ShardingTableConfigMapper;
import com.zxm.model.ShardingMQMessage;
import com.zxm.model.ShardingTableConfigDO;
import com.zxm.request.ShardingTableConfigRequest;
import com.zxm.service.ShardingTableConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxm
 * @since 2023-06-28
 */
@Slf4j
@Service
public class ShardingTableConfigServiceImpl extends ServiceImpl<ShardingTableConfigMapper, ShardingTableConfigDO> implements ShardingTableConfigService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void addShardingDbConfig(ShardingTableConfigRequest shardingTableConfigRequest) {
        ShardingTableConfigDO shardingTableConfigDO = new ShardingTableConfigDO();
        BeanUtils.copyProperties(shardingTableConfigRequest, shardingTableConfigDO);
        baseMapper.insert(shardingTableConfigDO);
        log.info("保存分表策略：{}", shardingTableConfigDO);
        ShardingMQMessage shardingMqMessage = new ShardingMQMessage();
        shardingMqMessage.setShardingType(MQShardingType.TABLE_SHARDING_CONFIG_REFRESH.name());
        shardingMqMessage.setShardingObj(MQShardingObjEnum.TABLE_SHARDING_BY_DATE_EQUIPMENT_DATA.name());
        rabbitTemplate.convertAndSend(RabbitMqConfig.DB_TABLE_SHARDING_CONFIG_EVENT_EXCHANGE, RabbitMqConfig.DB_TABLE_SHARDING_CONFIG_ROUTING_KEY, shardingMqMessage);
    }

    @Override
    public void refreshShardingTableConfig(String shardingObj) {
        if (MQShardingObjEnum.TABLE_SHARDING_BY_DATE_EQUIPMENT_DATA.name().equals(shardingObj)){
            initEquipmentDataTablePreciseAndRangeShardingAlgorithm();
        }
    }


    /**
     * 初始化设备数据标准分表策略与范围分库策略
     */
    @Override
    public void initEquipmentDataTablePreciseAndRangeShardingAlgorithm() {
        LambdaQueryWrapper<ShardingTableConfigDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.likeRight(ShardingTableConfigDO::getTableName, DBTableConstants.TABLE_EQUIPMENT);
        List<ShardingTableConfigDO> list = baseMapper.selectList(wrapper);
        EquipmentDataTablePreciseShardingAlgorithm.TABLE_PRECISE_SHARDING_CONFIG.addAll(list);
        log.error("EquipmentDataTablePreciseShardingAlgorithm.TABLE_PRECISE_SHARDING_CONFIG:{}", EquipmentDataTablePreciseShardingAlgorithm.TABLE_PRECISE_SHARDING_CONFIG);
        EquipmentDataTableRangeShardingAlgorithm.TABLE_RANGE_SHARDING_CONFIG.addAll(list);
        log.error("EquipmentDataTableRangeShardingAlgorithm.TABLE_PRECISE_SHARDING_CONFIG:{}", EquipmentDataTableRangeShardingAlgorithm.TABLE_RANGE_SHARDING_CONFIG);
    }
}
