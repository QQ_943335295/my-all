package com.zxm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.model.ShardingTableConfigDO;
import com.zxm.request.ShardingTableConfigRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2023-06-28
 */
public interface ShardingTableConfigService extends IService<ShardingTableConfigDO> {

    /**
     * 添加分库策略
     */
    void addShardingDbConfig(ShardingTableConfigRequest shardingTableConfigRequest);

    /**
     * 刷新分表策略
     */
    void refreshShardingTableConfig(String toString);

    /**
     * 初始化分表标准策略和范围策略
     */
    void initEquipmentDataTablePreciseAndRangeShardingAlgorithm();

}
