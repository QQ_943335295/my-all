package com.zxm.service;

import com.zxm.model.EquipmentDataDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.model.TestUserDO;
import com.zxm.request.EquipmentDataRequest;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
public interface EquipmentDataService extends IService<EquipmentDataDO> {

    /**
     * 添加设备信息
     */
    void add(EquipmentDataRequest equipmentDataRequest);

    /**
     * 查询设备数据信息
     */
    List<EquipmentDataDO> list(EquipmentDataRequest equipmentDataRequest);
}
