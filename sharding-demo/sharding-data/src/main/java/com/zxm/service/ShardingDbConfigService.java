package com.zxm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.model.ShardingDbConfigDO;
import com.zxm.request.ShardingDbRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxm
 * @since 2023-06-07
 */
public interface ShardingDbConfigService extends IService<ShardingDbConfigDO> {

    /**
     * 添加DB分片策略
     */
    void addShardingDbConfig(ShardingDbRequest shardingDbRequest);

    /**
     * 刷新DB分片配置
     */
    void refreshShardingDBConfig(String toString);

    /**
     * 初始化DB分片数据
     */
    void initTenantDBPreciseShardingAlgorithm();
}
