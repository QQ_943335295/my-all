package com.zxm.service;

import com.zxm.model.TestUserDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zxm.request.TestUserRequest;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
public interface TestUserService extends IService<TestUserDO> {

    /**
     * 添加测试用户
     */
    void add(TestUserRequest testUserRequest);

}
