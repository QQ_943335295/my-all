package com.zxm.request;

import lombok.Data;

/**
 * 数据库分库映射
 * @author zxm
 * @date 2023/5/30
 */
@Data
public class ShardingDbRequest {

    private Integer tenantId;

    private String dbName;
}
