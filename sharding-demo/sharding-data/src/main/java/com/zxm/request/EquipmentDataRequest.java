package com.zxm.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
@Data
public class EquipmentDataRequest implements Serializable {

    private Integer equipmentNo;

    private BigDecimal uploadData;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date recordDate;

    private Integer tenantId;


}
