package com.zxm.request;

import lombok.Data;

/**
 * @author zxm
 * @date 2023/5/30
 */
@Data
public class TestUserRequest {

    /**
     * 姓名
     */
    private String name;

    /**
     * 密码
     */
    private String password;


}
