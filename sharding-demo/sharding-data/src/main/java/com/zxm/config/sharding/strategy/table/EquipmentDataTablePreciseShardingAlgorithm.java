package com.zxm.config.sharding.strategy.table;

import cn.hutool.core.date.DateUtil;
import com.zxm.constants.CommonConstants;
import com.zxm.enums.BizCodeEnum;
import com.zxm.exception.BizException;
import com.zxm.model.ShardingTableConfigDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 设备数据 Table标准策略-精准分片
 * @author zxm
 * @date 2023/5/30
 */
@Slf4j
public class EquipmentDataTablePreciseShardingAlgorithm implements PreciseShardingAlgorithm<Date> {


    public static volatile CopyOnWriteArraySet<ShardingTableConfigDO> TABLE_PRECISE_SHARDING_CONFIG = new CopyOnWriteArraySet<>();


    /**
     * @param availableTargetNames 数据源集合
     *                             在分库时值为所有分片库的集合 databaseNames
     *                             分表时为对应分片库中所有分片表的集合 tablesNames
     * @param shardingValue        分片属性，包括
     *                             logicTableName 为逻辑表，
     *                             columnName 分片健（字段），
     *                             value 为从 SQL 中解析出的分片健的值
     */
    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Date> shardingValue) {
        System.err.println("EquipmentDataTablePreciseShardingAlgorithm:" + Thread.currentThread());
        Date currentDate = shardingValue.getValue();
        for (ShardingTableConfigDO item : TABLE_PRECISE_SHARDING_CONFIG){
            // currentDate位于  [ startDate, endDate ) 之间
            if (DateUtil.compare(currentDate, item.getStartDate()) >= CommonConstants.ZERO &&
                    DateUtil.compare(currentDate, item.getEndDate()) < CommonConstants.ZERO){
                    String tableName = item.getTableName();
                    for (String targetTable : availableTargetNames){
                        if (tableName.indexOf(targetTable) > -1){
                            return tableName;
                        }
                    }
            }
        }
        //抛异常
        throw new BizException(BizCodeEnum.DB_ROUTE_NOT_FOUND);
    }

}
