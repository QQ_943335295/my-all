package com.zxm.config.rabbit.listener;

import com.rabbitmq.client.Channel;
import com.zxm.config.rabbit.config.RabbitMqConfig;
import com.zxm.enums.BizCodeEnum;
import com.zxm.enums.MQShardingType;
import com.zxm.exception.BizException;
import com.zxm.model.ShardingMQMessage;
import com.zxm.service.ShardingDbConfigService;
import com.zxm.service.ShardingTableConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 数据库分库分表消息监听
 * @author zxm
 * @date 2023/6/29
 */
@Slf4j
@Component
@RabbitListener(queuesToDeclare = {
        @Queue(RabbitMqConfig.DB_TABLE_SHARDING_CONFIG_QUEUE)
}, ackMode = RabbitMqConfig.ACK_MODE_MANUAL)
public class DBTableConfigListener {

    @Autowired
    private ShardingDbConfigService shardingDbConfigService;

    @Autowired
    private ShardingTableConfigService shardingTableConfigService;

    @RabbitHandler
    public void dbTableConfigListener(ShardingMQMessage shardingMqMessage, Message message, Channel channel){
        log.info("监听到消息DBTableConfigListener:{}", shardingMqMessage);
        long msgTag = message.getMessageProperties().getDeliveryTag();
        try{
            String messageType = shardingMqMessage.getShardingType();
            if(MQShardingType.DB_SHARDING_CONFIG_REFRESH.name().equals(messageType)){
                shardingDbConfigService.refreshShardingDBConfig(shardingMqMessage.getShardingObj());
            } else if(MQShardingType.TABLE_SHARDING_CONFIG_REFRESH.name().equals(messageType)){
                shardingTableConfigService.refreshShardingTableConfig(shardingMqMessage.getShardingObj());
            }
            //确认消息消费成功
            channel.basicAck(msgTag, false);
        }catch (Exception e){
            log.error("消费者失败:{}", shardingMqMessage);
            try {
                channel.basicAck(msgTag, true);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            throw new BizException(BizCodeEnum.MQ_CONSUME_EXCEPTION);
        }
        log.info("消费成功:{}", shardingMqMessage);
    }

}
