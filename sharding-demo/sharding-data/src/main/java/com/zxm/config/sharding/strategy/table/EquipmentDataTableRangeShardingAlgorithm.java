package com.zxm.config.sharding.strategy.table;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.zxm.constants.CommonConstants;
import com.zxm.enums.BizCodeEnum;
import com.zxm.exception.BizException;
import com.zxm.model.ShardingTableConfigDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 设备数据 Table标准策略-范围分片
 * @author zxm
 * @date 2023/5/30
 */
@Slf4j
public class EquipmentDataTableRangeShardingAlgorithm implements RangeShardingAlgorithm<Date> {


    public static volatile CopyOnWriteArraySet<ShardingTableConfigDO> TABLE_RANGE_SHARDING_CONFIG = new CopyOnWriteArraySet<>();

    /**
     * @param collection 数据源集合
     *                             在分库时值为所有分片库的集合 databaseNames
     *                             分表时为对应分片库中所有分片表的集合 tablesNames
     * @param rangeShardingValue        分片属性，包括
     *                             logicTableName 为逻辑表，
     *                             columnName 分片健（字段），
     *                             value 为从 SQL 中解析出的分片健的值
     */
    @Override
    public Collection<String> doSharding(Collection<String> collection, RangeShardingValue<Date> rangeShardingValue) {
        System.err.println("EquipmentDataTableRangeShardingAlgorithm:" + Thread.currentThread());
        Date startDate = rangeShardingValue.getValueRange().lowerEndpoint();
        Date endDate = rangeShardingValue.getValueRange().upperEndpoint();
        Collection<String> tableNameSet = new HashSet<>();
        for (ShardingTableConfigDO item : TABLE_RANGE_SHARDING_CONFIG) {
            // [startTime, endTime) 分表区域时间
            if (DateUtil.compare(startDate, item.getStartDate()) >= CommonConstants.ZERO || DateUtil.compare(endDate, item.getEndDate()) <= CommonConstants.ZERO) {
                tableNameSet.add(item.getTableName());
            }
        }
        for (String table : collection){
            if (!tableNameSet.contains(table)){
                tableNameSet.remove(table);
            }
        }
        if (CollUtil.isNotEmpty(tableNameSet)){
            return tableNameSet;
        }
        //抛异常
        throw new BizException(BizCodeEnum.DB_ROUTE_NOT_FOUND);
    }
}
