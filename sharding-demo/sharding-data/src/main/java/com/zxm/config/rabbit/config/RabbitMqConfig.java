package com.zxm.config.rabbit.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MQ配置
 * @author zxm
 * @date 2023/6/29
 */
@Configuration
@Slf4j
public class RabbitMqConfig {

    /**
     * 消息转换器
     */
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    /** 分库分表配置详情 */
    public static final String DB_TABLE_SHARDING_CONFIG_EVENT_EXCHANGE = "db.table.sharding.config.exchange";

    /** 分库分表策略路对略 */
    public static final String DB_TABLE_SHARDING_CONFIG_QUEUE = "db.table.sharding.config.queue";

    /** 分库分表策略路由key */
    public static final String DB_TABLE_SHARDING_CONFIG_ROUTING_KEY = "db.table.sharding.config.routing.key";
    /** 消息手动确认模式 */
    public static final String ACK_MODE_MANUAL = "MANUAL";

    /**
     * 分库分表配置交换机创建，Fanout类型，一般一个微服务一个交换机
     */
    @Bean
    public Exchange dbTableShardingConfigExcChange(){
        return new FanoutExchange(DB_TABLE_SHARDING_CONFIG_EVENT_EXCHANGE,true,false);
    }

    /**
     * 分库分表配置队列的绑定关系建立
     */
    @Bean
    public Binding dbTableShardingConfigBinding(){
        return new Binding(DB_TABLE_SHARDING_CONFIG_QUEUE, Binding.DestinationType.QUEUE, DB_TABLE_SHARDING_CONFIG_EVENT_EXCHANGE,
                DB_TABLE_SHARDING_CONFIG_ROUTING_KEY,null);
    }
    /**
     * 队列
     */
    @Bean
    public Queue dbTableShardingConfigQueue(){
        return new Queue(DB_TABLE_SHARDING_CONFIG_QUEUE,true,false,false);
    }


}
