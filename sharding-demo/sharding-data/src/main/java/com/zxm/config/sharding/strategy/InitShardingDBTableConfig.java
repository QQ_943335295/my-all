package com.zxm.config.sharding.strategy;

import com.zxm.service.ShardingDbConfigService;
import com.zxm.service.ShardingTableConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 初始化数据库分库分表信息
 * @author zxm
 * @date 2023/6/28
 */
@Component
public class InitShardingDBTableConfig {

    @Autowired
    private ShardingDbConfigService shardingDbConfigService;
    @Autowired
    private ShardingTableConfigService shardingTableConfigService;

    @PostConstruct
    public void init(){
        // 初始化租户分库标准策略
        shardingDbConfigService.initTenantDBPreciseShardingAlgorithm();
        // 初始化设备数据标准分表策略、范围分表策略
        shardingTableConfigService.initEquipmentDataTablePreciseAndRangeShardingAlgorithm();
    }


}
