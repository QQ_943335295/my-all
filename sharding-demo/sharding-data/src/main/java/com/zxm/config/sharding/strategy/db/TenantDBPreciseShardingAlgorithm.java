package com.zxm.config.sharding.strategy.db;

import com.zxm.enums.BizCodeEnum;
import com.zxm.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * DB分库策略
 * @author zxm
 * @date 2023/5/30
 */
@Slf4j
public class TenantDBPreciseShardingAlgorithm implements PreciseShardingAlgorithm<Integer> {

    public static  volatile ConcurrentHashMap<Integer, String> DB_PRECISE_SHARDING_CONFIG = new ConcurrentHashMap<>();
//    static {
//        DB_PRECISE_SHARDING_CONFIG.put(1001, "ds0");
//        DB_PRECISE_SHARDING_CONFIG.put(1002, "ds1");
//        DB_PRECISE_SHARDING_CONFIG.put(1003, "ds1");
//        DB_PRECISE_SHARDING_CONFIG.put(1004, "ds0");
//    }

    /**
     * @param availableTargetNames 数据源集合
     *                             在分库时值为所有分片库的集合 databaseNames
     *                             分表时为对应分片库中所有分片表的集合 tablesNames
     * @param shardingValue        分片属性，包括
     *                             logicTableName 为逻辑表，
     *                             columnName 分片健（字段），
     *                             value 为从 SQL 中解析出的分片健的值
     * @return
     */
    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Integer> shardingValue) {
        System.err.println("TenantDBPreciseShardingAlgorithm:" + Thread.currentThread());
        Integer value = shardingValue.getValue();
        for (String targetName : availableTargetNames) {
            //获取库名的最后一位，真实配置的ds
            String dbName = DB_PRECISE_SHARDING_CONFIG.get(value);
            if (targetName.equals(dbName)){
                return dbName;
            }
        }
        //抛异常
        throw new BizException(BizCodeEnum.DB_ROUTE_NOT_FOUND);
    }

}
