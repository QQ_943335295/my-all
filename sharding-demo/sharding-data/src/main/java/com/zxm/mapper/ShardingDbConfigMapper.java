package com.zxm.mapper;

import com.zxm.model.ShardingDbConfigDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2023-06-07
 */
public interface ShardingDbConfigMapper extends BaseMapper<ShardingDbConfigDO> {

}
