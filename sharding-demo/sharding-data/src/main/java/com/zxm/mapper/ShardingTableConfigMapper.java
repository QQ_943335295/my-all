package com.zxm.mapper;

import com.zxm.model.ShardingTableConfigDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2023-06-28
 */
public interface ShardingTableConfigMapper extends BaseMapper<ShardingTableConfigDO> {

}
