package com.zxm.mapper;

import com.zxm.model.TestUserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
public interface TestUserMapper extends BaseMapper<TestUserDO> {

}
