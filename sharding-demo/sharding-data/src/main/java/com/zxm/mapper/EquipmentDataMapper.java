package com.zxm.mapper;

import com.zxm.model.EquipmentDataDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
public interface EquipmentDataMapper extends BaseMapper<EquipmentDataDO> {

}
