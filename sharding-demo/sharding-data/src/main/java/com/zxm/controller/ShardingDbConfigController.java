package com.zxm.controller;


import com.zxm.bean.JsonData;
import com.zxm.model.ShardingDbConfigDO;
import com.zxm.request.ShardingDbRequest;
import com.zxm.service.ShardingDbConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zxm
 * @since 2023-06-07
 */
@RestController
@RequestMapping("/shardingDbConfig")
public class ShardingDbConfigController {

    @Autowired
    private ShardingDbConfigService shardingDbConfigService;

    @GetMapping("/list")
    public JsonData list() {
        List<ShardingDbConfigDO> list = shardingDbConfigService.list();
        return JsonData.buildSuccess(list);
    }

    @PostMapping("/add")
    public JsonData add(@RequestBody ShardingDbRequest shardingDbRequest) {
        shardingDbConfigService.addShardingDbConfig(shardingDbRequest);
        return JsonData.buildSuccess();
    }

}

