package com.zxm.controller;


import com.zxm.bean.JsonData;
import com.zxm.model.TestUserDO;
import com.zxm.request.TestUserRequest;
import com.zxm.service.TestUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
@RestController
@RequestMapping("/test/user")
public class TestUserController {

    @Autowired
    private TestUserService testUserService;

    @PostMapping("/add")
    public JsonData add(@RequestBody TestUserRequest testUserRequest) {
        testUserService.add(testUserRequest);
        return JsonData.buildSuccess();
    }

    @GetMapping("/list")
    public JsonData list() {
        List<TestUserDO> list = testUserService.list();
        return JsonData.buildSuccess(list);
    }


}

