package com.zxm.controller;


import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxm.bean.JsonData;
import com.zxm.model.EquipmentDataDO;
import com.zxm.request.EquipmentDataRequest;
import com.zxm.service.EquipmentDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zxm
 * @since 2023-05-30
 */
@RestController
@RequestMapping("/equipment/data")
public class EquipmentDataController {


    @Autowired
    private EquipmentDataService equipmentDataService;

    @PostMapping("/add")
    public JsonData add(@RequestBody EquipmentDataRequest equipmentDataRequest) {
        equipmentDataService.add(equipmentDataRequest);
        return JsonData.buildSuccess();
    }

    @GetMapping("/list")
    public JsonData list(@RequestBody EquipmentDataRequest equipmentDataRequest) {
        List<EquipmentDataDO> list = equipmentDataService.list(equipmentDataRequest);
        return JsonData.buildSuccess(list);
    }

    @GetMapping("/page")
    public JsonData page() {
        Page<EquipmentDataDO> pageInfo = new Page<>(1, 2);
        LambdaQueryWrapper<EquipmentDataDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.between(EquipmentDataDO::getRecordDate,  DateUtil.parse("2024-03-05 00:00:00"), DateUtil.parse("2024-03-08 00:00:00"));
        //wrapper.gt(EquipmentDataDO::getRecordDate, DateUtil.parse("2024-03-08 00:00:00"));
        wrapper.eq(EquipmentDataDO::getTenantId, 1002);
        wrapper.orderByAsc(EquipmentDataDO::getRecordDate);
        Page<EquipmentDataDO> result = equipmentDataService.page(pageInfo, wrapper);
        return JsonData.buildSuccess(result);
    }

}

