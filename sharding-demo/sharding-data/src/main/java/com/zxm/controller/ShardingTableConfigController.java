package com.zxm.controller;


import com.zxm.bean.JsonData;
import com.zxm.model.ShardingTableConfigDO;
import com.zxm.request.ShardingTableConfigRequest;
import com.zxm.service.ShardingTableConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zxm
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/shardingTableConfig")
public class ShardingTableConfigController {

    @Autowired
    private ShardingTableConfigService shardingTableConfigService;

    @GetMapping("/list")
    public JsonData list() {
        List<ShardingTableConfigDO> list = shardingTableConfigService.list();
        return JsonData.buildSuccess(list);
    }

    @PostMapping("/add")
    public JsonData add(@RequestBody ShardingTableConfigRequest shardingTableConfigRequest) {
        shardingTableConfigService.addShardingDbConfig(shardingTableConfigRequest);
        return JsonData.buildSuccess();
    }
}

