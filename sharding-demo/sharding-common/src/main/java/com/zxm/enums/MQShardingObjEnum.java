package com.zxm.enums;

/**
 * MQ 分库分表对象
 * @author zxm
 * @date 2023/7/7
 */
public enum MQShardingObjEnum {
    // 根据租户分库
    DB_SHARDING_BY_TENANT,
    // 根据时间分表
    TABLE_SHARDING_BY_DATE_EQUIPMENT_DATA;
}

