package com.zxm.enums;

/**
 * MQ 消息类型
 * @author zxm
 * @date 2023/7/7
 */
public enum MQShardingType {
    // 数据库分库配置刷新
    DB_SHARDING_CONFIG_REFRESH,
    // 数据库分库配置刷新
    TABLE_SHARDING_CONFIG_REFRESH;
}
