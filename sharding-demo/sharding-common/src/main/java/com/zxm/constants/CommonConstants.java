package com.zxm.constants;

/**
 * 通用常量
 * @author zxm
 * @date 2022/8/28 18:10
 */
public interface CommonConstants {

    /**
     * 认证字符串
     */
    String AUTHORIZATION = "Authorization";

    /**
     * 用户代理
     */
    String USER_AGENT = "user-agent";

    /**
     * 下划线
     */
    String UNDERLINE = "_";

    /** 逗号 */
    String COMMA = ",";

    /**
     * -1
     */
    int NEGATIVE = -1;

    /**
     * 1
     */
    int ONE = 1;

    /**
     * 0
     */
    int ZERO = 0;

    /**
     * 点
     */
    String DOT = ".";

    /**
     * 盐前缀
     */
    String SALT_PREFIX = "$1$";

}
