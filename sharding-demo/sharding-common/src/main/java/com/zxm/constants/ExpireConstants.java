package com.zxm.constants;

/**
 * 过期时间常量
 * @author zxm
 * @date 2022/9/4 21:04
 */
public interface ExpireConstants {

    /**
     * 图形验证码 10分钟
     */
    long CAPTCHA_CODE_EXPIRED = 10 * 60 * 1000;

}
