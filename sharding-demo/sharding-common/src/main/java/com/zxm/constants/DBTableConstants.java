package com.zxm.constants;

/**
 * 数据库配置
 * @author zxm
 * @date 2023/6/28
 */
public interface DBTableConstants {

    String TABLE_EQUIPMENT = "equipment_data";

}
