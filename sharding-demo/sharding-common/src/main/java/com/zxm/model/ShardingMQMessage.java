package com.zxm.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * MQ消息
 * @author zxm
 * @date 2023/7/7
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShardingMQMessage implements Serializable {

    /**
     * 事件类型
     */
    private String shardingType;

    /**
     *
     */
    private String shardingObj;
}